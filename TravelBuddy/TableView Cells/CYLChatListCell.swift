//
//  CYLChatListCell.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/10/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLChatListCell: UITableViewCell {

    @IBOutlet weak var lblUnreadCount: UILabel!
    @IBOutlet weak var lblUsermessage: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblMessageTime: UILabel!
    @IBOutlet weak var vwMessageBlur: UIView!
    @IBOutlet weak var imgLocked: UIImageView!
    
    var comesController:String = "receiver"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var object : CYLChatListObject?
    {
        didSet
        {
            if let userImage = object?.otherUserImageUrl
            {
                self.imgUserProfile.sd_setImage(with: URL.init(string: (userImage)), placeholderImage: UIImage(named: "user_placeholder"), options: [], completed: { (loadedImage, error, cacheType, url) in
                    
                    if error != nil {
                        print("Error code: \(error!.localizedDescription)")
                    } else {
                        self.imgUserProfile.image = loadedImage
                    }
                    if self.comesController == "receiver"
                    {
                        self.imgLocked.image = self.imgLocked.image!.withRenderingMode(.alwaysTemplate)
                        self.imgLocked.tintColor =  UIColor.white
                        if CYLGlobals.sharedInstance.usrObject?.userPlanType == 1
                        {
                            print("Premium")
                            self.vwMessageBlur.isHidden = true
                        }
                        else if self.object?.maskingType != ""
                        {
                            if self.object?.customerType != nil && self.object?.customerType.capitalized == "Premium"
                            {
                                print("other user premium")
                                self.vwMessageBlur.isHidden = true
                            }
                            else
                            {
                                if self.object?.maskingType == "WAVE_UNMASKED"
                                {
                                    self.vwMessageBlur.isHidden = true
                                    print("unmask")
                                }
                                else
                                {
                                    self.vwMessageBlur.isHidden = false
                                    let context = CIContext(options: nil)
                                    let currentFilter = CIFilter(name: "CIGaussianBlur")
                                    let beginImage = CIImage(image: self.imgUserProfile.image ?? UIImage(named: "user_placeholder")!)
                                    currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
                                    currentFilter!.setValue(100, forKey: kCIInputRadiusKey)
                                    let cropFilter = CIFilter(name: "CICrop")
                                    cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
                                    cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
                                    let output = cropFilter!.outputImage
                                    let cgimg = context.createCGImage(output!, from: output!.extent)
                                    let processedImage = UIImage(cgImage: cgimg!)
                                    self.imgUserProfile.image = processedImage
                                    
                                    print("mask")
                                }
                            }
                        }
                        else
                        {
                            self.vwMessageBlur.isHidden = true
                        }
                    }
                    if let otherUserName = self.object?.otherUserName
                    {
                        self.lblUserName.text  = otherUserName
                    }
                    else  {self.lblUserName.text = "user"}
                    if let recentMessage = self.object?.recentMsg
                    {
                        self.lblUsermessage.text  = recentMessage
                    }
                    else  {self.lblUsermessage.text = "no message found"}
                    
                    if let unreadCount = self.object?.chatUnreadCount
                    {
                        if unreadCount == "0"
                        {
                            self.lblUnreadCount.isHidden=true
                        }
                        else
                        {
                            self.lblUnreadCount.isHidden=false
                            self.lblUnreadCount.text = "\(unreadCount)"
                        }
                    }
                    if let msgTime = self.object?.recentMsgTime
                    {
                        let timeAmPm = dayDifferenceFromDateDayTime(Double(msgTime)!)
                        self.lblMessageTime.text = timeAmPm
                        self.lblMessageTime.adjustsFontSizeToFitWidth = true
                    }else  {self.lblMessageTime.text = "time"}
                    
                })
            }
            else
            {
                if let otherUserName = object?.otherUserName
                {
                    self.lblUserName.text  = otherUserName
                }
                else  {self.lblUserName.text = "user"}
                if let recentMessage = object?.recentMsg
                {
                    self.lblUsermessage.text  = recentMessage
                }
                else  {self.lblUsermessage.text = "no message found"}
                
                if let unreadCount = object?.chatUnreadCount
                {
                    if unreadCount == "0"
                    {
                        self.lblUnreadCount.isHidden=true
                    }
                    else
                    {
                        self.lblUnreadCount.isHidden=false
                        self.lblUnreadCount.text = "\(unreadCount)"
                    }
                }
                if let msgTime = object?.recentMsgTime
                {
                    let timeAmPm = dayDifferenceFromDateDayTime(Double(msgTime)!)
                    self.lblMessageTime.text = timeAmPm
                    self.lblMessageTime.adjustsFontSizeToFitWidth = true
                }else  {self.lblMessageTime.text = "time"}
            }
            
            
        }
    }

}
