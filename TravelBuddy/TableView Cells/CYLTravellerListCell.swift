//
//  CYLTravellerListCell.swift
//  TravelBuddy
//
//  Created by CodeYeti on 7/11/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
//import UIButton

class CYLTravellerListCell: UITableViewCell {

    @IBOutlet weak var vwFirst: UIView!
    @IBOutlet weak var lblUserFirstNameAge: UILabel!
    @IBOutlet weak var lblUserFirstAge: UILabel!
    @IBOutlet weak var lblUserSecondNameAge: UILabel!
    @IBOutlet weak var lblUserSecondAge: UILabel!
    @IBOutlet weak var imgOnlineSecond: UIImageView!
    @IBOutlet weak var btnToSecond: UIButton!
    @IBOutlet weak var btnToFirst: UIButton!
    @IBOutlet weak var vwSecond: UIView!
    @IBOutlet weak var lblLocationSecond: UILabel!
    @IBOutlet weak var imgUserSecond: UIImageView!
    @IBOutlet weak var lblLocationFirst: UILabel!
    @IBOutlet weak var imgUserFirst: UIImageView!
    @IBOutlet weak var imgOnlineFirst: UIImageView!
    
    //Wave
    @IBOutlet weak var constSayFirstLeft: NSLayoutConstraint!
    @IBOutlet weak var imgWaveFirstIcon: UIImageView!
    @IBOutlet weak var constFirstWaveView: NSLayoutConstraint!
    @IBOutlet weak var btnSayWaveFirst: UIButton!
    
    @IBOutlet weak var constSaySecondLeft: NSLayoutConstraint!
    @IBOutlet weak var imgWaveSecondIcon: UIImageView!
    @IBOutlet weak var constSecondWaveView: NSLayoutConstraint!
    @IBOutlet weak var btnSayWaveSecond: UIButton!
    var comesEveryWhereNearMe: String = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var object:JSON?
    {
      didSet
      {
       self.vwFirst.isHidden=true
       self.vwSecond.isHidden=true
       if object?["first"] != JSON.null
       {
        self.imgOnlineFirst.isHidden = (object?["first"]["showOnline"].boolValue == false) ? true : false
        
        var locationCountryCity: String = String()
        if self.comesEveryWhereNearMe == "everywhere"
        {
            if object!["first"]["country"].exists() && object!["first"]["country"].string != nil && object!["first"]["country"].string != ""
            {
                locationCountryCity = (object?["first"]["country"].string)!
            }
            else if object!["first"]["cityv2"].exists() && object!["first"]["cityv2"].string != nil && object!["first"]["cityv2"].string != ""
            {
                    locationCountryCity = (object?["first"]["cityv2"].string)!
            }
        }
        else
        {
            if object!["first"]["cityv2"].exists() && object!["first"]["cityv2"].string != nil && object!["first"]["cityv2"].string != ""
            {
                locationCountryCity = (object?["first"]["cityv2"].string)!
            }
            else if object!["first"]["country"].exists() && object!["first"]["country"].string != nil && object!["first"]["country"].string != ""
            {
                locationCountryCity = (object?["first"]["country"].string)!
            }
        }
        self.lblLocationFirst.text = locationCountryCity
        
        self.imgUserFirst.sd_setImage(with: URL(string: (object?["first"]["userPicUrl"].stringValue)!))
        
        DispatchQueue.main.async
        {
            if self.object!["first"]["age"].exists() && self.object!["first"]["age"].string != nil && self.object!["first"]["age"].string != ""
            {
                self.lblUserFirstAge.text = ", " + self.object!["first"]["age"].stringValue
                self.lblUserFirstAge.sizeToFit()
            }
            else
            {
                self.lblUserFirstAge.text = ""
            }
        }
        
        if (object!["first"]["firstName"].exists() && object!["first"]["firstName"].string != nil)
        {
            self.lblUserFirstNameAge.text = object!["first"]["firstName"].stringValue
        }
        else
        {
            self.lblUserFirstNameAge.text = object!["first"]["userName"].stringValue
        }
        
        if object!["first"]["wave"].exists()
        {
            let waveType = object!["first"]["wave"]["waveEmoticonType"].intValue
            self.imgWaveFirstIcon.superview?.backgroundColor = UIColor.white
            self.btnSayWaveFirst.isUserInteractionEnabled = true
            if waveType == 0
            {
                self.constSayFirstLeft.constant = 30
                self.imgWaveFirstIcon.image = UIImage(named: "ic_hand")
                self.constFirstWaveView.constant = 54
            }
            else if waveType == 1
            {
                self.imgWaveFirstIcon.image = UIImage(named: "ic_hand")
                self.constFirstWaveView.constant = 24
                self.constSayFirstLeft.constant = 0
                self.imgWaveFirstIcon.superview?.backgroundColor = UIColor.clear
                self.btnSayWaveFirst.isUserInteractionEnabled = false
            }
            else if waveType == 2
            {
                self.imgWaveFirstIcon.image = UIImage(named: "ic_hand")
                self.constFirstWaveView.constant = 54
                self.constSayFirstLeft.constant = 30
            }
            else
            {
                self.imgWaveFirstIcon.image = UIImage(named: "ic_twohand")
                self.imgWaveFirstIcon.superview?.backgroundColor = UIColor.clear
                self.btnSayWaveFirst.isUserInteractionEnabled = false
                self.constFirstWaveView.constant = 24
                self.constSayFirstLeft.constant = 0
            }
            
        }
        else
        {
            self.constSayFirstLeft.constant = 30
            self.imgWaveFirstIcon.image = UIImage(named: "ic_hand")
            self.constFirstWaveView.constant = 54
        }
        
        if object!["first"]["showWave"].exists() && object!["first"]["showWave"].bool != nil
        {
            self.constFirstWaveView.constant = (object!["first"]["showWave"].bool == true) ? self.constFirstWaveView.constant : 0
        }
        else
        {
            self.constFirstWaveView.constant = 0
        }
        
        self.vwFirst.isHidden=false
       }
        
        
        // Second View Traveler
        if object?["second"] != JSON.null
        {
            self.imgOnlineSecond.isHidden = (object?["second"]["showOnline"].boolValue == false) ? true : false
            var locationCountryCity: String = String()
            if self.comesEveryWhereNearMe == "everywhere"
            {
                if object!["second"]["country"].exists() && object!["second"]["country"].string != nil && object!["second"]["country"].string != ""
                {
                    locationCountryCity = (object?["second"]["country"].string)!
                }
                else if object!["second"]["cityv2"].exists() && object!["second"]["cityv2"].string != nil && object!["second"]["cityv2"].string != ""
                {
                    locationCountryCity = (object?["second"]["cityv2"].string)!
                }
            }
            else
            {
                if object!["second"]["cityv2"].exists() && object!["second"]["cityv2"].string != nil && object!["second"]["cityv2"].string != ""
                {
                    locationCountryCity = (object?["second"]["cityv2"].string)!
                }
                else if object!["second"]["country"].exists() && object!["second"]["country"].string != nil && object!["second"]["country"].string != ""
                {
                    locationCountryCity = (object?["second"]["country"].string)!
                }
            }
            
            self.lblLocationSecond.text = locationCountryCity
            
            self.imgUserSecond.sd_setImage(with: URL(string: (object?["second"]["userPicUrl"].stringValue)!))
            DispatchQueue.main.async
            {
                if self.object!["second"]["age"].exists() && self.object!["second"]["age"].string != nil && self.object!["second"]["age"].string != ""
                {
                    self.lblUserSecondAge.text = ", " + self.object!["second"]["age"].stringValue
                    self.lblUserSecondAge.sizeToFit()
                }
                else
                {
                    self.lblUserSecondAge.text = ""
                }
            }
            
            if object!["second"]["firstName"].exists() && object!["second"]["firstName"].string != nil
            {
                self.lblUserSecondNameAge.text = object!["second"]["firstName"].stringValue
            }
            else
            {
                self.lblUserSecondNameAge.text = object!["second"]["userName"].stringValue
            }
            
            if object!["second"]["wave"].exists()
            {
                let waveType = object!["second"]["wave"]["waveEmoticonType"].intValue
                self.imgWaveSecondIcon.superview?.backgroundColor = UIColor.white
                self.btnSayWaveSecond.isUserInteractionEnabled = true
                if waveType == 0
                {
                    self.constSaySecondLeft.constant = 30
                    self.imgWaveSecondIcon.image = UIImage(named: "ic_hand")
                    self.constSecondWaveView.constant = 54
                }
                else if waveType == 1
                {
                    
                    self.imgWaveSecondIcon.image = UIImage(named: "ic_hand")
                    self.constSecondWaveView.constant = 24
                    self.constSaySecondLeft.constant = 0
                    self.imgWaveSecondIcon.superview?.backgroundColor = UIColor.clear
                    self.btnSayWaveSecond.isUserInteractionEnabled = false
                }
                else if waveType == 2
                {
                    self.imgWaveSecondIcon.image = UIImage(named: "ic_hand")
                    self.constSecondWaveView.constant = 54
                    self.constSaySecondLeft.constant = 30
                }
                else
                {
                    self.imgWaveSecondIcon.image = UIImage(named: "ic_twohand")
                    self.imgWaveSecondIcon.superview?.backgroundColor = UIColor.clear
                    self.constSecondWaveView.constant = 24
                    self.constSaySecondLeft.constant = 0
                    self.btnSayWaveSecond.isUserInteractionEnabled = false
                }
                
            }
            else
            {
                self.imgWaveSecondIcon.image = UIImage(named: "ic_hand")
                self.constSecondWaveView.constant = 54
                self.constSaySecondLeft.constant = 30
            }
            if object!["second"]["showWave"].exists() && object!["second"]["showWave"].bool != nil
            {
                self.constSecondWaveView.constant = (object!["second"]["showWave"].bool == true) ? self.constSecondWaveView.constant : 0
            }
            else
            {
                self.constSecondWaveView.constant = 0
            }
            
            self.vwSecond.isHidden=false
        }
      }
    }
    
}
