//
//  CYLNotificationCell.swift
//  TravelBuddy
//
//  Created by codeyeti on 14/08/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLNotificationCell: UITableViewCell {

    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserMessage: UILabel!
    @IBOutlet weak var lblMessageTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var object:JSON?
    {
        didSet
        {
            if (object!["userName"].exists() && object!["userName"].string != nil)
            {
                self.lblUserName.text = object!["userName"].stringValue
            } else{self.lblUserName.text = "user"}
            
            if (object!["message"].exists() && object!["message"].string != nil)
            {
                self.lblUserMessage.text  = object!["message"].stringValue
            }else  {self.lblUserMessage.text = "message"}
            self.lblUserMessage.sizeToFit()
           
            self.imgUserProfile.sd_setImage(with: URL(string: (object?["userPicUrl"].stringValue)!))
            
            if (object!["notificationTime"].exists() && object!["notificationTime"].stringValue != "")
            {
                let timeAmPm = dayDifferenceFromDateDayTimeString(object!["notificationTime"].stringValue)
                self.lblMessageTime.text = timeAmPm
                self.lblMessageTime.adjustsFontSizeToFitWidth = true
            }else  {self.lblMessageTime.text = ""}
        }
    }

}
