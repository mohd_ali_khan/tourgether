//
//  CYLChatDetailWaveSentBothSideCell.swift
//  TravelBuddy
//
//  Created by codeyeti on 25/05/18.
//  Copyright © 2018 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLChatDetailWaveSentBothSideCell: UITableViewCell {

@IBOutlet weak var lblSentText: UILabel!
@IBOutlet weak var lblSentMessage: UILabel!
@IBOutlet weak var lblSentTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
