//
//  CYLChatDetailCell.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/24/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLChatDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblReceiverTime: UILabel!
    @IBOutlet weak var imgReceivedBg: UIImageView!
    @IBOutlet weak var imgSentBg: UIImageView!
    @IBOutlet weak var lblSentText: UILabel!
    @IBOutlet weak var lblReceiverText: UILabel!
    @IBOutlet weak var imgReceiverProfile: UIImageView!
    @IBOutlet weak var vwReceivedMessage: UIView!
    @IBOutlet weak var vwSentMessage: UIView!
    @IBOutlet weak var lblSentTime: UILabel!
    var receiverProfileURL:String?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    var object:CYLMessageObject?
    {
        didSet
        {
            
            if object?.senderId == CYLGlobals.sharedInstance.usrObject?.id
            {
                self.vwSentMessage.isHidden=false
                self.vwReceivedMessage.isHidden=true
                self.lblSentText.text = object?.messageText
                self.lblSentText.isUserInteractionEnabled = true
                let timeAmPm = convertTimestampIntoDateString(date: Double((object?.creationTime)!), format: "h:mm aa")
                self.lblSentTime.text = timeAmPm
                self.lblSentText.textColor = UIColor.white
                self.imgSentBg.image = #imageLiteral(resourceName: "img_send_bubble").stretchableImage(withLeftCapWidth: 15, topCapHeight: 14)
            }
            else
            {
                self.vwSentMessage.isHidden=true
                self.vwReceivedMessage.isHidden=false
                self.lblReceiverText.text = object?.messageText
                self.lblReceiverText.isUserInteractionEnabled = true
                self.imgReceiverProfile.sd_setImage(with: URL.init(string: receiverProfileURL!))
                let timeAmPm = convertTimestampIntoDateString(date: Double((object?.creationTime)!), format: "h:mm aa")
                self.lblReceiverTime.text = timeAmPm
                self.imgReceivedBg.image = #imageLiteral(resourceName: "img_receive_bubble").stretchableImage(withLeftCapWidth: 15, topCapHeight: 14)
            }
        }
    }
    
}

