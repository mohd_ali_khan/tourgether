//
//  CYLLocationCell.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/12/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLLocationCell: UITableViewCell {

    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var constSubTitleH: NSLayoutConstraint!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var object:CYLLocationObject?
        {
        didSet
        {
            if object?.cityName == ""
            {
             self.lblTitle.text = object?.countryName!
             self.imgLocation.sd_setImage(with: URL.init(string: (object?.picUrl)!))
             //self.constSubTitleH.constant = 0
            }
            else
            {
             self.lblTitle.text = (object?.cityName!)! + ", " + (object?.countryName!)!
             self.imgLocation.sd_setImage(with: URL.init(string: (object?.picUrl)!))
             //self.lblSubtitle.text = object?.countryName!
             //self.constSubTitleH.constant = 20
            }
        }
    }
    var jsonDict:JSON?
        {
        didSet
        {
            if jsonDict?["cityName"].stringValue == ""
            {
                self.lblTitle.text = jsonDict?["countryName"].stringValue
                self.imgLocation.sd_setImage(with: URL.init(string: (jsonDict?["picUrl"].stringValue)!))
                //self.constSubTitleH.constant = 0
            }
            else
            {
                self.lblTitle.text = (jsonDict?["cityName"].stringValue)! + ", " + (jsonDict?["countryName"].stringValue)!
                self.imgLocation.sd_setImage(with: URL.init(string: (jsonDict?["picUrl"].stringValue)!))
                //self.lblSubtitle.text = object?.countryName!
                //self.constSubTitleH.constant = 20
            }
        }
    }

}
