//
//  CYLCityCountrySearchCell.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/6/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLCityCountrySearchCell: UITableViewCell {

    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var constSubtitleHeight: NSLayoutConstraint!
    @IBOutlet weak var imgBackground: UIImageView!
    var isCountrySearch:Bool?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var object:JSON?
    {
        didSet
        {
           if isCountrySearch!
           {
            //handle country type list here
            self.constSubtitleHeight.constant = 0
            self.lblTitle.text = object?["countryName"].stringValue.uppercased()
            self.imgBackground.sd_setImage(with: URL.init(string: (object?["picUrl"].stringValue)!))
           }
           else
           {
            //handle city type list here
            self.constSubtitleHeight.constant = 21
            self.lblTitle.text = object?["cityName"].stringValue.uppercased()
            self.lblSubtitle.text = object?["countryName"].stringValue
            self.imgBackground.sd_setImage(with: URL.init(string: (object?["picUrl"].stringValue)!))
           }
        }
    }
    
    var isDestinationSelected:Bool?
    {
        didSet
        {
                
        }
    }

}
