//
//  CityCountryCollectionCell.swift
//  TravelBuddy
//
//  Created by codeyeti on 14/04/18.
//  Copyright © 2018 Nikhil Sharma. All rights reserved.
//

import UIKit

class CityCountryCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var object:CYLLocationObject?
    {
        didSet
        {
            if object?.destinationMatched == true || object?.livesInDestinationCountry == true
            {
                self.backgroundColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
                self.lblTitle.textColor = UIColor.white
            }
            else
            {
                self.backgroundColor = UIColor.white
                self.lblTitle.textColor = UIColor.black
            }
            
            if object?.cityName == ""
            {
                self.lblTitle.text = object?.countryName!
                self.imgLocation.sd_setImage(with: URL.init(string: (object?.picUrl)!))
            }
            else
            {
                self.lblTitle.text = (object?.cityName!)! + ", " + (object?.countryName!)!
                self.imgLocation.sd_setImage(with: URL.init(string: (object?.picUrl)!))
            }
        }
    }
    var jsonDict:JSON?
    {
        didSet
        {
            if jsonDict?["cityName"].stringValue == ""
            {
                self.lblTitle.text = jsonDict?["countryName"].stringValue
                self.imgLocation.sd_setImage(with: URL.init(string: (jsonDict?["picUrl"].stringValue)!))
            }
            else
            {
                self.lblTitle.text = (jsonDict?["cityName"].stringValue)! + ", " + (jsonDict?["countryName"].stringValue)!
                self.imgLocation.sd_setImage(with: URL.init(string: (jsonDict?["picUrl"].stringValue)!))
            }
        }
}
}
