//
//  CYLCityListCell.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/6/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLCityListCell: UITableViewCell {
    @IBOutlet weak var imgLogo: UIImageView!

    @IBOutlet weak var btnToDelete: UIButton!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblCityName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var object:CYLLocationObject?
    {
        didSet
        {
        self.lblCountryName.text = object?.countryName
        self.lblCityName.text = object?.cityName
        self.imgLogo.sd_setImage(with: URL.init(string: (object?.picUrl)!))
        }
    }
    @IBAction func btnToDeleteClicked(_ sender: Any) {
    }

}
