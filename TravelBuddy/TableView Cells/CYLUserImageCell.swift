//
//  CYLUserImageCell.swift
//  TravelBuddy
//
//  Created by codeyeti on 28/03/18.
//  Copyright © 2018 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLUserImageCell: UICollectionViewCell {
    @IBOutlet weak var imgUserImage: UIImageView!
    @IBOutlet weak var btnDeleteImage: UIButton!
    @IBOutlet weak var btnAddImage: UIButton!
    
}
