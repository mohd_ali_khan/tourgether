//
//  CYLTravellerCell.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/10/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLTravellerCell: UICollectionViewCell
{
    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var lblTripMatched: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    var object : JSON?
    {
        didSet
        {
          self.lblLocation.text = object?["city"].stringValue
          self.imgUser.sd_setImage(with: URL(string: (object?["userPicUrl"].stringValue)!), placeholderImage: UIImage(named: "user_placeholder"))
            if object?["is_matched"].boolValue == true
            {
              self.lblTripMatched.isHidden=false
              self.lblTripMatched.layer.cornerRadius = 5
              self.lblTripMatched.layer.masksToBounds=true
            }
            else
            {
                self.lblTripMatched.isHidden=true
            }
            
        }
    }
    
}
