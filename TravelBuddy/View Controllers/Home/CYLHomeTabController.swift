//
//  CYLHomeTabController.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/10/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLHomeTabController: UITabBarController,UIGestureRecognizerDelegate,UITabBarControllerDelegate {
    
    var boolAgainTap: Bool = Bool()
    var appObject:AppDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBarItem.appearance().setTitleTextAttributes([kCTFontAttributeName as NSAttributedStringKey:UIFont(name: "Comfortaa-Bold", size: 10)!], for: .normal)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.selectedIndex=0
        self.delegate = self
        self.appObject = UIApplication.shared.delegate as? AppDelegate
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if let items = tabBar.items {
            items.enumerated().forEach {
                if $1 == item
                {
                    if self.selectedIndex == $0
                    {
                        if $0 == 0
                        {
                            if self.appObject?.selectedIndexPresent == 0
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_to_top_everywhere"), object: nil)
                            }
                            else if self.appObject?.selectedIndexPresent == 1
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_to_top_near"), object: nil)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_to_top_local"), object: nil)
                            }
                        }
                    }
                    else
                    {
                        self.selectedIndex = $0
                    }
                    
                }
                
            }
        }
    }
}

