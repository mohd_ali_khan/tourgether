//
//  CYLMatchedPopUpVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 01/08/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

protocol CYLSendMessagePopUpDelegate {
    func moveChatDetailController(nameRecever:String,chatId: String,imgRecever: String, receverId: Int)
}

class CYLMatchedPopUpVC: UIViewController {

    
    @IBOutlet weak var imgUserSender: UIImageView!
    @IBOutlet weak var imgUserRecever: UIImageView!
    @IBOutlet weak var actIndicater: UIActivityIndicatorView!
    var imgRecever: String = String()
    var id: Int = Int()
    var userName: String = String()
    var comesController:String = String()
    
    var moveChatDetailControllerDelegate: CYLSendMessagePopUpDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let aryProfileImg = CYLGlobals.sharedInstance.usrObject?.userImages
        var imgSender: String = String()
        if (aryProfileImg?.count)! > 0
        {
            let pictureObject = aryProfileImg!.first as! [String:AnyObject]
            imgSender = pictureObject["picUrl"] as! String
        }
        else
        {
            imgSender = ""
        }
        
        imgUserSender.sd_setImage(with: URL(string: (imgSender)), placeholderImage: UIImage(named: "user_placeholder"), options: [], completed: { (loadedImage, error, cacheType, url) in
            
            self.imgUserSender.sd_cancelCurrentImageLoad()
            if error != nil {
                print("Error code: \(error!.localizedDescription)")
            } else {
                self.imgUserSender.image = loadedImage
            }
        })
        imgUserRecever.sd_setImage(with: URL(string: (imgRecever)), placeholderImage: UIImage(named: "user_placeholder"), options: [], completed: { (loadedImage, error, cacheType, url) in
            self.imgUserRecever.sd_cancelCurrentImageLoad()
            if error != nil {
                print("Error code: \(error!.localizedDescription)")
            } else {
                self.imgUserRecever.image = loadedImage
            }
            
        })
        
        imgUserSender.layer.cornerRadius = 45
        imgUserSender.layer.borderWidth = 2
        imgUserSender.layer.borderColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0).cgColor
        imgUserSender.layer.masksToBounds = true
        
        imgUserRecever.layer.cornerRadius = 45
        imgUserRecever.layer.borderWidth = 2
        imgUserRecever.layer.borderColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0).cgColor
        imgUserRecever.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnNotNowAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSendMessageAction(_ sender: UIButton)
    {
        if comesController != "chatDetail"
        {
            self.actIndicater.startAnimating()
            let chatId = "cht_id_\(CYLGlobals.sharedInstance.usrObject!.id!)_\(self.id)"
            let chatIdArr = chatId.components(separatedBy: "_")
            let chatIdRev: String = "\(chatIdArr[0])_\(chatIdArr[1])_\(chatIdArr[3])_\(chatIdArr[2])"
            CYLDBRefProvider.sharedInstance.chatDetailReference.child(chatId).observeSingleEvent(of: .value, with: { (snapshot)  in
                
                if snapshot.exists()
                {
                    self.moveChatDetailVC(chatId: chatId)
                }
                else
                {
                    CYLDBRefProvider.sharedInstance.chatDetailReference.child(chatIdRev).observeSingleEvent(of: .value, with: { (snapshot)  in
                        self.moveChatDetailVC(chatId: chatIdRev)
                    })
                }
            })
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    func moveChatDetailVC(chatId: String)
    {
        self.actIndicater.stopAnimating()
        self.dismiss(animated: true, completion: nil)
        self.moveChatDetailControllerDelegate?.moveChatDetailController(nameRecever: userName, chatId: chatId, imgRecever: self.imgRecever, receverId: id)
    }
}

