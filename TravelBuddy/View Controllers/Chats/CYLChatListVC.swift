//
//  CYLChatListVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 9/18/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
import SJFluidSegmentedControl

class CYLChatListVC: CYLBaseViewController {

    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var vwHeaderSegment: SJFluidSegmentedControl!
    
    //var vwHeaderSegment: SJFluidSegmentedControl?
    var currentOffset:CGFloat?
    var currentDisplayVC:UIViewController?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.createChatListScreenDesign()
        self.setDefaultSegmentViewController()
        self.requestToEnablePushNotificationPopup()
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.tabBarController?.navigationItem.title = "Messages"
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.vwHeaderSegment?.reloadInputViews()
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        DispatchQueue.global().async
        {
//                CYLChatManager.sharedInstance.removeAllChatListObserversForUser("usr_id_\(CYLGlobals.sharedInstance.usrObject!.id!)")
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createChatListScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        var oldSelectedSegment = 0
        if self.vwHeaderSegment != nil
        {
            oldSelectedSegment = (self.vwHeaderSegment?.currentSegment)!
        }
        self.vwHeaderSegment.textFont = UIFont(name: "Comfortaa-Bold", size: 13)!
        self.vwHeaderSegment.layer.borderColor = themeColorBlue.cgColor
        self.vwHeaderSegment.layer.borderWidth = 1
        self.vwHeaderSegment.textColor = themeColorBlue
        self.vwHeaderSegment.reloadData()
        self.vwHeaderSegment?.currentSegment = oldSelectedSegment
        
    }
    func setDefaultSegmentViewController()
    {
        if let vc = self.viewControllerForSelectedSegmentIndex(index: 0)
        {
            self.addChildViewController(vc)
            self.vwContainer.addSubview(vc.view)
            vc.view.frame = self.vwContainer.bounds
            self.currentDisplayVC = vc
        }
    }
    func handleViewControllersForSelectedTravelType(currentTab:Int)
    {
        if let vwController = viewControllerForSelectedSegmentIndex(index: self.vwHeaderSegment!.currentSegment)
        {
            let option = (currentTab==0) ? UIViewAnimationOptions.transitionCurlDown : UIViewAnimationOptions.transitionCurlUp
            self.addChildViewController(vwController)
            self.transition(from: self.currentDisplayVC!, to: vwController, duration: 0.5, options: option, animations:
                {
                    self.currentDisplayVC!.view.removeFromSuperview()
                    vwController.view.frame = self.vwContainer.bounds
                    self.vwContainer.addSubview(vwController.view)
            }, completion:
                { finished in
                    vwController.didMove(toParentViewController: self)
                    self.currentDisplayVC!.removeFromParentViewController()
                    self.currentDisplayVC = vwController
            })
        }
    }
    
    func viewControllerForSelectedSegmentIndex(index: Int) -> UIViewController? {
        switch index
        {
        case 1:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CYLSendMessageVC") as! CYLSendMessageVC
            return vc
        case 0:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CYLReceivedMessageVC") as! CYLReceivedMessageVC
            return vc
        default:
            return nil
        }
    }
    func requestToEnablePushNotificationPopup()
    {
        let userDeflts = UserDefaults.standard
        if userDeflts.value(forKey: "isFirstInstall") == nil
        {
//            //show option to enable notification for the first time
//            let appObject = UIApplication.shared.delegate as! AppDelegate
//            appObject.requestToAllowPushNotification()
//            userDeflts.set("1", forKey: "isFirstInstall")
//            userDeflts.synchronize()
            let allowPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLAllowChatVC") as! CYLAllowChatVC
            allowPopup.modalPresentationStyle = .overCurrentContext
            allowPopup.modalTransitionStyle = .crossDissolve
            self.navigationController?.present(allowPopup, animated: true, completion: nil)
        }
        

    }
}

// MARK: - SJFluidSegmentedControl Data Source Methods

extension CYLChatListVC: SJFluidSegmentedControlDataSource
{
    
    func numberOfSegmentsInSegmentedControl(_ segmentedControl: SJFluidSegmentedControl) -> Int {
        return 2
    }
    
    func segmentedControl(_ segmentedControl: SJFluidSegmentedControl,
                          titleForSegmentAtIndex index: Int) -> String? {
        if index == 0
        {
            return "Received"
        }
        else if index == 1
        {
            return "Sent"
        }
        else
        {
            return ""
        }
    }
    
    func segmentedControl(_ segmentedControl: SJFluidSegmentedControl,
                          gradientColorsForSelectedSegmentAtIndex index: Int) -> [UIColor] {
        
        return [themeColorBlue]
    }
    
    func segmentedControl(_ segmentedControl: SJFluidSegmentedControl,
                          gradientColorsForBounce bounce: SJFluidSegmentedControlBounce) -> [UIColor]
    {
        return [themeColorBlue]
    }
    
}

// MARK: - SJFluidSegmentedControl Delegate Methods

extension CYLChatListVC: SJFluidSegmentedControlDelegate
{
    func segmentedControl(_ segmentedControl: SJFluidSegmentedControl,
                          didChangeFromSegmentAtIndex fromIndex: Int,
                          toSegmentAtIndex toIndex:Int)
    {
        self.handleViewControllersForSelectedTravelType(currentTab: toIndex)
    }
    
}

