//
//  CYLSendMessageVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 9/18/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
import Alamofire
class CYLSendMessageVC: CYLBaseViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var tblChatList: UITableView!
    @IBOutlet weak var vwNewChat: UIView!
    @IBOutlet weak var constImgNoMsgH: NSLayoutConstraint!
    @IBOutlet weak var constImgNoMsgW: NSLayoutConstraint!
    
    var currentOffset:CGFloat?
    var dictChatList = NSMutableDictionary()
    var aryMessageList=[CYLChatListObject]()
    var footerView:UIView?
    //for paging
    let pageSize = 10
    let preloadMargin = 1
    var lastLoadedPage = 0
    var maxResults = 0
    var aryTemp = [CYLChatListObject]()
    var vwLoader: UIView = UIView()
    var vwEmptyState: UIView = UIView()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loaderGifImage()
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Chat sender")
        CYLChatManager.sharedInstance.delegateChatList = self
        if checkInternetConnection()
        {
            CYLChatManager.sharedInstance.removeAllChatListObserversForUser("usr_id_\(CYLGlobals.sharedInstance.usrObject!.id!)")
            self.aryTemp.removeAll()
            self.aryMessageList.removeAll()
            self.requestToGetChatList()
        }
        else
        {
            self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
        }
        if screenHeight == 480
        {
            constImgNoMsgH.constant = 80
            constImgNoMsgW.constant = 80
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        createMessageListScreenDesign()
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.removeChatListObservers()
    }
    func loaderGifImage()
    {
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwLoader = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwLoader.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
            window.addSubview(vwLoader)
            let img = UIImageView(frame: CGRect(x: vwLoader.frame.width/2-40, y: vwLoader.frame.height/2-40, width: 80, height: 80))
            img.image = UIImage.sd_animatedGIF(with: imageData!)
            vwLoader.addSubview(img)
            vwLoader.isHidden = true
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createMessageListScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    func setupActivityIndicatorInTableViewFooter()
    {
        self.footerView = UIView.init(frame: CGRect(x:0,y:0,width:self.screenWidth!,height:40))
        
        let actIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        actIndicator.tag = 10
        actIndicator.frame = CGRect(x:0.0, y:0.0, width:20.0, height:20.0)
        actIndicator.center.x = (self.footerView?.center.x)!
        actIndicator.center.y = (self.footerView?.center.y)!
        actIndicator.hidesWhenStopped = true
        self.footerView?.addSubview(actIndicator)
        actIndicator.startAnimating()
        self.tblChatList.tableFooterView = self.footerView
    }
    func handlePagingForMessageListTable()
    {
        lastLoadedPage += 1
        //self.requestToGetMessageList(page:lastLoadedPage)
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Table view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.aryMessageList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CYLChatListCell") as! CYLChatListCell
        cell.comesController = "sender"
        //        cell.selectionStyle = (self.aryMessageList[indexPath.row].isBlocked == "false") ? .gray : .none
        cell.object = self.aryMessageList[indexPath.row]
        //        //*********for paging api call
        //        let lastRowIndex = tableView.numberOfRows(inSection: 0) - 1
        //        if indexPath.row == lastRowIndex && self.aryMessageList.count < maxResults
        //        {
        //            self.setupActivityIndicatorInTableViewFooter()
        //            self.handlePagingForMessageListTable()
        //        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let object = self.aryMessageList[indexPath.row]
        if object.isBlocked == "false"
        {
            let chatDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLChatDetailVC") as! CYLChatDetailVC
            chatDetailVC.userName = object.otherUserName
            chatDetailVC.imgUrl = object.otherUserImageUrl
            chatDetailVC.chatId = object.chatId
            chatDetailVC.receiverId = Int(object.otherUserId)
//            chatDetailVC.type = object.type
            self.navigationController?.pushViewController(chatDetailVC, animated: true)
            
        }
        else
        {
            showAlertView(title: "Blocked!!", message: "You can't chat with a blocked user.", ref: self)
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            
            let alertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
            let titleFont = [NSAttributedStringKey.font : UIFont(name: "Comfortaa-Bold", size: 18.0)!]
            let messageFont = [NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 14.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: "Delete message?", attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: "This will only be removed from your message list.", attributes: messageFont)
            
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            let deleteAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                self.requestToDeleteUserInSenderList(indexpath: indexPath)
            }
            let cancleAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                self.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(cancleAction)
            alertController.addAction(deleteAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        return [deleteAction]
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    
    func requestToDeleteUserInSenderList(indexpath: IndexPath)
    {
        vwLoader.isHidden = false
        let params:[String: Any] = ["fragmentName":"sender","chatId":"\(self.aryMessageList[indexpath.row].chatId)"]
        
        let request = CYLServices.deleteMessageFromFirebase(dict: params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                self.vwLoader.isHidden = true
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData!["status"]?.stringValue != "fail"
                    {
                        
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Message delete"])
                    self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
                   
                }
                
        },
                                                                  withError:
            {
                (error) in
                self.vwLoader.isHidden = true
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Message delete"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                self.vwLoader.isHidden = true
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Message delete"])
                self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
               
                
        })
        
    }
    
    func requestToGetChatList()
    {
        vwLoader.isHidden = false
        //        //KVNProgress.show()
        self.tblChatList.isHidden=true
        DispatchQueue.global().async
            {
                CYLChatManager.sharedInstance.getUserSentChatListArray()
                //CYLChatManager.sharedInstance.getUserSendReceiveChatListArray()
        }
        
        //CYLChatManager.sharedInstance.getUserChatsListingForCurrentUser()
    }
    func removeChatListObservers()
    {
        //        DispatchQueue.global().async
        //        {
        //            CYLChatManager.sharedInstance.removeAllChatListObserversForUser("usr_id_\(CYLGlobals.sharedInstance.usrObject!.id!)")
        //        }
    }
    func showLoadingView()
    {
        if self.tblChatList.tableFooterView == nil
        {
            vwLoader.isHidden = false
            //            //KVNProgress.show()
        }
    }
    func hideLoadingView()
    {
        if self.tblChatList.tableFooterView != nil
        {
            self.tblChatList.tableFooterView = nil
        }
        else
        {
            vwLoader.isHidden = true
            //            //KVNProgress.dismiss()
        }
    }
    @IBAction func btnExploreTravelers(_ sender: UIButton)
    {
        self.tabBarController?.selectedViewController = self.tabBarController?.viewControllers![0]
        self.tabBarController?.selectedIndex = 0
    }
    
}

extension CYLSendMessageVC:CYLChatListDelegate
{
    func delegateGetChatList(_ recentChat: CYLChatListObject)
    {
        /*self.dictChatList[recentChat.chatId] = recentChat
         self.aryMessageList = self.dictChatList.allValues as! [CYLChatListObject]
         self.tblChatList.reloadData()*/
    }
    func delegateUpdateUserModelInChatList(_ recentChat: CYLChatListObject)
    {
        /*var object = self.dictChatList[recentChat.chatId] as! CYLChatListObject
         object.otherUserImageUrl = recentChat.otherUserImageUrl
         object.otherUserName = recentChat.otherUserName
         self.dictChatList[recentChat.chatId] = object
         self.aryMessageList = self.dictChatList.allValues as! [CYLChatListObject]
         self.tblChatList.reloadData()*/
    }
    func sortChatlistArrayForMostRecentMessage(_ aryChatList:[CYLChatListObject])
    {
        //self.aryMessageList = (aryChatList as NSArray).sortedArray(using: [NSSortDescriptor(key: "time", ascending: false)])
    }
    func delegateGetChatListArray(_ aryResult:[CYLChatListObject])
    {
        
    }
    func delegateGetReceivedChatListArray(_ aryResult:[CYLChatListObject])
    {
        
    }
    func delegateFailedReceivedChatListArray()
    {
        
    }
    func delegateGetSentChatListArray(_ aryResult:[CYLChatListObject])
    {
        self.aryTemp.removeAll()
        self.aryTemp = aryResult
        
        self.aryMessageList.removeAll()
        self.aryMessageList.append(contentsOf: self.aryTemp)
        self.aryMessageList = self.aryMessageList.sorted(by: { $0.recentMsgTime > $1.recentMsgTime })
        
        if self.aryMessageList.count > 0
        {
            self.vwNewChat.isHidden = true
        }
        else
        {
            self.vwNewChat.isHidden = false
        }
        DispatchQueue.main.async
            {
                self.tblChatList.reloadData()
                self.vwLoader.isHidden = true
                //                //KVNProgress.dismiss()
                self.tblChatList.isHidden=false
        }
    }
    func delegateFailedSendChatListArray()
    {
        //self.aryMessageList.removeAll()
        vwLoader.isHidden = true
        //        //KVNProgress.dismiss()
        self.aryTemp.removeAll()
        self.vwNewChat.isHidden = false
        //        CYLChatManager.sharedInstance.getUserSentChatListArray()
    }
    
}
extension CYLSendMessageVC
{
    func refreshViewSlowOrMantinence(msgStr: String , imgName:String)
    {
        if vwEmptyState.frame.size.width == 0
        {
            vwEmptyState = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height))
            vwEmptyState.backgroundColor = UIColor.white
            self.view.addSubview(vwEmptyState)
            let img = UIImageView(frame: CGRect(x: vwEmptyState.frame.width/2-60, y: vwEmptyState.frame.height/2-200, width: 120, height: 120))
            img.image = UIImage(named: imgName)
            
            let lblMsg = UILabel(frame: CGRect(x: 40, y: img.frame.origin.y+img.frame.size.height+20, width: self.view.frame.size.width-80, height: 21))
            lblMsg.font = UIFont(name: "Comfortaa-Bold", size: 20)
            lblMsg.textAlignment = .center
            lblMsg.text = "Oops!"
            
            let lblAlertText = UILabel(frame: CGRect(x: 30, y: lblMsg.frame.origin.y+lblMsg.frame.size.height+15, width: self.view.frame.size.width-60, height: 40))
            lblAlertText.numberOfLines = 0
            lblAlertText.textAlignment = .center
            lblAlertText.text = msgStr
            lblAlertText.adjustsFontSizeToFitWidth = true
            lblAlertText.font = UIFont(name: "Comfortaa-Bold", size: 15)
            
            let btnRefresh: UIButton = UIButton(frame: CGRect(x: vwEmptyState.frame.width/2-100, y: lblAlertText.frame.origin.y+lblAlertText.frame.size.height+35, width: 200, height: 45))
            btnRefresh.addTarget(self, action: #selector(btnRefreshAction), for: .touchUpInside)
            btnRefresh.titleLabel?.textAlignment = .center
            btnRefresh.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 15)
            btnRefresh.titleLabel?.textColor = UIColor.white
            btnRefresh.setTitle("Retry", for: UIControlState.normal)
            btnRefresh.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
            btnRefresh.layer.cornerRadius = 23
            btnRefresh.layer.masksToBounds = true
            
            vwEmptyState.addSubview(img)
            vwEmptyState.addSubview(lblMsg)
            vwEmptyState.addSubview(lblAlertText)
            vwEmptyState.addSubview(btnRefresh)
            vwEmptyState.isHidden = false
        }
        else
        {
            vwEmptyState.isHidden = false
        }
    }
    @objc func btnRefreshAction(sender: UIButton)
    {
        if checkInternetConnection()
        {
            vwEmptyState.isHidden = true
            CYLChatManager.sharedInstance.removeAllChatListObserversForUser("usr_id_\(CYLGlobals.sharedInstance.usrObject!.id!)")
            self.requestToGetChatList()
        }
    }
}

