

//
//  CYLChatDetailVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/24/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
//import UITextView
import IQKeyboardManagerSwift
protocol CYLMessageDelegate: class
{
    //required delegate function
    func delegateToGetUpdatedChatId(strChatId:String)
    
}
class CYLChatDetailVC: CYLBaseViewController,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,CYLdelegateBlockedUser,CYLIAPReloadAppData {
    
    var vwFirstWave: UIView = UIView()
    @IBOutlet weak var btnSayHiWithWave: UIButton!
    @IBOutlet weak var lblWaveSayHiText: UILabel!
    @IBOutlet weak var constWaveViewH: NSLayoutConstraint!
    var pd:UIPasteboard = UIPasteboard.general
    var headerLoaderView:UIView?
    @IBOutlet weak var constEnableViewH: NSLayoutConstraint!
    @IBOutlet weak var lblSeeWhoChatWith: UILabel!
    weak var messageDelegate:CYLMessageDelegate?
    @IBOutlet weak var btnToEnable: UIButton!
    @IBOutlet weak var vwNewChatWave: UIView!
    @IBOutlet weak var constComposeTextBottom: NSLayoutConstraint!
    @IBOutlet weak var txtComposeMessage: UITextView!
    @IBOutlet weak var btnToComposeMessage: UIButton!
    @IBOutlet weak var tblChatDetail: UITableView!
    var isFirstTimeMessage: Bool = true
    var dictGroupedMessages = NSMutableDictionary()
    var aryGroupKeys = [String]()
    // 0 : Male
    // 1 : Female
    var navigationOverlay:UIView?
    var userName:String?
    var imgUrl:String?
    var aryChats=[CYLMessageObject]()
    var chatId:String?
    var receiverId:Int?
    var waveEmoticonType:Int = Int()
    var vwLoader: UIView = UIView()
    var timer = Timer().self
    var comesNotification: String = String()
    var lastMessageTime: Int = Int()
    var lastMessageTimeChangeBool: Bool = Bool()
    var totalMsg: Int = Int()
    var contentSize: CGSize = CGSize()
    var cleverTapEventData = [String:Any]()
    var checkKeyWordPresent: Bool = Bool()
    var createdByAdmin:Bool = Bool()
    var genderType: String = String()
    var isMessagePopupLimitExced: Bool = Bool()
    var isMessageLimitExcedMessage: String = String()
    //CleverTap
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIApplication.shared.applicationIconBadgeNumber = 0
        loaderGifImage()
        self.performEssentialSetupForChatDetailScreen()
        self.customizeNavigationBarTitleView()
        self.constWaveViewH.constant = 0
        self.vwNewChatWave.isHidden = true
        if comesNotification != ""
        {
            self.requestToRemoveObserversFromFirebase()
        }
        self.createChatScreenDesign()
        CYLChatManager.sharedInstance.keyValueMessage = ""
        CYLChatManager.sharedInstance.delegateChatDetail = self
        self.requestToGetMessagesFromFirebase()
        self.showNotificationEnableBanner()
        NotificationCenter.default.removeObserver(NSNotification.Name(rawValue: "notification_setting_updated"))
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationEnableBanner), name: NSNotification.Name(rawValue: "notification_setting_updated"), object: nil)
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Chat detail messages")
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.userDeletedBlockedCheck()
        
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Roboto-Regular", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.disableIQKeyboardManager()
        self.createChatScreenDesign()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationOverlay?.backgroundColor = UIColor.black.withAlphaComponent(1)
        IQKeyboardManager.shared.enable=true
        self.view.endEditing(true)
        timer.invalidate()
        if chatId != ""
        {
            DispatchQueue.global().async
                {
                    CYLChatManager.sharedInstance.resetUnreadCountForChatIdNew(self.chatId!, "", vwLoader: self.vwLoader)
            }
        }
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func performEssentialSetupForChatDetailScreen()
    {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        IQKeyboardManager.shared.enableAutoToolbar=false
    }
    func disableIQKeyboardManager()
    {
        IQKeyboardManager .shared.enable=false
        IQKeyboardManager.shared.enableAutoToolbar=false
    }
    func createChatScreenDesign()
    {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        
        self.tblChatDetail.tableFooterView=UIView()
        self.btnToComposeMessage.isEnabled=false
    }
    @objc func showNotificationEnableBanner()
    {
        self.lblSeeWhoChatWith.text = "See when \(userName!) replies."
        self.btnToEnable.layer.borderColor = UIColor.white.cgColor
        self.btnToEnable.layer.borderWidth = 1
        self.constEnableViewH.constant = (self.isValidateNotificationAllowed()) ? 0 : 44
    }
    func customizeNavigationBarTitleView()
    {
        self.navigationItem.titleView?.isUserInteractionEnabled = true
        
        let navView = UIView()
        navView.frame = CGRect(x: 0, y: 0, width: 220, height: 44)
        let label = UILabel()
        label.text = userName
        label.font = UIFont(name: "Roboto-Regular", size: 16.0)!
        label.textColor = UIColor.white
        label.sizeToFit()
        label.center = navView.center
        label.textAlignment = NSTextAlignment.center
        
        let image = UIImageView()
        image.sd_setImage(with: URL.init(string: imgUrl!))
        image.frame = CGRect(x: label.frame.origin.x-label.frame.size.height*1.5-4, y: label.frame.origin.y-4, width: label.frame.size.height*1.5, height: label.frame.size.height*1.5)
        image.contentMode = UIViewContentMode.scaleAspectFill
        image.layer.cornerRadius = (label.frame.size.height*1.5)/2
        image.layer.masksToBounds = true
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x:image.frame.origin.x,y:image.frame.origin.y,width:label.frame.size.width+8+image.frame.size.width,height:image.frame.size.height)
        button.setTitle("", for: .normal)
        button.backgroundColor = UIColor.clear
        button.addTarget(self, action: #selector(self.btnToUserProfileClicked), for: .touchUpInside)
        
        navView.addSubview(label)
        navView.addSubview(image)
        navView.addSubview(button)
        
        self.navigationItem.titleView = navView
        navView.sizeToFit()
    }
    @objc func btnToBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
        self.requestToRemoveObserversFromFirebase()
    }
    @objc func btnToUserProfileClicked()
    {
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserProfileVC") as! CYLUserProfileVC
        userProfileVC.travellerId = self.receiverId
        userProfileVC.delegateBlockedUser = self
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    
    func scrollTableViewToBottom()
    {
        if self.aryGroupKeys.count > 0
        {
            let arySecRows = self.dictGroupedMessages[self.aryGroupKeys[self.aryGroupKeys.count-1]] as! [CYLMessageObject]
            
            if (self.tblChatDetail.numberOfRows(inSection: self.aryGroupKeys.count-1) == arySecRows.count)
            {
                self.tblChatDetail.scrollToRow(at: IndexPath.init(row: arySecRows.count-1, section: self.aryGroupKeys.count-1), at: .bottom, animated: false)
            }
        }
        
    }
    
    func isValidateNotificationAllowed()->Bool
    {
        let notificationType = UIApplication.shared.currentUserNotificationSettings?.types
        if CYLGlobals.sharedInstance.usrObject?.newMessageNotify == 0 || notificationType?.rawValue == 0
        {
            return false
        }
        return true
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom button actions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnOtherUserProfile(_ sender: UIButton)
    {
        self.btnToUserProfileClicked()
    }
    @IBAction func btnToComposeMessageClicked(_ sender: Any)
    {
        if CYLGlobals.sharedInstance.usrObject!.createdByAdmin == true
        {
            if self.txtComposeMessage.text != ""
            {
                self.requestToComposeChatMessage()
            }
            else
            {
                self.btnToComposeMessage.isEnabled = false
            }
        }
        else
        {
            if self.txtComposeMessage.text != ""
            {
                if isFirstTimeMessage == false
                {
                    self.requestToComposeChatMessage()
                }
                else
                {
                    if isMessagePopupLimitExced == true
                    {
                        self.txtComposeMessage.text = ""
                        if CYLGlobals.sharedInstance.usrObject?.userPlanType == 0
                        {
                            let paymentPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserPaymentVC") as! CYLUserPaymentVC
                            paymentPopup.messageLimitExceed = "message"
                            paymentPopup.delegateMessageReload = self
                            self.navigationController?.pushViewController(paymentPopup, animated: false)
                        }
                        else
                        {
                            let text = showAlertTitleMessageFont(strTitle: "Message Limit Exceeded", strMessage: self.isMessageLimitExcedMessage)
                            let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)

                            alert.setValue(text.title, forKey: "attributedTitle")
                            alert.setValue(text.message, forKey: "attributedMessage")
                            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                                (sender) in
                            })
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        self.requestToComposeChatMessage()
                    }
                }
                
            }
            else
            {
                self.btnToComposeMessage.isEnabled = false
            }
            
        }
        
    }
    @IBAction func btnToEnableNotificationClicked(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "isFirstInstall") != nil
        {
            if !self.isNotificationEnabledFromSettings()
            {
                //show option to enable notification from device settings
                let text = showAlertTitleMessageFont(strTitle: "Allow Notifications", strMessage: "Please go to iOS settings and Allow Notifications for Tourgether. You can again turn it off later if you wish")
                let alertComtroller = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                alertComtroller.setValue(text.title, forKey: "attributedTitle")
                alertComtroller.setValue(text.message, forKey: "attributedMessage")
                let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler:
                { action -> Void in
                    self.dismiss(animated: true, completion: nil)
                })
                alertComtroller.addAction(cancelAction)
                
                let settingAction = UIAlertAction.init(title: "Change Settings", style: .default, handler:
                { action -> Void in
                    UIApplication.shared.openURL(URL.init(string: UIApplicationOpenSettingsURLString)!)
                    self.dismiss(animated: true, completion: nil)
                })
                alertComtroller.addAction(settingAction)
                
                self.present(alertComtroller, animated: true, completion: nil)
            }
            else
            {
                self.view.layoutIfNeeded()
                let allowPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLAllowChatVC") as! CYLAllowChatVC
                allowPopup.modalPresentationStyle = .overCurrentContext
                allowPopup.modalTransitionStyle = .crossDissolve
                self.navigationController?.present(allowPopup, animated: true, completion: nil)
            }
        }
        else
        {
            self.view.layoutIfNeeded()
            let allowPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLAllowChatVC") as! CYLAllowChatVC
            allowPopup.modalPresentationStyle = .overCurrentContext
            allowPopup.modalTransitionStyle = .crossDissolve
            self.navigationController?.present(allowPopup, animated: true, completion: nil)
        }
        
        
    }
    func isNotificationEnabledFromSettings()->Bool
    {
        let notificationType = UIApplication.shared.currentUserNotificationSettings?.types
        if notificationType?.rawValue == 0
        {
            return false
        } else {
            return true
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Keyboard Notifications
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @objc func keyboardNotification(notification: NSNotification)
    {
        if let userInfo = notification.userInfo
        {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.constComposeTextBottom?.constant = 0.0
                //self.scrollTableViewToBottom()
            } else {
                self.constComposeTextBottom?.constant = endFrame?.size.height ?? 0.0
                //self.scrollTableViewToBottom()
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
        self.scrollTableViewToBottom()
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Text field delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func textViewDidChangeSelection(_ textView: UITextView) {
        
        if textView == txtComposeMessage
        {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 2
            textView.typingAttributes = [NSAttributedStringKey.paragraphStyle.rawValue : style,NSAttributedStringKey.font.rawValue: UIFont(name: "Roboto-Regular", size: 14)!]
            
            let inputText = textView.text!
            let trimmedString: String = inputText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            if newSize.height > 100
            {
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: 100)
                textView.isScrollEnabled = true
            }
            else
            {
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                textView.isScrollEnabled = false
            }
            textView.frame = newFrame
            
            textView.translatesAutoresizingMaskIntoConstraints = true
            
            if trimmedString.count > 0
            {
                self.btnToComposeMessage.isEnabled=true
            }
            else
            {
                self.btnToComposeMessage.isEnabled=false
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Table view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if self.dictGroupedMessages.allKeys.count != 0
        {
            let hederView = UIView.init(frame:CGRect(x:0,y:0,width:self.screenWidth!,height:23))
            let label = UILabel.init(frame:CGRect(x:8,y:2,width:120,height:19))
            label.text = self.aryGroupKeys[section]
            label.textColor = UIColor.white
            label.font = UIFont(name: "Roboto-Regular", size: 10)
            label.center = hederView.center
            label.textAlignment = .center
            label.backgroundColor = UIColor(red: 25/255, green: 34/255, blue: 38/255, alpha: 1)
            label.layer.cornerRadius = 19/2
            label.layer.masksToBounds = true
            hederView.addSubview(label)
            return hederView
        }
        else
        {
            let hederView = UIView.init(frame:CGRect(x:0,y:20,width:self.screenWidth!,height:0))
            return hederView
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 23
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if self.aryGroupKeys.count != 0
        {
            return self.aryGroupKeys.count
        }
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.dictGroupedMessages.allKeys.count != 0
        {
            let arySecRows = self.dictGroupedMessages[self.aryGroupKeys[section]] as! [CYLMessageObject]
            return arySecRows.count
        }
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.dictGroupedMessages.allKeys.count != 0
        {
            let arySecRows: [CYLMessageObject] = self.dictGroupedMessages[self.aryGroupKeys[indexPath.section]] as! [CYLMessageObject]
            let object: CYLMessageObject = arySecRows[indexPath.row]
            if object.waveType != nil
            {
                if object.waveType == "WAVE_SENT"
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CYLChatDetailWaveSentBothSideCell") as! CYLChatDetailWaveSentBothSideCell
                    cell.selectionStyle = .none
                    cell.lblSentText.text = object.messageText
                    cell.lblSentMessage.text = (object.receiverId == CYLGlobals.sharedInstance.usrObject?.id) ? userName! + " waved! \nwaiting for you..." : "You waved! \nwaiting for " + userName! + "..."
                    let timeAmPm = convertTimestampIntoDateString(date: Double((object.creationTime)!), format: "h:mm aa")
                    cell.lblSentTime.text = timeAmPm
                    return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CYLChatDetailWaveSentBothSideCell") as! CYLChatDetailWaveSentBothSideCell
                    cell.selectionStyle = .none
                    cell.lblSentText.text = object.messageText
                    cell.lblSentMessage.text = "You and " + userName! + " at each other!"
                    let timeAmPm = convertTimestampIntoDateString(date: Double((object.creationTime)!), format: "h:mm aa")
                    cell.lblSentTime.text = timeAmPm
                    return cell
                }
                
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CYLChatDetailCell") as! CYLChatDetailCell
                cell.selectionStyle = .none
                cell.receiverProfileURL = self.imgUrl!
                cell.object = arySecRows[indexPath.row]
                if self.tblChatDetail.contentOffset.y < 0
                {
                    setupActivityIndicatorInTableViewHeader()
                    self.contentSize = self.tblChatDetail.contentSize
                    CYLChatManager.sharedInstance.getUserChatsMessageForChatIdPaging(self.chatId!, lastMessageTime: lastMessageTime)
                    self.lastMessageTimeChangeBool = true
                }
                
                ////////////Check First Message
                self.isFirstTimeMessage = false
                return cell
            }
            
        }
        let cell: UITableViewCell = UITableViewCell()
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, canPerformAction action:
        Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        
        if (action.description == "copy:") {
            return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, performAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) {
        if (action.description == "copy:")  {
            let arySecRows = self.dictGroupedMessages[self.aryGroupKeys[indexPath.section]] as! [CYLMessageObject]
            pd.string = arySecRows[indexPath.row].messageText
        }
    }
    
    func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func setupActivityIndicatorInTableViewHeader()
    {
        self.headerLoaderView = UIView.init(frame: CGRect(x:0,y:0,width:self.screenWidth!,height:40))
        
        let actIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        actIndicator.tag = 10
        actIndicator.frame = CGRect(x:0.0, y:0.0, width:20.0, height:20.0)
        actIndicator.center.x = (self.headerLoaderView?.center.x)!
        actIndicator.center.y = (self.headerLoaderView?.center.y)!
        actIndicator.hidesWhenStopped = true
        self.headerLoaderView?.addSubview(actIndicator)
        actIndicator.startAnimating()
        self.tblChatDetail.tableHeaderView = self.headerLoaderView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.dictGroupedMessages.allKeys.count != 0
        {
            let arySecRows = self.dictGroupedMessages[self.aryGroupKeys[indexPath.section]] as! [CYLMessageObject]
            let object = arySecRows[indexPath.row]
            if object.waveType != nil
            {
                return 126
            }
            return self.getCellHeightForString(object.messageText!)
        }
        return 0
        
    }
    func getCellHeightForString(_ strMessage:String)->CGFloat
    {
        
        return strMessage.height(withConstrainedWidth: self.screenWidth!-172, font: UIFont(name: "Roboto-Regular", size: 14)!) + 55
        
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToGetMessagesFromFirebase()
    {
        self.aryChats.removeAll()
        self.tblChatDetail.reloadData()
        if chatId != ""
        {
            vwLoader.isHidden = false
            DispatchQueue.main.async
                {
                    CYLChatManager.sharedInstance.getUserChatsMessageForChatIdPagingFirst(self.chatId!, vwLoader: self.vwLoader)
                    self.txtComposeMessage.text = ""
                    CYLChatManager.sharedInstance.resetUnreadCountForChatIdNew(self.chatId!,"", vwLoader: self.vwLoader)
            }
        }
        else
        {
            self.chatId = "cht_id_\(CYLGlobals.sharedInstance.usrObject!.id!)_\(self.receiverId!)"
            self.requestToGetMessagesFromFirebase()
        }
    }
    
    func userDeletedBlockedCheck()
    {
        let params:[String: Any] = ["userId":"\(receiverId!)"]
        let request = CYLServices.getOtherUserProfile(dict: params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData!["status"]?.stringValue != "fail"
                    {
                        if (responseData?["data"]!["wave"].exists())!
                        {
                            
                            self.waveEmoticonType = (responseData?["data"]!["wave"]["waveEmoticonType"].intValue)!
                            
                            if responseData?["data"]!["createdByAdmin"].boolValue == true
                            {
                                self.createdByAdmin = true
                                self.tblChatDetail.reloadData()
                            }
                            
                            //Wave Emoticon Type:- 0(Unsent),1(Sent),2(Received),3(Mutual)
                            if responseData?["data"]!["wave"]["waveEmoticonType"].intValue == 0
                            {
                                self.vwNewChatWave.isHidden = false
                                self.constWaveViewH.constant = 120
                                self.lblWaveSayHiText.text = "Say Hi to " + self.userName! + " with a wave"
                                self.btnSayHiWithWave.setTitle("    Wave    ", for: .normal)
                            }
                            else if responseData?["data"]!["wave"]["waveEmoticonType"].intValue == 2
                            {
                                self.vwNewChatWave.isHidden = false
                                self.constWaveViewH.constant = 120
                                self.lblWaveSayHiText.text = "Say Hi to " + self.userName! + " with a wave"//self.userName! + " waved! waiting for you..."
                                self.btnSayHiWithWave.setTitle("    Wave    ", for: .normal)
                            }
                            else
                            {
                                self.vwNewChatWave.isHidden = true
                                self.constWaveViewH.constant = 0
                            }
                        }
                        else
                        {
                            self.waveEmoticonType = 0
                            self.vwNewChatWave.isHidden = false
                            self.constWaveViewH.constant = 120
                            self.lblWaveSayHiText.text = "Say Hi to" + self.userName! + "with a wave"
                        }
                        if responseData?["data"]!["showWave"].bool != true
                        {
                            self.vwNewChatWave.isHidden = true
                            self.constWaveViewH.constant = 0
                        }
                        let aryCities = responseData?["data"]!["cities"].arrayValue
                        let aryCountries = responseData?["data"]!["countries"].arrayValue
                        var detinationType: String!
                        if aryCities?.count != 0{
                            detinationType = "City"
                            self.cleverTapEventData["DestinationCount"] = aryCities!.count
                        }
                        else if aryCountries?.count != 0{
                            detinationType = "Country"
                            self.cleverTapEventData["DestinationCount"] = aryCountries!.count
                        }
                        else{
                            detinationType = "No Plan"
                            self.cleverTapEventData["destinationCount"] = 0
                        }
                        self.genderType = (responseData?["data"]!["gender"].intValue == 0) ? "he" : "she"
                        self.cleverTapEventData["LivesInDestinationCountry"] = responseData?["data"]!["livesInDestinationCountry"].boolValue
                        self.cleverTapEventData["UserId"] = responseData?["data"]!["id"].stringValue
                        self.cleverTapEventData["UserName"] = responseData?["data"]!["userName"].stringValue
                        self.cleverTapEventData["Age"] = responseData?["data"]!["age"].stringValue
                        self.cleverTapEventData["DetinationType"] = detinationType
                        self.cleverTapEventData["Isolocation"] = responseData?["data"]!["isoLocation"].stringValue
                        let g = (responseData?["data"]!["gender"].intValue == 0) ? "Male" : "Female"
                        self.cleverTapEventData["Gender"] = g
                        self.cleverTapEventData["IsMatched"] = responseData?["data"]!["is_matched"].boolValue
                        self.cleverTapEventData["isOnline"] = responseData?["data"]!["showOnline"].boolValue
                        if let isGenderMatched = responseData?["data"]!["isGenderMatched"].boolValue
                        {
                            self.cleverTapEventData["IsGenderMatched"] = isGenderMatched
                        }
                        if let isCountryMatched = responseData?["data"]!["isCountryMatched"].boolValue
                        {
                            self.cleverTapEventData["IsCountryMatched"] = isCountryMatched
                        }
                        
                        if (responseData?["data"]!["is_deleted"].boolValue)! == true
                        {
                            self.txtComposeMessage.isEditable = false
                            showAlertView(title: "", message: "This user has deactivated his/her profile.", ref: self)
                        }
                        else if (responseData?["data"]!["is_deleted"].boolValue)! != true
                        {
                            if (responseData?["data"]!["blocked"].boolValue)! == true && (responseData?["data"]!["messageDisable"].boolValue)! == true
                            {
                                showAlertView(title: "", message: "You have blocked this user. Please visit their profile and unblock them, from the top right menu option, to continue sending messages.", ref: self)
                                self.txtComposeMessage.isEditable = false
                            }
                            else if (responseData?["data"]!["blocked"].boolValue)! == false && (responseData?["data"]!["messageDisable"].boolValue)! == true
                            {
                                self.txtComposeMessage.isEditable = false
                                showAlertView(title: "", message: "You have been blocked by this user.", ref: self)
                            }
                            if (responseData?["data"]!["isMessageLimitExceed"].exists())!
                            {
                                self.isMessageLimitExcedMessage = (responseData?["data"]!["messageLimitExceedMessage"].stringValue)!
                                if responseData?["data"]!["isMessageLimitExceed"].boolValue == true
                                {
                                    self.isMessagePopupLimitExced = true
                                }
                                else
                                {
                                    self.isMessagePopupLimitExced = false
                                }
                            }
                        }
                    }
                    
                }
        },
                                                                  withError:
            {
                (error) in
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
        
    }
    func sendPushNotificatonOnServer(test: String,unread_count: Int)  {
        
        let isFirstChat = self.isFirstTimeMessage == true ? true : false
        let params:[String: Any] = ["sender_id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","receiver_id":"\(receiverId!)","message":test, "unread_count": "\(unread_count)","chat_id": self.chatId!,"isFirstChat": isFirstChat]
        let request = CYLServices.postComposeMessage(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                    }
                    
                }
        },
                                                                  withError:
            {
                (error) in
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Chat detail messages"])
        },
                                                                  andNetworkErr: {
                                                                    (networkFailure) in
                                                                    sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Chat detail messages"])
                                                                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    
    func requestToRemoveObserversFromFirebase()
    {
        if chatId != ""
        {
            DispatchQueue.global().async
                {
                    CYLChatManager.sharedInstance.removeAllChatMessageObserversForChatId(self.chatId!)
            }
        }
    }
    func requestToComposeChatMessage()
    {
        self.btnToComposeMessage.isEnabled=false
        
        DispatchQueue.main.async {
            
            var unreadCount: Int = 1
            if self.chatId == ""
            {
                self.chatId = "cht_id_\(CYLGlobals.sharedInstance.usrObject!.id!)_\(self.receiverId!)"
            }
            CYLDBRefProvider.sharedInstance.chatListReference.child("usr_id_\(self.receiverId!)").child("reciever").child(self.chatId!).observeSingleEvent(of: .value,  with: { (snapshot) in
                
                if snapshot.exists()
                {
                    let dict = snapshot.value as? NSDictionary
                    unreadCount = dict!["unread_count"] as! Int + 1
                    self.sendMessageWithUnReadCount(unreadCount: unreadCount)
                }
                else
                {
                    self.sendMessageWithUnReadCount(unreadCount: unreadCount)
                }
            })
        }
    }
    func loaderGifImage()
    {
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwLoader = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwLoader.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
            window.addSubview(vwLoader)
            let img = UIImageView(frame: CGRect(x: vwLoader.frame.width/2-40, y: vwLoader.frame.height/2-40, width: 80, height: 80))
            img.image = UIImage.sd_animatedGIF(with: imageData!)
            vwLoader.addSubview(img)
            vwLoader.isHidden = true
        }
    }
    
    @objc func updateTimer() {
        vwLoader.isHidden = false
        //        //KVNProgress.show()
    }
    func sendMessageWithUnReadCount(unreadCount: Int)
    {
        var msgSentUniqueBidirectionalMe: Int = 1
        var msgSentUniqueBidirectionalOther: Int = Int()
        if self.aryGroupKeys.count == 0
        {
            self.cleverTapEventData["MessageType"] = "firstMessage"
            sentEventPropertyOnCleverTap("Send message click", _property: self.cleverTapEventData)
        }
        else
        {
            var myMsgCount:Int = 1
            var otherMsgCount: Int = Int()
            
            for i in 0..<self.aryGroupKeys.count
            {
                let aryMsg = self.dictGroupedMessages[self.aryGroupKeys[i]] as! [CYLMessageObject]
                for j in 0..<aryMsg.count
                {
                    self.checkKeyWord(str: aryMsg[j].messageText!)
                    if aryMsg[j].senderId == CYLGlobals.sharedInstance.usrObject?.id!
                    {
                        myMsgCount = myMsgCount+1
                        msgSentUniqueBidirectionalMe = msgSentUniqueBidirectionalMe + 1
                    }
                    else
                    {
                        msgSentUniqueBidirectionalOther = msgSentUniqueBidirectionalOther + 1
                        otherMsgCount = otherMsgCount+1
                    }
                }
                
            }
            if otherMsgCount == 0
            {
                var dict:[String: Any] = self.cleverTapEventData
                dict["MessageType"] = "unidirectional"
                sentEventPropertyOnCleverTap("Send message click", _property: dict)
            }
            else if myMsgCount >= 3 && otherMsgCount >= 3
            {
                var dict:[String: Any] = self.cleverTapEventData
                dict["MessageType"] = "3each"
                sentEventPropertyOnCleverTap("Send message click", _property: dict)
                
                if checkKeyWordPresent == false
                {
                    var checkText:Bool = Bool()
                    if isValidEmail(testStr: self.txtComposeMessage.text)
                    {
                        checkText = true
                    }
                    else if isValidatePhone(value: self.txtComposeMessage.text)
                    {
                        checkText = true
                    }
                    else
                    {
                        let regexp = "(?=.*whatsapp*)|(?=.*watsapp*)|(?=.*instagram*)|(?=.*facebook*)|(?=.*wechat*)|(?=.*instagram*)|(?=.*email*)|(?=.*line*)|(?=.*insta*)|(?=.*telegram*)|(?=.*gmail*)"
                        if let _ = self.txtComposeMessage.text.lowercased().range(of:regexp, options: .regularExpression)
                        {
                            checkText = true
                        }
                    }
                    if checkText
                    {
                        sentEventPropertyOnCleverTap("Personal messenger exchange", _property: self.cleverTapEventData)
                    }
                }
                
                
            }
            else
            {
                var dict:[String: Any] = self.cleverTapEventData
                dict["MessageType"] = "bidirectional"
                sentEventPropertyOnCleverTap("Send message click", _property: dict)
            }
            if msgSentUniqueBidirectionalMe == 1 && msgSentUniqueBidirectionalOther > 0
            {
                sentEventPropertyOnCleverTap("Unique bidirectional message", _property: self.cleverTapEventData)
            }
            else if msgSentUniqueBidirectionalMe == 3 && msgSentUniqueBidirectionalOther >= 3
            {
                sentEventPropertyOnCleverTap("Unique 3 Messages exchange", _property: self.cleverTapEventData)
            }
        }
        
        timer = Timer.scheduledTimer(timeInterval: 0.2, target: self,   selector: (#selector(CYLChatDetailVC.updateTimer)), userInfo: nil, repeats: true)
        let sentTime = Date().millisecondsSince1970
        
        let userSenderNode = CYLDBRefProvider.sharedInstance.chatListReference.child("usr_id_\(CYLGlobals.sharedInstance.usrObject!.id!)").child("sender").child(self.chatId!)
        
        userSenderNode.setValue(["chat_id": self.chatId!,"is_blocked": false, "last_message":"\(self.txtComposeMessage.text!)","last_message_time":sentTime,"other_user_id":self.receiverId!,"type":"sender","unread_count":0])
        
        let userRecieverNode = CYLDBRefProvider.sharedInstance.chatListReference.child("usr_id_\(self.receiverId!)").child("reciever").child(self.chatId!)
        
        userRecieverNode.setValue(["chat_id": self.chatId!,"is_blocked": false, "last_message":"\(self.txtComposeMessage.text!)","last_message_time":sentTime,"other_user_id":CYLGlobals.sharedInstance.usrObject!.id!,"type":"reciever","unread_count":unreadCount])
        
        let sentMessage = CYLDBRefProvider.sharedInstance.chatDetailReference.child(self.chatId!).childByAutoId()
        sentMessage.setValue(["message": self.txtComposeMessage.text!,"message_id": "", "sender_id":CYLGlobals.sharedInstance.usrObject!.id!,"sent_time":sentTime])
        
        self.sendPushNotificatonOnServer(test: self.txtComposeMessage.text!, unread_count: unreadCount)
        //        self.type = "sender"
        self.messageDelegate?.delegateToGetUpdatedChatId(strChatId: self.chatId!)
        //        self.appObject?.activeChatId = self.chatId
        self.txtComposeMessage.text = ""
    }
    
    func checkKeyWord(str: String)
    {
        if isValidEmail(testStr: str)
        {
            checkKeyWordPresent = true
        }
        else if isValidatePhone(value: str)
        {
            checkKeyWordPresent = true
        }
        else
        {
            let regexp = "(?=.*whatsapp*)|(?=.*watsapp*)|(?=.*instagram*)|(?=.*facebook*)|(?=.*wechat*)|(?=.*instagram*)|(?=.*email*)|(?=.*line*)|(?=.*insta*)|(?=.*telegram*)|(?=.*gmail*)"
            if let _ = str.lowercased().range(of:regexp, options: .regularExpression)
            {
                checkKeyWordPresent = true
            }
        }
    }
    
    func blockUserCheck()
    {
        self.txtComposeMessage.isEditable = false
    }
    //MARK:- Button Say Hello On Wave
    
    @IBAction func btnWaveCancelView(_ sender: UIButton)
    {
        self.constWaveViewH.constant = 0
        self.vwNewChatWave.isHidden = true
    }
    @IBAction func btnSayHelloWave(_ sender: UIButton)
    {
        if CYLGlobals.sharedInstance.usrObject!.wavesRemaining != 0
        {
            if UserDefaults.standard.value(forKey: "messageWave_count") as! Int == 0
            {
                self.createFirstWavePopUp()
            }
            else
            {
                self.requestToComposeWavebackMessage()
            }
        }
        else
        {
            
            if CYLGlobals.sharedInstance.usrObject?.userPlanType == 0
            {
                let paymentPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserPaymentVC") as! CYLUserPaymentVC
                paymentPopup.messageLimitExceed = "wave"
                paymentPopup.delegateMessageReload = self
                self.navigationController?.pushViewController(paymentPopup, animated: false)
            }
            else
            {
                let text = showAlertTitleMessageFont(strTitle: "Wave Limit Exceeded", strMessage: (CYLGlobals.sharedInstance.usrObject!.wavesMessage ?? "")!)
                let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                
                alert.setValue(text.title, forKey: "attributedTitle")
                alert.setValue(text.message, forKey: "attributedMessage")
                let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                    (sender) in
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    func reloadMessageReceiverListDelegatePayment()
    {
        if CYLGlobals.sharedInstance.usrObject?.userPlanType == 1
        {
            self.txtComposeMessage.isEditable = true
        }
    }
    func createFirstWavePopUp()
    {
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwFirstWave = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwFirstWave.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.8)
            
            let popUpView = UIView(frame: CGRect(x: 40, y: vwFirstWave.frame.height/2-145, width: vwLoader.frame.width-80, height: 290))
            popUpView.backgroundColor = UIColor.white
            popUpView.layer.cornerRadius = 8
            popUpView.layer.masksToBounds = true
            vwFirstWave.addSubview(popUpView)
            
            let lblName = UILabel(frame: CGRect(x: 20, y: 40, width: popUpView.frame.width-40, height: 30))
            lblName.text = "Wanna wave at \(userName!)?"
            lblName.font = UIFont(name: "Pacifico-Bold", size: 24.0)
            lblName.textColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            lblName.adjustsFontSizeToFitWidth = true
            lblName.textAlignment = .center
            popUpView.addSubview(lblName)
            
            let lblTitle = UILabel(frame: CGRect(x: 20, y: lblName.frame.origin.y+lblName.frame.size.height+30, width: popUpView.frame.width-40, height: 30))
            lblTitle.text = "Secretly wave at \(self.genderType == "he" ? "him":"her"). If \(self.genderType) also wave at you, you'll be able to chat"
            lblTitle.font = UIFont(name: "Comfortaa-Bold", size: 16.0)
            lblTitle.textColor = UIColor.lightGray
            lblTitle.numberOfLines = 0
            lblTitle.textAlignment = .center
            lblTitle.sizeToFit()
            popUpView.addSubview(lblTitle)
            
            let btnCancle: UIButton = UIButton(frame: CGRect(x: 0, y: popUpView.frame.size.height-45, width: popUpView.frame.width, height: 45))
            btnCancle.addTarget(self, action: #selector(btnFirstWaveAction(_:)), for: .touchUpInside)
            btnCancle.titleLabel?.textAlignment = .center
            btnCancle.tag = 1002
            btnCancle.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 16)
            btnCancle.setTitleColor(UIColor.lightGray, for: .normal)
            btnCancle.setTitle("Cancel", for: UIControlState.normal)
            popUpView.addSubview(btnCancle)
            
            let vwLine1 = UIView(frame: CGRect(x: 0, y: btnCancle.frame.origin.y-1, width: popUpView.frame.width, height: 1))
            vwLine1.backgroundColor = UIColor.lightGray
            popUpView.addSubview(vwLine1)
            
            let btnWaveSecretly: UIButton = UIButton(frame: CGRect(x: 0, y: vwLine1.frame.origin.y-45, width: popUpView.frame.width, height: 45))
            btnWaveSecretly.addTarget(self, action: #selector(btnFirstWaveAction(_:)), for: .touchUpInside)
            btnWaveSecretly.tag = 1001
            popUpView.addSubview(btnWaveSecretly)
            
            let imgHand = UIImageView(frame: CGRect(x: popUpView.frame.width/2-60, y: btnWaveSecretly.frame.origin.y+12, width: 20, height: 20))
            imgHand.image = UIImage(named: "ic_hand.png")
            imgHand.image = imgHand.image!.withRenderingMode(.alwaysTemplate)
            imgHand.tintColor = UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            popUpView.addSubview(imgHand)
            
            let lblWaveSecretly = UILabel(frame: CGRect(x: imgHand.frame.size.width+imgHand.frame.origin.x+5, y: btnWaveSecretly.frame.origin.y, width: 150, height: 45))
            lblWaveSecretly.text = "Wave Secretly"
            lblWaveSecretly.font = UIFont(name: "Comfortaa-Bold", size: 16.0)
            lblWaveSecretly.textColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            lblWaveSecretly.textAlignment = .left
            popUpView.addSubview(lblWaveSecretly)
            
            let vwLine2 = UIView(frame: CGRect(x: 0, y: btnWaveSecretly.frame.origin.y-1, width: popUpView.frame.width, height: 1))
            vwLine2.backgroundColor = UIColor.lightGray
            popUpView.addSubview(vwLine2)
            
            let imgLogo = UIImageView(frame: CGRect(x: vwFirstWave.frame.width/2-40, y: popUpView.frame.origin.y-40, width: 80, height: 80))
            imgLogo.image = UIImage(named: "pay_wave")
            imgLogo.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
            vwFirstWave.addSubview(imgLogo)
            window.addSubview(vwFirstWave)
        }
    }
    
    func requestToComposeWavebackMessage()
    {
        let params:[String: Any] = ["otherUserId":"\(receiverId!)","waveCallingLocation":"MESSAGE_DETAIL"]
        let request = CYLServices.postWaveMessageForServer(dict: params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.wavesRemaining = responseData?["wavesRemaining"]?.intValue
                        userObject.wavesMessage = responseData?["message"]?.stringValue
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        UserDefaults.standard.set(1, forKey: "messageWave_count")
                        self.vwNewChatWave.isHidden = true
                        self.constWaveViewH.constant = 0
                        var cleverTapEvent: [String: Any] = self.cleverTapEventData
                        cleverTapEvent["page type"] = "message"
                        if self.waveEmoticonType == 0
                        {
                            cleverTapEvent["wave type"] =  "wave sent"
                        }
                        else
                        {
                            cleverTapEvent["wave type"] =  "wave sent back"
                        }
                        sentEventPropertyOnCleverTap("Wave clicked", _property: cleverTapEvent)
                        if self.waveEmoticonType == 2
                        {
                            self.view.layoutIfNeeded()
                            let matchedPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLMatchedPopUpVC") as! CYLMatchedPopUpVC
                            matchedPopup.imgRecever = self.imgUrl ?? ""
                            matchedPopup.comesController = "chatDetail"
                            self.navigationController?.present(matchedPopup, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.wavesMessage = responseData?["message"]?.stringValue
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                    }
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Wave click message"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
        },
                                                                  withError:
            {
                (error) in
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Wave click message"])
        },
                                                                  andNetworkErr: {
                                                                    (networkFailure) in
                                                                    sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Wave click message"])
                                                                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    @objc func btnFirstWaveAction(_ sender: UIButton)
    {
        if sender.tag == 1001
        {
            self.vwFirstWave.isHidden = true
            self.requestToComposeWavebackMessage()
        }
        else
        {
            self.vwFirstWave.isHidden = true
        }
    }
    
}

extension CYLChatDetailVC:CYLChatDetailDelegate
{
    func delegateGetChatDetailFirstCome(_ recentChat: [AnyObject]) {
        
        for chatObj in recentChat
        {
            let object = CYLMessageObject.init(object: chatObj)
            
            if object.waveType == "WAVE_SENT" && object.senderId != CYLGlobals.sharedInstance.usrObject!.id!
            {
                
            }
            else
            {
                self.addNewMessageElementInGroupedDictionary(object)
                lastMessageTime = (lastMessageTimeChangeBool == false) ? (object.creationTime)! : lastMessageTime
                lastMessageTimeChangeBool = true
                
            }
            DispatchQueue.main.async{
                self.tblChatDetail.reloadData()
                self.scrollTableViewToBottom()
                self.vwLoader.isHidden = true
            }
        }
    }
    
    func delegateGetChatDetailPaging(_ recentChat: CYLMessageObject, _ lastIndex: Int) {
        
        print(recentChat)
        if recentChat.waveType == "WAVE_SENT" && recentChat.senderId != CYLGlobals.sharedInstance.usrObject!.id!
        {
            self.tblChatDetail.reloadData()
            let content = self.tblChatDetail.contentSize
            self.tblChatDetail.tableHeaderView = nil
            self.tblChatDetail.setContentOffset(CGPoint(x: 0, y: content.height-self.contentSize.height), animated: false)
        }
        else
        {
            self.totalMsg = totalMsg + 1
            self.addNewMessageElementInGroupedDictionaryPaging(recentChat)
            
            if totalMsg == lastIndex-1
            {
                self.tblChatDetail.reloadData()
                let content = self.tblChatDetail.contentSize
                self.tblChatDetail.tableHeaderView = nil
                self.tblChatDetail.setContentOffset(CGPoint(x: 0, y: content.height-self.contentSize.height), animated: false)
                self.totalMsg = 0
            }
        }
    }
    
    
    func delegateGetChatDetail(_ recentChat: CYLMessageObject)
    {
        if recentChat.waveType == "WAVE_SENT" && recentChat.senderId != CYLGlobals.sharedInstance.usrObject!.id!
        {
            
        }
        else
        {
            self.addNewMessageElementInGroupedDictionary(recentChat)
            lastMessageTime = (lastMessageTimeChangeBool == false) ? (recentChat.creationTime)! : lastMessageTime
            lastMessageTimeChangeBool = true
        }
        DispatchQueue.main.async
            {
                self.tblChatDetail.reloadData()
                self.scrollTableViewToBottom()
                if self.timer.isValid
                {
                    self.timer.invalidate()
                    self.vwLoader.isHidden = true
                }
        }
    }
    func delegateFailedChatDetail()
    {
        if headerLoaderView != nil
        {
            self.tblChatDetail.tableHeaderView = nil
        }
        DispatchQueue.main.async
            {
                self.vwLoader.isHidden = true
        }
    }
    func addNewMessageElementInGroupedDictionary(_ recentChat: CYLMessageObject)
    {
        let strDate = dayDifferenceFromDate(Double(recentChat.creationTime!))
        
        if self.dictGroupedMessages.value(forKey: strDate) != nil
        {
            //this date group already exists so just add the message object
            var aryObject = self.dictGroupedMessages[strDate] as! [CYLMessageObject]
            aryObject.append(recentChat)
            self.dictGroupedMessages.setValue(aryObject, forKey: strDate)
            
        }
        else
        {
            //create new date group and add the message object
            var aryObject = [CYLMessageObject]()
            aryObject.append(recentChat)
            self.dictGroupedMessages.setValue(aryObject, forKey: strDate)
            self.aryGroupKeys.append(strDate)
        }
    }
    func addNewMessageElementInGroupedDictionaryPaging(_ recentChat: CYLMessageObject)
    {
        let strDate = dayDifferenceFromDate(Double(recentChat.creationTime!))
        lastMessageTime = recentChat.creationTime!
        if self.dictGroupedMessages.value(forKey: strDate) != nil
        {
            //this date group already exists so just add the message object
            var aryObject = self.dictGroupedMessages[strDate] as! [CYLMessageObject]
            aryObject.insert(recentChat, at: 0)//.append(recentChat)
            self.dictGroupedMessages.setValue(aryObject, forKey: strDate)
            
        }
        else
        {
            //create new date group and add the message object
            var aryObject = [CYLMessageObject]()
            aryObject.insert(recentChat, at: 0)
            self.dictGroupedMessages.setValue(aryObject, forKey: strDate)
            self.aryGroupKeys.insert(strDate, at: 0)//.append(strDate)
        }
    }
}

