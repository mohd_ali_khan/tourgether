//
//  CYLAllowChatVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 7/14/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
protocol CYLAllowChatDelegate: class
{
    //required delegate function
    func delegateToHandleNotNowEvent()
    
}

class CYLAllowChatVC: UIViewController {

    var userDeflts:UserDefaults?
    weak var delegate: CYLAllowChatDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        userDeflts = UserDefaults.standard
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Allow chat messages")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnToAllowChatClicked(_ sender: Any)
    {
        if userDeflts?.value(forKey: "isFirstInstall") != nil
        {
            if CYLGlobals.sharedInstance.usrObject?.newMessageNotify == 0
            {
               //show option to enable notification from app settings
                let aryViewController = UIApplication.shared.keyWindow?.rootViewController?.childViewControllers
                for controllers in aryViewController!
                {
                    if controllers.isKind(of: CYLHomeTabController.self)
                    {
                        self.dismiss(animated: true, completion: nil)
                        let tabBarController = controllers as! UITabBarController
                        //tabBarController.selectedIndex = 0
                        let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLSettingsVC") as! CYLSettingsVC
                        tabBarController.navigationController?.pushViewController(settingVC, animated: true)
                        break
                        
                    }
                }
                
            }
        }
        else
        {
            //show option to enable notification for the first time
            let appObject = UIApplication.shared.delegate as! AppDelegate
            appObject.requestToAllowPushNotification()
            userDeflts?.set("1", forKey: "isFirstInstall")
            userDeflts?.set("1", forKey: "isNotificationRequested")
            userDeflts?.synchronize()
            self.delegate?.delegateToHandleNotNowEvent()
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func btnToRejectChatClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
        if userDeflts?.value(forKey: "isNotificationRequested") == nil
        {
        userDeflts?.set("1", forKey: "isNotificationRequested")
        }
//        else
//        {
//        userDeflts?.set("1", forKey: "isFirstInstall")
//        }
        userDeflts?.synchronize()
    }
    func isNotificationEnabledFromSettings()->Bool
    {
        let notificationType = UIApplication.shared.currentUserNotificationSettings?.types
        if notificationType?.rawValue == 0
        {
            return false
        } else {
            return true
        }
    }
    
    
}


