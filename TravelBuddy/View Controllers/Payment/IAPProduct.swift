//
//  IAPProduct.swift
//  TravelBuddy
//
//  Created by codeyeti on 17/08/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import Foundation

enum IAPProduct: String {
    case autoRenewable = "com.travelbuddy.codeyeti.Payment.AutoSubscriptionPayment"
}
