//
//  IAPService.swift
//  TravelBuddy
//
//  Created by codeyeti on 17/08/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import Foundation
import StoreKit

protocol CLYPriceDelegate : class
{
    func priceGetByRegion(price: String, localizeString: String)
}

class IAPService: NSObject {
    
    static let shared = IAPService()
    
    
    var products = [SKProduct]()
    let paymentQueue = SKPaymentQueue.default()
    
    private override init() { }
    weak var  priceDelegate: CLYPriceDelegate?

    func getProducts()
    {
        let products: Set = [IAPProduct.autoRenewable.rawValue]
        let request = SKProductsRequest(productIdentifiers: products)
        request.delegate = self
        request.start()
        paymentQueue.add(self)
    }
    
    func purchase(product: IAPProduct)
    {
        guard let productToPurchase = self.products.filter({ $0.productIdentifier == product.rawValue }).first else { return}
        let payment = SKPayment(product: productToPurchase)
        paymentQueue.add(payment)
    }
    
    func restoreSubscription()
    {
        paymentQueue.restoreCompletedTransactions()
    }
    //MARK:- Payment Api Call
    func inAppPurchaseApi()
    {
        var receiptData:NSData?
        do{
            receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
        }
        catch{
            print("ERROR: " + error.localizedDescription)
        }
        let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
        
        let params:[String: Any] = ["deviceType":"IPHONE","amount":"39.99","androidPurcahseToken":"","androidSubscriptionId":"","iosPaymentReceipt":"\(base64encodedReceipt!)"]
        let request = CYLServices.postByePlan(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.userPlanType = 1
                        userObject.wavesRemaining = responseData?["data"]?["user"]["wavesRemaining"].intValue
                        userObject.wavesMessage = responseData?["data"]?["user"]["wavesMessage"].stringValue
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        let gender = (userObject.gender == 0) ? "Male":"Female"
                        let cleverTapDetail:[String: String] = ["userName":userObject.firstName!,"UserId":"\(userObject.id!)","Isolocation":userObject.isoLocation!,"Gender": gender]
                        sentEventPropertyOnCleverTap("Premium User", _property: cleverTapDetail)
                        
                        NotificationCenter.default.post(name: NSNotification.Name("notify_handle_preselected_user_payment"), object: nil)
                        
                    }
                    else if responseData?["status"]?.stringValue == "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.userPlanType = 1
                        userObject.wavesRemaining = 20
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"User Payment"])
                        NotificationCenter.default.post(name: NSNotification.Name("notify_handle_preselected_user_payment"), object: nil)
                        
                    }
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"User Payment"])
                }
                
        },
                                                                  withError:
            {
                (error) in
                sentEventPropertyOnCleverTap("Backend error", _property:  ["view":"User Payment"])
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"User Payment"])
        })
    }
    
}

extension IAPService: SKProductsRequestDelegate
{
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        self.products = response.products
        let numberFormatter: NumberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = NumberFormatter.Behavior.behavior10_4
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = response.products[0].priceLocale
        let thisProduct: SKProduct = response.products[0]
        
        self.priceDelegate?.priceGetByRegion(price: numberFormatter.string(from: thisProduct.price)!, localizeString: thisProduct.localizedDescription)
    }
}

extension IAPService: SKPaymentTransactionObserver
{
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions
        {
            print(transaction.transactionState)
            print(transaction.transactionState.status(), transaction.payment.productIdentifier)
            
            switch transaction.transactionState
            {
                case .purchasing: break
                case .purchased: self.inAppPurchaseApi()
                    queue.finishTransaction(transaction)
                case .failed: queue.finishTransaction(transaction)
                default: queue.finishTransaction(transaction)
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue)
    {
        
    }
}

extension SKPaymentTransactionState
{
    func status() -> String
    {
        switch self {
        case .deferred: return "deferred"
        case .failed: return "failed"
        case .purchasing: return "purchasing"
        case .purchased: return "purchased"
        case .restored: return "restored"
        }
    }
}







