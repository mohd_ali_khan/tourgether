//
//  CYLUserPaymentVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 01/08/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

protocol CYLIAPReloadAppData {
    func reloadMessageReceiverListDelegatePayment()
}

class CYLUserPaymentVC: CYLBaseViewController,UIScrollViewDelegate,CLYPriceDelegate {
    
    @IBOutlet weak var pgControlPaymentMessage: UIPageControl!
    @IBOutlet weak var sVPayment: UIScrollView!
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var vwDiscount: UIView!
    @IBOutlet weak var imgDiscountBack: UIImageView!
    @IBOutlet weak var btnNotNow: UIButton!
    @IBOutlet weak var sVPayIntruction: UIScrollView!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var btnPrivacy: UIButton!
    var delegateMessageReload: CYLIAPReloadAppData!
    
    var messageLimitExceed: String = String()
    //Popup message
    @IBOutlet weak var vwPopUpFirst: UIView!
    @IBOutlet weak var imgPopUpFirst: UIImageView!
    @IBOutlet weak var lblPopUpFirstTitle: UILabel!
    @IBOutlet weak var lblPopUpFirstDis: UILabel!
    
    @IBOutlet weak var vwPopUpSecond: UIView!
    @IBOutlet weak var imgPopUpSecond: UIImageView!
    @IBOutlet weak var lblPopUpSecondTitle: UILabel!
    @IBOutlet weak var lblPopUpSecondDis: UILabel!
    
    @IBOutlet weak var vwPopUpThird: UIView!
    @IBOutlet weak var imgPopUpThird: UIImageView!
    @IBOutlet weak var lblPopUpThirdTitle: UILabel!
    @IBOutlet weak var lblPopUpThirdDis: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgDiscountBack.image = self.imgDiscountBack.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.fillPopupDesign()
        
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Payment")
        NotificationCenter.default.removeObserver(NSNotification.Name("notify_handle_preselected_user_payment"))
        NotificationCenter.default.addObserver(self, selector: #selector(self.notifyHandlePreselectedUserPayment), name: NSNotification.Name("notify_handle_preselected_user_payment"), object: nil)
        
        sVPayment.layer.cornerRadius = 3
        sVPayment.clipsToBounds = true
        KVNProgress.show()
        IAPService.shared.priceDelegate = self
        IAPService.shared.getProducts()
        self.navigationController?.navigationBar.isHidden = true
        
        let notNowStr = NSMutableAttributedString(string: (self.btnNotNow.titleLabel?.text)!)
        notNowStr.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: ((self.btnNotNow.titleLabel?.text)?.count)!))
        self.btnNotNow.setAttributedTitle(notNowStr, for: .normal)
        
        let termsStr = NSMutableAttributedString(string: (self.btnTerms.titleLabel?.text)!)
        termsStr.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: ((self.btnTerms.titleLabel?.text)?.count)!))
        self.btnTerms.setAttributedTitle(termsStr, for: .normal)
        
        let privacyStr = NSMutableAttributedString(string: (self.btnPrivacy.titleLabel?.text)!)
        privacyStr.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: ((self.btnPrivacy.titleLabel?.text)?.count)!))
        self.btnPrivacy.setAttributedTitle(privacyStr, for: .normal)
        
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.sVPayIntruction.contentSize = CGSize(width: screenWidth!-30, height: 110)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- fill the popup Design
    func fillPopupDesign()
    {
        if messageLimitExceed == "message"
        {
            self.imgDiscountBack.tintColor = UIColor(red: 90/255.0, green: 239/255.0, blue: 94/255.0, alpha: 1.0)
            
            self.vwPopUpFirst.backgroundColor = UIColor(red: 90/255.0, green: 239/255.0, blue: 94/255.0, alpha: 1.0)
            self.imgPopUpFirst.image = UIImage(named: "pay_message")
            self.lblPopUpFirstTitle.text = "Increase message limit!"
            self.lblPopUpFirstDis.text = "Your can make up to 20 introductions per day"
            
            self.vwPopUpSecond.backgroundColor = UIColor(red: 227/255.0, green: 175/255.0, blue: 49/255.0, alpha: 1.0)
            self.imgPopUpSecond.image = UIImage(named: "pay_hand")
            self.lblPopUpSecondTitle.text = "Increase wave limit!"
            self.lblPopUpSecondDis.text = "Your can wave up to 50 new users per day"
            
            self.vwPopUpThird.backgroundColor = UIColor(red: 120/255.0, green: 49/255.0, blue: 228/255.0, alpha: 1.0)
            self.imgPopUpThird.image = UIImage(named: "pay_mask")
            self.lblPopUpThirdTitle.text = "See who waved!"
            self.lblPopUpThirdDis.text = "Unmask all your waves. No more secrets!"
        }
        else if messageLimitExceed == "receiver"
        {
            self.imgDiscountBack.tintColor = UIColor(red: 120/255.0, green: 49/255.0, blue: 228/255.0, alpha: 1.0)
            
            self.vwPopUpFirst.backgroundColor = UIColor(red: 120/255.0, green: 49/255.0, blue: 228/255.0, alpha: 1.0)
            self.imgPopUpFirst.image = UIImage(named: "pay_mask")
            self.lblPopUpFirstTitle.text = "See who waved!"
            self.lblPopUpFirstDis.text = "Unmask all your waves. No more secrets!"
            
            self.vwPopUpSecond.backgroundColor = UIColor(red: 227/255.0, green: 175/255.0, blue: 49/255.0, alpha: 1.0)
            self.imgPopUpSecond.image = UIImage(named: "pay_hand")
            self.lblPopUpSecondTitle.text = "Increase wave limit!"
            self.lblPopUpSecondDis.text = "Your can wave up to 50 new users per day"
            
            self.vwPopUpThird.backgroundColor = UIColor(red: 90/255.0, green: 239/255.0, blue: 94/255.0, alpha: 1.0)
            self.imgPopUpThird.image = UIImage(named: "pay_message")
            self.lblPopUpThirdTitle.text = "Increase message limit!"
            self.lblPopUpThirdDis.text = "Your can make up to 20 introductions per day"
        }
        else
        {
            self.imgDiscountBack.tintColor = UIColor(red: 227/255.0, green: 175/255.0, blue: 49/255.0, alpha: 1.0)
            
            self.vwPopUpFirst.backgroundColor = UIColor(red: 227/255.0, green: 175/255.0, blue: 49/255.0, alpha: 1.0)
            self.imgPopUpFirst.image = UIImage(named: "pay_hand")
            self.lblPopUpFirstTitle.text = "Increase wave limit!"
            self.lblPopUpFirstDis.text = "Your can wave up to 50 new users per day"
            
            self.vwPopUpSecond.backgroundColor = UIColor(red: 90/255.0, green: 239/255.0, blue: 94/255.0, alpha: 1.0)
            self.imgPopUpSecond.image = UIImage(named: "pay_message")
            self.lblPopUpSecondTitle.text = "Increase message limit!"
            self.lblPopUpSecondDis.text = "Your can make up to 20 introductions per day"
            
            self.vwPopUpThird.backgroundColor = UIColor(red: 120/255.0, green: 49/255.0, blue: 228/255.0, alpha: 1.0)
            self.imgPopUpThird.image = UIImage(named: "pay_mask")
            self.lblPopUpThirdTitle.text = "See who waved!"
            self.lblPopUpThirdDis.text = "Unmask all your waves. No more secrets!"
            
        }
    }
    //MARK:- Payment button Action
    @IBAction func btnToUserPaymentAction(_ sender: UIButton)
    {
        IAPService.shared.purchase(product: .autoRenewable)
    }
    @IBAction func btnDismissPaymentPopUp(_ sender: UIButton) {
        
//        IAPService.shared.restoreSubscription()
        self.navigationController?.popViewController(animated: false)
    }
    @objc func notifyHandlePreselectedUserPayment(notyfn:Notification)
    {
        self.delegateMessageReload.reloadMessageReceiverListDelegatePayment()
        self.navigationController?.popViewController(animated: false)
    }
    //MARK: Scroll view delegate functions
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
            let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
            self.pgControlPaymentMessage.currentPage = Int(pageNumber)
        
        if pageNumber == 0.0
        {
            if messageLimitExceed == "message"
            {
                self.imgDiscountBack.tintColor = UIColor(red: 90/255.0, green: 239/255.0, blue: 94/255.0, alpha: 1.0)
            }
            else if messageLimitExceed == "receiver"
            {
                self.imgDiscountBack.tintColor = UIColor(red: 120/255.0, green: 49/255.0, blue: 228/255.0, alpha: 1.0)
            }
            else
            {
                self.imgDiscountBack.tintColor = UIColor(red: 227/255.0, green: 175/255.0, blue: 49/255.0, alpha: 1.0)
            }
        }
        else if pageNumber == 1.0
        {
            if messageLimitExceed == "message"
            {
                self.imgDiscountBack.tintColor = UIColor(red: 227/255.0, green: 175/255.0, blue: 49/255.0, alpha: 1.0)
            }
            else if messageLimitExceed == "receiver"
            {
                self.imgDiscountBack.tintColor = UIColor(red: 227/255.0, green: 175/255.0, blue: 49/255.0, alpha: 1.0)
            }
            else
            {
                self.imgDiscountBack.tintColor = UIColor(red: 90/255.0, green: 239/255.0, blue: 94/255.0, alpha: 1.0)
            }
        }
        else if pageNumber == 2.0
        {
            if messageLimitExceed == "message"
            {
                self.imgDiscountBack.tintColor = UIColor(red: 120/255.0, green: 49/255.0, blue: 228/255.0, alpha: 1.0)
            }
            else if messageLimitExceed == "receiver"
            {
                self.imgDiscountBack.tintColor = UIColor(red: 90/255.0, green: 239/255.0, blue: 94/255.0, alpha: 1.0)
            }
            else
            {
                self.imgDiscountBack.tintColor = UIColor(red: 120/255.0, green: 49/255.0, blue: 228/255.0, alpha: 1.0)
            }
        }
        else if pageNumber == 3.0
        {
            self.imgDiscountBack.tintColor = UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
        }
    }
    //MARK:- Delegate Get price According to country
    func priceGetByRegion(price: String, localizeString: String) {
        KVNProgress.dismiss()
        self.vwDiscount.isHidden = false
        self.btnPay.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
        self.btnPay.setTitle("\(localizeString) \(price)/month", for: .normal)
    }
    @IBAction func btnTermsAndPrivacy(_ sender: UIButton) {
        
        if sender.tag == 102
        {
            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLTermsCondition") as! CYLTermsCondition
            termsVC.titleStr = "Privacy Policy"
            termsVC.urlStr = "https://www.tourgetherapp.com/privacy-policy.html"
            self.navigationController?.present(termsVC, animated: true, completion: nil)
        }
        else
        {
            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLTermsCondition") as! CYLTermsCondition
            termsVC.urlStr = "https://www.tourgetherapp.com/terms-services.html"
            termsVC.titleStr = "Terms of Services"
            self.navigationController?.present(termsVC, animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
