//
//  CYLDestinationScreenVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/5/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
import CoreLocation

class CYLDestinationScreenVC: CYLBaseViewController,UITableViewDelegate,UITableViewDataSource,CYLSearchDelegate,CLLocationManagerDelegate {
    

    @IBOutlet weak var scrlDestination: UIScrollView!
    @IBOutlet weak var tblCountry: UITableView!
    @IBOutlet weak var imgCitiesBack: UIImageView!
    @IBOutlet weak var constImageCities: NSLayoutConstraint!
    @IBOutlet weak var constImageCountry: NSLayoutConstraint!
    @IBOutlet weak var lblSelectedSegmentText: UILabel!
    @IBOutlet weak var imgCountryBack: UIImageView!
    @IBOutlet weak var constBtnContinue: NSLayoutConstraint!
    @IBOutlet weak var tblCity: UITableView!
    
    @IBOutlet weak var lblFemaleIcon: UILabel!
    @IBOutlet weak var imgFemaleIcon: UIImageView!
    @IBOutlet weak var lblMaleText: UILabel!
    @IBOutlet weak var imgMaleIcon: UIImageView!
    @IBOutlet weak var btnToMale: UIButton!
    @IBOutlet weak var btntoFemale: UIButton!
    @IBOutlet weak var vwGenderUpdate: UIView!
    
    var aryCountry=[CYLLocationObject]()
    var aryCity=[CYLLocationObject]()
    var arySelectedCity=[String]()
    var arySelectedCountry=[String]()
    var isApiRunning = false
    var checkSelectedSegmentDestination: Int = Int()
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: View Controller Life Cycle
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.handleCountrySelection()
        self.createEmptyCityCountryTableHeaderView(tblView: self.tblCountry)
        self.createEmptyCityCountryTableHeaderView(tblView: self.tblCity)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.clear], for: .normal)
        createDestinationScreenDesign()
        
        if CYLGlobals.sharedInstance.usrObject?.gender == 2 || CYLGlobals.sharedInstance.usrObject?.gender == 3
        {
            self.vwGenderUpdate.isHidden = false
        } 
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func handleCitiesSelection()
    {
        self.checkSelectedSegmentDestination = 0
        imgCitiesBack.image = UIImage(named: "Rectangle")
        constImageCities.constant = 15
        imgCountryBack.image = UIImage(named: "uncheck_btn")
        constImageCountry.constant = 0
        self.lblSelectedSegmentText.text = "For my next trip, I wanna travel to any of the following cities"
        if aryCity.count <= 0
        {
            self.constBtnContinue.constant = 0
        }
        else
        {
            self.constBtnContinue.constant = 48
        }
        
    }
    func handleCountrySelection()
    {
        self.checkSelectedSegmentDestination = 1
        imgCitiesBack.image = UIImage(named: "uncheck_btn")
        constImageCities.constant = 0
        imgCountryBack.image = UIImage(named: "Rectangle")
        constImageCountry.constant = 15
        self.lblSelectedSegmentText.text = "For my next trip, I wanna travel anywhere within the following countries"
        if aryCountry.count <= 0
        {
            self.constBtnContinue.constant = 0
        }
        else
        {
            self.constBtnContinue.constant = 48
        }
        
    }
    func createDestinationScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Travel Destination"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.tblCountry.tableFooterView=UIView()
        self.tblCity.tableFooterView=UIView()
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "New user destination")
    }
    func createEmptyCityCountryTableHeaderView(tblView: UITableView)
    {
        let hederView: UIView = UIView()
        let imgView:UIImageView = UIImageView()
        if screenHeight == 480 || screenHeight == 568
        {
            hederView.frame = CGRect(x:0,y:0,width:self.screenWidth!,height:180)
            imgView.frame = CGRect(x: 80, y: 40, width: self.screenWidth!-160, height: 100)
            imgView.image = UIImage(named: "empty_state.png")
            hederView.addSubview(imgView)
        }
        else
        {
            hederView.frame = CGRect(x:0,y:0,width:self.screenWidth!,height:280)
            imgView.frame = CGRect(x: 40, y: 60, width: self.screenWidth!-80, height: 200)
            
        }
        imgView.image = UIImage(named: "empty_state.png")
        hederView.addSubview(imgView)
        let lblTitle = UILabel.init(frame:CGRect(x:0,y:imgView.frame.origin.y+imgView.frame.size.height+10,width:self.screenWidth!,height:30))
        lblTitle.text = "No Destination Yet"
        lblTitle.textColor = UIColor(red: 178/255, green: 192/255, blue: 198/255, alpha: 1)
        lblTitle.font = UIFont(name: "Comfortaa-Bold", size: 18)
        lblTitle.textAlignment = .center
        hederView.addSubview(lblTitle)
        
        tblView.tableHeaderView = hederView
    }
    func covertJSONArrayIntoObjectArray(aryInput:[JSON])->[CYLLocationObject]
    {
        var aryOutput = [CYLLocationObject]()
        for dictionary in aryInput
        {
            let object = CYLLocationObject.init(json: dictionary)
            aryOutput.append(object)
        }
        return aryOutput
    }
    
    
    
    func doConfirmationForDestinationDelete(indxx:Int)
    {
        let text = showAlertTitleMessageFont(strTitle: "Confirm", strMessage: "Delete this destination?")
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        alert.setValue(text.title, forKey: "attributedTitle")
        alert.setValue(text.message, forKey: "attributedMessage")
        let yesAction = UIAlertAction.init(title: "Delete", style: .destructive, handler: {
            (sender) in
            if indxx/1000 == 2
            {
                self.deleteCityAtIndex(indxx: indxx-2000)
            }
            else
            {
                self.deleteCountryAtIndex(indxx: indxx-3000)
            }
        })
        let noAction = UIAlertAction.init(title: "Cancel", style: .default, handler:
        {
            (sender) in
            
        })
        alert.addAction(noAction)
        alert.addAction(yesAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    func deleteCityAtIndex(indxx:Int)
    {
        self.arySelectedCity.remove(at: indxx)
        self.aryCity.remove(at: indxx)
        if self.aryCity.count == 0
        {
            self.createEmptyCityCountryTableHeaderView(tblView: self.tblCity)
            self.constBtnContinue.constant = 0
        }
        self.tblCity.reloadData()
        // self.constCityTableH.constant = CGFloat(self.aryCity.count) * 40
    }
    func deleteCountryAtIndex(indxx:Int)
    {
        self.arySelectedCountry.remove(at: indxx)
        self.aryCountry.remove(at: indxx)
        if self.aryCountry.count == 0
        {
            self.createEmptyCityCountryTableHeaderView(tblView: self.tblCountry)
            self.constBtnContinue.constant = 0
        }
        
        self.tblCountry.reloadData()
        // self.constCountryTableH.constant = CGFloat(self.aryCountry.count) * 40
    }
    
    
//    func getUserLocationFromDeviceLocale()
//    {
//        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
//        {
//            //print(countryCode)
//            self.isoCountryCode = countryCode
//        }
//        self.currentLocation = self.countryName(countryCode: self.isoCountryCode)!
//        self.requestToSubmitDestinationData()
//
//    }
//    func countryName(countryCode: String) -> String?
//    {
//        let current = Locale(identifier: "en_US")
//        return current.localizedString(forRegionCode: countryCode) ?? ""
//    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom Button Actions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToDeleteCountryClicked(_ sender: UIButton)
    {
        self.doConfirmationForDestinationDelete(indxx: sender.tag)
    }
    @IBAction func btnToDeleteCityClicked(_ sender: UIButton)
    {
        self.doConfirmationForDestinationDelete(indxx: sender.tag)
    }
    @IBAction func btnToContinueClicked(_ sender: Any)
    {
        if self.aryCountry.count == 0 && self.checkSelectedSegmentDestination == 1
        {
            showAlertView(title: "", message: "Please select atleast one country or choose a different destination option", ref: self)
        }
        else if self.aryCity.count == 0 && self.checkSelectedSegmentDestination == 0
        {
            showAlertView(title: "", message: "Please select atleast one city or choose a different destination option", ref: self)
        }
        else
        {
            self.requestToSubmitDestinationData()
        }
    }
    @IBAction func btnToAddCityClicked(_ sender: Any)
    {
        if self.aryCity.count == 5
        {
            showAlertView(title: "Alert!", message: "You have reached maximum limit of adding destination.", ref: self)
        }
        else
        {
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLCountrySearchVC") as! CYLCountrySearchVC
            searchVC.title = "City"
            searchVC.isCountrySearch=false
            searchVC.delegate=self
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
    }
    @IBAction func btnToAddCountryClicked(_ sender: Any)
    {
        if self.aryCountry.count == 5
        {
            showAlertView(title: "Alert!", message: "You have reached maximum limit of adding destination.", ref: self)
        }
        else
        {
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLCountrySearchVC") as! CYLCountrySearchVC
            searchVC.title = "Country"
            searchVC.isCountrySearch=true
            searchVC.delegate=self
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
    }
    func delegateToGetSelectedCityCountry(object: JSON)
    {
        if object["cityName"] != ""
        {
            //handle city selection here
            if !self.arySelectedCity.contains(object["id"].stringValue)
            {
                self.arySelectedCity.append(object["id"].stringValue)
                self.aryCity.append(CYLLocationObject.init(json: object))
                self.tblCity.tableHeaderView = nil
                self.constBtnContinue.constant = 48
                self.tblCity.reloadData()
            }
        }
        else
        {
            //handle country selection here
            if !self.arySelectedCountry.contains(object["id"].stringValue)
            {
                self.arySelectedCountry.append(object["id"].stringValue)
                self.aryCountry.append(CYLLocationObject.init(json: object))
                self.tblCountry.tableHeaderView = nil
                self.constBtnContinue.constant = 48
                self.tblCountry.reloadData()
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Table view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (tableView.tag == 401) ? self.aryCountry.count : self.aryCity.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView.tag == 401
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CYLCountryListCell") as! CYLCountryListCell
            cell.object = self.aryCountry[indexPath.row]
            cell.btnToDeleteCountry.tag = 3000 + indexPath.row
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CYLCityListCell") as! CYLCityListCell
            cell.object = self.aryCity[indexPath.row]
            cell.btnToDelete.tag = 2000 + indexPath.row
            cell.selectionStyle = .none
            return cell
            
        }
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToSubmitDestinationData()
    {
            self.isApiRunning=true
            KVNProgress.show()
            let countries = (self.arySelectedCountry.count == 0 || self.checkSelectedSegmentDestination != 1) ? "" : self.arySelectedCountry.joined(separator: ",")
            let cities = (self.arySelectedCity.count == 0 || self.checkSelectedSegmentDestination != 0) ? "" : self.arySelectedCity.joined(separator: ",")
            
            var type: String = String()
            var count: Int = Int()
            if self.arySelectedCity.count != 0 && self.checkSelectedSegmentDestination == 0
            {
                type = "Fixed"
                count = self.arySelectedCity.count
            }
            else if self.arySelectedCountry.count != 0 && self.checkSelectedSegmentDestination == 1
            {
                type = "Flexible"
                count = self.arySelectedCountry.count
            }
            sentEventPropertyOnCleverTap("Destination preferences", _property: ["Flexibility":type,"DestinationCount":count])
            let params:[String: Any] = ["countries":countries,"cities":cities,"country":"","isoLocation":""]
            let request = CYLServices.postDestinationList(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
            CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
                { (response) in
                    KVNProgress.dismiss()
                    if (response?.isValid)!
                    {
//                        self.isApiRunning=false
                        let responseData = response?.object?.dictionaryValue
                        if responseData?["status"]?.stringValue != "fail"
                        {
                            let userObject = CYLGlobals.sharedInstance.usrObject!
                            userObject.progress = 1
                            if self.checkSelectedSegmentDestination == 0
                            {
                                userObject.cities = self.aryCity
                                userObject.countries = [CYLLocationObject]()
                            }
                            else if self.checkSelectedSegmentDestination == 1
                            {
                                userObject.countries = self.aryCountry
                                userObject.cities = [CYLLocationObject]()
                            }
                            //userObject.countries = self.aryCountry
                            //userObject.cities = self.aryCity
//                            userObject.city = self.currentLocation
//                            userObject.isoLocation = self.isoCountryCode
                            CYLGlobals.sharedInstance.usrObject = userObject
                            let userDeflts = UserDefaults.standard
                            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                            userDeflts.set(encodedData, forKey: "userObject")
                            userDeflts.synchronize()
                            
                            let calendarVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLCalendarScreenVC")
                            self.navigationController?.pushViewController(calendarVC!, animated: true)
                        }
                        
                    }
                    else
                    {
                        self.isApiRunning=false
                        sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"New user destination"])
                        showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    }
                    
            },
                                                                      withError:
                {
                    (error) in
                    self.isApiRunning=false
                    KVNProgress.dismiss()
                    sentEventPropertyOnCleverTap("Backend error", _property: ["view":"New user destination"])
                    showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
            },
                                                                      andNetworkErr:
                {
                    (networkFailure) in
                    self.isApiRunning=false
                    sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"New user destination"])
                    KVNProgress.dismiss()
                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
            })
    }
    @IBAction func btnCitiesCountryAction(_ sender: UIButton)
    {
        if sender.tag == 0
        {
            self.handleCitiesSelection()
            self.scrlDestination.scrollRectToVisible(CGRect(x:self.scrlDestination.frame.size.width,y:0,width:self.scrlDestination.frame.size.width,height:self.scrlDestination.frame.size.height), animated: false)
        }
        else
        {
            self.handleCountrySelection()
            self.scrlDestination.scrollRectToVisible(CGRect(x:0,y:0,width:self.scrlDestination.frame.size.width,height:self.scrlDestination.frame.size.height), animated: false)
        }
    }
    
    //MARK:- Gender Update
    @IBAction func btnToMaleClicked(_ sender: Any)
    {
        self.showSelectionOnMaleTab()
        
    }
    
    @IBAction func btnToFemaleClicked(_ sender: Any)
    {
        self.showSelectionOnFemaleTab()
    }
    func showSelectionOnMaleTab()
    {
        self.btnToMale.isSelected = true
        self.imgMaleIcon.image = #imageLiteral(resourceName: "ic_male_act")
        self.lblMaleText.textColor = UIColor.white
        self.btnToMale.superview?.backgroundColor = themeColorBlue
        
        self.btntoFemale.isSelected = false
        self.imgFemaleIcon.image = #imageLiteral(resourceName: "ic_female_inact")
        self.lblFemaleIcon.textColor = UIColor(red: 208/255.0, green: 215/255.0, blue: 217/255.0, alpha: 1.0)
        self.btntoFemale.superview?.backgroundColor = UIColor.white
    }
    func showSelectionOnFemaleTab()
    {
        self.btntoFemale.isSelected = true
        self.imgFemaleIcon.image = #imageLiteral(resourceName: "ic_female_act")
        self.lblFemaleIcon.textColor = UIColor.white
        self.btntoFemale.superview?.backgroundColor = themeColorBlue
        
        self.btnToMale.isSelected = false
        self.imgMaleIcon.image = #imageLiteral(resourceName: "ic_male_inact")
        self.lblMaleText.textColor = UIColor(red: 208/255.0, green: 215/255.0, blue: 217/255.0, alpha: 1.0)
        self.btnToMale.superview?.backgroundColor = UIColor.white
    }
    @IBAction func btnSaveGender(_ sender: UIButton)
    {
        if self.btnToMale.isSelected == true || self.btntoFemale.isSelected == true
        {
            KVNProgress.show()
            let gender = (self.btnToMale.isSelected == true) ? 0:1
            let params:[String: Any] = ["gender":gender]
            let request = CYLServices.postGenderUpdate(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
            CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
                { (response) in
                    KVNProgress.dismiss()
                    if (response?.isValid)!
                    {
                        let responseData = response?.object?.dictionaryValue
                        if responseData?["status"]?.stringValue != "fail"
                        {
                            self.vwGenderUpdate.isHidden = true
                            let userObject = CYLGlobals.sharedInstance.usrObject!
                            userObject.gender = gender
                        }
                        
                    }
                    else
                    {
                        self.isApiRunning=false
                        sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"New user destination"])
                        showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    }
                    
            },
                                                                      withError:
                {
                    (error) in
                    self.isApiRunning=false
                    KVNProgress.dismiss()
                    sentEventPropertyOnCleverTap("Backend error", _property: ["view":"New user destination"])
                    showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
            },
                                                                      andNetworkErr:
                {
                    (networkFailure) in
                    self.isApiRunning=false
                    sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"New user destination"])
                    KVNProgress.dismiss()
                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
            })
        }
        else
        {
            showAlertView(title: "", message: "Please select gender.", ref: self)
        }
        
        
    }
    
}

