//
//  CYLPageViewSignInController.swift
//  TravelBuddy
//
//  Created by codeyeti on 06/07/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLPageViewSignInController: UIPageViewController, UIPageViewControllerDataSource {

    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "CYLFirstAndLastNameVC"),
                self.newVc(viewController: "CYLEmailOTPVC"),self.newVc(viewController: "CYLPasswordVC"),self.newVc(viewController: "CYLSelectGenderVC"),self.newVc(viewController: "CYLSelectDOBVC"),self.newVc(viewController: "CYLUserImageSellectionLoginVC"),self.newVc(viewController: "CYLCurrentLocationVC")]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        configurePageControl()
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
        NotificationCenter.default.removeObserver(NSNotification.Name("notify_handle_preselected_segment_forward"))
        NotificationCenter.default.addObserver(self, selector: #selector(self.notifyHandlePreselectedForward), name: NSNotification.Name("notify_handle_preselected_segment_forward"), object: nil)
        NotificationCenter.default.removeObserver(NSNotification.Name("notify_handle_preselected_segment_reverse"))
        NotificationCenter.default.addObserver(self, selector: #selector(self.notifyHandlePreselectedReverse), name: NSNotification.Name("notify_handle_preselected_segment_reverse"), object: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configurePageControl()
    {
//        let imgBG: UIImageView = UIImageView()
//        
//        if self.view.frame.size.height == 480
//        {
//            imgBG.frame = CGRect(x: 0, y: self.view.frame.size.height-70, width: self.view.frame.size.width, height: 0)
//        }
//        else
//        {
//            imgBG.frame = CGRect(x: 0, y: self.view.frame.size.height-100, width: self.view.frame.size.width, height: 100)
//        }
//        
//        imgBG.image = UIImage(named: "ic_building")
//        imgBG.tintColor = UIColor.red
//        self.view.addSubview(imgBG)
    }
    @objc func notifyHandlePreselectedForward(notyfn:Notification)
    {
        let notifIndex = notyfn.userInfo?["index"] ?? 0
        let initialContenViewController = self.orderedViewControllers[notifIndex as! Int]
        self.setViewControllers([initialContenViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
    }
    @objc func notifyHandlePreselectedReverse(notyfn:Notification)
    {
        let notifIndex = notyfn.userInfo?["index"] ?? 0
        let initialContenViewController = self.orderedViewControllers[notifIndex as! Int]
        self.setViewControllers([initialContenViewController], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
    }
    
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    // MARK: Data source functions.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        //         User is on the first view controller and swiped left to loop to
        //         the last view controller.
        guard previousIndex >= 0
            else
        {
            self.view.backgroundColor = UIColor(red: 0/255.0, green: 194/255.0, blue: 147/255.0, alpha: 1.0)
            return nil//orderedViewControllers.last
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            // return nil
        }
        
        guard orderedViewControllers.count > previousIndex
            else
        {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        //         User is on the last view controller and swiped right to loop to
        //         the first view controller.
        guard orderedViewControllersCount != nextIndex
            else
        {
            self.view.backgroundColor = UIColor(red: 227/255.0, green: 0/255.0, blue: 69/255.0, alpha: 1.0)
            return nil//orderedViewControllers.first
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            // return nil
        }
        
        guard orderedViewControllersCount > nextIndex
            else
        {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}
