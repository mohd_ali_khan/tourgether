//
//  CYLCurrentLocationVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 27/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class CYLCurrentLocationVC: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var txtCountry: TextField!
    @IBOutlet weak var imgGPS: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnAllowLocation: UIButton!
    
    let locationManager = CLLocationManager()
    var appObject:AppDelegate?
    var userImageStr: String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var isoCountryCode = ""
        var currentLocation = ""
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
        {
            isoCountryCode = countryCode
            let current = Locale(identifier: "en_US")
            currentLocation = current.localizedString(forRegionCode: countryCode) ?? ""
        }
        
        self.appObject = UIApplication.shared.delegate as? AppDelegate
        self.appObject?.userSignUpDetail!["country"] = currentLocation
        self.appObject?.userSignUpDetail!["city"] = ""
        self.appObject?.userSignUpDetail!["latitude"] = ""
        self.appObject?.userSignUpDetail!["longitude"] = ""
        self.appObject?.userSignUpDetail!["isoLocation"] = isoCountryCode
        self.createScreenDesign()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=true
        self.btnBack.setImage(UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnBack.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
    }
    @IBAction func btnToBackClicked(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_reverse"), object: nil, userInfo: ["index":5])
    }
    
    @objc func btnToBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    @IBAction func btnUpdateLocation(_ sender: UIButton)
    {
        
        if CLLocationManager.locationServicesEnabled()
        {
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways
            {
                self.startRotation()
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.delegate = self
                locationManager.startUpdatingLocation()
            }
            else if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted
            {
                let text = showAlertTitleMessageFont(strTitle: "Update your current location", strMessage: "To enable location, please go to Settings -> Tourgether -> Location and choose the option - While using the app")
                let alertComtroller = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                alertComtroller.setValue(text.title, forKey: "attributedTitle")
                alertComtroller.setValue(text.message, forKey: "attributedMessage")
                let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel, handler:
                { action -> Void in
                    self.dismiss(animated: true, completion: nil)
                })
                alertComtroller.addAction(cancelAction)
                
                self.present(alertComtroller, animated: true, completion: nil)
            }
            else if CLLocationManager.authorizationStatus() == .notDetermined
            {
                self.startRotation()
                self.locationManager.requestWhenInUseAuthorization()
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.delegate = self
                locationManager.startUpdatingLocation()
            }
        }
        else
        {
            let text = showAlertTitleMessageFont(strTitle: "Update your current location", strMessage: "To enable location, please go to Settings -> Tourgether -> Location and choose the option - While using the app")
            let alertComtroller = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            alertComtroller.setValue(text.title, forKey: "attributedTitle")
            alertComtroller.setValue(text.message, forKey: "attributedMessage")
            let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel, handler:
            { action -> Void in
                self.dismiss(animated: true, completion: nil)
            })
            alertComtroller.addAction(cancelAction)
            
            self.present(alertComtroller, animated: true, completion: nil)
        }
    }
    func startRotation() {
        self.imgGPS.image = UIImage(named: "refresh_spineer.png")
        let kAnimationKey = "rotation"
        self.btnAllowLocation.isUserInteractionEnabled = false
        if self.imgGPS.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = 2.0
            animate.repeatCount = Float.infinity
            animate.fromValue = 0.0
            animate.toValue = Float(CGFloat.pi * 2.0)
            self.imgGPS.layer.add(animate, forKey: kAnimationKey)
        }
    }
    func endRotation()
    {
        self.btnAllowLocation.isUserInteractionEnabled = true
        let kAnimationKey = "rotation"
        if self.imgGPS.layer.animation(forKey: kAnimationKey) != nil {
            self.imgGPS.layer.removeAnimation(forKey: kAnimationKey)
            self.imgGPS.image = UIImage(named: "ic_gps.png")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied || status == CLAuthorizationStatus.restricted)
        {
            // The user denied authorization
            self.endRotation()
        } else if (status == CLAuthorizationStatus.authorizedWhenInUse)
        {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        manager.stopUpdatingLocation()
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        if locations.count == 1
        {
            CleverTap.setLocation(locValue)
            
            // Add below code to get address for touch coordinates.
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler:
                { (placemarks, error) -> Void in
                    // Place details
                    var placeMark: CLPlacemark!
                    placeMark = placemarks?[0]
                    var city:String = String()
                    var country: String = String()
                    var isoLocation: String = String()
                    if (placeMark != nil)
                    {
                        city = placeMark.addressDictionary!["City"] as? String ?? ""
                        country = placeMark.addressDictionary!["Country"] as? String ?? ""
                        self.txtCountry.text = "\(country), \(city)"
                        self.endRotation()
                        isoLocation = placeMark.isoCountryCode ?? ""
                        
                        self.appObject?.userSignUpDetail!["country"] = country
                        self.appObject?.userSignUpDetail!["city"] = city
                        self.appObject?.userSignUpDetail!["latitude"] = "\(locValue.latitude)"
                        self.appObject?.userSignUpDetail!["longitude"] = "\(locValue.longitude)"
                        self.appObject?.userSignUpDetail!["isoLocation"] = isoLocation
                        
                    }
            })
            
        }
        
    }
    @IBAction func btnNextActoin(_ sender: UIButton)
    {
        KVNProgress.show()
        
        var aryImgUrl:[String] = self.appObject?.userSignUpDetail!["userImage"] as! [String]
        if aryImgUrl.count != 0
        {
            if aryImgUrl.count == 1
            {
                self.appObject?.userSignUpDetail!["imgUrl"] = aryImgUrl[0]
            }
            else
            {
                self.appObject?.userSignUpDetail!["imgUrl"] = aryImgUrl[1]
            }
            self.userImageStr = aryImgUrl.joined(separator: ",")
        }
        self.appObject?.userSignUpDetail?.removeValue(forKey: "userImage")
        
        print(self.appObject?.userSignUpDetail! ?? "")
        
        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
        let version = nsObject as! String
        
        var retrievedUUID: String? = KeychainWrapper.standard.string(forKey: "iPhoneUUID")
        if retrievedUUID == nil
        {
            KeychainWrapper.standard.set(UIDevice.current.identifierForVendor!.uuidString, forKey: "iPhoneUUID")
            retrievedUUID = UIDevice.current.identifierForVendor!.uuidString
        }
        self.appObject?.userSignUpDetail!["appVersion"] = version
        self.appObject?.userSignUpDetail!["deviceType"] = "IPHONE"
        self.appObject?.userSignUpDetail!["uniqueDeviceId"] = retrievedUUID ?? ""
        
        let request = CYLServices.postUserLoginFromEmail(dict:(self.appObject?.userSignUpDetail)!)
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLUserObject.init(json: (responseData?["data"])!)
                        userObject.newMessageNotify = responseData?["data"]?["newMessageNotify"].intValue
                        userObject.email = self.appObject?.userSignUpDetail!["email"] as? String
                        userObject.isMatchNotification = responseData?["data"]?["matchNotification"].intValue
                        userObject.isEmailNotification = responseData?["data"]?["subscribe"].intValue
                        userObject.isOtherNotifications = responseData?["data"]?["isOtherNotifications"].intValue
                        userObject.userPlanType = (responseData?["data"]?["paymentObj"]["planType"].int != nil) ? responseData?["data"]?["paymentObj"]["planType"].int : 0
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.set(Date(), forKey: "date")
                        userDeflts.set(0, forKey: "rating_count")
                        userDeflts.set(0, forKey: "messageWave_count")
                        if let dictFilter = responseData?["user_filter"]?.dictionaryObject
                        {
                            userDeflts.setValue(dictFilter, forKey: "travel_filter")
                            CYLGlobals.sharedInstance.isFilterApplied=true
                        }
                        else
                        {
                            CYLGlobals.sharedInstance.isFilterApplied=false
                        }

                        userDeflts.synchronize()

                        var strProfileImg = String()
                        if self.userImageStr != ""
                        {
                            strProfileImg = aryImgUrl[0]
                            self.requestToUpdateFacebookImages(strUrlAry: self.userImageStr)
                        }
                        let g = (CYLGlobals.sharedInstance.usrObject!.gender! == 0) ? "M" : "F"
                        var localTimeZoneName: String { return TimeZone.current.identifier }
                        var firstName = String()
                        if let _ = CYLGlobals.sharedInstance.usrObject!.firstName
                        {
                            firstName =  (CYLGlobals.sharedInstance.usrObject!.firstName == "") ? CYLGlobals.sharedInstance.usrObject!.userName!: CYLGlobals.sharedInstance.usrObject!.firstName!
                        }
                        else
                        {
                            firstName = CYLGlobals.sharedInstance.usrObject!.userName!
                        }
                        let profile: Dictionary<String, Any> = [
                            "Name": "\(firstName)",
                            "Identity": CYLGlobals.sharedInstance.usrObject!.id!,
                            "Email": "\(CYLGlobals.sharedInstance.usrObject!.email!)",
                            "Phone": "",
                            "Gender": "\(g)",
                            "Employed": "",
                            "Education": "",
                            "Married": "",
                            "DOB": (CYLGlobals.sharedInstance.usrObject?.dateOfBirth==nil) ? Date() : convertTimestampIntoDateString(date: Double((CYLGlobals.sharedInstance.usrObject?.dateOfBirth)!), format: "dd/MM/yyyy"),
                            "Age": 0,
                            "Tz":"\(localTimeZoneName)",
                            "Photo": "\(strProfileImg)",
                            "MSG-email": true,
                            "MSG-push": true,
                            "MSG-sms": false
                        ]
                        var locationObject: CLLocationCoordinate2D = CLLocationCoordinate2D()
                        if let latitude = responseData?["data"]?["latitude"].doubleValue
                        {
                            locationObject.latitude = latitude
                        }
                        if let longitude = responseData?["data"]?["longitude"].doubleValue
                        {
                            locationObject.longitude = longitude
                        }
                        CleverTap.setLocation(locationObject)
                        CleverTap.sharedInstance()?.profileAddMultiValue("\(CYLGlobals.sharedInstance.usrObject!.isoLocation!)", forKey: "Other Details")
                        CleverTap.sharedInstance()?.profilePush(profile)

                        let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLDestinationScreenVC")
                        self.navigationController?.pushViewController(destinationVC!, animated: true)

                    }
                    else if responseData?["status"]?.stringValue == "fail"
                    {
                        showAlertView(title: "", message: (responseData?["reason"]?.stringValue)!, ref: self)
                    }

                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"User email register"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }

        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"User email register"])
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"User email register"])
        })
        
        
    }
    func requestToUpdateFacebookImages(strUrlAry:String)
    {
        let params:[String: Any] = ["images":strUrlAry]
        let request = CYLServices.postFacebookImageUrl(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryObject
                    if responseData?["status"] as! String != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject
                        userObject?.userImages = responseData?["data"] as! [Any]?
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject!)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                    } 
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Upload image"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Upload image"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Upload image"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
