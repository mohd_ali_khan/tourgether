//
//  CYLSelectDOBVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 27/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLSelectDOBVC: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate,UITextFieldDelegate {

    @IBOutlet weak var txtDateOfBirth: TextField!
    @IBOutlet weak var btnBack: UIButton!
    var appObject:AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.appObject = UIApplication.shared.delegate as? AppDelegate
        self.createScreenDesign()
        self.datePickerTypes()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=true
        self.btnBack.setImage(UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnBack.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
    }
    
    func datePickerTypes()
    {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePickerMode.date
        
        let calender: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let currentDate: Date = Date()
        var components: DateComponents = DateComponents()
        components.year = -16
        
        let maxDate: Date = calender.date(byAdding: components, to: currentDate)!
//        let maxDate: NSDate = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
        datePicker.maximumDate = maxDate
        
        datePicker.addTarget(self, action: #selector(CYLSelectDOBVC.datePickerValueChanged), for: UIControlEvents.valueChanged)
        txtDateOfBirth.inputView = datePicker

    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtDateOfBirth.text = dateFormatter.string(from: sender.date)
    }
    @IBAction func btnToBackClicked(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_reverse"), object: nil, userInfo: ["index":3])
    }
    
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    @IBAction func btnNextActoin(_ sender: UIButton)
    {
        if txtDateOfBirth.text != ""
        {
            self.appObject?.userSignUpDetail!["dateOfBirth"] = getMilliesFromDateString(strInput: txtDateOfBirth.text!, formate: "dd/MM/yyyy")
            self.appObject?.userSignUpDetail!["dob"] = getFromDate(strInput: txtDateOfBirth.text!, formate: "dd/MM/yyyy")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_forward"), object: nil, userInfo: ["index":5])
            
        }
        else
        {
            showAlertView(title: "", message: "Select date of birth.", ref: self)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
