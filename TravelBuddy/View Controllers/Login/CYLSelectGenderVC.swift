//
//  CYLSelectGenderVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 27/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLSelectGenderVC: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var lblFemaleIcon: UILabel!
    @IBOutlet weak var imgFemaleIcon: UIImageView!
    @IBOutlet weak var lblMaleText: UILabel!
    @IBOutlet weak var imgMaleIcon: UIImageView!
    @IBOutlet weak var btnToMale: UIButton!
    @IBOutlet weak var btntoFemale: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var appObject:AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.createScreenDesign()
        self.appObject = UIApplication.shared.delegate as? AppDelegate
//        self.showSelectionOnMaleTab()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=true
        self.btnBack.setImage(UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnBack.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
    }
    @IBAction func btnToBackClicked(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_reverse"), object: nil, userInfo: ["index":2])
    }
    
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    
    @IBAction func btnToMaleClicked(_ sender: Any)
    {
        self.showSelectionOnMaleTab()
        
    }
    
    @IBAction func btnToFemaleClicked(_ sender: Any)
    {
        self.showSelectionOnFemaleTab()
    }
    func showSelectionOnMaleTab()
    {
        self.btnToMale.isSelected = true
        self.imgMaleIcon.image = #imageLiteral(resourceName: "ic_male_act")
        self.lblMaleText.textColor = UIColor.white
        self.btnToMale.superview?.backgroundColor = themeColorBlue
        
        self.btntoFemale.isSelected = false
        self.imgFemaleIcon.image = #imageLiteral(resourceName: "ic_female_inact")
        self.lblFemaleIcon.textColor = UIColor(red: 208/255.0, green: 215/255.0, blue: 217/255.0, alpha: 1.0)
        self.btntoFemale.superview?.backgroundColor = UIColor.white
    }
    func showSelectionOnFemaleTab()
    {
        self.btntoFemale.isSelected = true
        self.imgFemaleIcon.image = #imageLiteral(resourceName: "ic_female_act")
        self.lblFemaleIcon.textColor = UIColor.white
        self.btntoFemale.superview?.backgroundColor = themeColorBlue
        
        self.btnToMale.isSelected = false
        self.imgMaleIcon.image = #imageLiteral(resourceName: "ic_male_inact")
        self.lblMaleText.textColor = UIColor(red: 208/255.0, green: 215/255.0, blue: 217/255.0, alpha: 1.0)
        self.btnToMale.superview?.backgroundColor = UIColor.white
    }
    @IBAction func btnNextAction(_ sender: UIButton)
    {
        if self.btntoFemale.isSelected == true || self.btnToMale.isSelected == true
        {
            self.appObject?.userSignUpDetail!["gender"] = (self.btnToMale.isSelected == true) ? 0:1
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_forward"), object: nil, userInfo: ["index":4])
        }
        else
        {
            showAlertView(title: "", message: "Please select gender.", ref: self)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
