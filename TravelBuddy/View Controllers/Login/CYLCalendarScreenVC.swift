//
//  CYLCalendarScreenVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/5/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
import SJFluidSegmentedControl

class CYLCalendarScreenVC: CYLBaseViewController,UIGestureRecognizerDelegate,UINavigationControllerDelegate
{


    @IBOutlet weak var lblEndDatePlaceholder: UILabel!
    @IBOutlet weak var lblStartDatePlaceholder: UILabel!
    
    @IBOutlet weak var vwFlexStartContainer: UIView!
    @IBOutlet weak var vwFlexEndContainer: UIView!
    @IBOutlet weak var constEndPickerH: NSLayoutConstraint!
    @IBOutlet weak var constStartPickerH: NSLayoutConstraint!
    @IBOutlet weak var pickerEndDate: UIDatePicker!
    @IBOutlet weak var pickerStartDate: UIDatePicker!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var constLastVwTop: NSLayoutConstraint!
    
    
    @IBOutlet weak var imgFixedBack: UIImageView!
    @IBOutlet weak var imgFixed: UIImageView!
    @IBOutlet weak var imgFlexible: UIImageView!
    @IBOutlet weak var imgFlexibleBack: UIImageView!
    @IBOutlet weak var imgSomedayBack: UIImageView!
    @IBOutlet weak var imgSomeDay: UIImageView!
    @IBOutlet weak var vwSomeDay: UIView!
    var pickerFlexStartDate = NTMonthYearPicker()
    var pickerFlexEndDate = NTMonthYearPicker()
    var checkSelectedSegment: Int = Int()
    
    @IBOutlet weak var constCalanderPicH: NSLayoutConstraint!
    @IBOutlet weak var constCalanderPicW: NSLayoutConstraint!
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: View Controller Life Cycle
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.vwSomeDay.isHidden = true
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.clear], for: .normal)
        self.pickerStartDate.minimumDate = Date()
        self.pickerStartDate.maximumDate = Date().addingTimeInterval(1*365*60*60*24)
        self.pickerEndDate.minimumDate = Date()
        self.pickerEndDate.maximumDate = Date().addingTimeInterval(1*365*60*60*24)
        self.pickerStartDate.setDate(Date().addingTimeInterval(1*30*60*60*24), animated: false)
        self.pickerEndDate.setDate(Date().addingTimeInterval(1*40*60*60*24), animated: false)
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "New user travelling date")
  
        createCalendarScreenDesign()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if screenHeight == 480
        {
            constCalanderPicH.constant = 120
            constCalanderPicW.constant = 141
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createCalendarScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Travel Date"
        
        self.pickerFlexStartDate.minimumDate = Date()
        self.pickerFlexStartDate.maximumDate = Date().addingTimeInterval(1*365*60*60*24)
        self.pickerFlexStartDate.datePickerMode = NTMonthYearPickerModeMonthAndYear
        self.pickerFlexStartDate.addTarget(self, action: #selector(self.flexStartPickerValueChanged), for: .valueChanged)
        self.pickerFlexStartDate.frame = CGRect(x:0,y:-30,width:UIScreen.main.bounds.width-48,height:self.vwFlexStartContainer.frame.size.height)
        self.vwFlexStartContainer.addSubview(self.pickerFlexStartDate)
        
        self.pickerFlexEndDate.minimumDate = Date()
        self.pickerFlexEndDate.maximumDate = Date().addingTimeInterval(1*365*60*60*24)
        self.pickerFlexEndDate.datePickerMode = NTMonthYearPickerModeMonthAndYear
        self.pickerFlexEndDate.addTarget(self, action: #selector(self.flexEndPickerValueChanged), for: .valueChanged)
        self.pickerFlexEndDate.frame = CGRect(x:0,y:-30,width:UIScreen.main.bounds.width-48,height:self.vwFlexEndContainer.frame.size.height)
        self.vwFlexEndContainer.addSubview(self.pickerFlexEndDate)
        self.pickerFlexStartDate.date = Date().addingTimeInterval(2*30)
        self.pickerFlexEndDate.date = Date().addingTimeInterval(1*30*60*60*24)
        self.handleFlexibleDateSelection()
        
    }
    func btnToBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    func validateSelectedDates()->Bool
    {
        switch self.checkSelectedSegment
        {
        case 1:
            return (self.pickerFlexEndDate.date.timeIntervalSince1970 < self.pickerFlexStartDate.date.timeIntervalSince1970) ? false : true
        case 0:
            return (self.pickerEndDate.date.timeIntervalSince1970 < self.pickerStartDate.date.timeIntervalSince1970) ? false : true
        case 2:
            return true
        default:
            return false
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom Button Actions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToContinueClicked(_ sender: Any)
    {
        if self.validateSelectedDates() == true
        {
        self.requestToSubmitCalendarData()
        }
        else
        {
          showAlertView(title: "", message: "Invalid dates. Start date should be before the end date.", ref: self)
        }
    }
    func handleFixedDateSelection()
    {
        self.checkSelectedSegment = 0
        imgSomedayBack.image = UIImage(named: "uncheck_btn")
        imgSomeDay.isHidden = true
        
        imgFixedBack.image = UIImage(named: "Rectangle")
        imgFixed.isHidden = false
        
        imgFlexibleBack.image = UIImage(named: "uncheck_btn")
        imgFlexible.isHidden = true
        
        self.vwFlexStartContainer.isHidden=true
        self.vwFlexEndContainer.isHidden=true
        self.constStartPickerH.constant = 146
        self.constEndPickerH.constant = 146
        self.lblStartDate.text = convertDateIntoString(date: self.pickerStartDate.date,format:"dd MMM yyyy")
        self.lblEndDate.text = convertDateIntoString(date: self.pickerEndDate.date,format:"dd MMM yyyy")
    }
    func handleFlexibleDateSelection()
    {
        self.checkSelectedSegment = 1
        imgSomedayBack.image = UIImage(named: "uncheck_btn")
        imgSomeDay.isHidden = true
        
        imgFixedBack.image = UIImage(named: "uncheck_btn")
        imgFixed.isHidden = true
        
        imgFlexibleBack.image = UIImage(named: "Rectangle")
        imgFlexible.isHidden = false
        
        self.navigationItem.rightBarButtonItem?.isEnabled=true
        self.vwFlexStartContainer.isHidden=false
        self.vwFlexEndContainer.isHidden=false
        self.constStartPickerH.constant = 146
        self.constEndPickerH.constant = 146
        self.lblStartDate.text = convertDateIntoString(date: self.pickerFlexStartDate.date, format: "MMM yyyy")
        self.lblEndDate.text = convertDateIntoString(date: self.pickerFlexEndDate.date, format: "MMM yyyy")
    }
    func handleSomedayDateSelection()
    {
        self.checkSelectedSegment = 2
        imgSomedayBack.image = UIImage(named: "Rectangle")
        imgSomeDay.isHidden = false
        
        imgFixedBack.image = UIImage(named: "uncheck_btn")
        imgFixed.isHidden = true
        
        imgFlexibleBack.image = UIImage(named: "uncheck_btn")
        imgFlexible.isHidden = true
        
        self.navigationItem.rightBarButtonItem?.isEnabled=true
        self.vwFlexStartContainer.isHidden=true
        self.vwFlexEndContainer.isHidden=true
        self.constStartPickerH.constant = 0
        self.constEndPickerH.constant = 0
        self.lblStartDate.text = "Flexible"
        self.lblEndDate.text = "Flexible"
        
    }

    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Picker view Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func startDatePickerValueChanged(_ sender: UIDatePicker)
    {
        self.navigationItem.rightBarButtonItem?.isEnabled=true
        self.lblStartDate.text = convertDateIntoString(date: self.pickerStartDate.date,format:"dd MMM yyyy")

    }
    @IBAction func endDatePickerValueChanged(_ sender: UIDatePicker)
    {
        self.navigationItem.rightBarButtonItem?.isEnabled=true
        self.lblEndDate.text = convertDateIntoString(date: sender.date,format:"dd MMM yyyy")
    }
    @objc func flexStartPickerValueChanged()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        self.lblStartDate.text = dateFormatter .string(from: self.pickerFlexStartDate.date)
    }
    @objc func flexEndPickerValueChanged()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        self.lblEndDate.text = dateFormatter .string(from: self.pickerFlexEndDate.date)
    }

    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToSubmitCalendarData()
    {
        KVNProgress.show()
        var startDate = [String]()
        var endDate = [String]()
        switch self.checkSelectedSegment
        {
        case 2:
            startDate = ["0","0","0"]
            endDate = ["0","0","0"]
            sentEventPropertyOnCleverTap("Date preferences", _property: ["Flexibility" : "SomeDay"])
        case 1:
            
            startDate = convertDateIntoString(date: self.pickerFlexStartDate.date, format: "dd MM yyyy").components(separatedBy: " ")
            startDate[0] = "0"
            endDate = convertDateIntoString(date: self.pickerFlexEndDate.date, format: "dd MM yyyy").components(separatedBy: " ")
            endDate[0] = "0"
            sentEventPropertyOnCleverTap("Date preferences", _property: ["Flexibility" : "Flexible"])
        case 0:
            startDate = convertDateIntoString(date: self.pickerStartDate.date, format: "dd MM yyyy").components(separatedBy: " ")
            endDate = convertDateIntoString(date: self.pickerEndDate.date, format: "dd MM yyyy").components(separatedBy: " ")
            
            let dayFuture = dateDifferenceInDay(firstDate: self.pickerStartDate.date, secondDate: Date(), dateFormate: "dd MM yyyy")
            let duration = dateDifferenceInDay(firstDate: self.pickerEndDate.date, secondDate: self.pickerStartDate.date, dateFormate: "dd MM yyyy")
            sentEventPropertyOnCleverTap("Date preferences", _property: ["Flexibility" : "Fixed","DaysInFuture":dayFuture,"Duration":duration])
        default:
            break
        }

        let params:[String: Any] = ["start_year":"\(startDate[2])","start_month":"\(startDate[1])","start_date":"\(startDate[0])","end_year":"\(endDate[2])","end_month":"\(endDate[1])","end_date":"\(endDate[0])"]
        let request = CYLServices.postCalendarList(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                    let userObject = CYLGlobals.sharedInstance.usrObject!
                    userObject.progress = 2
                    userObject.startDate = self.lblStartDate.text
                    userObject.endDate = self.lblEndDate.text
                    CYLGlobals.sharedInstance.usrObject = userObject
                    let userDeflts = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                    userDeflts.set(encodedData, forKey: "userObject")
                    userDeflts.synchronize()
                        
                        let aboutVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLAboutMe") as! CYLAboutMe
                        aboutVC.checkLoginFlow = "login"
                        self.navigationController?.pushViewController(aboutVC, animated: true)
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"New user travelling date"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"New user travelling date"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"New user travelling date"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    @IBAction func btnFixedFlexibleSomedayChange(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 0:
            self.checkSelectedSegment = 0
            vwSomeDay.isHidden = true
            self.lblStartDatePlaceholder.text = "Start Date"
            self.lblEndDatePlaceholder.text = "End Date"
            self.handleFixedDateSelection()
        case 1:
            self.checkSelectedSegment = 1
            vwSomeDay.isHidden = true
            self.lblStartDatePlaceholder.text = "Start Month"
            self.lblEndDatePlaceholder.text = "End Month"
            self.handleFlexibleDateSelection()
        case 2:
            self.checkSelectedSegment = 2
            vwSomeDay.isHidden = false
            self.lblStartDatePlaceholder.text = "Start Month"
            self.lblEndDatePlaceholder.text = "End Month"
            self.handleSomedayDateSelection()
        default:
            break
        }
    }

}

