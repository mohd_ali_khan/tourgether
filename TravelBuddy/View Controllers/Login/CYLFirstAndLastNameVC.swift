//
//  CYLFirstAndLastNameVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 27/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLFirstAndLastNameVC: UIViewController, UIGestureRecognizerDelegate{

    @IBOutlet weak var txtFirstName: TextField!
    @IBOutlet weak var txtLastName: TextField!
    @IBOutlet weak var btnBack: UIButton!
    
    var appObject:AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.appObject = UIApplication.shared.delegate as? AppDelegate
        self.createScreenDesign()
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=true
        self.btnBack.setImage(UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnBack.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
    }
    @IBAction func btnToBackClicked(_ sender: UIButton) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton)
    {
        if txtFirstName.text != ""
        {
            if txtLastName.text != ""
            {
                self.appObject?.userSignUpDetail = ["firstName":txtFirstName.text!,"lastName":txtLastName.text!,"middleName":"","userName":"\(txtFirstName.text!) \(txtLastName.text!)"]
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_forward"), object: nil, userInfo: ["index":1])

//                self.navigationController?.pushViewController(emailVC, animated: true)
            }
            else
            {
                showAlertView(title: "", message: "Last name cannot be empty.", ref: self)
            }
        }
        else
        {
            showAlertView(title: "", message: "First name cannot be empty.", ref: self)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
