//
//  CYLRedVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 05/07/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLRedVC: UIViewController {

    @IBOutlet weak var constImageHight: NSLayoutConstraint!
    @IBOutlet weak var constImageWidth: NSLayoutConstraint!
    @IBOutlet weak var lblMeet: UILabel!
    @IBOutlet weak var lblExplorers: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.view.frame.size.height == 568
        {
            constImageWidth.constant = 140
            constImageHight.constant = 98
            lblMeet.font = UIFont(name: "Roboto-Bold", size: 30.0)
            lblExplorers.font = UIFont(name: "Roboto-Bold", size: 30.0)
            lblDesc.font = UIFont(name: "Roboto-Regular", size: 15.0)
        }
        else if self.view.frame.size.height == 480
        {
            constImageWidth.constant = 112
            constImageHight.constant = 70
            lblMeet.font = UIFont(name: "Roboto-Bold", size: 25.0)
            lblExplorers.font = UIFont(name: "Roboto-Bold", size: 25.0)
            lblDesc.font = UIFont(name: "Roboto-Regular", size: 12.0)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
