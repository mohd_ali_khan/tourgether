//
//  CYLPasswordVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 27/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLPasswordVC: UIViewController, UINavigationControllerDelegate, UIGestureRecognizerDelegate,UITextFieldDelegate {

    @IBOutlet weak var txtPassword: TextField!
    @IBOutlet weak var txtConfirmPassword: TextField!
    @IBOutlet weak var btnBack: UIButton!
    
    var appObject:AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.appObject = UIApplication.shared.delegate as? AppDelegate
        self.createScreenDesign()
        self.txtPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=true
        self.btnBack.setImage(UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnBack.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
    }
    @IBAction func btnToBackClicked(_ sender: UIButton)
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_reverse"), object: nil, userInfo: ["index":1])
    }
    
    @IBAction func btnNextAction(_ sender: UIButton)
    {
        if txtPassword.text != ""
        {
            if (txtPassword.text?.count)! >= 8
            {
                if txtConfirmPassword.text == txtPassword.text
                {
                    self.appObject?.userSignUpDetail!["password"] = self.txtPassword.text!
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_forward"), object: nil, userInfo: ["index":3])
                }
                else
                {
                    showAlertView(title: "", message: "Password and confirm password should be same.", ref: self)
                }
            }
            else
            {
                showAlertView(title: "", message: "Minimum 8 character", ref: self)
            }
            
        }
        else
        {
            showAlertView(title: "", message: "Password cannot be empty.", ref: self)
        }
        
    }
    @IBAction func btnShowPassWord(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        
        if sender.tag == 201
        {
            if sender.isSelected
            {
                self.txtPassword.isSecureTextEntry = false
            }
            else
            {
                self.txtPassword.isSecureTextEntry = true
            }
        }
        else
        {
            if sender.isSelected
            {
                self.txtConfirmPassword.isSecureTextEntry = false
            }
            else
            {
                self.txtConfirmPassword.isSecureTextEntry = true
            }
        }
        
        
    }
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == " ") {
            return false
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
