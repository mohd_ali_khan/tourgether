//
//  CYLCountrySearchVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/6/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
protocol CYLSearchDelegate: class
{
    //required delegate function
    func delegateToGetSelectedCityCountry(object:JSON)
    
}
class CYLCountrySearchVC: CYLBaseViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIGestureRecognizerDelegate
{
    weak var delegate: CYLSearchDelegate?
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var imgCityEmpty: UIImageView!
    @IBOutlet weak var constTableViewBottom: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: UISearchBar!
    @IBOutlet weak var tblSearch: UITableView!
    var aryResponse=[JSON]()
    var aryFilterResults=[JSON]()
    var isCountrySearch=false
    var vwLoader: UIView = UIView()
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: View controller lifecycle
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.clear], for: .normal)
        self.createCountryScreenDesign()
        self.requestToGetCityCountrySearch()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
//        self.txtSearch.becomeFirstResponder()
        let eventName = (self.isCountrySearch) ? "Country search" : "City search"
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: eventName)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.view.endEditing(true)
        self.updateViewConstraints()
    }
    override func viewWillAppear(_ animated: Bool) {
//        self.txtSearch.becomeFirstResponder()
        self.view.endEditing(true)
    }
    override func viewDidLayoutSubviews()
    {
        self.updateViewConstraints()
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createCountryScreenDesign()
    {
        let cancelButtonAttributes: NSDictionary = [NSAttributedStringKey.foregroundColor: UIColor(red: 25/255, green: 34/255, blue: 38/255, alpha: 1)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes as? [NSAttributedStringKey : Any], for: .normal)
        
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.txtSearch.placeholder = (self.isCountrySearch) ? "Try \"Indonesia\"" : "Try \"Paris\" or \"France\""
        self.lblNoDataFound.text = (!self.isCountrySearch) ? "No City Found \n \nCheck the spelling, or enter the country name to find the available popular cities." : "No Country Found \n\nPlease check the spelling, or choose another country."
        self.txtSearch.showsCancelButton = false
    }
    @objc func btnToBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Keyboard Notifications
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @objc func keyboardNotification(notification: NSNotification)
    {
        if let userInfo = notification.userInfo
        {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.constTableViewBottom?.constant = 0.0
            } else {
                self.constTableViewBottom?.constant = (endFrame?.size.height)!+0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Search bar delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //self.requestToGetCityCountrySearch()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if(searchBar.text?.count==0)
        {
            self.aryFilterResults=self.aryResponse
        }
        else
        {
            if isCountrySearch
            {
                self.aryFilterResults = filterResultsInArray(aryInput: self.aryResponse, key: "countryName", keySecond: "countryNameAliasis", searchWord: searchBar.text!)
            }
            else
            {
                self.aryFilterResults = filterResultsInArray(aryInput:self.aryResponse,key:"cityName",keySecond: "countryName",searchWord:searchBar.text!)
            }
        }
        self.lblNoDataFound.isHidden = (self.aryFilterResults.count == 0) ? false : true
        self.imgCityEmpty.isHidden = (self.aryFilterResults.count == 0) ? false : true
        if self.aryFilterResults.count == 0
        {
            let type = (self.isCountrySearch) ? "Country" : "City"
            sentEventPropertyOnCleverTap("Destination not found", _property: ["Type":type,"Value":"\(searchText)"])
        }
        self.tblSearch.reloadData()
    }
    // Called when search bar obtains focus.  I.e., user taps
    // on the search bar to enter text.
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.text = nil
        searchBar.showsCancelButton = false
        self.aryFilterResults=self.aryResponse
        self.lblNoDataFound.isHidden = (self.aryFilterResults.count == 0) ? false : true
        self.imgCityEmpty.isHidden = (self.aryFilterResults.count == 0) ? false : true
        self.tblSearch.reloadData()
        // Remove focus from the search bar.
        searchBar.endEditing(true)
        
        // Perform any necessary work.  E.g., repopulating a table view
        // if the search bar performs filtering.
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Table view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.aryFilterResults.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CYLCityCountrySearchCell") as! CYLCityCountrySearchCell
        cell.isCountrySearch=self.isCountrySearch
        cell.object = self.aryFilterResults[indexPath.row]
        
        if indexPath.row % 2 == 0 && indexPath.row % 4 != 0
        {
            if #available(iOS 10.0, *) {
                cell.imgBackground.backgroundColor = UIColor(displayP3Red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
            } else {
                cell.imgBackground.backgroundColor = UIColor(red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
            }
        }
        else if indexPath.row % 3 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgBackground.backgroundColor = UIColor(displayP3Red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
            } else {
                cell.imgBackground.backgroundColor = UIColor(red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
            }
        }
        else if indexPath.row % 4 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgBackground.backgroundColor = UIColor(displayP3Red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
            } else {
                cell.imgBackground.backgroundColor = UIColor(red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
            }
        }
        else if indexPath.row % 5 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgBackground.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            } else {
                cell.imgBackground.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            }
        }
        else
        {
            if #available(iOS 10.0, *) {
                cell.imgBackground.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            } else {
                cell.imgBackground.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            }
        }
        cell.selectionStyle = .gray
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 140
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cityCountryName = (self.isCountrySearch) ? self.aryFilterResults[indexPath.row]["countryName"].stringValue : self.aryFilterResults[indexPath.row]["cityName"].stringValue
        sentEventPropertyOnCleverTap("Destination name", _property: ["Name":cityCountryName])
        self.delegate?.delegateToGetSelectedCityCountry(object: self.aryFilterResults[indexPath.row])
        
        _=self.navigationController?.popViewController(animated: true)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToGetCityCountrySearch()
    {
        KVNProgress.show()
        let isCountry = self.isCountrySearch ? 1 : 0
        let params:[String: Any] = ["record":"1000","start":"0","isCountry":"\(isCountry)"]
        let request = CYLServices.getAllCityCountryList(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        self.aryResponse=(responseData?["data"]?.arrayValue)!
                        self.aryFilterResults=(responseData?["data"]?.arrayValue)!
                        self.lblNoDataFound.isHidden = (self.aryFilterResults.count == 0) ? false : true
                        self.imgCityEmpty.isHidden = (self.aryFilterResults.count == 0) ? false : true
                        self.tblSearch.reloadData()
                    }
                    
                }
                else
                {
                    let eventName = (self.isCountrySearch) ? "Country search" : "City search"
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":eventName])
                    self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                let eventName = (self.isCountrySearch) ? "Country search" : "City search"
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":eventName])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                let eventName = (self.isCountrySearch) ? "Country search" : "City search"
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":eventName])
                self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
        })
    }
    
}
extension CYLCountrySearchVC
{
    func refreshViewSlowOrMantinence(msgStr: String , imgName:String)
    {
        if vwLoader.frame.size.width == 0
        {
            vwLoader = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height))
            vwLoader.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.0)
            self.view.addSubview(vwLoader)
            let img = UIImageView(frame: CGRect(x: vwLoader.frame.width/2-60, y: vwLoader.frame.height/2-160, width: 120, height: 120))
            img.image = UIImage(named: imgName)
            
            let lblMsg = UILabel(frame: CGRect(x: 40, y: img.frame.origin.y+img.frame.size.height+20, width: self.view.frame.size.width-80, height: 21))
            lblMsg.font = UIFont(name: "Comfortaa-Bold", size: 20)
            lblMsg.textAlignment = .center
            lblMsg.text = "Oops!"
            
            let lblAlertText = UILabel(frame: CGRect(x: 60, y: lblMsg.frame.origin.y+lblMsg.frame.size.height+15, width: self.view.frame.size.width-120, height: 40))
            lblAlertText.numberOfLines = 0
            lblAlertText.textAlignment = .center
            lblAlertText.text = msgStr
            lblAlertText.font = UIFont(name: "Comfortaa-Bold", size: 15)
            
            let btnRefresh: UIButton = UIButton(frame: CGRect(x: vwLoader.frame.width/2-100, y: lblAlertText.frame.origin.y+lblAlertText.frame.size.height+35, width: 200, height: 45))
            btnRefresh.addTarget(self, action: #selector(btnRefreshAction), for: .touchUpInside)
            btnRefresh.titleLabel?.textAlignment = .center
            btnRefresh.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 15)
            btnRefresh.titleLabel?.textColor = UIColor.white
            btnRefresh.setTitle("Retry", for: UIControlState.normal)
            btnRefresh.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
            btnRefresh.layer.cornerRadius = 23
            btnRefresh.layer.masksToBounds = true
            
            vwLoader.addSubview(img)
            vwLoader.addSubview(lblMsg)
            vwLoader.addSubview(lblAlertText)
            vwLoader.addSubview(btnRefresh)
            vwLoader.isHidden = false
        }
        else
        {
            vwLoader.isHidden = false
        }
    }
    @objc func btnRefreshAction(sender: UIButton)
    {
        if checkInternetConnection()
        {
            self.requestToGetCityCountrySearch()
            vwLoader.isHidden = true
        }
    }
}


