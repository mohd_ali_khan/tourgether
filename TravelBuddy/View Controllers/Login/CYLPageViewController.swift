//
//  CYLPageViewController.swift
//  TravelBuddy
//
//  Created by codeyeti on 21/03/18.
//  Copyright © 2018 Nikhil Sharma. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class CYLPageViewController: UIPageViewController , UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    
    var pageControl = UIPageControl()
    var btnToFacebook:UIButton = UIButton()
    var actIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var facebookObject:CYLFacebookHelper?
    var loginInitiated = false
    var isoCountryCode = ""
    var currentLocation = ""
    // MARK: UIPageViewControllerDataSource
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "CYLGreenVC"),
                self.newVc(viewController: "CYLBlueVC"),self.newVc(viewController: "CYLRedVC")]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
        {
            self.isoCountryCode = countryCode
            let current = Locale(identifier: "en_US")
            self.currentLocation = current.localizedString(forRegionCode: countryCode) ?? ""
        }
        self.initiateFacebookSDKSetup()
        self.navigationController?.navigationBar.isHidden=true
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "User login")
        sentEventPropertyOnCleverTap("User isolocation", _property: ["IsoLocation": "\(isoCountryCode)"])
        // This sets up the first view that will show up on our page control
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        configurePageControl()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=true
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        self.navigationController?.navigationBar.isHidden=false
    }
    
    func configurePageControl() {
        
        let vwLoginButton: UIView = UIView(frame: CGRect(x: 40, y: UIScreen.main.bounds.maxY - 125, width: self.view.frame.size.width-80, height: 56))
        vwLoginButton.backgroundColor = UIColor.white
        vwLoginButton.layer.cornerRadius = 8
        vwLoginButton.clipsToBounds = true
        self.view.addSubview(vwLoginButton)
        
        let imgFb: UIImageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 30, height: 30))
        imgFb.image = UIImage(named: "ic_facebook")
        imgFb.tintColor = UIColor.red
        vwLoginButton.addSubview(imgFb)
        
        let lblFb: UILabel = UILabel(frame: CGRect(x: imgFb.frame.origin.x+50, y: 15, width: vwLoginButton.frame.size.width-80, height: 26))
        lblFb.text = "Login with Facebook"
        lblFb.textColor = UIColor(red: 102/255.0, green: 102/255.0, blue: 102/255.0, alpha: 1.0)
        lblFb.font = UIFont(name: "Roboto-Medium", size: 18)
        lblFb.adjustsFontSizeToFitWidth = true
        vwLoginButton.addSubview(lblFb)
        
        actIndicator = UIActivityIndicatorView(frame: CGRect(x: lblFb.frame.origin.x+lblFb.frame.size.width-30, y: 18, width: 20, height: 20))
        actIndicator.activityIndicatorViewStyle = .gray
        vwLoginButton.addSubview(actIndicator)
        
        btnToFacebook = UIButton(frame: CGRect(x: 0, y: 0, width: vwLoginButton.frame.size.width, height: vwLoginButton.frame.size.height))
        btnToFacebook.setTitle("", for: .normal)
        btnToFacebook.addTarget(self, action:#selector(self.buttonFacebookClicked), for: .touchUpInside)
        vwLoginButton.addSubview(btnToFacebook)
        
        let lblPublish: UILabel = UILabel(frame: CGRect(x: 0, y: vwLoginButton.frame.origin.y-23, width: self.view.frame.size.width, height: 20))
        lblPublish.text = "We will never publish anything on Facebook"
        lblPublish.textAlignment = .center
        lblPublish.textColor = UIColor.white
        lblPublish.font = UIFont(name: "Roboto-Light", size: 12)
        self.view.addSubview(lblPublish)
        
        let lblTerms: UILabel = UILabel(frame: CGRect(x: 40, y: UIScreen.main.bounds.maxY - 35, width: self.view.frame.size.width-80, height: 30))
        lblTerms.textAlignment = .center
        lblTerms.textColor = UIColor(red: 216/255.0, green: 216/255.0, blue: 216/255.0, alpha: 1.0)
        lblTerms.font = UIFont(name: "Roboto-Light", size: 12)
        lblTerms.numberOfLines = 0
        let text = "Terms of Services and "
        let textRange = NSMakeRange(0, 17)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedStringKey , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        let text1 = "Privacy Policy"
        let textRange1 = NSMakeRange(0, text1.count)
        let attributedText1 = NSMutableAttributedString(string: text1)
        attributedText1.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedStringKey , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange1)
        let combination = NSMutableAttributedString()
        combination.append(attributedText)
        combination.append(attributedText1)
        lblTerms.attributedText = combination
        self.view.addSubview(lblTerms)
        
        let btnTerms = UIButton(frame: CGRect(x: 40, y: UIScreen.main.bounds.maxY - 35, width: self.view.frame.size.width-80, height: 30))
        btnTerms.setTitle("", for: .normal)
        btnTerms.addTarget(self, action:#selector(self.btnToPrivacyAndTermsClicked), for: .touchUpInside)
        self.view.addSubview(btnTerms)
        
        let btnOtherLogin = UIButton(frame: CGRect(x: 40, y: lblTerms.frame.origin.y-25, width: self.view.frame.size.width-80, height: 25))
        btnOtherLogin.setTitle("Other Login Options", for: .normal)
        btnOtherLogin.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 14)
        btnOtherLogin.addTarget(self, action:#selector(self.buttonOtherLogin), for: .touchUpInside)
        self.view.addSubview(btnOtherLogin)
        
        pageControl = UIPageControl(frame: CGRect(x: 0,y: lblPublish.frame.origin.y - 25,width: UIScreen.main.bounds.width,height: 30))
        self.pageControl.numberOfPages = orderedViewControllers.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.white
        self.pageControl.isUserInteractionEnabled = false
        self.pageControl.pageIndicatorTintColor = UIColor(red: 149/255.0, green: 152/255.0, blue: 154/255.0, alpha: 1.0)
        self.pageControl.currentPageIndicatorTintColor = UIColor.white
        self.view.addSubview(pageControl)
    }
    @objc func buttonFacebookClicked()
    {
        self.actIndicator.startAnimating()
        self.btnToFacebook.isUserInteractionEnabled=false
        self.doLoginWithFacebookSDK()
        self.loginInitiated=true
    }
    @objc func btnToPrivacyAndTermsClicked()
    {
        self.showActionSheetForPrivacyandTermsView()
    }
    @objc func buttonOtherLogin()
    {
        let privacyVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLSignInEmailVC") as! CYLSignInEmailVC
        self.navigationController?.pushViewController(privacyVC, animated: true)
    }
    
    func showActionSheetForPrivacyandTermsView()
    {
        let alert = UIAlertController()
        let blockAction = UIAlertAction.init(title: "Terms of Service", style: .default, handler: {
            (sender) in
            
            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLTermsCondition") as! CYLTermsCondition
            termsVC.urlStr = "https://www.tourgetherapp.com/terms-services.html"
            termsVC.titleStr = "Terms of Services"
            self.navigationController?.present(termsVC, animated: true, completion: nil)
        })
        let unblockAction = UIAlertAction.init(title: "Privacy Policy", style: .default, handler: {
            (sender) in
            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLTermsCondition") as! CYLTermsCondition
            termsVC.titleStr = "Privacy Policy"
            termsVC.urlStr = "https://www.tourgetherapp.com/privacy-policy.html"
            self.navigationController?.present(termsVC, animated: true, completion: nil)
            
        })
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler:
        {
            (sender) in
            
        })
        alert.addAction(blockAction)
        alert.addAction(unblockAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    // MARK: Delegate methords
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
    }
    
    // MARK: Data source functions.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
//         User is on the first view controller and swiped left to loop to
//         the last view controller.
        guard previousIndex >= 0
            else
        {
            self.view.backgroundColor = UIColor(red: 0/255.0, green: 194/255.0, blue: 147/255.0, alpha: 1.0)
            return nil//orderedViewControllers.last
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            // return nil
        }
        
        guard orderedViewControllers.count > previousIndex
        else
        {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
//         User is on the last view controller and swiped right to loop to
//         the first view controller.
        guard orderedViewControllersCount != nextIndex
            else
        {
            self.view.backgroundColor = UIColor(red: 227/255.0, green: 0/255.0, blue: 69/255.0, alpha: 1.0)
            return nil//orderedViewControllers.first
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            // return nil
        }
        
        guard orderedViewControllersCount > nextIndex
            else
        {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Facebook Login Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func initiateFacebookSDKSetup()
    {
        facebookObject=CYLFacebookHelper.sharedFInstance()
        facebookObject?.baseViewController=self
    }
    func doLoginWithFacebookSDK()
    {
        facebookObject?.doFacebookLogin(completionHandler:
            {
                loginResponse in
                self.requestToLoginWithFacebook(dict: loginResponse!)
                self.loginInitiated=false
                KVNProgress.dismiss()
        },
                                        andError:
            {
                loginError in
                self.actIndicator.stopAnimating()
                showAlertView(title: "", message: (loginError?.localizedDescription)!, ref: self)
                self.btnToFacebook.isUserInteractionEnabled=true
                self.loginInitiated=false
                KVNProgress.dismiss()
                
        },
                                            andCancelEvent: {
                                            loginCancelled in
                                            self.actIndicator.stopAnimating()
                                            self.btnToFacebook.isUserInteractionEnabled=true
                                            self.loginInitiated=false
                                            KVNProgress.dismiss()
                                            
        })
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Facebook Images Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func getUserProfileImagesFromFacebook(profileImg: String)
    {
        facebookObject?.getAlbumImagesListFromFacebook(completionHandler:
            {
                response in
                var aryImgUrl = [String]()
                for strURL in response!
                {
                    aryImgUrl.append(strURL as! String)
                }
                if aryImgUrl.count != 0
                {
                    self.requestToUpdateFacebookImagesOnServer(strUrlAry: aryImgUrl.joined(separator: ","))
                }
                else if profileImg != ""
                {
                    self.requestToUpdateFacebookImagesOnServer(strUrlAry: profileImg)
                }
                else
                {
                    self.actIndicator.stopAnimating()
                    self.btnToFacebook.isUserInteractionEnabled=true
                    let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLDestinationScreenVC")
                    self.navigationController?.pushViewController(destinationVC!, animated: true)
                }
                
        }, andError:
            {
                error in
        })
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToLoginWithFacebook(dict:Dictionary<AnyHashable, Any>)
    {
        KVNProgress.show()
        let currentAccessToken: String = (facebookObject?.tokenString == nil) ? "" : facebookObject!.tokenString
        let gender = (dict["gender"] == nil) ? 2 : ((dict["gender"] as! String == "male") ? 0 : (dict["gender"] as! String == "female") ? 1 : 3)
        let firstName = (dict["first_name"] == nil) ? "" : dict["first_name"]!
        let lastName = (dict["last_name"] == nil) ? "" : dict["last_name"]!
        let fullName = "\(firstName) \(lastName)"
        let middelName = (dict["middle_name"] == nil) ? "" : dict["middle_name"]!
        let email = (dict["email"]==nil) ? "" : dict["email"]!
        let dob = (dict["birthday"]==nil) ? "" : "\(getMilliesFromDateString(strInput: dict["birthday"] as! String, formate: "MM/dd/yyyy"))"
        let dobDate = (dict["birthday"]==nil) ? "" : "\(getFromDate(strInput: dict["birthday"] as! String, formate: "MM/dd/yyyy"))"
        let aObject = dict["picture"] as! [String : AnyObject]
        let strProfileImg = aObject["data"]!["url"] as? String
        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
        let version = nsObject as! String
        var params:[String: Any] = [:]
        let location = (dict["location"] == nil) ? nil : dict["location"] as? [String : AnyObject]
        
        var retrievedUUID: String? = KeychainWrapper.standard.string(forKey: "iPhoneUUID")
        if retrievedUUID == nil
        {
            KeychainWrapper.standard.set(UIDevice.current.identifierForVendor!.uuidString, forKey: "iPhoneUUID")
            retrievedUUID = UIDevice.current.identifierForVendor!.uuidString
        }
        
        if location != nil
        {
            let city = (location == nil) ? "" : location!["location"]!["city"] as? String
            let country = (location == nil) ? self.currentLocation : location!["location"]!["country"] as? String
            let isoCountryCode = (location == nil) ? "" : location!["location"]!["country_code"] as? String
            let latitude = (location == nil) ? 0 : location!["location"]!["latitude"] as? Double
            let longitude = (location == nil) ? 0 : location!["location"]!["longitude"] as? Double
            
            let iso = (isoCountryCode == nil) ? self.isoCountryCode : isoCountryCode
            
            params = ["userName":fullName,"dateOfBirth":"\(dob)","fbToken":dict["id"]!,"gender":"\(gender)","email":email,"dob":dobDate,"deviceType":"IPHONE","appVersion":version,"country":country!,"city":city!,"latitude":"\(latitude!)","longitude":"\(longitude!)","isoLocation":iso!,"firstName": firstName,"lastName": lastName,"middleName": middelName,"accessToken":currentAccessToken,"uniqueDeviceId":retrievedUUID ?? ""]
        }
        else
        {
            let city = (location == nil) ? "" : location!["location"]!["city"] as? String
            let country = (location == nil) ? "" : location!["location"]!["country"] as? String
            let isoCountryCode = (location == nil) ? "" : location!["location"]!["country_code"] as? String
            let latitude = (location == nil) ? 0 : location!["location"]!["latitude"] as? Double
            let longitude = (location == nil) ? 0 : location!["location"]!["longitude"] as? Double
            
            let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
            let version = nsObject as! String
            params = ["userName":fullName,"dateOfBirth":"\(dob)","fbToken":dict["id"]!,"gender":"\(gender)","email":email,"dob":dobDate,"deviceType":"IPHONE","appVersion":version,"country":"","isoLocation":"","firstName": firstName,"lastName": lastName,"middleName": middelName,"city":"","latitude":"","longitude":"","accessToken":currentAccessToken,"uniqueDeviceId":retrievedUUID ?? ""]
        }
        
        let request = CYLServices.postUserLogin(dict:params)
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLUserObject.init(json: (responseData?["data"])!)
                        userObject.newMessageNotify = responseData?["data"]?["newMessageNotify"].intValue
                        userObject.email = email as? String
                        userObject.isMatchNotification = responseData?["data"]?["matchNotification"].intValue
                        userObject.isEmailNotification = responseData?["data"]?["subscribe"].intValue
                        userObject.isOtherNotifications = responseData?["data"]?["isOtherNotifications"].intValue
                        userObject.userPlanType = (responseData?["data"]?["paymentObj"]["planType"].int != nil) ? responseData?["data"]?["paymentObj"]["planType"].int : 0
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.set(Date(), forKey: "date")
                        userDeflts.set(0, forKey: "rating_count")
                        userDeflts.set(0, forKey: "messageWave_count")
                        if let dictFilter = responseData?["user_filter"]?.dictionaryObject
                        {
                            userDeflts.setValue(dictFilter, forKey: "travel_filter")
                            CYLGlobals.sharedInstance.isFilterApplied=true
                        }
                        else
                        {
                            CYLGlobals.sharedInstance.isFilterApplied=false
                        }
                        
                        userDeflts.synchronize()
                        
                        if CYLGlobals.sharedInstance.usrObject!.progress! == 0
                        {
                            self.getUserProfileImagesFromFacebook(profileImg: strProfileImg!)
                        }
                        else
                        {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.changeMainViewToRootVC()
                        }
                        
                        var strProfileImg = String()
                        if ((CYLGlobals.sharedInstance.usrObject?.userImages)?.count)! > 0
                        {
                            let pictureObject = (CYLGlobals.sharedInstance.usrObject?.userImages)?.first as! [String:AnyObject]
                            strProfileImg = pictureObject["picUrl"] as! String
                        }
                        let g = (dict["gender"] == nil) ? "M" : ((dict["gender"] as! String == "male") ? "M" : "F")
                        var localTimeZoneName: String { return TimeZone.current.identifier }
                        var firstName = String()
                        if let _ = CYLGlobals.sharedInstance.usrObject!.firstName
                        {
                            firstName =  (CYLGlobals.sharedInstance.usrObject!.firstName == "") ? CYLGlobals.sharedInstance.usrObject!.userName!: CYLGlobals.sharedInstance.usrObject!.firstName!
                        }
                        else
                        {
                            firstName = CYLGlobals.sharedInstance.usrObject!.userName!
                        }
                        let profile: Dictionary<String, Any> = [
                            "Name": "\(firstName)",
                            "Identity": CYLGlobals.sharedInstance.usrObject!.id!,
                            "Email": "\(email)",
                            "Phone": "",
                            "Gender": "\(g)",
                            "Employed": "",
                            "Education": "",
                            "Married": "",
                            "DOB": (dict["birthday"]==nil) ? Date() : getDateOfBirthForCleverTap(strInput: dict["birthday"] as! String),
                            "Age": 0,
                            "Tz":"\(localTimeZoneName)",
                            "Photo": "\(strProfileImg)",
                            "MSG-email": true,
                            "MSG-push": true,
                            "MSG-sms": false
                        ]
                        var locationObject: CLLocationCoordinate2D = CLLocationCoordinate2D()
                        if let latitude = responseData?["data"]?["latitude"].doubleValue
                        {
                            locationObject.latitude = latitude
                        }
                        if let longitude = responseData?["data"]?["longitude"].doubleValue
                        {
                            locationObject.longitude = longitude
                        }
                        CleverTap.setLocation(locationObject)
                        CleverTap.sharedInstance()?.profileAddMultiValue("\(self.isoCountryCode)", forKey: "Other Details")
                        CleverTap.sharedInstance()?.profilePush(profile)
                        
                    }
                    else if responseData?["status"]?.stringValue == "fail"
                    {
                        self.actIndicator.stopAnimating()
                        showAlertView(title: "", message: (responseData?["reason"]?.stringValue)!, ref: self)
                        self.btnToFacebook.isUserInteractionEnabled=true
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"User login"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    self.actIndicator.stopAnimating()
                    self.btnToFacebook.isUserInteractionEnabled=true
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
                self.actIndicator.stopAnimating()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"User login"])
                self.btnToFacebook.isUserInteractionEnabled=true
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"User login"])
                self.actIndicator.stopAnimating()
                self.btnToFacebook.isUserInteractionEnabled=true
        })
    }
    func requestToUpdateFacebookImagesOnServer(strUrlAry:String)
    {
        self.actIndicator.stopAnimating()
        self.btnToFacebook.isUserInteractionEnabled=true
        let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLDestinationScreenVC")
        self.navigationController?.pushViewController(destinationVC!, animated: true)
        
        let params:[String: Any] = ["images":strUrlAry]
        let request = CYLServices.postFacebookImageUrl(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryObject
                    if responseData?["status"] as! String != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject
                        userObject?.userImages = responseData?["data"] as! [Any]?
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject!)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"User login"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    self.actIndicator.stopAnimating()
                    self.btnToFacebook.isUserInteractionEnabled=true
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
                self.actIndicator.stopAnimating()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Upload image"])
                self.btnToFacebook.isUserInteractionEnabled=true
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                self.actIndicator.stopAnimating()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Upload image"])
                self.btnToFacebook.isUserInteractionEnabled=true
        })
        
    }
    
    
    
}

