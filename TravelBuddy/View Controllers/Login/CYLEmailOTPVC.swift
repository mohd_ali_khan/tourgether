//
//  CYLEmailOTPVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 27/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLEmailOTPVC: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var txtEmailId: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    
    var appObject:AppDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.appObject = UIApplication.shared.delegate as? AppDelegate
        self.createScreenDesign()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    func createScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=true
        self.btnBack.setImage(UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnBack.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
    }
    
    @IBAction func btnToBackClicked(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_reverse"), object: nil, userInfo: ["index":0])
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton)
    {
        if txtEmailId.text != ""
        {
            if isValidEmail(testStr: txtEmailId.text!)
            {
                KVNProgress.show()
                let params: [String: Any] = ["firstName":self.appObject?.userSignUpDetail!["firstName"] as! String,"emailId":txtEmailId.text!]
                let request = CYLServices.getUserLoginEmailValid(dict:params)
                CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
                    { (response) in
                        KVNProgress.dismiss()
                        if (response?.isValid)!
                        {
                            let responseData = response?.object?.dictionaryValue
                            if responseData?["status"]?.stringValue != "fail"
                            {
                                if responseData?["isEmailExist"]?.boolValue == false
                                {
                                    self.appObject?.userSignUpDetail!["email"] = self.txtEmailId.text!
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_forward"), object: nil, userInfo: ["index":2])
                                }
                                else
                                {
                                    showAlertView(title: "", message: "Email already exists.", ref: self)
                                }
                            }
                            
                        }
                        else
                        {
                            sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Check email"])
                            showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                        }
                        
                },
                                                                          withError:
                    {
                        (error) in
                        KVNProgress.dismiss()
                        showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
                        sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Check email"])
                },
                                                                          andNetworkErr:
                    {
                        (networkFailure) in
                        KVNProgress.dismiss()
                        showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                        sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Check email"])
                })
            }
            else
            {
                showAlertView(title: "", message: "Please enter valid email.", ref: self)
            }
            
        }
    else
    {
    showAlertView(title: "", message: "Email cannot be empty", ref: self)
    }
    }
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
