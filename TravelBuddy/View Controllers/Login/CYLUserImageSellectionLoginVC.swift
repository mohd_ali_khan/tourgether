//
//  CYLUserImageSellectionLoginVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 14/08/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit
import INSPhotoGallery

class CYLUserImageSellectionLoginVC: CYLBaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var collectionUserImage: UICollectionView!
    
    var selectedIndex:Int = Int()
    var aryImages=[Data]()
    var aryProfileImg = [String]()
    @IBOutlet weak var btnBack: UIButton!
    
    var appObject:AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createScreenDesign()
        self.appObject = UIApplication.shared.delegate as? AppDelegate
        
//        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "User images updation")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=true
        self.btnBack.setImage(UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnBack.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
    }
    
    @IBAction func btnToBackClicked(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_reverse"), object: nil, userInfo: ["index":4])
    }
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    @IBAction func btnNextAction(_ sender: UIButton)
    {
            self.appObject?.userSignUpDetail!["userImage"] = self.aryProfileImg
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notify_handle_preselected_segment_forward"), object: nil, userInfo: ["index":6])
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Button Add Delete Action
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnAddImages(_ sender: UIButton)
    {
        let alertController = UIAlertController.init(title: "", message: "Share photos of yourself and travel photos.", preferredStyle: .actionSheet)
        
        let takePhoto = UIAlertAction.init(title: "Camera", style: .default, handler:
        { (action) in
            
            let picker = UIImagePickerController.init()
            picker.delegate=self
            picker.allowsEditing=true
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
            alertController.dismiss(animated: true, completion: nil)
            
        })
        alertController.addAction(takePhoto)
        
        let choosePhoto = UIAlertAction.init(title: "Gallery", style: .default, handler:
        { (action) in
            
            let picker = UIImagePickerController.init()
            picker.delegate=self
            picker.allowsEditing=true
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
            alertController.dismiss(animated: true, completion: nil)
            
        })
        alertController.addAction(choosePhoto)
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler:
            nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func btnDeleteImage(_ sender: UIButton)
    {
        let selectedIndex = sender.tag - 100
        let alert = UIAlertController.init()
        let yesAction = UIAlertAction.init(title: "Delete Photo", style: .destructive, handler: {
            (sender) in
            self.aryProfileImg.remove(at: selectedIndex)
            self.collectionUserImage.reloadData()
        })
        let noAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler:
        {
            (sender) in
            
        })
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Image picker functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info["UIImagePickerControllerEditedImage"] as! UIImage
        let imageData = UIImageJPEGRepresentation(chosenImage, 0.2)!
        aryImages.removeAll()
        aryImages.append(imageData)
        self.requestToUploadProfileImagesLoginFlow()
        picker.dismiss(animated: true, completion: nil)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    
    func requestToUploadProfileImagesLoginFlow()
    {
        KVNProgress.show()
        let params:[String: Any] = ["":""]
        let request = CYLServices.postAllProfileImagesLogin(dict:params,aryImages: self.aryImages)
        CYLServiceMaster.sharedInstance.callDataUploadServiceWithRequest(rqst: request, withResponse: {
            (response) in
        
            KVNProgress.dismiss()
            if (response?.isValid)!
            {
                let responseData = response?.object?.dictionaryObject
                if responseData?["status"] as! String != "fail"
                {
                    KVNProgress.showSuccess()
                    self.aryProfileImg.append(responseData?["imgUrl"] as! String)
                    self.collectionUserImage.reloadData()
                }
                else
                {
                    KVNProgress.dismiss()
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Single upload image"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
            }
            else
            {
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Single upload image"])
                showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
            }
        }, withError: {
            (error) in
            KVNProgress.dismiss()
            sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Single upload image"])
            showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        }, andNetworkErr: {
            (networkFailure) in
            KVNProgress.dismiss()
            sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Single upload image"])
            showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    
}
extension CYLUserImageSellectionLoginVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CYLUserImageCell", for: indexPath) as? CYLUserImageCell{
            
            cell.btnDeleteImage.tag = indexPath.row + 100
            cell.imgUserImage.tag = indexPath.row
            if indexPath.row <= aryProfileImg.count - 1
            {
                if aryProfileImg[indexPath.row] != ""
                {
                    cell.imgUserImage.setShowActivityIndicator(true)
                    cell.imgUserImage.setIndicatorStyle(.whiteLarge)
                    cell.imgUserImage.sd_setImage(with: URL(string: (aryProfileImg[indexPath.row])), placeholderImage: UIImage(named: ""), options: [], completed: { (loadedImage, error, cacheType, url) in
                        
                        cell.imgUserImage.sd_cancelCurrentImageLoad()
                        if error != nil {
                            print("Error code: \(error!.localizedDescription)")
                        } else {
                            cell.imgUserImage.image = loadedImage
                        }
                    })
                }
                cell.btnAddImage.isHidden = true
                cell.imgUserImage.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
                cell.btnDeleteImage.isHidden = false
                
            }
            else if indexPath.row > aryProfileImg.count - 1
            {
                cell.imgUserImage.image = UIImage(named: "")
                cell.btnAddImage.isHidden = false
                cell.imgUserImage.backgroundColor = UIColor(red: 187/255.0, green: 187/255.0, blue: 187/255.0, alpha: 1.0)
                cell.btnDeleteImage.isHidden = true
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (self.view.frame.width-40)/2, height: (self.view.frame.width-45)/2)
    }
    
}

