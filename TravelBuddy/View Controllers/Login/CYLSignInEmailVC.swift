//
//  CYLEmailIdOTPVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 27/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class CYLSignInEmailVC: UIViewController, UIGestureRecognizerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var txtEmailId: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var vwPopupForgotPassword: UIControl!
    
    @IBOutlet weak var txtFieldForgotEmail: TextField!
    @IBOutlet weak var txtFieldForNewPass: TextField!
    @IBOutlet weak var txtFieldForNewPassConf: TextField!
    
    @IBOutlet weak var btnJoinTourgether: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var constLoginViewH1: NSLayoutConstraint!
    @IBOutlet weak var constLoginViewH2: NSLayoutConstraint!
    @IBOutlet weak var constLoginViewH3: NSLayoutConstraint!
    @IBOutlet weak var constLoginViewH4: NSLayoutConstraint!
    @IBOutlet weak var constLoginViewH5: NSLayoutConstraint!
    @IBOutlet weak var imgBack: UIImageView!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createScreenDesign()
        self.txtPassword.delegate = self
        self.txtFieldForNewPass.delegate = self
        self.txtFieldForNewPassConf.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.view.frame.size.height == 480
        {
            self.constLoginViewH1.constant = 10
            self.constLoginViewH2.constant = 10
            self.constLoginViewH3.constant = 10
            self.constLoginViewH4.constant = 10
            self.constLoginViewH5.constant = 10
            imgBack.isHidden = true
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden=true
        self.btnBack.setImage(UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnBack.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=true
        self.btnJoinTourgether.layer.borderWidth = 1
        self.btnJoinTourgether.layer.borderColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0).cgColor
    }
    //MARK:- UIButton Action
    @IBAction func btnToBackClicked(_ sender: UIButton) {
        _=self.navigationController?.popViewController(animated: true)
    }
    @IBAction func vwPopupHidden(_ sender: UIControl)
    {
        vwPopupForgotPassword.isHidden = true
        self.view.endEditing(true)
    }
    @IBAction func btnSignInSignUpForgotPassClick(_ sender: UIButton)
    {
        if sender.tag == 101  //ForGot Pass
        {
            self.vwPopupForgotPassword.isHidden = false
        }
        else if sender.tag == 102 //Sign In
        {
            if txtEmailId.text != ""
            {
                if txtPassword.text != ""
                {
                    if (txtPassword.text?.count)! >= 8
                    {
                        self.userSignInFromUserId()
                    }
                    else
                    {
                        showAlertView(title: "", message: "Minimum 8 character", ref: self)
                    }
                }
                else
                {
                    showAlertView(title: "", message: "Password cannot be empty.", ref: self)
                }
            }
            else
            {
                showAlertView(title: "", message: "Email cannot be empty.", ref: self)
            }
        }
        else if sender.tag == 103 //Sign Up
        {
            let firstLastNameVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLPageViewSignInController") as! CYLPageViewSignInController
            self.navigationController?.pushViewController(firstLastNameVC, animated: true)
        }
    }
    @IBAction func btnForgotPasswordSubmitAction(_ sender: UIButton)
    {
        if txtFieldForgotEmail.text != ""
        {
            if txtFieldForNewPass.text != ""
            {
                if (txtFieldForNewPass.text?.count)! >= 8
                {
                    if txtFieldForNewPassConf.text != ""
                    {
                        if (txtFieldForNewPassConf.text?.count)! >= 8
                        {
                            if txtFieldForNewPass.text == txtFieldForNewPassConf.text
                            {
                                self.forgotPassWord()
                            }
                            else
                            {
                                showAlertView(title: "", message: "Password and confirm password should be same.", ref: self)
                            }
                        }
                        else
                        {
                            showAlertView(title: "", message: "Minimum 8 character", ref: self)
                        }
                        
                    }
                    else
                    {
                        showAlertView(title: "", message: "Confirm password cannot be empty.", ref: self)
                    }
                }
                else
                {
                    showAlertView(title: "", message: "Minimum 8 character", ref: self)
                }
                
            }
            else
            {
                showAlertView(title: "", message: "Password cannot be empty.", ref: self)
            }
        }
        else
        {
            showAlertView(title: "", message: "Email cannot be empty.", ref: self)
        }
    }
    @IBAction func btnShowPasswordAction(_ sender: UIButton)
    {
        if sender.tag == 201
        {
            sender.isSelected = !sender.isSelected
            if sender.isSelected
            {
                txtPassword.isSecureTextEntry = false
            }
            else
            {
                txtPassword.isSecureTextEntry = true
            }
        }
        else if sender.tag == 202
        {
            sender.isSelected = !sender.isSelected
            if sender.isSelected
            {
                txtFieldForNewPass.isSecureTextEntry = false
            }
            else
            {
                txtFieldForNewPass.isSecureTextEntry = true
            }
        }
        else
        {
            sender.isSelected = !sender.isSelected
            if sender.isSelected
            {
                txtFieldForNewPassConf.isSecureTextEntry = false
            }
            else
            {
                txtFieldForNewPassConf.isSecureTextEntry = true
            }
        }
    }
    
    //MARK:- API Calls
    func userSignInFromUserId()
    {
        KVNProgress.show()
        var retrievedUUID: String? = KeychainWrapper.standard.string(forKey: "iPhoneUUID")
        if retrievedUUID == nil
        {
            KeychainWrapper.standard.set(UIDevice.current.identifierForVendor!.uuidString, forKey: "iPhoneUUID")
            retrievedUUID = UIDevice.current.identifierForVendor!.uuidString
        }
        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
        let version = nsObject as! String

        let params:[String: Any] = ["email":self.txtEmailId.text!,"password": txtPassword.text!,"uniqueDeviceId":retrievedUUID ?? "","deviceType":"IPHONE","appVersion":version,"gcmId":"","deviceId":""]
        print(params)
        let request = CYLServices.postUserSigninFromEmail(dict:params)
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLUserObject.init(json: (responseData?["data"])!)
                        userObject.newMessageNotify = responseData?["data"]?["newMessageNotify"].intValue
                        userObject.email = self.txtEmailId.text!
                        userObject.isMatchNotification = responseData?["data"]?["matchNotification"].intValue
                        userObject.isEmailNotification = responseData?["data"]?["subscribe"].intValue
                        userObject.isOtherNotifications = responseData?["data"]?["isOtherNotifications"].intValue
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.set(Date(), forKey: "date")
                        userDeflts.set(0, forKey: "rating_count")
                        userDeflts.set(0, forKey: "messageWave_count")
                        if let dictFilter = responseData?["user_filter"]?.dictionaryObject
                        {
                            userDeflts.setValue(dictFilter, forKey: "travel_filter")
                            CYLGlobals.sharedInstance.isFilterApplied=true
                        }
                        else
                        {
                            CYLGlobals.sharedInstance.isFilterApplied=false
                        }
                        
                        userDeflts.synchronize()
                        
                        var strProfileImg = String()
                         if ((CYLGlobals.sharedInstance.usrObject?.userImages)?.count)! > 0
                         {
                         let pictureObject = (CYLGlobals.sharedInstance.usrObject?.userImages)?.first as! [String:AnyObject]
                         strProfileImg = pictureObject["picUrl"] as! String
                         }
                         let g = (CYLGlobals.sharedInstance.usrObject!.gender! == 0) ? "M" : "F"
                         var localTimeZoneName: String { return TimeZone.current.identifier }
                         var firstName = String()
                         if let _ = CYLGlobals.sharedInstance.usrObject!.firstName
                         {
                         firstName =  (CYLGlobals.sharedInstance.usrObject!.firstName == "") ? CYLGlobals.sharedInstance.usrObject!.userName!: CYLGlobals.sharedInstance.usrObject!.firstName!
                         }
                         else
                         {
                         firstName = CYLGlobals.sharedInstance.usrObject!.userName!
                         }
                         let profile: Dictionary<String, Any> = [
                         "Name": "\(firstName)",
                         "Identity": CYLGlobals.sharedInstance.usrObject!.id!,
                         "Email": "\(CYLGlobals.sharedInstance.usrObject!.email!)",
                         "Phone": "",
                         "Gender": "\(g)",
                         "Employed": "",
                         "Education": "",
                         "Married": "",
                         "DOB": (CYLGlobals.sharedInstance.usrObject?.dateOfBirth==nil) ? Date() : convertTimestampIntoDateString(date: Double((CYLGlobals.sharedInstance.usrObject?.dateOfBirth)!), format: "dd/MM/yyyy"),
                         "Age": 0,
                         "Tz":"\(localTimeZoneName)",
                         "Photo": "\(strProfileImg)",
                         "MSG-email": true,
                         "MSG-push": true,
                         "MSG-sms": false
                         ]
                         var locationObject: CLLocationCoordinate2D = CLLocationCoordinate2D()
                         if let latitude = responseData?["data"]?["latitude"].doubleValue
                         {
                         locationObject.latitude = latitude
                         }
                         if let longitude = responseData?["data"]?["longitude"].doubleValue
                         {
                         locationObject.longitude = longitude
                         }
                         CleverTap.setLocation(locationObject)
                        CleverTap.sharedInstance()?.profileAddMultiValue("\(CYLGlobals.sharedInstance.usrObject!.isoLocation!)", forKey: "Other Details")
                         CleverTap.sharedInstance()?.profilePush(profile)
                        
                        var rootVC:UIViewController?
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        switch CYLGlobals.sharedInstance.usrObject!.progress!
                        {
                        case 0:
                            //push to destination screen here
                            rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLDestinationScreenVC") as! CYLDestinationScreenVC
                        case 1:
                            //push to destination screen here
                            if CYLGlobals.sharedInstance.usrObject?.cities?.count != 0 || CYLGlobals.sharedInstance.usrObject?.countries?.count != 0
                            {
                                rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLCalendarScreenVC") as! CYLCalendarScreenVC
                            }
                            else
                            {
                                rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
                            }
                        case 2:
                            //push to home screen screen here
                            rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
                        default:
                            rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
                        }
                        self.navigationController?.pushViewController(rootVC!, animated: true)
                        
                    }
                    else if responseData?["status"]?.stringValue == "fail"
                    {
                        showAlertView(title: "", message: (responseData?["reason"]?.stringValue)!, ref: self)
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Email login"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Email login"])
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Email login"])
        })
        
    }
    func forgotPassWord()
    {
        KVNProgress.show()
        let params:[String: Any] = ["email":self.txtFieldForgotEmail.text!,"newPassword": txtFieldForNewPass.text!]
        let request = CYLServices.postUserForgotPassword(dict:params)
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        self.vwPopupForgotPassword.isHidden = true
                        showAlertView(title: "", message: "Email sent to \(self.txtFieldForgotEmail.text ?? "your id"). Please follow the instruction in the email to reset your password", ref: self)
                    }
                    else if responseData?["status"]?.stringValue == "fail"
                    {
                        showAlertView(title: "", message: (responseData?["reason"]?.stringValue)!, ref: self)
                    }

                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Forgot password"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }

        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Forgot password"])
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Forgot password"])
        })
    }
    
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == " ") {
            return false
        }
        return true
    }
    
}
