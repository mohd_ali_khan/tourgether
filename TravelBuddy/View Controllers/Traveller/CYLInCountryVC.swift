//
//  CYLInCountryVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 18/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit
import SDWebImage

class CYLInCountryVC: CYLBaseViewController, UINavigationControllerDelegate, CLLocationManagerDelegate,CYLIAPReloadAppData
{
    var waveFirstSecondClick:Int = Int()
    var waveButtinClick:UIButton = UIButton()
    var vwFirstWave: UIView = UIView()
    
    @IBOutlet weak var vwNoMatches: UIView!
    @IBOutlet weak var lblNoMatch: UILabel!
    @IBOutlet weak var tblTraveller: UITableView!
    @IBOutlet weak var btnEditLocation: UIButton!
    var footerView:UIView?
    //for travellers list
    var aryInCountryTravellers = [JSON]()
    //    var aryMatchedInCountryTravellers = [JSON]()
    var aryUnmatchedInCountryTravellers = [JSON]()
    var dictCurrentLocation = [String:JSON]()
    //for paging
    let pageSize = 20
    let preloadMargin = 1
    var lastLoadedPage = 0
    var maxResults = 0
    var vwLoader: UIView = UIView()
    var vwEmptyState: UIView = UIView()
    var showWaveOption:Bool = Bool()
    let locationManager = CLLocationManager()
    var btnRefreshLocation:UIButton = UIButton()
    var lblCountryName: UILabel = UILabel()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loaderGifImage()
        NotificationCenter.default.removeObserver(NSNotification.Name(rawValue: "update_list_for_applied_filter_near"))
        NotificationCenter.default.addObserver(self, selector: #selector(handleFilterAppliedConditionsPush(notification:)), name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: nil)
        NotificationCenter.default.removeObserver(NSNotification.Name(rawValue: "update_list_for_applied_to_top_near"))
        NotificationCenter.default.addObserver(self, selector: #selector(handleTopScrollNear(notification:)), name: NSNotification.Name(rawValue: "update_list_for_applied_to_top_near"), object: nil)
        
        lastLoadedPage = 0
        self.aryInCountryTravellers.removeAll()
        requestToGetTravellersList(page: 0, str: "")
        self.creatingMyCountryScreenDesign()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Search travellers near me")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loaderGifImage()
    {
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwLoader = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwLoader.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
            window.addSubview(vwLoader)
            let img = UIImageView(frame: CGRect(x: vwLoader.frame.width/2-40, y: vwLoader.frame.height/2-40, width: 80, height: 80))
            img.image = UIImage.sd_animatedGIF(with: imageData!)
            vwLoader.addSubview(img)
            vwLoader.isHidden = true
        }
    }
    
    @objc func handleFilterAppliedConditionsPush(notification: NSNotification)
    {
        print(notification.userInfo?["name"] ?? "")
        let notifStr = notification.userInfo?["name"] ?? ""
        handleFilterAppliedConditions(str: notifStr as! String)
    }
    @objc func handleTopScrollNear(notification: NSNotification)
    {
        self.tblTraveller.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    func handleFilterAppliedConditions(str: String)
    {
        self.lastLoadedPage = 0
        self.aryInCountryTravellers.removeAll()
        requestToGetTravellersList(page: 0, str: str)
        tblTraveller.setContentOffset(CGPoint.zero, animated: true)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func creatingMyCountryScreenDesign()
    {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        if #available(iOS 10.0, *)
        {
            self.tblTraveller.refreshControl = refreshControl
        }
        else
        {
            self.tblTraveller.backgroundView = refreshControl
        }
    }
    @objc func refresh(_ refreshControl: UIRefreshControl)
    {
        lastLoadedPage = 0
        self.aryInCountryTravellers.removeAll()
        self.requestToGetTravellersList(page: 0, str: "")
        refreshControl.endRefreshing()
    }
    func setupActivityIndicatorInTableViewFooter()
    {
        self.footerView = UIView.init(frame: CGRect(x:0,y:0,width:self.screenWidth!,height:40))
        
        let actIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        actIndicator.tag = 10
        actIndicator.frame = CGRect(x:0.0, y:0.0, width:20.0, height:20.0)
        actIndicator.center.x = (self.footerView?.center.x)!
        actIndicator.center.y = (self.footerView?.center.y)!
        actIndicator.hidesWhenStopped = true
        self.footerView?.addSubview(actIndicator)
        actIndicator.startAnimating()
        self.tblTraveller.tableFooterView = self.footerView
    }
    func handlePagingForInCountryTravellerTable()
    {
        if self.maxResults > 0
        {
            lastLoadedPage += 1
            self.requestToGetTravellersList(page: lastLoadedPage, str: "")
        }
    }
    func separateArrayForMatchedAndUnmatchedUsers()
    {
        var aryUnmatched = [JSON]()
        for object in self.aryInCountryTravellers
        {
            aryUnmatched.append(object)
        }
        self.createDisplaArrayForUnmatchedTravellers(aryUnmatched: aryUnmatched)
    }
    
    
    func createDisplaArrayForUnmatchedTravellers(aryUnmatched:[JSON])
    {
        for i in 0..<aryUnmatched.count/2
        {
            let dict = JSON.init(["first":aryUnmatched[2*i],"second":aryUnmatched[(2*i)+1]])
            self.aryUnmatchedInCountryTravellers.append(dict)
        }
        if aryUnmatched.count%2 != 0
        {
            let dict = JSON.init(["first":aryUnmatched.last])
            self.aryUnmatchedInCountryTravellers.append(dict)
        }
        let view = UIView.init(frame: CGRect(x: 0.0, y: 0.0, width: self.tblTraveller.bounds.size.width, height: CGFloat.leastNormalMagnitude))
        self.tblTraveller.tableHeaderView = view
        self.tblTraveller.reloadData()
    }
    func reloadCollectionData()
    {
        //        self.aryBastMatchedInCountryTravellers.removeAll()
        self.aryUnmatchedInCountryTravellers.removeAll()
        //        self.aryMatchedInCountryTravellers.removeAll()
        self.separateArrayForMatchedAndUnmatchedUsers()
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom Button Click Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToUserProfileFirstClicked(_ sender: UIButton)
    {
        //        let travellerId = (sender.tag/1000 == 2) ? self.aryBastMatchedInCountryTravellers[sender.tag-2000]["first"]["id"].intValue : (sender.tag/1000 == 3) ? self.aryMatchedInCountryTravellers[sender.tag-3000]["first"]["id"].intValue :
        let travellerId = self.aryUnmatchedInCountryTravellers[sender.tag-4000]["first"]["id"].intValue
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserProfileVC") as! CYLUserProfileVC
        userProfileVC.travellerId = travellerId
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    @IBAction func btnToUserProfileSecondCLicked(_ sender: UIButton)
    {
        //        let travellerId = (sender.tag/1000 == 2) ? self.aryBastMatchedInCountryTravellers[sender.tag-2000]["second"]["id"].intValue : (sender.tag/1000 == 3) ? self.aryMatchedInCountryTravellers[sender.tag-3000]["second"]["id"].intValue :
        let travellerId = self.aryUnmatchedInCountryTravellers[sender.tag-4000]["second"]["id"].intValue
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserProfileVC") as! CYLUserProfileVC
        userProfileVC.travellerId = travellerId
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    @IBAction func btnToSayWaveFirstClick(_ sender: UIButton)
    {
        if CYLGlobals.sharedInstance.usrObject!.wavesRemaining != 0
        {
            self.waveButtinClick = sender
            self.waveFirstSecondClick = 1
            if UserDefaults.standard.value(forKey: "messageWave_count") as! Int == 0
            {
                self.createFirstWavePopUp()
            }
            else
            {
                waveClickStartAnimationFirstCountry()
            }
        }
        else
        {
            self.waveLimitExcedTodayNearMe()
            self.waveFirstSecondClick = 1
        }
    }
    
    func waveClickStartAnimationFirstCountry()
    {
        let moveX = (self.view.frame.size.width == 320) ? 80:100
        let views = self.waveButtinClick.superview
        
        for view in views!.subviews
        {
            if (view.isKind(of: UIImageView.self))
            {
                let imageView: UIImageView = view as! UIImageView
                
                UIView.animate(withDuration: 2.5, animations: {
                    views?.alpha = 0.0
                })
                imageView.run(.sequence([
                    .moveX(CGFloat(-moveX), widht: 40, height: 40),
                    .swing(),
                    .moveX(CGFloat(moveX), widht: -40, height: -40)
                    
                    ]), completion: {
                        
                        
                        UIView.animate(withDuration: 1.0, animations: {
                            views?.alpha = 1.0
                            
                            var userDetail: JSON = self.aryUnmatchedInCountryTravellers[self.waveButtinClick.tag-4000]
                            userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["first"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["first"]["wave"]["waveEmoticonType"].intValue)
                            
                            self.aryUnmatchedInCountryTravellers.remove(at: self.waveButtinClick.tag-4000)
                            
                            self.aryUnmatchedInCountryTravellers.insert(userDetail, at: self.waveButtinClick.tag-4000)
                            
                            let waveObject: JSON = self.aryUnmatchedInCountryTravellers[self.waveButtinClick.tag-4000]["first"]["wave"]
                            let otherUserId = self.aryUnmatchedInCountryTravellers[self.waveButtinClick.tag-4000]["first"]["id"].intValue
                            let index = self.aryInCountryTravellers.index(where: {$0["id"].intValue == otherUserId})
                            self.aryInCountryTravellers[index!]["wave"] = waveObject
                            
                            DispatchQueue.main.async {
                                let indexPath = IndexPath(item: self.waveButtinClick.tag-4000, section: 0)
                                self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                            }
                            self.requestToSendWaveForTravelerNearMeApi(index: self.waveButtinClick.tag-4000, arr: self.aryUnmatchedInCountryTravellers, cell: "first", section: 0)
                            
                        })
                })
            }
        }
    }
    
    func createFirstWavePopUp()
    {
        var firstName = String()
        var gender = String()
        let userDetail: JSON = self.aryUnmatchedInCountryTravellers[self.waveButtinClick.tag-4000]
        if self.waveFirstSecondClick == 1
        {
            firstName = userDetail["first"]["firstName"].stringValue != "" ? userDetail["first"]["firstName"].stringValue : userDetail["first"]["userName"].stringValue
            gender = userDetail["first"]["gender"].intValue == 0 ? "he":"she"
        }else
        {
            firstName = userDetail["second"]["firstName"].stringValue != "" ? userDetail["second"]["firstName"].stringValue : userDetail["second"]["userName"].stringValue
            gender = userDetail["second"]["gender"].intValue == 0 ? "he":"she"
        }
        
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwFirstWave = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwFirstWave.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.8)
            
            let popUpView = UIView(frame: CGRect(x: 40, y: vwFirstWave.frame.height/2-145, width: vwLoader.frame.width-80, height: 290))
            popUpView.backgroundColor = UIColor.white
            popUpView.layer.cornerRadius = 8
            popUpView.layer.masksToBounds = true
            vwFirstWave.addSubview(popUpView)
            
            let lblName = UILabel(frame: CGRect(x: 20, y: 40, width: popUpView.frame.width-40, height: 30))
            lblName.text = "Wanna wave at \(firstName)?"
            lblName.font = UIFont(name: "Pacifico-Bold", size: 24.0)
            lblName.textColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            lblName.adjustsFontSizeToFitWidth = true
            lblName.textAlignment = .center
            popUpView.addSubview(lblName)
            
            let lblTitle = UILabel(frame: CGRect(x: 20, y: lblName.frame.origin.y+lblName.frame.size.height+30, width: popUpView.frame.width-40, height: 30))
            lblTitle.text = "Secretly wave at \(gender == "he" ? "him":"her"). If \(gender) also wave at you, you'll be able to chat"
            lblTitle.font = UIFont(name: "Comfortaa-Bold", size: 16.0)
            lblTitle.textColor = UIColor.lightGray
            lblTitle.numberOfLines = 0
            lblTitle.textAlignment = .center
            lblTitle.sizeToFit()
            popUpView.addSubview(lblTitle)
            
            let btnCancle: UIButton = UIButton(frame: CGRect(x: 0, y: popUpView.frame.size.height-45, width: popUpView.frame.width, height: 45))
            btnCancle.addTarget(self, action: #selector(btnFirstWaveAction(_:)), for: .touchUpInside)
            btnCancle.titleLabel?.textAlignment = .center
            btnCancle.tag = 1002
            btnCancle.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 16)
            btnCancle.setTitleColor(UIColor.lightGray, for: .normal)
            btnCancle.setTitle("Cancel", for: UIControlState.normal)
            popUpView.addSubview(btnCancle)
            
            let vwLine1 = UIView(frame: CGRect(x: 0, y: btnCancle.frame.origin.y-1, width: popUpView.frame.width, height: 1))
            vwLine1.backgroundColor = UIColor.lightGray
            popUpView.addSubview(vwLine1)
            
            let btnWaveSecretly: UIButton = UIButton(frame: CGRect(x: 0, y: vwLine1.frame.origin.y-45, width: popUpView.frame.width, height: 45))
            btnWaveSecretly.addTarget(self, action: #selector(btnFirstWaveAction(_:)), for: .touchUpInside)
            btnWaveSecretly.tag = 1001
            popUpView.addSubview(btnWaveSecretly)
            
            let imgHand = UIImageView(frame: CGRect(x: popUpView.frame.width/2-60, y: btnWaveSecretly.frame.origin.y+12, width: 20, height: 20))
            imgHand.image = UIImage(named: "ic_hand.png")
            imgHand.image = imgHand.image!.withRenderingMode(.alwaysTemplate)
            imgHand.tintColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            popUpView.addSubview(imgHand)
            
            let lblWaveSecretly = UILabel(frame: CGRect(x: imgHand.frame.size.width+imgHand.frame.origin.x+5, y: btnWaveSecretly.frame.origin.y, width: 150, height: 45))
            lblWaveSecretly.text = "Wave Secretly"
            lblWaveSecretly.font = UIFont(name: "Comfortaa-Bold", size: 16.0)
            lblWaveSecretly.textColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            lblWaveSecretly.textAlignment = .left
            popUpView.addSubview(lblWaveSecretly)
            
            let vwLine2 = UIView(frame: CGRect(x: 0, y: btnWaveSecretly.frame.origin.y-1, width: popUpView.frame.width, height: 1))
            vwLine2.backgroundColor = UIColor.lightGray
            popUpView.addSubview(vwLine2)
            
            let imgLogo = UIImageView(frame: CGRect(x: vwFirstWave.frame.width/2-40, y: popUpView.frame.origin.y-40, width: 80, height: 80))
            imgLogo.image = UIImage(named: "pay_wave")
            imgLogo.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
            vwFirstWave.addSubview(imgLogo)
            window.addSubview(vwFirstWave)
        }
    }
    
    @IBAction func btnToSayWaveSecondClick(_ sender: UIButton)
    {
        if CYLGlobals.sharedInstance.usrObject!.wavesRemaining != 0
        {
            self.waveButtinClick = sender
            self.waveFirstSecondClick = 2
            if UserDefaults.standard.value(forKey: "messageWave_count") as! Int == 0
            {
                self.createFirstWavePopUp()
            }
            else
            {
                waveClickStartAnimationSecondCountry()
            }
        }
        else
        {
            self.waveLimitExcedTodayNearMe()
            self.waveFirstSecondClick = 2
        }
    }
    func waveClickStartAnimationSecondCountry()
    {
        let moveX = (self.view.frame.size.width == 320) ? 80:100
        let views = self.waveButtinClick.superview
        
        for view in views!.subviews
        {
            if (view.isKind(of: UIImageView.self))
            {
                let imageView: UIImageView = view as! UIImageView
                UIView.animate(withDuration: 2.5, animations: {
                    views?.alpha = 0.0
                })
                imageView.run(.sequence([
                    .moveX(CGFloat(-moveX), widht: 40, height: 40),
                    .swing(),
                    .moveX(CGFloat(moveX), widht: -40, height: -40)
                    
                    ]), completion: {
                        
                        UIView.animate(withDuration: 1.0, animations: {
                            views?.alpha = 1.0
                            
                            var userDetail: JSON = self.aryUnmatchedInCountryTravellers[self.waveButtinClick.tag-4000]
                            userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["second"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["second"]["wave"]["waveEmoticonType"].intValue)
                            
                            self.aryUnmatchedInCountryTravellers.remove(at: self.waveButtinClick.tag-4000)
                            
                            self.aryUnmatchedInCountryTravellers.insert(userDetail, at: self.waveButtinClick.tag-4000)
                            
                            let waveObject: JSON = self.aryUnmatchedInCountryTravellers[self.waveButtinClick.tag-4000]["second"]["wave"]
                            let otherUserId = self.aryUnmatchedInCountryTravellers[self.waveButtinClick.tag-4000]["second"]["id"].intValue
                            let index = self.aryInCountryTravellers.index(where: {$0["id"].intValue == otherUserId})
                            self.aryInCountryTravellers[index!]["wave"] = waveObject
                            
                            DispatchQueue.main.async {
                                let indexPath = IndexPath(item: self.waveButtinClick.tag-4000, section: 0)
                                self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                            }
                            self.requestToSendWaveForTravelerNearMeApi(index: self.waveButtinClick.tag-4000, arr: self.aryUnmatchedInCountryTravellers, cell: "second", section: 0)
                            
                        })
                })
            }
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToSendWaveForTravelerNearMeApi(index: Int, arr: [JSON], cell: String, section: Int)
    {
        let receiverId = arr[index][cell]["id"].intValue
        let params:[String: Any] = ["otherUserId":"\(receiverId)","waveCallingLocation":"NEAR_ME"]
        let request = CYLServices.postWaveMessageForServer(dict: params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in

                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue

                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                                            userObject.wavesRemaining = responseData?["wavesRemaining"]?.intValue
                                            userObject.wavesMessage = responseData?["message"]?.stringValue
                                            CYLGlobals.sharedInstance.usrObject = userObject
                                            let userDeflts = UserDefaults.standard
                                            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                                                userDeflts.set(encodedData, forKey: "userObject")
                                                userDeflts.synchronize()
                        UserDefaults.standard.set(1, forKey: "messageWave_count")
    
                        let waveEmoticonType = arr[index]["cell"]["wave"]["waveEmoticonType"].intValue == 1 ? "wave sent" : "wave sent back"
                        var cleverTapEventData = [String:Any]()
                        let aryCities = arr[index][cell]["cities"].arrayValue
                        let aryCountries = arr[index][cell]["countries"].arrayValue
                        var detinationType: String!
                        if aryCities.count != 0{
                            detinationType = "City"
                            cleverTapEventData["DestinationCount"] = aryCities.count
                        }
                        else if aryCountries.count != 0{
                            detinationType = "Country"
                            cleverTapEventData["DestinationCount"] = aryCountries.count
                        }
                        else{
                            detinationType = "No Plan"
                            cleverTapEventData["destinationCount"] = 0
                        }
                        cleverTapEventData["DetinationType"] = detinationType
                        cleverTapEventData["IsGenderMatched"] = arr[index][cell]["isGenderMatched"].bool
                        cleverTapEventData["IsCountryMatched"] = arr[index][cell]["isCountryMatched"].bool
    
                        let liveInDestionation = arr[index][cell]["livesInDestinationCountry"].boolValue
    
                        cleverTapEventData["UserId"] = arr[index][cell]["id"].stringValue
                        cleverTapEventData["UserName"] = arr[index][cell]["userName"].stringValue
                        cleverTapEventData["Age"] = arr[index][cell]["age"].stringValue
                        cleverTapEventData["Isolocation"] = arr[index][cell]["isoLocation"].stringValue
                        let g = (arr[index][cell]["gender"].intValue == 0) ? "Male" : "Female"
                        cleverTapEventData["Gender"] = g
                        cleverTapEventData["IsMatched"] = arr[index][cell]["is_matched"].boolValue
                        cleverTapEventData["isOnline"] = arr[index][cell]["showOnline"].boolValue
                        cleverTapEventData["page type"] = "user list"
                        cleverTapEventData["wave type"] = waveEmoticonType
                        cleverTapEventData["LivesInDestinationCountry"] = liveInDestionation
                        sentEventPropertyOnCleverTap("Wave clicked", _property: cleverTapEventData)
                        
                        if arr[index][cell]["wave"]["waveEmoticonType"].intValue == 3
                        {
                            self.view.layoutIfNeeded()
                            let matchedPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLMatchedPopUpVC") as! CYLMatchedPopUpVC
                            matchedPopup.imgRecever = arr[index][cell]["userPicUrl"].stringValue
                            matchedPopup.id = arr[index][cell]["id"].intValue
                            matchedPopup.userName = arr[index][cell]["firstName"].stringValue
                            matchedPopup.modalPresentationStyle = .overCurrentContext
                            matchedPopup.modalTransitionStyle = .crossDissolve
                            matchedPopup.moveChatDetailControllerDelegate = self
                            self.navigationController?.present(matchedPopup, animated: true, completion: nil)
                        }
                        
                    }
                    else
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                                            userObject.wavesMessage = responseData?["message"]?.stringValue
                                            CYLGlobals.sharedInstance.usrObject = userObject
                                            let userDeflts = UserDefaults.standard
                                            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                                                userDeflts.set(encodedData, forKey: "userObject")
                                                userDeflts.synchronize()
                        DispatchQueue.main.async {
                            self.waveApiFailRequestInCountry(index: index, arr: arr, cell: cell, section: section)
                        }
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.waveApiFailRequestInCountry(index: index, arr: arr, cell: cell, section: section)
                    }
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Wave click near me"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
        },
                                                                  withError:
            {
                (error) in
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Wave click near me"])
                DispatchQueue.main.async {
                    self.waveApiFailRequestInCountry(index: index, arr: arr, cell: cell, section: section)
                }
                self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
        },
                                                                  andNetworkErr: {
                                                                    (networkFailure) in
                                                                    sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Wave click near me"])
                                                                    DispatchQueue.main.async {
                                                                        self.waveApiFailRequestInCountry(index: index, arr: arr, cell: cell, section: section)
                                                                    }
                                                                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    
    func waveApiFailRequestInCountry(index: Int, arr: [JSON], cell: String, section: Int)
    {
        if cell == "first"
        {
            var userDetail: JSON = self.aryUnmatchedInCountryTravellers[index]
            userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue :  JSON(0).intValue)
            
            self.aryUnmatchedInCountryTravellers.remove(at: index)
            
            self.aryUnmatchedInCountryTravellers.insert(userDetail, at: index)
            
            let waveObject: JSON = self.aryUnmatchedInCountryTravellers[index]["first"]["wave"]
            let otherUserId = self.aryUnmatchedInCountryTravellers[index]["first"]["id"].intValue
            let index = self.aryInCountryTravellers.index(where: {$0["id"].intValue == otherUserId})
            self.aryInCountryTravellers[index!]["wave"] = waveObject
        }
        else
        {
            var userDetail: JSON = self.aryUnmatchedInCountryTravellers[index]
            userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue : JSON(0).intValue)
            
            self.aryUnmatchedInCountryTravellers.remove(at: index)
            
            self.aryUnmatchedInCountryTravellers.insert(userDetail, at: index)
            
            let waveObject: JSON = self.aryUnmatchedInCountryTravellers[index]["second"]["wave"]
            let otherUserId = self.aryUnmatchedInCountryTravellers[index]["second"]["id"].intValue
            let index = self.aryInCountryTravellers.index(where: {$0["id"].intValue == otherUserId})
            self.aryInCountryTravellers[index!]["wave"] = waveObject
        }
        let indexPath = IndexPath(item: index, section: section)
        self.tblTraveller.reloadRows(at: [indexPath], with: .none)
    }
    func waveLimitExcedTodayNearMe()
    {
        if CYLGlobals.sharedInstance.usrObject?.userPlanType == 0
        {
            let paymentPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserPaymentVC") as! CYLUserPaymentVC
            paymentPopup.messageLimitExceed = "wave"
            paymentPopup.delegateMessageReload = self
            self.navigationController?.pushViewController(paymentPopup, animated: false)
        }
        else
        {
            let text = showAlertTitleMessageFont(strTitle: "Wave Limit Exceeded", strMessage: CYLGlobals.sharedInstance.usrObject!.wavesMessage ?? "")
            let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            
            alert.setValue(text.title, forKey: "attributedTitle")
            alert.setValue(text.message, forKey: "attributedMessage")
            
            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                (sender) in
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func reloadMessageReceiverListDelegatePayment() {
        
    }
    
    func requestToGetTravellersList(page:Int,str: String)
    {
        lastLoadedPage = page
        if str == ""
        {
            self.showLoadingView()
        }
        let params:[String: Any] = ["record":"\(pageSize)","start":"\(page*pageSize)"]
        let request = CYLServices.getTravellersListNearMe(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                self.hideLoadingView()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        self.maxResults = (responseData?["total_records"]?.intValue)!
                        self.aryInCountryTravellers.append(contentsOf: (responseData?["data"]?.arrayValue)!)
                        
                        if (responseData?["location"] != nil)
                        {
                            if responseData?["location"] != JSON.null
                            {
                                self.dictCurrentLocation = (responseData?["location"]?.dictionaryValue)!
                            }
                        }
                        
                        if self.aryInCountryTravellers.count == 0
                        {
                            self.vwNoMatches.isHidden = false
                            let location = CYLGlobals.sharedInstance.usrObject?.city == "" ? CYLGlobals.sharedInstance.usrObject?.country:CYLGlobals.sharedInstance.usrObject?.country
                            
                            var isoLocation = ""
                            if let _ = CYLGlobals.sharedInstance.usrObject!.isoLocation
                            {
                                isoLocation = CYLGlobals.sharedInstance.usrObject!.isoLocation!
                            }
                            if location == "" || isoLocation == ""
                            {
                                self.lblNoMatch.text = "Your present country could not be detected. Please edit your “my current location” in the “Profile” section."
                                self.lblNoMatch.sizeToFit()
                                self.btnEditLocation.isHidden = false
                            }
                            else
                            {
                                self.btnEditLocation.isHidden = true
                                self.lblNoMatch.text = "No matches found in \(String(describing: location!)). Please search “Everywhere”."
                            }
                            
                        }
                        else
                        {
                            self.vwNoMatches.isHidden = true
                            self.reloadCollectionData()
                        }
                        
                    }
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Search travellers near me"])
                    if str == "" && page == 0
                    {
                        self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
                    }
                    else
                    {
                        showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    }
                }
                
        },
                                                                  withError:
            {
                (error) in
                self.hideLoadingView()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Search travellers near me"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                self.hideLoadingView()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Search travellers near me"])
                if str == "" && page == 0
                {
                    self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
                }
                else
                {
                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                }
        })
    }
    
    func showLoadingView()
    {
        if self.footerView == nil || lastLoadedPage == 0
        {
            self.vwLoader.isHidden = false
        }
    }
    func hideLoadingView()
    {
        if self.footerView != nil
        {
            self.tblTraveller.tableFooterView = nil
            self.vwLoader.isHidden = true
        }
        else
        {
            self.vwLoader.isHidden = true
        }
    }
    
    @IBAction func btnEditLocationAction(_ sender: UIButton)
    {
        self.vwNoMatches.isHidden = true
        self.tabBarController?.selectedViewController = self.tabBarController?.viewControllers![3]
        self.tabBarController?.selectedIndex = 3
    }
    @objc func btnLocationRefreshClicked()
    {
        if CLLocationManager.locationServicesEnabled()
        {
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways
            {
                self.startRotation()
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.delegate = self
                locationManager.startUpdatingLocation()
            }
            else if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted
            {
                let text = showAlertTitleMessageFont(strTitle: "Update your current location", strMessage: "To enable location, please go to Settings -> Tourgether -> Location and choose the option - While using the app")
                let alertComtroller = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                alertComtroller.setValue(text.title, forKey: "attributedTitle")
                alertComtroller.setValue(text.message, forKey: "attributedMessage")
                let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel, handler:
                { action -> Void in
                    self.dismiss(animated: true, completion: nil)
                })
                alertComtroller.addAction(cancelAction)
                
                self.present(alertComtroller, animated: true, completion: nil)
            }
            else if CLLocationManager.authorizationStatus() == .notDetermined
            {
                self.startRotation()
                self.locationManager.requestWhenInUseAuthorization()
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.delegate = self
                locationManager.startUpdatingLocation()
            }
        }
        else
        {
            let text = showAlertTitleMessageFont(strTitle: "Update your current location", strMessage: "To enable location, please go to Settings -> Tourgether -> Location and choose the option - While using the app")
            let alertComtroller = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            alertComtroller.setValue(text.title, forKey: "attributedTitle")
            alertComtroller.setValue(text.message, forKey: "attributedMessage")
            let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel, handler:
            { action -> Void in
                self.dismiss(animated: true, completion: nil)
            })
            alertComtroller.addAction(cancelAction)
            
            self.present(alertComtroller, animated: true, completion: nil)
        }
    }
    func startRotation() {
        btnRefreshLocation.setImage(UIImage(named: "refresh_spineer.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnRefreshLocation.tintColor = .white
        self.btnRefreshLocation.isUserInteractionEnabled = false
        let kAnimationKey = "rotation"
        if self.btnRefreshLocation.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = 2.0
            animate.repeatCount = Float.infinity
            animate.fromValue = 0.0
            animate.toValue = Float(CGFloat.pi * 2.0)
            self.btnRefreshLocation.layer.add(animate, forKey: kAnimationKey)
        }
    }
    func endRotation()
    {
        let kAnimationKey = "rotation"
        self.btnRefreshLocation.isUserInteractionEnabled = true
        if self.btnRefreshLocation.layer.animation(forKey: kAnimationKey) != nil {
            self.btnRefreshLocation.layer.removeAnimation(forKey: kAnimationKey)
            btnRefreshLocation.setImage(UIImage(named: "ic_gps")?.withRenderingMode(.alwaysTemplate), for: .normal)
            btnRefreshLocation.tintColor = .white
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied || status == CLAuthorizationStatus.restricted)
        {
            self.endRotation()
            //            self.requestToUpdateCurrentLocation(latitude: 0, longitude: 0, city: "", country: "", isoLocation: "")
            // The user denied authorization
        } else if (status == CLAuthorizationStatus.authorizedWhenInUse)
        {
            manager.startUpdatingLocation()
            // The user accepted authorization
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        manager.stopUpdatingLocation()
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        if locations.count == 1
        {
            CleverTap.setLocation(locValue)
            
            // Add below code to get address for touch coordinates.
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler:
                { (placemarks, error) -> Void in
                    // Place details
                    var placeMark: CLPlacemark!
                    placeMark = placemarks?[0]
                    var city:String = String()
                    var country: String = String()
                    var isoLocation: String = String()
                    if (placeMark != nil)
                    {
                        city = placeMark.addressDictionary!["City"] as? String ?? ""
                        country = placeMark.addressDictionary!["Country"] as? String ?? ""
                        self.lblCountryName.text = "\(country)"
                        isoLocation = placeMark.isoCountryCode ?? ""
                    }
                    self.requestToUpdateCurrentLocation(latitude: locValue.latitude, longitude: locValue.longitude, city: city, country: country, isoLocation: isoLocation)
            })
            
        }
        
    }
    func requestToUpdateCurrentLocation(latitude:Double,longitude:Double,city: String, country: String,isoLocation:String)
    {
        //        //KVNProgress.show()
        let params:[String: Any] = ["latitude":"\(latitude)","longitude":"\(longitude)","city":city,"country":country,"isoLocation":isoLocation]
        let request = CYLServices.postUserCurrentLocation(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.showSuccess()
                self.endRotation()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.cityv2 = responseData!["data"]!["userLocation"]["city"].stringValue
                        userObject.country = responseData!["data"]!["userLocation"]["country"].stringValue
                        userObject.isoLocation = responseData!["data"]!["userLocation"]["isoLocation"].stringValue
                        userObject.countryPicUrl = responseData!["data"]!["countryPicUrl"].stringValue
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: nil, userInfo: ["name":"profile"])
                        
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Current Location"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    self.endRotation()
                }
                
        },
                                                                  withError:
            {
                (error) in
                ////KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Current Location"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
                self.endRotation()
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                ////KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Current Location"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                self.endRotation()
                
        })
    }
    @objc func btnFirstWaveAction(_ sender: UIButton)
    {
        if sender.tag == 1001
        {
            self.vwFirstWave.isHidden = true
            if waveFirstSecondClick == 1
            {
                waveClickStartAnimationFirstCountry()
            }
            else
            {
                waveClickStartAnimationSecondCountry()
            }
        }
        else
        {
            self.vwFirstWave.isHidden = true
        }
    }
    
}
//MARK:- SendMessage PopUp Delegate
extension CYLInCountryVC:CYLSendMessagePopUpDelegate
{
    func moveChatDetailController(nameRecever: String, chatId: String, imgRecever: String, receverId: Int) {
        
        let chatDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLChatDetailVC") as! CYLChatDetailVC
        chatDetailVC.userName = nameRecever
        chatDetailVC.chatId = chatId
        chatDetailVC.imgUrl = imgRecever
        chatDetailVC.receiverId = receverId
        chatDetailVC.messageDelegate = (chatId == "") ? self : nil
        self.navigationController?.pushViewController(chatDetailVC, animated: true)
        
    }
}
extension CYLInCountryVC:CYLMessageDelegate
{
    func delegateToGetUpdatedChatId(strChatId:String)
    {
        //        self.strChatId = strChatId
    }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//MARK: table view data source and delegate functions
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
extension CYLInCountryVC:UITableViewDataSource
{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        let hederView = UIView(frame:CGRect(x:0,y:0,width:self.screenWidth!,height:60))
        hederView.backgroundColor = .white
        
        let imgCountry = UIImageView(frame: CGRect(x:0,y:0,width:self.screenWidth!,height:60))
        imgCountry.backgroundColor = UIColor(red: 25.0/255.0, green: 34.0/255.0, blue: 38.0/255.0, alpha: 1.0)
        imgCountry.sd_setImage(with: URL(string: "\(dictCurrentLocation["countryPicUrl"]?.stringValue ?? "")"))
        hederView.addSubview(imgCountry)
        
        lblCountryName = UILabel(frame: CGRect(x:0,y:0,width:self.screenWidth!,height:60))
        lblCountryName.text = dictCurrentLocation["country"]?.stringValue ?? "Update location"
        lblCountryName.font = UIFont(name: "Comfortaa-Bold", size: 14)
        lblCountryName.textAlignment = .center
        lblCountryName.textColor = UIColor.white
        hederView.addSubview(lblCountryName)
        
        btnRefreshLocation = UIButton(frame: CGRect(x:self.screenWidth!-40,y:10,width:40,height:40))
        btnRefreshLocation.setImage(UIImage(named: "ic_gps")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnRefreshLocation.addTarget(self, action:#selector(self.btnLocationRefreshClicked), for: .touchUpInside)
        btnRefreshLocation.tintColor = .white
        btnRefreshLocation.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        hederView.addSubview(btnRefreshLocation)
        
        return hederView
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (self.screenWidth!/2.0)+8.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.aryUnmatchedInCountryTravellers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CYLTravellerListCell") as! CYLTravellerListCell
        
        self.setTravelerUnloadImageColor(index: indexPath.row, cell: cell)
        
        var displayArray = [JSON]()
        
        displayArray = self.aryUnmatchedInCountryTravellers
        cell.btnToFirst.tag = 4000+indexPath.row
        cell.btnToSecond.tag = 4000+indexPath.row
        cell.btnSayWaveFirst.tag = 4000+indexPath.row
        cell.btnSayWaveSecond.tag = 4000+indexPath.row
        cell.comesEveryWhereNearMe = "nearme"
        cell.object = displayArray[indexPath.row]
        cell.selectionStyle = .none
        //*********for paging api call
        if (tblTraveller.contentOffset.y + tblTraveller.frame.size.height) >= (tblTraveller.contentSize.height - tblTraveller.frame.size.height) && self.maxResults > 0
        {
            self.setupActivityIndicatorInTableViewFooter()
            self.handlePagingForInCountryTravellerTable()
        }
        return cell
    }
    
    //MARK:-- Cell Rendom Color
    func setTravelerUnloadImageColor(index: Int , cell: CYLTravellerListCell)
    {
        if index % 2 == 0 && index % 4 != 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
                
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
            }
        }
        else if index % 3 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            }
        }
        else if index % 4 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            }
        }
        else if index % 5 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
            }
        }
        else
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
            }
        }
    }
    
}
extension CYLInCountryVC:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}

extension CYLInCountryVC
{
    func refreshViewSlowOrMantinence(msgStr: String , imgName:String)
    {
        if vwEmptyState.frame.size.width == 0
        {
            vwEmptyState = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height))
            vwEmptyState.backgroundColor = UIColor.white
            self.view.addSubview(vwEmptyState)
            let img = UIImageView(frame: CGRect(x: vwEmptyState.frame.width/2-60, y: vwEmptyState.frame.height/2-160, width: 120, height: 120))
            img.image = UIImage(named: imgName)
            
            let lblMsg = UILabel(frame: CGRect(x: 40, y: img.frame.origin.y+img.frame.size.height+20, width: self.view.frame.size.width-80, height: 21))
            lblMsg.font = UIFont(name: "Comfortaa-Bold", size: 20)
            lblMsg.textAlignment = .center
            lblMsg.text = "Oops!"
            
            let lblAlertText = UILabel(frame: CGRect(x: 30, y: lblMsg.frame.origin.y+lblMsg.frame.size.height+15, width: self.view.frame.size.width-60, height: 40))
            lblAlertText.numberOfLines = 0
            lblAlertText.textAlignment = .center
            lblAlertText.text = msgStr
            lblAlertText.adjustsFontSizeToFitWidth = true
            lblAlertText.font = UIFont(name: "Comfortaa-Bold", size: 15)
            
            let btnRefresh: UIButton = UIButton(frame: CGRect(x: vwEmptyState.frame.width/2-100, y: lblAlertText.frame.origin.y+lblAlertText.frame.size.height+35, width: 200, height: 45))
            btnRefresh.addTarget(self, action: #selector(btnRefreshAction), for: .touchUpInside)
            btnRefresh.titleLabel?.textAlignment = .center
            btnRefresh.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 15)
            btnRefresh.titleLabel?.textColor = UIColor.white
            btnRefresh.setTitle("Retry", for: UIControlState.normal)
            btnRefresh.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
            btnRefresh.layer.cornerRadius = 23
            btnRefresh.layer.masksToBounds = true
            
            vwEmptyState.addSubview(img)
            vwEmptyState.addSubview(lblMsg)
            vwEmptyState.addSubview(lblAlertText)
            vwEmptyState.addSubview(btnRefresh)
            vwEmptyState.isHidden = false
        }
        else
        {
            vwEmptyState.isHidden = false
        }
    }
    @objc func btnRefreshAction(sender: UIButton)
    {
        if checkInternetConnection()
        {
            self.requestToGetTravellersList(page: 0, str: "")
            vwEmptyState.isHidden = true
        }
    }
}
