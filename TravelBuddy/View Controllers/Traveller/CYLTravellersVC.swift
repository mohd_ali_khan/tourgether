//
//  CYLTravellersVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/10/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
import SJFluidSegmentedControl
import CarbonKit

class CYLTravellersVC: CYLBaseViewController, CarbonTabSwipeNavigationDelegate
{
    @IBOutlet weak var vwContainer: UIView!
    
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var currentOffset:CGFloat?
    var currentDisplayVC:UIViewController?
    var showSelectedIndex: UInt = UInt()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.requestToGetUnreadMessageCount()
        self.requestToGetNotificationBadgeCount()
        
        let items = ["EVERYWHERE", "NEAR ME","LOCAL"]
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        self.style()
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.createTravellerListScreenDesign()
        self.setupUIForCustomSegmentControl()
        
        let btnFilter = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        
        if CYLGlobals.sharedInstance.isFilterApplied!
        {
            btnFilter.setImage(#imageLiteral(resourceName: "ic_filter_act"), for: .normal)
        }
        else
        {
            btnFilter.setImage(#imageLiteral(resourceName: "ic_filter_inact"), for: .normal)
            btnFilter.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 2)
        }
        btnFilter.addTarget(self, action: #selector(self.btnToFilterClicked), for: .touchUpInside)
        btnFilter.backgroundColor = UIColor.init(red: 231/255.0, green: 38/255.0, blue: 59/255.0, alpha: 1.0)
        btnFilter.layer.cornerRadius = 20
        btnFilter.layer.masksToBounds = true
        self.tabBarController?.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: btnFilter)]
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.navigationItem.title = "Travelers"
        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createTravellerListScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!] 
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    func setupUIForCustomSegmentControl()
    {
        //        self.navigationController!.topViewController!.navigationItem.titleView = self.vwHeaderSegment
//        self.tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_filter_inact"), style: .plain, target: self, action: #selector(self.btnToFilterClicked))
    }
    @objc func btnToFilterClicked()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CYLFilterVC") as! CYLFilterVC
        vc.comesControllerIdentifier = showSelectedIndex
        let navController = UINavigationController.init(rootViewController: vc)
        self.present(navController, animated: true, completion: nil)
    }
    
    func handleFilterAppliedConditions()
    {
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToGetUnreadMessageCount()
    {
        CYLChatManager.sharedInstance.delegateTotalUnread=self
        CYLChatManager.sharedInstance.getTotalChatsUnreadCountBadge()
    }

    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        switch index {
        case 0:
            return self.storyboard!.instantiateViewController(withIdentifier: "CYLEverywhereVC") as! CYLEverywhereVC
        case 1:
            return self.storyboard!.instantiateViewController(withIdentifier: "CYLInCountryVC") as! CYLInCountryVC
        case 2:
            return self.storyboard!.instantiateViewController(withIdentifier: "CYLLocallyCV") as! CYLLocallyCV
        default:
            return UIViewController()
            
        }
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        
        showSelectedIndex = index
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.selectedIndexPresent = showSelectedIndex
        NSLog("Did move at index: %ld", index)
    }
    
    func style() {
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = themeColorBlue
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.white)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(CGFloat(screenWidth!/3), forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(CGFloat(screenWidth!/3), forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(CGFloat(screenWidth!/3), forSegmentAt: 2)
        carbonTabSwipeNavigation.setNormalColor(UIColor(red: 167/255, green: 43/255, blue: 53/255, alpha: 1), font: UIFont(name: "Comfortaa-Bold", size: 14)!)
        carbonTabSwipeNavigation.setSelectedColor(UIColor.white, font: UIFont(name: "Comfortaa-Bold", size: 14)!)
        if screenHeight == 480 || screenHeight == 568
        {
            carbonTabSwipeNavigation.setNormalColor(UIColor(red: 167/255, green: 43/255, blue: 53/255, alpha: 1), font: UIFont(name: "Comfortaa-Bold", size: 12)!)
            carbonTabSwipeNavigation.setSelectedColor(UIColor.white, font: UIFont(name: "Comfortaa-Bold", size: 12)!)
        }
        carbonTabSwipeNavigation.currentTabIndex = 0
        self.navigationItem.titleView?.addSubview(carbonTabSwipeNavigation.carbonTabSwipeScrollView)
        
    }
    
    func requestToGetNotificationBadgeCount()
    {
        let params:[String: Any] = ["record":"\(1)","start":"\(0)"]
        let request = CYLServices.getNotificationMessageList(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                if (response?.object!.dictionaryValue.count != 0)
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        self.tabBarController?.tabBar.items?[2].badgeValue = (responseData?["unreadCount"]?.stringValue == "0" ? nil : responseData?["unreadCount"]?.stringValue)
                    }
                }
                else
                {

                }

        },
                                                                  withError:
            {
                (error) in
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
        })
    }
    
}


extension CYLTravellersVC:CYLUnreadCountDelegate
{
    func delegateGetTotalUnreadCount(_ unreadCount: String)
    {
        self.tabBarController?.tabBar.items?[1].badgeValue = (unreadCount == "0") ? nil : unreadCount
    }
}


