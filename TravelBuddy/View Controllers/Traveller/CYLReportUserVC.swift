//
//  CYLReportUserVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 6/12/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
//import UITextView

class CYLReportUserVC: UIViewController,UIGestureRecognizerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var txtReportText: UITextView!
    var travellerId:Int?
    var travellerDetail:[String: JSON] = [:]
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.delegate = self
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Report by one to other user")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    @objc func btnToBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnToReportUserClicked(_ sender: Any)
    {
        if self.txtReportText.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""
        {
            self.requestToReportUser()
        }
        else
        {
            showAlertView(title: "", message: "This field cannot be empty", ref: self)
        }
        
    }
    func requestToReportUser()
    {
        KVNProgress.show()
        let params:[String: Any] = ["message":self.txtReportText.text!,"reportedUserId":"\(travellerId!)"]
        let request = CYLServices.postReportUser(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let g = (self.travellerDetail["gender"]?.intValue == 0) ? "Male" : "Female"
                    let dict = ["UserId":self.travellerDetail["id"]?.stringValue,"UserName":self.travellerDetail["userName"]?.stringValue,"Age":self.travellerDetail["age"]?.stringValue,"Isolocation":self.travellerDetail["isoLocation"]?.stringValue,"Gender":g]
                    print(dict as! [String: String])
                    sentEventPropertyOnCleverTap("Report user", _property: dict as! [String: String])
                    
                    KVNProgress.showSuccess()
                    //let responseData = response?.object?.dictionaryValue
                    _ = self.navigationController?.popViewController(animated: true)
                    
                    //showAlertView(title: "Success", message: "You have successfully reported this user.", ref: self)
                    
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Report by one to other user"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Report by one to other user"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Report by one to other user"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }


}
