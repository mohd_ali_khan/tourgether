//
//  CYLFilterVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 8/28/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
import SwiftRangeSlider

class CYLFilterVC: UIViewController {
    
    @IBOutlet weak var vwSlider: UIView!
    @IBOutlet weak var lblFemaleIcon: UILabel!
    @IBOutlet weak var imgFemaleIcon: UIImageView!
    @IBOutlet weak var lblMaleText: UILabel!
    @IBOutlet weak var imgMaleIcon: UIImageView!
    @IBOutlet weak var btnToMale: UIButton!
    @IBOutlet weak var btntoFemale: UIButton!
    var comesControllerIdentifier: UInt = UInt()
    @IBOutlet weak var lblRange: UILabel!
    
    var rangeSlider: RangeSlider = RangeSlider()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.customizeNavigationBar()
        
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Travellers filter")

        rangeSlider = RangeSlider(frame: CGRect(x: 10, y: 40, width: self.view.frame.size.width-36, height: 45))
        rangeSlider.lowerValue = 20.0
        rangeSlider.upperValue = 60.0
        rangeSlider.minimumValue = 20.0
        rangeSlider.maximumValue = 60.0
        rangeSlider.minimumDistance = 1.0
        rangeSlider.trackHighlightTintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
        rangeSlider.addTarget(self, action: #selector(self.rangeSliderValueChanged(sender:)),for: .valueChanged)
        self.vwSlider.addSubview(rangeSlider)
        
        if CYLGlobals.sharedInstance.isFilterApplied!
        {
            setPreviousFilterData()
        }
        else
        {
            self.showSelectionOnFirstTime()
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func rangeSliderValueChanged(sender: RangeSlider)  {

        changeSlideRange(min: Int(rangeSlider.lowerValue), max: Int(rangeSlider.upperValue))
    }
    func changeSlideRange(min: Int, max: Int)
    {
        if min == 20 && max == 60
        {
            lblRange.text = "<" + String(min) + " - " + String(max) + "+"
        }
        else if min == 20
        {
            lblRange.text = "<" + String(min) + " - " + String(max)
        }
        else if max == 60
        {
            lblRange.text = String(min) + " - " + String(max) + "+"
        }
        else
        {
            lblRange.text = String(min) + " - " + String(max)
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func customizeNavigationBar()
    {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.white], for: .normal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "close_nav"), style: .plain, target: self, action: #selector(self.btnToCancelClicked))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "save"), style: .plain, target: self, action: #selector(self.btnToDoneClicked))
        self.title = "Filter"
    }
    @objc func btnToCancelClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func btnToDoneClicked()
    {
        if !self.btnToMale.isSelected && !self.btntoFemale.isSelected
        {
            showAlertView(title: "", message: "Please select atleast one gender to continue.", ref: self)
        }
        else
        {
            self.requestToPostFilterData()
        }
    }
    
    @IBAction func btnToMaleClicked(_ sender: Any)
    {
        if !self.btnToMale.isSelected
        {
            self.showSelectionOnMaleTab()
        }
        else
        {
            self.showDeselectionOnMaleTab()
        }
    }
    
    @IBAction func btnToFemaleClicked(_ sender: Any)
    {
        if !self.btntoFemale.isSelected
        {
            self.showSelectionOnFemaleTab()
        }
        else
        {
            self.showDeselectionOnFemaleTab()
        }
    }
    
    func showSelectionOnFirstTime()
    {
        self.changeSlideRange(min: 20, max: 60)
        if CYLGlobals.sharedInstance.usrObject?.gender == 0
        {
            self.btntoFemale.isSelected = true
            self.imgFemaleIcon.image = #imageLiteral(resourceName: "ic_female_act")
            self.lblFemaleIcon.textColor = UIColor.white
            self.btntoFemale.superview?.backgroundColor = themeColorBlue
        }
        else
        {
            self.btnToMale.isSelected = true
            self.imgMaleIcon.image = #imageLiteral(resourceName: "ic_male_act")
            self.lblMaleText.textColor = UIColor.white
            self.btnToMale.superview?.backgroundColor = themeColorBlue
            self.btntoFemale.isSelected = true
            self.imgFemaleIcon.image = #imageLiteral(resourceName: "ic_female_act")
            self.lblFemaleIcon.textColor = UIColor.white
            self.btntoFemale.superview?.backgroundColor = themeColorBlue
        }
        
    }
    
    func showSelectionOnMaleTab()
    {
        //self.showDeselectionOnFemaleTab()
        self.btnToMale.isSelected = true
        self.imgMaleIcon.image = #imageLiteral(resourceName: "ic_male_act")
        self.lblMaleText.textColor = UIColor.white
        self.btnToMale.superview?.backgroundColor = themeColorBlue
    }
    func showSelectionOnFemaleTab()
    {
        //self.showDeselectionOnMaleTab()
        self.btntoFemale.isSelected = true
        self.imgFemaleIcon.image = #imageLiteral(resourceName: "ic_female_act")
        self.lblFemaleIcon.textColor = UIColor.white
        self.btntoFemale.superview?.backgroundColor = themeColorBlue
    }
    func showDeselectionOnMaleTab()
    {
        self.btnToMale.isSelected = false
        self.imgMaleIcon.image = #imageLiteral(resourceName: "ic_male_inact")
        self.lblMaleText.textColor = UIColor(red: 208/255.0, green: 215/255.0, blue: 217/255.0, alpha: 1.0)
        self.btnToMale.superview?.backgroundColor = UIColor.white
    }
    func showDeselectionOnFemaleTab()
    {
        self.btntoFemale.isSelected = false
        self.imgFemaleIcon.image = #imageLiteral(resourceName: "ic_female_inact")
        self.lblFemaleIcon.textColor = UIColor(red: 208/255.0, green: 215/255.0, blue: 217/255.0, alpha: 1.0)
        self.btntoFemale.superview?.backgroundColor = UIColor.white
    }
    func setPreviousFilterData()
    {
        let dictFilter = UserDefaults.standard.value(forKey: "travel_filter") as! [String:AnyObject]
        self.rangeSlider.upperValue = Double(dictFilter["max_search_age"] as! String)!
        self.rangeSlider.lowerValue = Double(dictFilter["min_search_age"]  as! String)!
        _ = (dictFilter["is_female_search"] as! String == "true") ? showSelectionOnFemaleTab() : showDeselectionOnFemaleTab()
        _ = (dictFilter["is_male_search"] as! String == "true") ? self.showSelectionOnMaleTab() : showDeselectionOnMaleTab()
        
        self.changeSlideRange(min: Int(dictFilter["min_search_age"]  as! String)!, max: Int(dictFilter["max_search_age"] as! String)!)
        
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToPostFilterData()
    {
        KVNProgress.show()
        let isFemaleSelected = (!self.btntoFemale.isSelected) ? "false" : "true"
        let isMaleSelected = (!self.btnToMale.isSelected) ? "false" : "true"
        let min = Int(self.rangeSlider.lowerValue)
        let max = Int(self.rangeSlider.upperValue)
        let params:[String: Any] = ["min_search_age":"\(min)","max_search_age":"\(max)","is_female_search":"\(isFemaleSelected)","is_male_search":"\(isMaleSelected)"]
        let request = CYLServices.postTravellerFilter(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        CYLGlobals.sharedInstance.isFilterApplied = true
                        let usrDefaults = UserDefaults.standard
                        usrDefaults.setValue(params, forKey: "travel_filter")
                        usrDefaults.synchronize()
                        
//                        if self.comesControllerIdentifier == 0
//                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: nil)
//                        }
//                        else
//                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: nil)
//                        }
                        
                        
//                        //KVNProgress.showSuccess()
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Travellers filter"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Travellers filter"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Travellers filter"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    
    
    
    
}
