//
//  CYLLocallyCV.swift
//  TravelBuddy
//
//  Created by codeyeti on 03/07/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLLocallyCV: CYLBaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,CYLIAPReloadAppData {
    
    var waveFirstSecondClick:Int = Int()
    var waveButtinClick:UIButton = UIButton()
    var vwFirstWave: UIView = UIView()
    
    @IBOutlet weak var vwNoMatches: UIView!
    @IBOutlet weak var lblNoMatch: UILabel!
    @IBOutlet weak var tblTraveller: UITableView!
    @IBOutlet weak var btnEditLocation: UIButton!
    var cVCityCountryList:UICollectionView!
    
    var footerView:UIView?
    var aryLocallyTravellers = [JSON]()
    var aryUnmatchedLocallyTravellers = [JSON]()
    var aryDestinationCityCountryLocalTravellers = [JSON]()
    let pageSize = 20
    let preloadMargin = 1
    var lastLoadedPage = 0
    var maxResults = 0
    var vwLoader: UIView = UIView()
    var vwEmptyState: UIView = UIView()
    var showWaveOption:Bool = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loaderGifImage()
        
        NotificationCenter.default.removeObserver(NSNotification.Name(rawValue: "update_list_for_applied_filter_local"))
        NotificationCenter.default.addObserver(self, selector: #selector(handleFilterAppliedConditionsPush(notification:)), name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: nil)
        
        NotificationCenter.default.removeObserver(NSNotification.Name(rawValue: "update_list_for_applied_to_top_local"))
        NotificationCenter.default.addObserver(self, selector: #selector(handleTopScrollLocal(notification:)), name: NSNotification.Name(rawValue: "update_list_for_applied_to_top_local"), object: nil)
        
        self.creatingLocallyScreenDesign()
        self.collectionViewAddCityCountry()
        lastLoadedPage = 0
        self.aryLocallyTravellers.removeAll()
        requestToGetTravellersListLocally(page: 0, str: "")
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Search travellers locally")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Loader View Function
    func loaderGifImage()
    {
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwLoader = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwLoader.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
            window.addSubview(vwLoader)
            let img = UIImageView(frame: CGRect(x: vwLoader.frame.width/2-40, y: vwLoader.frame.height/2-40, width: 80, height: 80))
            img.image = UIImage.sd_animatedGIF(with: imageData!)
            vwLoader.addSubview(img)
            vwLoader.isHidden = true
        }
    }
    
    @objc func handleFilterAppliedConditionsPush(notification: NSNotification)
    {
        print(notification.userInfo?["name"] ?? "")
        let notifStr = notification.userInfo?["name"] ?? ""
        handleFilterAppliedConditions(str: notifStr as! String)
    }
    @objc func handleTopScrollLocal(notification: NSNotification)
    {
        self.tblTraveller.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    func handleFilterAppliedConditions(str: String)
    {
        self.lastLoadedPage = 0
        self.aryLocallyTravellers.removeAll()
        requestToGetTravellersListLocally(page: 0, str: str)
        tblTraveller.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func creatingLocallyScreenDesign()
    {
    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
    if #available(iOS 10.0, *)
    {
    self.tblTraveller.refreshControl = refreshControl
    }
    else
    {
    self.tblTraveller.backgroundView = refreshControl
    }
}
    
    func collectionViewAddCityCountry()
    {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        layout.itemSize = CGSize(width: 60, height: 30)
        layout.scrollDirection = .horizontal
        cVCityCountryList = UICollectionView(frame: CGRect(x: 0, y: 5, width: self.view.frame.size.width, height: 40), collectionViewLayout: layout)
        cVCityCountryList.dataSource = self
        cVCityCountryList.delegate = self
        cVCityCountryList.showsHorizontalScrollIndicator = false
        cVCityCountryList.register(cityCountryLocalCollectionViewCell.self, forCellWithReuseIdentifier: "cityCountryCell")
        cVCityCountryList.register(plusButtonLocalCollectionViewCell.self, forCellWithReuseIdentifier: "plusButtonCell")
        cVCityCountryList.backgroundColor = UIColor.clear
    }
@objc func refresh(_ refreshControl: UIRefreshControl)
{
    lastLoadedPage = 0
    self.aryLocallyTravellers.removeAll()
    self.requestToGetTravellersListLocally(page: 0, str: "")
    refreshControl.endRefreshing()
}
    func setupActivityIndicatorInTableViewFooter()
    {
        self.footerView = UIView.init(frame: CGRect(x:0,y:0,width:self.screenWidth!,height:40))
        
        let actIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        actIndicator.tag = 10
        actIndicator.frame = CGRect(x:0.0, y:0.0, width:20.0, height:20.0)
        actIndicator.center.x = (self.footerView?.center.x)!
        actIndicator.center.y = (self.footerView?.center.y)!
        actIndicator.hidesWhenStopped = true
        self.footerView?.addSubview(actIndicator)
        actIndicator.startAnimating()
        self.tblTraveller.tableFooterView = self.footerView
    }
    func handlePagingForInCountryTravellerTable()
    {
        if self.maxResults > 0
        {
            lastLoadedPage += 1
            self.requestToGetTravellersListLocally(page: lastLoadedPage, str: "")
        }
    }
    func createDisplaArrayForUnmatchedTravellers()
    {
        var aryUnmatched = [JSON]()
        for object in self.aryLocallyTravellers
        {
            aryUnmatched.append(object)
        }
        
        for i in 0..<aryUnmatched.count/2
        {
            let dict = JSON.init(["first":aryUnmatched[2*i],"second":aryUnmatched[(2*i)+1]])
            self.aryUnmatchedLocallyTravellers.append(dict)
        }
        if aryUnmatched.count%2 != 0
        {
            let dict = JSON.init(["first":aryUnmatched.last])
            self.aryUnmatchedLocallyTravellers.append(dict)
        }
        let view = UIView.init(frame: CGRect(x: 0.0, y: 0.0, width: self.tblTraveller.bounds.size.width, height: CGFloat.leastNormalMagnitude))
        self.tblTraveller.tableHeaderView = view
        self.tblTraveller.reloadData()
    }
    func reloadCollectionData()
    {
        self.aryUnmatchedLocallyTravellers.removeAll()
        self.createDisplaArrayForUnmatchedTravellers()
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom Button Wave Click Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToUserProfileFirstClicked(_ sender: UIButton)
    {
        let travellerId = self.aryUnmatchedLocallyTravellers[sender.tag-4000]["first"]["id"].intValue
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserProfileVC") as! CYLUserProfileVC
        userProfileVC.travellerId = travellerId
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    @IBAction func btnToUserProfileSecondCLicked(_ sender: UIButton)
    {
        let travellerId = self.aryUnmatchedLocallyTravellers[sender.tag-4000]["second"]["id"].intValue
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserProfileVC") as! CYLUserProfileVC
        userProfileVC.travellerId = travellerId
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    @IBAction func btnToSayWaveFirstClick(_ sender: UIButton)
    {
        if CYLGlobals.sharedInstance.usrObject!.wavesRemaining != 0
        {
            self.waveButtinClick = sender
            self.waveFirstSecondClick = 1
            if UserDefaults.standard.value(forKey: "messageWave_count") as! Int == 0
            {
                self.createFirstWavePopUp()
            }
            else
            {
                waveClickStartAnimationFirstLocal()
            }
        }
        else
        {
            self.waveLimitExcedTodayLocal()
            self.waveFirstSecondClick = 1
        }
    }
    @IBAction func btnToSayWaveSecondClick(_ sender: UIButton)
    {
        if CYLGlobals.sharedInstance.usrObject!.wavesRemaining != 0
        {
            self.waveButtinClick = sender
            self.waveFirstSecondClick = 2
            if UserDefaults.standard.value(forKey: "messageWave_count") as! Int == 0
            {
                self.createFirstWavePopUp()
            }
            else
            {
                waveClickStartAnimationSecondLocal()
            }
        }
        else
        {
            self.waveLimitExcedTodayLocal()
            self.waveFirstSecondClick = 2
        }
    }
    
    func waveApiFailRequestLocal(index: Int, arr: [JSON], cell: String, section: Int)
    {
        if cell == "first"
        {
            var userDetail: JSON = self.aryUnmatchedLocallyTravellers[index]
            userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue :  JSON(0).intValue)

            self.aryUnmatchedLocallyTravellers.remove(at: index)

            self.aryUnmatchedLocallyTravellers.insert(userDetail, at: index)

            let waveObject: JSON = self.aryUnmatchedLocallyTravellers[index]["first"]["wave"]
            let otherUserId = self.aryUnmatchedLocallyTravellers[index]["first"]["id"].intValue
            let index = self.aryLocallyTravellers.index(where: {$0["id"].intValue == otherUserId})
            self.aryLocallyTravellers[index!]["wave"] = waveObject
        }
        else
        {
            var userDetail: JSON = self.aryUnmatchedLocallyTravellers[index]
            userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue : JSON(0).intValue)
            
            self.aryUnmatchedLocallyTravellers.remove(at: index)
            
            self.aryUnmatchedLocallyTravellers.insert(userDetail, at: index)
            
            let waveObject: JSON = self.aryUnmatchedLocallyTravellers[index]["second"]["wave"]
            let otherUserId = self.aryUnmatchedLocallyTravellers[index]["second"]["id"].intValue
            let index = self.aryLocallyTravellers.index(where: {$0["id"].intValue == otherUserId})
            self.aryLocallyTravellers[index!]["wave"] = waveObject
        }
        let indexPath = IndexPath(item: index, section: section)
        self.tblTraveller.reloadRows(at: [indexPath], with: .none)
        
    }
    func waveLimitExcedTodayLocal()
    {
        if CYLGlobals.sharedInstance.usrObject?.userPlanType == 0
        {
            let paymentPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserPaymentVC") as! CYLUserPaymentVC
            paymentPopup.messageLimitExceed = "wave"
            paymentPopup.delegateMessageReload = self
            self.navigationController?.pushViewController(paymentPopup, animated: false)
        }
        else
        {
            let text = showAlertTitleMessageFont(strTitle: "Wave Limit Exceeded", strMessage: CYLGlobals.sharedInstance.usrObject!.wavesMessage ?? "")
            let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            
            alert.setValue(text.title, forKey: "attributedTitle")
            alert.setValue(text.message, forKey: "attributedMessage")
            
            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                (sender) in
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func reloadMessageReceiverListDelegatePayment() {
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToSendWaveForTravelerLocal(index: Int, arr: [JSON], cell: String, section: Int)
    {
        let receiverId = arr[index][cell]["id"].intValue
        let params:[String: Any] = ["otherUserId":"\(receiverId)","waveCallingLocation":"NEAR_ME"]
        let request = CYLServices.postWaveMessageForServer(dict: params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in

                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue

                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                            userObject.wavesRemaining = responseData?["wavesRemaining"]?.intValue
                            userObject.wavesMessage = responseData?["message"]?.stringValue
                            CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                            userDeflts.set(encodedData, forKey: "userObject")
                            userDeflts.synchronize()
                        UserDefaults.standard.set(1, forKey: "messageWave_count")
                        let waveEmoticonType = arr[index]["cell"]["wave"]["waveEmoticonType"].intValue == 1 ? "wave sent" : "wave sent back"
                        var cleverTapEventData = [String:Any]()
                        let aryCities = arr[index][cell]["cities"].arrayValue
                        let aryCountries = arr[index][cell]["countries"].arrayValue
                        var detinationType: String!
                        if aryCities.count != 0{
                            detinationType = "City"
                            cleverTapEventData["DestinationCount"] = aryCities.count
                        }
                        else if aryCountries.count != 0{
                            detinationType = "Country"
                            cleverTapEventData["DestinationCount"] = aryCountries.count
                        }
                        else{
                            detinationType = "No Plan"
                            cleverTapEventData["destinationCount"] = 0
                        }
                        cleverTapEventData["DetinationType"] = detinationType
                        cleverTapEventData["IsGenderMatched"] = arr[index][cell]["isGenderMatched"].bool
                        cleverTapEventData["IsCountryMatched"] = arr[index][cell]["isCountryMatched"].bool
                        
                        let liveInDestionation = arr[index][cell]["livesInDestinationCountry"].boolValue
                        
                        cleverTapEventData["UserId"] = arr[index][cell]["id"].stringValue
                        cleverTapEventData["UserName"] = arr[index][cell]["userName"].stringValue
                        cleverTapEventData["Age"] = arr[index][cell]["age"].stringValue
                        cleverTapEventData["Isolocation"] = arr[index][cell]["isoLocation"].stringValue
                        let g = (arr[index][cell]["gender"].intValue == 0) ? "Male" : "Female"
                        cleverTapEventData["Gender"] = g
                        cleverTapEventData["IsMatched"] = arr[index][cell]["is_matched"].boolValue
                        cleverTapEventData["isOnline"] = arr[index][cell]["showOnline"].boolValue
                        cleverTapEventData["page type"] = "user list"
                        cleverTapEventData["wave type"] = waveEmoticonType
                        cleverTapEventData["LivesInDestinationCountry"] = liveInDestionation
                        sentEventPropertyOnCleverTap("Wave clicked", _property: cleverTapEventData)
                        
                        
                        
                        if arr[index][cell]["wave"]["waveEmoticonType"].intValue == 3
                        {
                            self.view.layoutIfNeeded()
                            let matchedPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLMatchedPopUpVC") as! CYLMatchedPopUpVC
                            matchedPopup.imgRecever = arr[index][cell]["userPicUrl"].stringValue
                            matchedPopup.id = arr[index][cell]["id"].intValue
                            matchedPopup.userName = arr[index][cell]["firstName"].stringValue
                            matchedPopup.modalPresentationStyle = .overCurrentContext
                            matchedPopup.modalTransitionStyle = .crossDissolve
                            matchedPopup.moveChatDetailControllerDelegate = self
                            self.navigationController?.present(matchedPopup, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                                           userObject.wavesMessage = responseData?["message"]?.stringValue
                                           CYLGlobals.sharedInstance.usrObject = userObject
                                           let userDeflts = UserDefaults.standard
                                           let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                                               userDeflts.set(encodedData, forKey: "userObject")
                                               userDeflts.synchronize()
                        DispatchQueue.main.async {
                            self.waveApiFailRequestLocal(index: index, arr: arr, cell: cell, section: section)
                        }
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.waveApiFailRequestLocal(index: index, arr: arr, cell: cell, section: section)
                    }
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Wave click local"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
        },
                                                                  withError:
            {
                (error) in
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Wave click local"])
                DispatchQueue.main.async {
                    self.waveApiFailRequestLocal(index: index, arr: arr, cell: cell, section: section)
                }
                self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
        },
                                                                  andNetworkErr: {
                                                                    (networkFailure) in
                                                                    sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Wave click local"])
                                                                    DispatchQueue.main.async {
                                                                        self.waveApiFailRequestLocal(index: index, arr: arr, cell: cell, section: section)
                                                                    }
                                                                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    
    func requestToGetTravellersListLocally(page:Int,str: String)
    {
        lastLoadedPage = page
        if str == ""
        {
            self.showLoadingView()
        }
        let params:[String: Any] = ["record":"\(pageSize)","start":"\(page*pageSize)"]
        let request = CYLServices.getTravellersListLocally(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                self.hideLoadingView()
                if (response?.object!.dictionaryValue.count != 0)
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        self.maxResults = (responseData?["total_records"]?.intValue)!
                        self.aryLocallyTravellers.append(contentsOf: (responseData?["data"]?.arrayValue)!)
                        
                        if self.lastLoadedPage == 0
                        {
                            self.aryDestinationCityCountryLocalTravellers.removeAll()
                            if (responseData?["countries"] != nil)
                            {
                                if responseData?["countries"] != JSON.null
                                {
                                    self.aryDestinationCityCountryLocalTravellers.append(contentsOf: (responseData?["countries"]?.arrayValue)!)
                                }
                            }
                            if (responseData?["cities"] != nil)
                            {
                                if responseData?["cities"] != JSON.null
                                {
                                    self.aryDestinationCityCountryLocalTravellers.append(contentsOf: (responseData?["cities"]?.arrayValue)!)
                                }
                            }
                        }
                        
                        
                        if self.aryLocallyTravellers.count == 0
                        {
                            self.vwNoMatches.isHidden = false
                            self.btnEditLocation.isHidden = true
                            self.lblNoMatch.text = "Oops! No users found"
                            self.reloadCollectionData()
                        }
                        else
                        {
                            self.vwNoMatches.isHidden = true
                            self.reloadCollectionData()
                        }
                        
                    }
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Search travellers local"])
                    if str == "" && page == 0
                    {
                        self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
                    }
                    else
                    {
                        showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    }
                }
                
        },
                                                                  withError:
            {
                (error) in
                self.hideLoadingView()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Search travellers local"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                self.hideLoadingView()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Search travellers local"])
                if str == "" && page == 0
                {
                    self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
                }
                else
                {
                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                }
        })
    }
    func showLoadingView()
    {
        if self.footerView == nil || lastLoadedPage == 0
        {
            self.vwLoader.isHidden = false
        }
    }
    func hideLoadingView()
    {
        if self.footerView != nil
        {
            self.tblTraveller.tableFooterView = nil
            self.vwLoader.isHidden = true
        }
        else
        {
            self.vwLoader.isHidden = true
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Collection view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.aryDestinationCityCountryLocalTravellers.count != 0
        {
            return aryDestinationCityCountryLocalTravellers.count+1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == aryDestinationCityCountryLocalTravellers.count
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "plusButtonCell", for: indexPath as IndexPath) as! plusButtonLocalCollectionViewCell
            //            myCell.backgroundColor = UIColor.init(red: 238/255.0, green: 241/255.0, blue: 245/255.0, alpha: 1.0)
            myCell.layer.cornerRadius = 15
            myCell.clipsToBounds = true
            
            myCell.plusImage.image = UIImage(named: "add_city.png")
            return myCell
        }
        else
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cityCountryCell", for: indexPath as IndexPath) as! cityCountryLocalCollectionViewCell
            myCell.backgroundColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
            myCell.layer.cornerRadius = 15
            myCell.clipsToBounds = true
            myCell.titleLabel.text = aryDestinationCityCountryLocalTravellers[indexPath.row]["cityName"].stringValue == "" ? aryDestinationCityCountryLocalTravellers[indexPath.row]["countryName"].stringValue : aryDestinationCityCountryLocalTravellers[indexPath.row]["cityName"].stringValue
            myCell.titleLabel.sizeToFit()
            return myCell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if indexPath.row == aryDestinationCityCountryLocalTravellers.count
        {
            return CGSize(width: 30, height: 30)
        }
        else
        {
            let name = aryDestinationCityCountryLocalTravellers[indexPath.row]["cityName"].stringValue == "" ? aryDestinationCityCountryLocalTravellers[indexPath.row]["countryName"].stringValue : aryDestinationCityCountryLocalTravellers[indexPath.row]["cityName"].stringValue
            let wirth = widthForInputString(text: name)
            return CGSize(width: wirth+20, height: 30)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("User tapped on item \(indexPath.row)")
        let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLEditDestinationVC") as! CYLEditDestinationVC
        destinationVC.postNotificationRefreshDestination = "home"
        destinationVC.aryCountry = CYLGlobals.sharedInstance.usrObject!.countries!
        destinationVC.aryCity = CYLGlobals.sharedInstance.usrObject!.cities!
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    @objc func btnFirstWaveAction(_ sender: UIButton)
    {
        if sender.tag == 1001
        {
            self.vwFirstWave.isHidden = true
            if waveFirstSecondClick == 1
            {
                waveClickStartAnimationFirstLocal()
            }
            else
            {
                waveClickStartAnimationSecondLocal()
            }
        }
        else
        {
            self.vwFirstWave.isHidden = true
        }
    }
    func createFirstWavePopUp()
    {
        var firstName = String()
        var gender = String()
        let userDetail: JSON = self.aryUnmatchedLocallyTravellers[self.waveButtinClick.tag-4000]
        if self.waveFirstSecondClick == 1
        {
            firstName = userDetail["first"]["firstName"].stringValue != "" ? userDetail["first"]["firstName"].stringValue : userDetail["first"]["userName"].stringValue
            gender = userDetail["first"]["gender"].intValue == 0 ? "he":"she"
        }else
        {
            firstName = userDetail["second"]["firstName"].stringValue != "" ? userDetail["second"]["firstName"].stringValue : userDetail["second"]["userName"].stringValue
            gender = userDetail["second"]["gender"].intValue == 0 ? "he":"she"
        }
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwFirstWave = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwFirstWave.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.8)
            
            let popUpView = UIView(frame: CGRect(x: 40, y: vwFirstWave.frame.height/2-145, width: vwLoader.frame.width-80, height: 290))
            popUpView.backgroundColor = UIColor.white
            popUpView.layer.cornerRadius = 8
            popUpView.layer.masksToBounds = true
            vwFirstWave.addSubview(popUpView)
            
            let lblName = UILabel(frame: CGRect(x: 20, y: 40, width: popUpView.frame.width-40, height: 30))
            lblName.text = "Wanna wave at \(firstName)?"
            lblName.font = UIFont(name: "Pacifico-Bold", size: 24.0)
            lblName.textColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            lblName.adjustsFontSizeToFitWidth = true
            lblName.textAlignment = .center
            popUpView.addSubview(lblName)
            
            let lblTitle = UILabel(frame: CGRect(x: 20, y: lblName.frame.origin.y+lblName.frame.size.height+30, width: popUpView.frame.width-40, height: 30))
            lblTitle.text = "Secretly wave at \(gender == "he" ? "him":"her"). If \(gender) also wave at you, you'll be able to chat"
            lblTitle.font = UIFont(name: "Comfortaa-Bold", size: 16.0)
            lblTitle.textColor = UIColor.lightGray
            lblTitle.numberOfLines = 0
            lblTitle.textAlignment = .center
            lblTitle.sizeToFit()
            popUpView.addSubview(lblTitle)
            
            let btnCancle: UIButton = UIButton(frame: CGRect(x: 0, y: popUpView.frame.size.height-45, width: popUpView.frame.width, height: 45))
            btnCancle.addTarget(self, action: #selector(btnFirstWaveAction(_:)), for: .touchUpInside)
            btnCancle.titleLabel?.textAlignment = .center
            btnCancle.tag = 1002
            btnCancle.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 16)
            btnCancle.setTitleColor(UIColor.lightGray, for: .normal)
            btnCancle.setTitle("Cancel", for: UIControlState.normal)
            popUpView.addSubview(btnCancle)
            
            let vwLine1 = UIView(frame: CGRect(x: 0, y: btnCancle.frame.origin.y-1, width: popUpView.frame.width, height: 1))
            vwLine1.backgroundColor = UIColor.lightGray
            popUpView.addSubview(vwLine1)
            
            let btnWaveSecretly: UIButton = UIButton(frame: CGRect(x: 0, y: vwLine1.frame.origin.y-45, width: popUpView.frame.width, height: 45))
            btnWaveSecretly.addTarget(self, action: #selector(btnFirstWaveAction(_:)), for: .touchUpInside)
            btnWaveSecretly.tag = 1001
            popUpView.addSubview(btnWaveSecretly)
            
            let imgHand = UIImageView(frame: CGRect(x: popUpView.frame.width/2-60, y: btnWaveSecretly.frame.origin.y+12, width: 20, height: 20))
            imgHand.image = UIImage(named: "ic_hand.png")
            imgHand.image = imgHand.image!.withRenderingMode(.alwaysTemplate)
            imgHand.tintColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            popUpView.addSubview(imgHand)
            
            let lblWaveSecretly = UILabel(frame: CGRect(x: imgHand.frame.size.width+imgHand.frame.origin.x+5, y: btnWaveSecretly.frame.origin.y, width: 150, height: 45))
            lblWaveSecretly.text = "Wave Secretly"
            lblWaveSecretly.font = UIFont(name: "Comfortaa-Bold", size: 16.0)
            lblWaveSecretly.textColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            lblWaveSecretly.textAlignment = .left
            popUpView.addSubview(lblWaveSecretly)
            
            let vwLine2 = UIView(frame: CGRect(x: 0, y: btnWaveSecretly.frame.origin.y-1, width: popUpView.frame.width, height: 1))
            vwLine2.backgroundColor = UIColor.lightGray
            popUpView.addSubview(vwLine2)
            
            let imgLogo = UIImageView(frame: CGRect(x: vwFirstWave.frame.width/2-40, y: popUpView.frame.origin.y-40, width: 80, height: 80))
            imgLogo.image = UIImage(named: "pay_wave")
            imgLogo.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
            vwFirstWave.addSubview(imgLogo)
            window.addSubview(vwFirstWave)
        }
    }
    func waveClickStartAnimationFirstLocal()
    {
        let moveX = (self.view.frame.size.width == 320) ? 80:100
        let views = self.waveButtinClick.superview
        for view in views!.subviews
        {
            if (view.isKind(of: UIImageView.self))
            {
                let imageView: UIImageView = view as! UIImageView
                
                UIView.animate(withDuration: 2.5, animations: {
                    views?.alpha = 0.0
                })
                imageView.run(.sequence([
                    .moveX(CGFloat(-moveX), widht: 40, height: 40),
                    .swing(),
                    .moveX(CGFloat(moveX), widht: -40, height: -40)
                    
                    ]), completion: {
                        UIView.animate(withDuration: 1.0, animations: {
                            views?.alpha = 1.0
                            
                            var userDetail: JSON = self.aryUnmatchedLocallyTravellers[self.waveButtinClick.tag-4000]
                            userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["first"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["first"]["wave"]["waveEmoticonType"].intValue)
                            
                            self.aryUnmatchedLocallyTravellers.remove(at: self.waveButtinClick.tag-4000)
                            
                            self.aryUnmatchedLocallyTravellers.insert(userDetail, at: self.waveButtinClick.tag-4000)
                            
                            let waveObject: JSON = self.aryUnmatchedLocallyTravellers[self.waveButtinClick.tag-4000]["first"]["wave"]
                            let otherUserId = self.aryUnmatchedLocallyTravellers[self.waveButtinClick.tag-4000]["first"]["id"].intValue
                            let index = self.aryLocallyTravellers.index(where: {$0["id"].intValue == otherUserId})
                            self.aryLocallyTravellers[index!]["wave"] = waveObject
                            
                            DispatchQueue.main.async {
                                let indexPath = IndexPath(item: self.waveButtinClick.tag-4000, section: 0)
                                self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                            }
                            
                            self.requestToSendWaveForTravelerLocal(index: self.waveButtinClick.tag-4000, arr: self.aryUnmatchedLocallyTravellers, cell: "first", section: 0)
                            
                        })
                        
                })
            }
        }
    }
    func waveClickStartAnimationSecondLocal()
    {
        let moveX = (self.view.frame.size.width == 320) ? 80:100
        let views = self.waveButtinClick.superview
        for view in views!.subviews
        {
            if (view.isKind(of: UIImageView.self))
            {
                let imageView: UIImageView = view as! UIImageView
                
                UIView.animate(withDuration: 2.5, animations: {
                    views?.alpha = 0.0
                })
                imageView.run(.sequence([
                    .moveX(CGFloat(-moveX), widht: 40, height: 40),
                    .swing(),
                    .moveX(CGFloat(moveX), widht: -40, height: -40)
                    
                    ]), completion: {
                        UIView.animate(withDuration: 1.0, animations: {
                            views?.alpha = 1.0
                            var userDetail: JSON = self.aryUnmatchedLocallyTravellers[self.waveButtinClick.tag-4000]
                            userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["second"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["second"]["wave"]["waveEmoticonType"].intValue)
                            
                            self.aryUnmatchedLocallyTravellers.remove(at: self.waveButtinClick.tag-4000)
                            
                            self.aryUnmatchedLocallyTravellers.insert(userDetail, at: self.waveButtinClick.tag-4000)
                            
                            let waveObject: JSON = self.aryUnmatchedLocallyTravellers[self.waveButtinClick.tag-4000]["second"]["wave"]
                            let otherUserId = self.aryUnmatchedLocallyTravellers[self.waveButtinClick.tag-4000]["second"]["id"].intValue
                            let index = self.aryLocallyTravellers.index(where: {$0["id"].intValue == otherUserId})
                            self.aryLocallyTravellers[index!]["wave"] = waveObject
                            
                            DispatchQueue.main.async {
                                let indexPath = IndexPath(item: self.waveButtinClick.tag-4000, section: 0)
                                self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                            }
                            
                            self.requestToSendWaveForTravelerLocal(index: self.waveButtinClick.tag-4000, arr: self.aryUnmatchedLocallyTravellers, cell: "second", section: 0)
                        })
                })
            }
        }
    }
    

}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//MARK: Collection view Cell functions
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
class cityCountryLocalCollectionViewCell: UICollectionViewCell {
    
    var titleLabel:UILabel = {
        let label = UILabel(frame: CGRect(x:10, y: 8.5, width: 60 , height: 30))
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont(name: "Comfortaa-Bold", size: 12)!
        label.numberOfLines = 1
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.titleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class plusButtonLocalCollectionViewCell: UICollectionViewCell {
    
    
    var plusImage:UIImageView = {
        let plusImage = UIImageView(frame: CGRect(x:0, y: 3, width: 24 , height: 24))
        
        return plusImage
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.plusImage)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK:- SendMessage PopUp Delegate
extension CYLLocallyCV:CYLSendMessagePopUpDelegate
{
    func moveChatDetailController(nameRecever: String, chatId: String, imgRecever: String, receverId: Int) {
        
        let chatDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLChatDetailVC") as! CYLChatDetailVC
        chatDetailVC.userName = nameRecever
        chatDetailVC.chatId = chatId
        chatDetailVC.imgUrl = imgRecever
        chatDetailVC.receiverId = receverId
        chatDetailVC.messageDelegate = (chatId == "") ? self : nil
        self.navigationController?.pushViewController(chatDetailVC, animated: true)
        
    }
}
extension CYLLocallyCV:CYLMessageDelegate
{
    func delegateToGetUpdatedChatId(strChatId:String)
    {
        //        self.strChatId = strChatId
    }
}

//MARK: table view data source and delegate functions
extension CYLLocallyCV:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        let hederView = UIView(frame:CGRect(x:0,y:0,width:self.screenWidth!,height:50))
        hederView.backgroundColor = .white
        createMatchedTripsHeadeView(viewHeader: hederView)
        return hederView
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (self.screenWidth!/2.0)+8.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.aryUnmatchedLocallyTravellers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CYLTravellerListCell") as! CYLTravellerListCell
        
        self.setTravelerUnloadImageColor(index: indexPath.row, cell: cell)
        
        var displayArray = [JSON]()
        
        displayArray = self.aryUnmatchedLocallyTravellers
        cell.btnToFirst.tag = 4000+indexPath.row
        cell.btnToSecond.tag = 4000+indexPath.row
        cell.btnSayWaveFirst.tag = 4000+indexPath.row
        cell.btnSayWaveSecond.tag = 4000+indexPath.row
        cell.comesEveryWhereNearMe = "nearme"
        cell.object = displayArray[indexPath.row]
        cell.selectionStyle = .none
        //*********for paging api call
        if (tblTraveller.contentOffset.y + tblTraveller.frame.size.height) >= (tblTraveller.contentSize.height - tblTraveller.frame.size.height) && self.maxResults > 0
        {
            self.setupActivityIndicatorInTableViewFooter()
            self.handlePagingForInCountryTravellerTable()
        }
        return cell
    }
    
    //MARK:-- Cell Rendom Color
    func setTravelerUnloadImageColor(index: Int , cell: CYLTravellerListCell)
    {
        if index % 2 == 0 && index % 4 != 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
                
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
            }
        }
        else if index % 3 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            }
        }
        else if index % 4 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            }
        }
        else if index % 5 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
            }
        }
        else
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(displayP3Red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(displayP3Red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Header view for Matched Trips
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createMatchedTripsHeadeView(viewHeader: UIView)
    {
        cVCityCountryList.removeFromSuperview()
        viewHeader.addSubview(cVCityCountryList)
        cVCityCountryList.reloadData()
    }
    
}
extension CYLLocallyCV
{
    func refreshViewSlowOrMantinence(msgStr: String , imgName:String)
    {
        if vwEmptyState.frame.size.width == 0
        {
            vwEmptyState = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height))
            vwEmptyState.backgroundColor = UIColor.white
            self.view.addSubview(vwEmptyState)
            let img = UIImageView(frame: CGRect(x: vwEmptyState.frame.width/2-60, y: vwEmptyState.frame.height/2-160, width: 120, height: 120))
            img.image = UIImage(named: imgName)
            
            let lblMsg = UILabel(frame: CGRect(x: 40, y: img.frame.origin.y+img.frame.size.height+20, width: self.view.frame.size.width-80, height: 21))
            lblMsg.font = UIFont(name: "Comfortaa-Bold", size: 20)
            lblMsg.textAlignment = .center
            lblMsg.text = "Oops!"
            
            let lblAlertText = UILabel(frame: CGRect(x: 30, y: lblMsg.frame.origin.y+lblMsg.frame.size.height+15, width: self.view.frame.size.width-60, height: 40))
            lblAlertText.numberOfLines = 0
            lblAlertText.textAlignment = .center
            lblAlertText.text = msgStr
            lblAlertText.adjustsFontSizeToFitWidth = true
            lblAlertText.font = UIFont(name: "Comfortaa-Bold", size: 15)
            
            let btnRefresh: UIButton = UIButton(frame: CGRect(x: vwEmptyState.frame.width/2-100, y: lblAlertText.frame.origin.y+lblAlertText.frame.size.height+35, width: 200, height: 45))
            btnRefresh.addTarget(self, action: #selector(btnRefreshAction), for: .touchUpInside)
            btnRefresh.titleLabel?.textAlignment = .center
            btnRefresh.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 15)
            btnRefresh.titleLabel?.textColor = UIColor.white
            btnRefresh.setTitle("Retry", for: UIControlState.normal)
            btnRefresh.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
            btnRefresh.layer.cornerRadius = 23
            btnRefresh.layer.masksToBounds = true
            
            vwEmptyState.addSubview(img)
            vwEmptyState.addSubview(lblMsg)
            vwEmptyState.addSubview(lblAlertText)
            vwEmptyState.addSubview(btnRefresh)
            vwEmptyState.isHidden = false
        }
        else
        {
            vwEmptyState.isHidden = false
        }
    }
    @objc func btnRefreshAction(sender: UIButton)
    {
        if checkInternetConnection()
        {
            self.requestToGetTravellersListLocally(page: 0, str: "")
            vwEmptyState.isHidden = true
        }
    }
}
