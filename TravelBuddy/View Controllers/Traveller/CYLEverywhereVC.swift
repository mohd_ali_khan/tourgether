//
//  CYLEverywhereVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 19/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit
import SDWebImage
import FloatRatingView
import KMPlaceholderTextView

class CYLEverywhereVC: CYLBaseViewController , UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,FloatRatingViewDelegate,CYLIAPReloadAppData
{
    @IBOutlet weak var tblTraveller: UITableView!
    
    var footerView:UIView?
    var isApiCallActive = false
    //for travellers list
    var aryEverywhereTravellers = [JSON]()
    var aryBastMatchedEverywhereTravellers = [JSON]()
    var aryMatchedEverywhereTravellers = [JSON]()
    var aryUnmatchedEverywhereTravellers = [JSON]()
    var aryDestinationCityCountryEverywhereTravellers = [JSON]()
    var cVCityCountryList:UICollectionView!
    //for paging
    let pageSize = 20
    var waveFirstSecondClick:Int = Int()
    var waveButtinClick:UIButton = UIButton()
    var lastLoadedPage = 0
    var maxResults = 0
    var startDate:String = String()
    var endDate: String = String()
    var vwLoader: UIView = UIView()
    var vwEmptyState: UIView = UIView()
    var alertStarRatting = UIAlertController()
    var vwFirstWave: UIView = UIView()
    
    //MARK:- View Controller Method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loaderGifImage()
        self.collectionViewAddCityCountry()
        lastLoadedPage = 0
        self.aryEverywhereTravellers.removeAll()
        
        self.tblTraveller.sectionHeaderHeight = UITableViewAutomaticDimension;
        self.tblTraveller.estimatedSectionHeaderHeight = 50
        
        requestToGetTravellersList(page: 0, str: "")
        self.createEverywhereScreenDesign()
        NotificationCenter.default.removeObserver(NSNotification.Name("notify_handle_preselected_user_profile"))
        NotificationCenter.default.addObserver(self, selector: #selector(self.notifyHandlePreselectedUser), name: NSNotification.Name("notify_handle_preselected_user_profile"), object: nil)
        
        NotificationCenter.default.removeObserver(NSNotification.Name(rawValue: "update_list_for_applied_filter"))
        NotificationCenter.default.addObserver(self, selector: #selector(handleFilterAppliedConditionsPush(notification:)), name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: nil)
        
        NotificationCenter.default.removeObserver(NSNotification.Name(rawValue: "update_list_for_applied_to_top_everywhere"))
        NotificationCenter.default.addObserver(self, selector: #selector(handleTopScrollEverwhere(notification:)), name: NSNotification.Name(rawValue: "update_list_for_applied_to_top_everywhere"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Search travellers everywhere")
    }
    override func viewWillLayoutSubviews() {
        
        alertStarRatting = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let customView = UIView()
        customView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let controller = UIViewController()
        customView.frame = controller.view.frame
        controller.view.addSubview(customView)
        alertStarRatting.setValue(controller, forKey: "contentViewController")
        var ratingCount: Int = Int()
        if let _ = UserDefaults.standard.value(forKey: "rating_count")
        {
            ratingCount = UserDefaults.standard.value(forKey: "rating_count") as! Int
        }
        if CYLGlobals.sharedInstance.usrObject?.isAppFeedBack == false && ratingCount >= 10
        {
            alertStarRatting.title = "Do you love tourgether?"
            alertStarRatting.message = "Tap a star to rate us. Thanks!"
            let height: NSLayoutConstraint = NSLayoutConstraint(item: alertStarRatting.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 180)
            alertStarRatting.view.addConstraint(height)
            
            let rateView: FloatRatingView = FloatRatingView(frame: CGRect(x: 40, y: 0, width: customView.frame.size.width-80, height: customView.frame.size.height-30))
            rateView.delegate = self
            rateView.emptyImage = UIImage(named: "ic_star_empty")
            rateView.fullImage = UIImage(named: "ic_star")
            rateView.minImageSize = CGSize(width: 25, height: 25)
            rateView.halfRatings = false
            rateView.minRating = 0
            rateView.maxRating = 5
            rateView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin]
            customView.addSubview(rateView)
            
            let mayBeLater = UIAlertAction(title: "Maybe later", style: .default) { (alert) in
                UserDefaults.standard.set(0, forKey: "rating_count")
            }
            let noThanks = UIAlertAction(title: "No, Thanks", style: .default) { (alert) in
                self.requestToSentReating(review: "", rating: 0.0)
            }
            alertStarRatting.addAction(noThanks)
            alertStarRatting.addAction(mayBeLater)
            present(alertStarRatting, animated: true, completion: nil)
        }
        
    }
    
    func loaderGifImage()
    {
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwLoader = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwLoader.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
            window.addSubview(vwLoader)
            let img = UIImageView(frame: CGRect(x: vwLoader.frame.width/2-40, y: vwLoader.frame.height/2-40, width: 80, height: 80))
            img.image = UIImage.sd_animatedGIF(with: imageData!)
            vwLoader.addSubview(img)
            vwLoader.isHidden = true
        }
    }
    func collectionViewAddCityCountry()
    {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        layout.itemSize = CGSize(width: 60, height: 30)
        layout.scrollDirection = .horizontal
        
        cVCityCountryList = UICollectionView(frame: CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: 40), collectionViewLayout: layout)
        cVCityCountryList.dataSource = self
        cVCityCountryList.delegate = self
        cVCityCountryList.showsHorizontalScrollIndicator = false
        cVCityCountryList.register(cityCountryCollectionViewCell.self, forCellWithReuseIdentifier: "cityCountryCell")
        cVCityCountryList.register(plusButtonCollectionViewCell.self, forCellWithReuseIdentifier: "plusButtonCell")
        cVCityCountryList.backgroundColor = UIColor.white
    }
    
    @objc func handleFilterAppliedConditionsPush(notification: NSNotification)
    {
        print(notification.userInfo?["name"] ?? "")
        let notifStr = notification.userInfo?["name"] ?? ""
        handleFilterAppliedConditions(str: notifStr as! String)
    }
    @objc func handleTopScrollEverwhere(notification: NSNotification)
    {
        self.tblTraveller.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    func handleFilterAppliedConditions(str: String)
    {
        self.lastLoadedPage = 0
        self.aryEverywhereTravellers.removeAll()
        requestToGetTravellersList(page: 0, str: str)
        tblTraveller.setContentOffset(CGPoint.zero, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createEverywhereScreenDesign()
    {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        if #available(iOS 10.0, *)
        {
            self.tblTraveller.refreshControl = refreshControl
        }
        else
        {
            self.tblTraveller.backgroundView = refreshControl
        }
        
        
    }
    @objc func refresh(_ refreshControl: UIRefreshControl)
    {
        lastLoadedPage = 0
        self.aryEverywhereTravellers.removeAll()
        self.requestToGetTravellersList(page: 0, str: "")
        refreshControl.endRefreshing()
    }
    func setupActivityIndicatorInTableViewFooter()
    {
        self.footerView = UIView(frame: CGRect(x:0,y:0,width:self.screenWidth!,height:40))
        let actIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actIndicator.tag = 10
        actIndicator.frame = CGRect(x:0.0, y:0.0, width:20.0, height:20.0)
        actIndicator.center.x = (self.footerView?.center.x)!
        actIndicator.center.y = (self.footerView?.center.y)!
        actIndicator.hidesWhenStopped = true
        self.footerView?.addSubview(actIndicator)
        actIndicator.startAnimating()
        self.tblTraveller.tableFooterView = self.footerView
    }
    func handlePagingForEverywhereTravellerTable()
    {
        if !self.isApiCallActive && self.maxResults > 0
        {
            lastLoadedPage += 1
            self.requestToGetTravellersList(page: lastLoadedPage, str: "")
        }
    }
    func separateArrayForMatchedAndUnmatchedUsers()
    {
        var aryMatched = [JSON]()
        var aryUnmatched = [JSON]()
        var aryBastMatched = [JSON]()
        for object in self.aryEverywhereTravellers
        {
            if object["is_BestMatched"].boolValue == true
            {
                aryBastMatched.append(object)
            }
            else if object["is_matched"].bool == true
            {
                aryMatched.append(object)
            }
            else
            {
                aryUnmatched.append(object)
            }
        }
        self.createDisplaArrayForBastMatchedTravellers(aryBastMatched: aryBastMatched)
        self.createDisplaArrayForMatchedTravellers(aryMatched: aryMatched)
        self.createDisplaArrayForUnmatchedTravellers(aryUnmatched: aryUnmatched)
    }
    
    func createDisplaArrayForBastMatchedTravellers(aryBastMatched:[JSON])
    {
        for i in 0..<aryBastMatched.count/2
        {
            let dict = JSON(["first":aryBastMatched[2*i],"second":aryBastMatched[(2*i)+1]])
            self.aryBastMatchedEverywhereTravellers.append(dict)
        }
        if aryBastMatched.count%2 != 0
        {
            let dict = JSON(["first":aryBastMatched.last])
            self.aryBastMatchedEverywhereTravellers.append(dict)
        }
        
    }
    func createDisplaArrayForMatchedTravellers(aryMatched:[JSON])
    {
        for i in 0..<aryMatched.count/2
        {
            let dict = JSON(["first":aryMatched[2*i],"second":aryMatched[(2*i)+1]])
            self.aryMatchedEverywhereTravellers.append(dict)
        }
        if aryMatched.count%2 != 0
        {
            let dict = JSON(["first":aryMatched.last])
            self.aryMatchedEverywhereTravellers.append(dict)
        }
        
    }
    func createDisplaArrayForUnmatchedTravellers(aryUnmatched:[JSON])
    {
        
        for i in 0..<aryUnmatched.count/2
        {
            let dict = JSON(["first":aryUnmatched[2*i],"second":aryUnmatched[(2*i)+1]])
            self.aryUnmatchedEverywhereTravellers.append(dict)
        }
        if aryUnmatched.count%2 != 0
        {
            let dict = JSON(["first":aryUnmatched.last])
            self.aryUnmatchedEverywhereTravellers.append(dict)
        }
        if self.aryBastMatchedEverywhereTravellers.count == 0
        {
            if self.aryMatchedEverywhereTravellers.count == 0
            {
                self.createNoMatchTableHeaderView()
            }
            else
            {
                let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.tblTraveller.bounds.size.width, height: CGFloat.leastNormalMagnitude))
                self.tblTraveller.tableHeaderView = view
            }
        }
        else
        {
            let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.tblTraveller.bounds.size.width, height: CGFloat.leastNormalMagnitude))
            self.tblTraveller.tableHeaderView = view
        }
        
        self.tblTraveller.reloadData()
        
    }
    func reloadCollectionData()
    {
        self.aryBastMatchedEverywhereTravellers.removeAll()
        self.aryUnmatchedEverywhereTravellers.removeAll()
        self.aryMatchedEverywhereTravellers.removeAll()
        self.separateArrayForMatchedAndUnmatchedUsers()
    }
    func createNoMatchTableHeaderView()
    {
        let hederView = UIView(frame:CGRect(x:0,y:0,width:self.screenWidth!,height:130))
        let lblTitle = UILabel(frame:CGRect(x:20,y:0,width:self.screenWidth!/2,height:50))
        lblTitle.text = "Matched Trips"
        lblTitle.textColor = UIColor(red: 29/255, green: 32/255, blue: 33/255, alpha: 1)
        lblTitle.font = UIFont(name: "Comfortaa-Bold", size: 18)
        lblTitle.textAlignment = .left
        lblTitle.backgroundColor = UIColor.white
        hederView.addSubview(lblTitle)
        hederView.addSubview(showDateButtonHeaderView())
        hederView.frame.size.height = 270
        if self.aryDestinationCityCountryEverywhereTravellers.count > 0
        {
            createMatchedTripsHeadeView(viewHeader: hederView)
        }
        else
        {
            hederView.addSubview(showNoPlanHeaderView(viewHeader: hederView))
        }
        
        let vwShowMessage: UIView = UIView(frame: CGRect(x:0,y:lblTitle.frame.size.height+self.cVCityCountryList.frame.size.height ,width:self.screenWidth!,height:180))
        vwShowMessage.backgroundColor = UIColor.groupTableViewBackground
        
        let imgNoUserFound: UIImageView = UIImageView(frame: CGRect(x: self.screenWidth!/2-50, y: 10, width: 100, height: 90))
        imgNoUserFound.image = UIImage(named: "ic_nouser")
        vwShowMessage.addSubview(imgNoUserFound)
        
        let lblNoContent = UILabel(frame:CGRect(x:16,y:imgNoUserFound.frame.origin.y+imgNoUserFound.frame.size.height+10 ,width:self.screenWidth!-32,height:80))
        lblNoContent.text = (checkTravelerSelectedDate(endDateStr: self.endDate) == true) ? "No travelers matched going to the chosen destinations between selected dates. You may fine-tune your destinations and dates to get some relevant trip matches." : "your travel dates are in the past! Please update your plan for your next trip."
        lblNoContent.textColor = UIColor.darkText
        lblNoContent.font = UIFont(name: "Comfortaa-Bold", size: 13)
        lblNoContent.textAlignment = .center
        lblNoContent.numberOfLines = 0
        lblNoContent.sizeToFit()
        vwShowMessage.addSubview(lblNoContent)
        vwShowMessage.frame.size.height = imgNoUserFound.frame.size.height + lblNoContent.frame.size.height + 30
        hederView.addSubview(vwShowMessage)
        hederView.backgroundColor = UIColor.white
        hederView.frame.size.height = vwShowMessage.frame.size.height + vwShowMessage.frame.origin.y
        self.tblTraveller.tableHeaderView = hederView
    }
    //MARK:- Check Post Date
    func checkTravelerSelectedDate(endDateStr: String) -> Bool
    {
        let formatter = DateFormatter()
        if endDateStr.components(separatedBy: " ").count == 3
        {
            formatter.dateFormat = "dd MMM yy"
            formatter.timeZone = TimeZone.current
            let startDate = formatter.date(from: endDateStr)
            let currentDate = Date()
            let dateStr = formatter.string(from: currentDate)
            let againDate = formatter.date(from: dateStr)
            
            guard startDate != nil else {
                return true
            }
            let order = Calendar.current.compare(againDate!, to: startDate!, toGranularity: .day)
            switch order {
            case .orderedAscending:
                return true
            case .orderedDescending:
                return false
            default:
                return true
            }
        }
        else if endDateStr.components(separatedBy: " ").count == 2
        {
            formatter.dateFormat = "MMM yy"
            formatter.timeZone = TimeZone.current
            let startDate = formatter.date(from: endDateStr)
            let currentDate = Date()
            let dateStr = formatter.string(from: currentDate)
            let againDate = formatter.date(from: dateStr)
            
            guard startDate != nil else {
                return true
            }
            
            let order = Calendar.current.compare(againDate!, to: startDate!, toGranularity: .month)
            switch order {
            case .orderedAscending:
                return true
            case .orderedDescending:
                return false
            default:
                return true
            }
        }
        else
        {
            return true
        }
    }
    
    //MARK:- Add Date Button on section
    func showDateButtonHeaderView() -> UIButton
    {
        let btnTerms: UIButton = UIButton(frame: CGRect(x: self.screenWidth!/2+20, y: 5, width: self.screenWidth!/2-32, height: 45))
        btnTerms.addTarget(self, action: #selector(btnDateClick), for: .touchUpInside)
        btnTerms.titleLabel?.textAlignment = .right
        btnTerms.titleLabel?.textColor = UIColor(red: 26/255.0, green: 39/255.0, blue: 55/255.0, alpha: 1.0)
        var textDate:String = String()
        if self.startDate.components(separatedBy: " ").count == 3
        {
            let startDateYear = self.startDate.components(separatedBy: " ")
            let endDateYear = self.endDate.components(separatedBy: " ")
            if startDateYear.last == endDateYear.last
            {
                textDate = startDateYear[0] + " " + startDateYear[1] + " - " + endDateYear[0] + " " + endDateYear[1]
            }
            else
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMM yy"
                let startDateFormate = formatter.string(from: (formatter.date(from: self.startDate))!).uppercased()
                let endDateFormate = formatter.string(from: (formatter.date(from: self.endDate))!).uppercased()
                textDate = startDateFormate + " - " + endDateFormate
            }
        }
        else if self.startDate.components(separatedBy: " ").count == 2
        {
            textDate = self.startDate + " - " + self.endDate
        }
        else
        {
            textDate = (self.startDate.lowercased() == "flexible") ? "Someday" : "Someday"
        }
        let textRange = NSMakeRange(0, textDate.count)
        let attributedText = NSMutableAttributedString(string: textDate)
        attributedText.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedStringKey , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        attributedText.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 26/255.0, green: 39/255.0, blue: 55/255.0, alpha: 1.0) , range: NSMakeRange(0, textDate.count))
        attributedText.addAttributes([kCTFontAttributeName as NSAttributedStringKey : UIFont(name: "Comfortaa-Bold", size: 12)!], range: textRange)
        let combination = NSMutableAttributedString()
        combination.append(attributedText)
        btnTerms.contentHorizontalAlignment = .right
        btnTerms.setAttributedTitle(combination, for: .normal)
        return btnTerms
    }
    
    func showNoPlanHeaderView(viewHeader: UIView) -> UIView
    {
        let vwNoPlan: UIView = UIView(frame: CGRect(x: 0, y: 50, width: self.screenWidth!, height: 40))
        let lblNoPlantext: UILabel = UILabel(frame: CGRect(x: 15, y: 10, width: self.screenWidth!-60, height: 30))
        lblNoPlantext.numberOfLines = 1
        lblNoPlantext.textAlignment = .center
        lblNoPlantext.textColor = UIColor(red: 29/255, green: 32/255, blue: 33/255, alpha: 1)
        lblNoPlantext.font = UIFont(name: "Comfortaa-Bold", size: 13)!
        lblNoPlantext.text = "No destination chosen"
        lblNoPlantext.sizeToFit()
        vwNoPlan.addSubview(lblNoPlantext)
        
        let btnPlus: UIButton = UIButton(frame: CGRect(x: lblNoPlantext.frame.origin.x+lblNoPlantext.frame.size.width + 10, y: 3, width: 30, height: 30))
        btnPlus.addTarget(self, action: #selector(btnNoPlanPlus), for: .touchUpInside)
        btnPlus.setImage(UIImage(named: "add_city.png"), for: .normal)
        btnPlus.imageEdgeInsets = UIEdgeInsetsMake(3, 3, 3, 3)
        btnPlus.layer.cornerRadius = 15
        btnPlus.clipsToBounds = true
        //        btnPlus.backgroundColor = UIColor.init(red: 238/255.0, green: 241/255.0, blue: 245/255.0, alpha: 1.0)
        vwNoPlan.addSubview(btnPlus)
        return vwNoPlan
    }
    @objc func btnNoPlanPlus()
    {
        let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLEditDestinationVC") as! CYLEditDestinationVC
        destinationVC.postNotificationRefreshDestination = "home"
        destinationVC.aryCountry = CYLGlobals.sharedInstance.usrObject!.countries!
        destinationVC.aryCity = CYLGlobals.sharedInstance.usrObject!.cities!
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    @objc func btnDateClick()
    {
        let calendarVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLEditCalendarVC") as! CYLEditCalendarVC
        calendarVC.postNotificationRefreshCalender = "home"
        calendarVC.startDate = self.startDate
        calendarVC.endDate = self.endDate
        self.navigationController?.pushViewController(calendarVC, animated: true)
    }
    @objc func notifyHandlePreselectedUser(notyfn:Notification)
    {
        let object = notyfn.object as AnyObject
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserProfileVC") as! CYLUserProfileVC
        userProfileVC.travellerId = Int(object["user_id"] as! String)!
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom Button Click Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToUserProfileSecondClicked(_ sender: UIButton)
    {
        let travellerId = (sender.tag/1000 == 2) ? self.aryBastMatchedEverywhereTravellers[sender.tag-2000]["second"]["id"].intValue : (sender.tag/1000 == 3) ? self.aryMatchedEverywhereTravellers[sender.tag-3000]["second"]["id"].intValue : self.aryUnmatchedEverywhereTravellers[sender.tag-4000]["second"]["id"].intValue
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserProfileVC") as! CYLUserProfileVC
        userProfileVC.travellerId = travellerId
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    @IBAction func btnToSayWaveSecondClick(_ sender: UIButton)
    {
        if CYLGlobals.sharedInstance.usrObject!.wavesRemaining != 0
        {
            self.waveButtinClick = sender
            self.waveFirstSecondClick = 2
            if UserDefaults.standard.value(forKey: "messageWave_count") as! Int == 0
            {
                self.createFirstWavePopUp()
            }
            else
            {
                waveClickStartAnimationSecondEveryWhere()
            }
        }
        else
        {
            self.waveLimitExcedTodayEverywhere()
            self.waveFirstSecondClick = 2
        }
    }
    func createFirstWavePopUp()
    {
        var firstName = String()
        var gender: String = String()
        if self.waveButtinClick.tag/1000 == 2
        {
            let userDetail: JSON = self.aryBastMatchedEverywhereTravellers[self.waveButtinClick.tag-2000]
            if self.waveFirstSecondClick == 1
            {
                firstName = userDetail["first"]["firstName"].stringValue != "" ? userDetail["first"]["firstName"].stringValue : userDetail["first"]["userName"].stringValue
                gender = userDetail["first"]["gender"].intValue == 0 ? "he":"she"
            }else
            {
                firstName = userDetail["second"]["firstName"].stringValue != "" ? userDetail["second"]["firstName"].stringValue : userDetail["second"]["userName"].stringValue
                gender = userDetail["second"]["gender"].intValue == 0 ? "he":"she"
            }
            
        }
        else if self.waveButtinClick.tag/1000 == 3
        {
            let userDetail: JSON = self.aryMatchedEverywhereTravellers[self.waveButtinClick.tag-3000]
            if self.waveFirstSecondClick == 1
            {
                firstName = userDetail["first"]["firstName"].stringValue != "" ? userDetail["first"]["firstName"].stringValue : userDetail["first"]["userName"].stringValue
                gender = userDetail["first"]["gender"].intValue == 0 ? "he":"she"
            }else
            {
                firstName = userDetail["second"]["firstName"].stringValue != "" ? userDetail["second"]["firstName"].stringValue : userDetail["second"]["userName"].stringValue
                gender = userDetail["second"]["gender"].intValue == 0 ? "he":"she"
            }
            
        }
        else
        {
            let userDetail: JSON = self.aryUnmatchedEverywhereTravellers[self.waveButtinClick.tag-4000]
            if self.waveFirstSecondClick == 1
            {
                firstName = userDetail["first"]["firstName"].stringValue != "" ? userDetail["first"]["firstName"].stringValue : userDetail["first"]["userName"].stringValue
                gender = userDetail["first"]["gender"].intValue == 0 ? "he":"she"
            }else
            {
                firstName = userDetail["second"]["firstName"].stringValue != "" ? userDetail["second"]["firstName"].stringValue : userDetail["second"]["userName"].stringValue
                gender = userDetail["second"]["gender"].intValue == 0 ? "he":"she"
            }
        }
        
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwFirstWave = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwFirstWave.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.8)
            
            let popUpView = UIView(frame: CGRect(x: 40, y: vwFirstWave.frame.height/2-145, width: vwLoader.frame.width-80, height: 290))
            popUpView.backgroundColor = UIColor.white
            popUpView.layer.cornerRadius = 8
            popUpView.layer.masksToBounds = true
            vwFirstWave.addSubview(popUpView)
            
            let lblName = UILabel(frame: CGRect(x: 20, y: 40, width: popUpView.frame.width-40, height: 30))
            lblName.text = "Wanna wave at \(firstName)?"
            lblName.font = UIFont(name: "Pacifico-Bold", size: 24.0)
            lblName.textColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            lblName.adjustsFontSizeToFitWidth = true
            lblName.textAlignment = .center
            popUpView.addSubview(lblName)
            
            let lblTitle = UILabel(frame: CGRect(x: 20, y: lblName.frame.origin.y+lblName.frame.size.height+30, width: popUpView.frame.width-40, height: 30))
            lblTitle.text = "Secretly wave at \(gender == "he" ? "him":"her"). If \(gender) also wave at you, you'll be able to chat"
            lblTitle.font = UIFont(name: "Comfortaa-Bold", size: 16.0)
            lblTitle.textColor = UIColor.lightGray
            lblTitle.numberOfLines = 0
            lblTitle.textAlignment = .center
            lblTitle.sizeToFit()
            popUpView.addSubview(lblTitle)
            
            let btnCancle: UIButton = UIButton(frame: CGRect(x: 0, y: popUpView.frame.size.height-45, width: popUpView.frame.width, height: 45))
            btnCancle.addTarget(self, action: #selector(btnFirstWaveAction(_:)), for: .touchUpInside)
            btnCancle.titleLabel?.textAlignment = .center
            btnCancle.tag = 1002
            btnCancle.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 16)
            btnCancle.setTitleColor(UIColor.lightGray, for: .normal)
            btnCancle.setTitle("Cancel", for: UIControlState.normal)
            popUpView.addSubview(btnCancle)
            
            let vwLine1 = UIView(frame: CGRect(x: 0, y: btnCancle.frame.origin.y-1, width: popUpView.frame.width, height: 1))
            vwLine1.backgroundColor = UIColor.lightGray
            popUpView.addSubview(vwLine1)
            
            let btnWaveSecretly: UIButton = UIButton(frame: CGRect(x: 0, y: vwLine1.frame.origin.y-45, width: popUpView.frame.width, height: 45))
            btnWaveSecretly.addTarget(self, action: #selector(btnFirstWaveAction(_:)), for: .touchUpInside)
            btnWaveSecretly.tag = 1001
            popUpView.addSubview(btnWaveSecretly)
            
            let imgHand = UIImageView(frame: CGRect(x: popUpView.frame.width/2-60, y: btnWaveSecretly.frame.origin.y+12, width: 20, height: 20))
            imgHand.image = UIImage(named: "ic_hand.png")
            imgHand.image = imgHand.image!.withRenderingMode(.alwaysTemplate)
            imgHand.tintColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            popUpView.addSubview(imgHand)
            
            let lblWaveSecretly = UILabel(frame: CGRect(x: imgHand.frame.size.width+imgHand.frame.origin.x+5, y: btnWaveSecretly.frame.origin.y, width: 150, height: 45))
            lblWaveSecretly.text = "Wave Secretly"
            lblWaveSecretly.font = UIFont(name: "Comfortaa-Bold", size: 16.0)
            lblWaveSecretly.textColor =  UIColor(red: 238/255.0, green: 51/255.0, blue: 128/255.0, alpha: 1.0)
            lblWaveSecretly.textAlignment = .left
            popUpView.addSubview(lblWaveSecretly)
            
            let vwLine2 = UIView(frame: CGRect(x: 0, y: btnWaveSecretly.frame.origin.y-1, width: popUpView.frame.width, height: 1))
            vwLine2.backgroundColor = UIColor.lightGray
            popUpView.addSubview(vwLine2)
            
            let imgLogo = UIImageView(frame: CGRect(x: vwFirstWave.frame.width/2-40, y: popUpView.frame.origin.y-40, width: 80, height: 80))
            imgLogo.image = UIImage(named: "pay_wave")
            imgLogo.tintColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
            vwFirstWave.addSubview(imgLogo)
            window.addSubview(vwFirstWave)
        }
    }
    
    func waveClickStartAnimationSecondEveryWhere()
    {
        let moveX = (self.view.frame.size.width == 320) ? 80:100
        let views = self.waveButtinClick.superview//sender.superview
        
        for view in views!.subviews
        {
            if (view.isKind(of: UIImageView.self))
            {
                let imageView: UIImageView = view as! UIImageView
                UIView.animate(withDuration: 2.5, animations: {
                    views?.alpha = 0.0
                })
                imageView.run(.sequence([
                    .moveX(CGFloat(-moveX), widht: 40, height: 40),
                    .swing(),
                    .moveX(CGFloat(moveX), widht: -40, height: -40)
                    
                    ]), completion: {
                        UIView.animate(withDuration: 1.0, animations: {
                            views?.alpha = 1.0
                            
                            if self.waveButtinClick.tag/1000 == 2
                            {
                                var userDetail: JSON = self.aryBastMatchedEverywhereTravellers[self.waveButtinClick.tag-2000]
                                
                                userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["second"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["second"]["wave"]["waveEmoticonType"].intValue)
                                
                                self.aryBastMatchedEverywhereTravellers.remove(at: self.waveButtinClick.tag-2000)
                                
                                self.aryBastMatchedEverywhereTravellers.insert(userDetail, at: self.waveButtinClick.tag-2000)
                                
                                let waveObject: JSON = self.aryBastMatchedEverywhereTravellers[self.waveButtinClick.tag-2000]["second"]["wave"]
                                
                                let otherUserId = self.aryBastMatchedEverywhereTravellers[self.waveButtinClick.tag-2000]["second"]["id"].intValue
                                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                                self.aryEverywhereTravellers[index!]["wave"] = waveObject
                                
                                DispatchQueue.main.async {
                                    let indexPath = IndexPath(item: self.waveButtinClick.tag-2000, section: 0)
                                    self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                                }
                                
                                self.requestToSendWaveForTravelerEverywhere(index: self.waveButtinClick.tag-2000, arr: self.aryBastMatchedEverywhereTravellers, cell: "second", section: 0, sectionIndexReload: self.waveButtinClick.tag/1000)
                                
                            }
                            else if self.waveButtinClick.tag/1000 == 3
                            {
                                var userDetail: JSON = self.aryMatchedEverywhereTravellers[self.waveButtinClick.tag-3000]
                                
                                userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["second"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["second"]["wave"]["waveEmoticonType"].intValue)
                                
                                self.aryMatchedEverywhereTravellers.remove(at: self.waveButtinClick.tag-3000)
                                
                                self.aryMatchedEverywhereTravellers.insert(userDetail, at: self.waveButtinClick.tag-3000)
                                
                                let waveObject: JSON = self.aryMatchedEverywhereTravellers[self.waveButtinClick.tag-3000]["second"]["wave"]
                                let otherUserId = self.aryMatchedEverywhereTravellers[self.waveButtinClick.tag-3000]["second"]["id"].intValue
                                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                                self.aryEverywhereTravellers[index!]["wave"] = waveObject
                                
                                let section = (self.aryBastMatchedEverywhereTravellers.count == 0) ? 0 : 1
                                
                                DispatchQueue.main.async {
                                    let indexPath = IndexPath(item: self.waveButtinClick.tag-3000, section: section)
                                    self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                                }
                                
                                self.requestToSendWaveForTravelerEverywhere(index: self.waveButtinClick.tag-3000, arr: self.aryMatchedEverywhereTravellers, cell: "second", section: section, sectionIndexReload: self.waveButtinClick.tag/1000)
                            }
                            else
                            {
                                var userDetail: JSON = self.aryUnmatchedEverywhereTravellers[self.waveButtinClick.tag-4000]
                                
                                userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["second"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["second"]["wave"]["waveEmoticonType"].intValue)
                                
                                self.aryUnmatchedEverywhereTravellers.remove(at: self.waveButtinClick.tag-4000)
                                
                                self.aryUnmatchedEverywhereTravellers.insert(userDetail, at: self.waveButtinClick.tag-4000)
                                
                                let waveObject: JSON = self.aryUnmatchedEverywhereTravellers[self.waveButtinClick.tag-4000]["second"]["wave"]
                                let otherUserId = self.aryUnmatchedEverywhereTravellers[self.waveButtinClick.tag-4000]["second"]["id"].intValue
                                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                                self.aryEverywhereTravellers[index!]["wave"] = waveObject
                                
                                let section = (self.aryBastMatchedEverywhereTravellers.count > 0 && self.aryMatchedEverywhereTravellers.count > 0) ? 2 : (self.aryMatchedEverywhereTravellers.count>0 && self.aryBastMatchedEverywhereTravellers.count==0) ? 1 : (self.aryMatchedEverywhereTravellers.count==0 && self.aryBastMatchedEverywhereTravellers.count>0) ? 2 : 0
                                
                                DispatchQueue.main.async {
                                    let indexPath = IndexPath(item: self.waveButtinClick.tag-4000, section: section)
                                    self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                                }
                                
                                self.requestToSendWaveForTravelerEverywhere(index: self.waveButtinClick.tag-4000, arr: self.aryUnmatchedEverywhereTravellers, cell: "second", section: section, sectionIndexReload: self.waveButtinClick.tag/1000)
                            }
                        })
                })
            }
        }
    }
    @IBAction func btnToUserProfileFirstClicked(_ sender: UIButton)
    {
        let travellerId = (sender.tag/1000 == 2) ? self.aryBastMatchedEverywhereTravellers[sender.tag-2000]["first"]["id"].intValue : (sender.tag/1000 == 3) ? self.aryMatchedEverywhereTravellers[sender.tag-3000]["first"]["id"].intValue : self.aryUnmatchedEverywhereTravellers[sender.tag-4000]["first"]["id"].intValue
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserProfileVC") as! CYLUserProfileVC
        userProfileVC.travellerId = travellerId
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    @IBAction func btnToSayWaveFirstClick(_ sender: UIButton)
    {
        if CYLGlobals.sharedInstance.usrObject!.wavesRemaining != 0
        {
            self.waveButtinClick = sender
            self.waveFirstSecondClick = 1
            if UserDefaults.standard.value(forKey: "messageWave_count") as! Int == 0
            {
                self.createFirstWavePopUp()
            }
            else
            {
                waveClickStartAnimationFirstEveryWhere()
            }
        }
        else
        {
            self.waveLimitExcedTodayEverywhere()
            self.waveFirstSecondClick = 1
        }
    }
    func waveClickStartAnimationFirstEveryWhere()
    {
        let moveX = (self.view.frame.size.width == 320) ? 80:100
        let views = self.waveButtinClick.superview
        
        for view in views!.subviews
        {
            if (view.isKind(of: UIImageView.self))
            {
                let imageView: UIImageView = view as! UIImageView
                
                UIView.animate(withDuration: 2.5, animations: {
                    views?.alpha = 0.0
                })
                imageView.run(.sequence([
                    .moveX(CGFloat(-moveX), widht: 40, height: 40),
                    .swing(),
                    .moveX(CGFloat(moveX), widht: -40, height: -40)
                    
                    ]), completion: {
                        UIView.animate(withDuration: 1.0, animations: {
                            views?.alpha = 1.0
                            
                            if self.waveButtinClick.tag/1000 == 2
                            {
                                var userDetail: JSON = self.aryBastMatchedEverywhereTravellers[self.waveButtinClick.tag-2000]
                                userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["first"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["first"]["wave"]["waveEmoticonType"].intValue)
                                
                                self.aryBastMatchedEverywhereTravellers.remove(at: self.waveButtinClick.tag-2000)
                                
                                self.aryBastMatchedEverywhereTravellers.insert(userDetail, at: self.waveButtinClick.tag-2000)
                                
                                let waveObject: JSON = self.aryBastMatchedEverywhereTravellers[self.waveButtinClick.tag-2000]["first"]["wave"]
                                let otherUserId = self.aryBastMatchedEverywhereTravellers[self.waveButtinClick.tag-2000]["first"]["id"].intValue
                                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                                self.aryEverywhereTravellers[index!]["wave"] = waveObject
                                
                                DispatchQueue.main.async {
                                    let indexPath = IndexPath(item: self.waveButtinClick.tag-2000, section: 0)
                                    self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                                }
                                
                                self.requestToSendWaveForTravelerEverywhere(index: self.waveButtinClick.tag-2000, arr: self.aryBastMatchedEverywhereTravellers, cell: "first", section: 0, sectionIndexReload: self.waveButtinClick.tag/1000)
                            }
                            else if self.waveButtinClick.tag/1000 == 3
                            {
                                var userDetail: JSON = self.aryMatchedEverywhereTravellers[self.waveButtinClick.tag-3000]
                                userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["first"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["first"]["wave"]["waveEmoticonType"].intValue)
                                
                                self.aryMatchedEverywhereTravellers.remove(at: self.waveButtinClick.tag-3000)
                                
                                self.aryMatchedEverywhereTravellers.insert(userDetail, at: self.waveButtinClick.tag-3000)
                                
                                let waveObject: JSON = self.aryMatchedEverywhereTravellers[self.waveButtinClick.tag-3000]["first"]["wave"]
                                let otherUserId = self.aryMatchedEverywhereTravellers[self.waveButtinClick.tag-3000]["first"]["id"].intValue
                                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                                self.aryEverywhereTravellers[index!]["wave"] = waveObject
                                
                                let section = (self.aryBastMatchedEverywhereTravellers.count == 0) ? 0 : 1
                                DispatchQueue.main.async {
                                    let indexPath = IndexPath(item: self.waveButtinClick.tag-3000, section: section)
                                    self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                                }
                                self.requestToSendWaveForTravelerEverywhere(index: self.waveButtinClick.tag-3000, arr: self.aryMatchedEverywhereTravellers, cell: "first", section: section, sectionIndexReload: self.waveButtinClick.tag/1000)
                            }
                            else
                            {
                                var userDetail: JSON = self.aryUnmatchedEverywhereTravellers[self.waveButtinClick.tag-4000]
                                userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 0) ? JSON(1).intValue : (userDetail["first"]["wave"]["waveEmoticonType"].intValue == 2) ? JSON(3).intValue : userDetail["first"]["wave"]["waveEmoticonType"].intValue)
                                
                                self.aryUnmatchedEverywhereTravellers.remove(at: self.waveButtinClick.tag-4000)
                                
                                self.aryUnmatchedEverywhereTravellers.insert(userDetail, at: self.waveButtinClick.tag-4000)
                                
                                let waveObject: JSON = self.aryUnmatchedEverywhereTravellers[self.waveButtinClick.tag-4000]["first"]["wave"]
                                let otherUserId = self.aryUnmatchedEverywhereTravellers[self.waveButtinClick.tag-4000]["first"]["id"].intValue
                                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                                self.aryEverywhereTravellers[index!]["wave"] = waveObject
                                
                                let section = (self.aryBastMatchedEverywhereTravellers.count > 0 && self.aryMatchedEverywhereTravellers.count > 0) ? 2 : (self.aryMatchedEverywhereTravellers.count>0 && self.aryBastMatchedEverywhereTravellers.count==0) ? 1 : (self.aryMatchedEverywhereTravellers.count==0 && self.aryBastMatchedEverywhereTravellers.count>0) ? 2 : 0
                                
                                DispatchQueue.main.async {
                                    let indexPath = IndexPath(item: self.waveButtinClick.tag-4000, section: section)
                                    self.tblTraveller.reloadRows(at: [indexPath], with: .none)
                                }
                                
                                self.requestToSendWaveForTravelerEverywhere(index: self.waveButtinClick.tag-4000, arr: self.aryUnmatchedEverywhereTravellers, cell: "first", section: section, sectionIndexReload: self.waveButtinClick.tag/1000)
                            }
                            
                        })
                })
            }
        }
    }
    func waveApiFailRequestEverWhere(index: Int, arr: [JSON], cell: String, section: Int,sectionIndexReload: Int)
    {
        if sectionIndexReload == 2
        {
            if cell == "first"
            {
                var userDetail: JSON = self.aryBastMatchedEverywhereTravellers[index]
                userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue :  JSON(0).intValue)
                
                self.aryBastMatchedEverywhereTravellers.remove(at: index)
                
                self.aryBastMatchedEverywhereTravellers.insert(userDetail, at: index)
                
                let waveObject: JSON = self.aryBastMatchedEverywhereTravellers[index]["first"]["wave"]
                let otherUserId = self.aryBastMatchedEverywhereTravellers[index]["first"]["id"].intValue
                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                self.aryEverywhereTravellers[index!]["wave"] = waveObject
            }
            else
            {
                var userDetail: JSON = self.aryBastMatchedEverywhereTravellers[index]
                userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue : JSON(0).intValue)
                
                self.aryBastMatchedEverywhereTravellers.remove(at: index)
                
                self.aryBastMatchedEverywhereTravellers.insert(userDetail, at: index)
                
                let waveObject: JSON = self.aryBastMatchedEverywhereTravellers[index]["second"]["wave"]
                let otherUserId = self.aryBastMatchedEverywhereTravellers[index]["second"]["id"].intValue
                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                self.aryEverywhereTravellers[index!]["wave"] = waveObject
            }
        }
        else if sectionIndexReload == 3
        {
            
            if cell == "first"
            {
                var userDetail: JSON = self.aryMatchedEverywhereTravellers[index]
                userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue :  JSON(0).intValue)
                
                self.aryMatchedEverywhereTravellers.remove(at: index)
                
                self.aryMatchedEverywhereTravellers.insert(userDetail, at: index)
                
                let waveObject: JSON = self.aryMatchedEverywhereTravellers[index]["first"]["wave"]
                let otherUserId = self.aryMatchedEverywhereTravellers[index]["first"]["id"].intValue
                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                self.aryEverywhereTravellers[index!]["wave"] = waveObject
            }
            else
            {
                var userDetail: JSON = self.aryMatchedEverywhereTravellers[index]
                userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue : JSON(0).intValue)
                
                self.aryMatchedEverywhereTravellers.remove(at: index)
                
                self.aryMatchedEverywhereTravellers.insert(userDetail, at: index)
                
                let waveObject: JSON = self.aryMatchedEverywhereTravellers[index]["second"]["wave"]
                let otherUserId = self.aryMatchedEverywhereTravellers[index]["second"]["id"].intValue
                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                self.aryEverywhereTravellers[index!]["wave"] = waveObject
            }
        }
        else
        {
            
            if cell == "first"
            {
                var userDetail: JSON = self.aryUnmatchedEverywhereTravellers[index]
                userDetail["first"]["wave"]["waveEmoticonType"] = JSON((userDetail["first"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue :  JSON(0).intValue)
                
                self.aryUnmatchedEverywhereTravellers.remove(at: index)
                
                self.aryUnmatchedEverywhereTravellers.insert(userDetail, at: index)
                
                let waveObject: JSON = self.aryUnmatchedEverywhereTravellers[index]["first"]["wave"]
                let otherUserId = self.aryUnmatchedEverywhereTravellers[index]["first"]["id"].intValue
                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                self.aryEverywhereTravellers[index!]["wave"] = waveObject
            }
            else
            {
                var userDetail: JSON = self.aryUnmatchedEverywhereTravellers[index]
                userDetail["second"]["wave"]["waveEmoticonType"] = JSON((userDetail["second"]["wave"]["waveEmoticonType"].intValue == 3) ? JSON(2).intValue : JSON(0).intValue)
                
                self.aryUnmatchedEverywhereTravellers.remove(at: index)
                
                self.aryUnmatchedEverywhereTravellers.insert(userDetail, at: index)
                
                let waveObject: JSON = self.aryUnmatchedEverywhereTravellers[index]["second"]["wave"]
                let otherUserId = self.aryUnmatchedEverywhereTravellers[index]["second"]["id"].intValue
                let index = self.aryEverywhereTravellers.index(where: {$0["id"].intValue == otherUserId})
                self.aryEverywhereTravellers[index!]["wave"] = waveObject
            }
        }
        let indexPath = IndexPath(item: index, section: section)
        self.tblTraveller.reloadRows(at: [indexPath], with: .none)
    }
    func waveLimitExcedTodayEverywhere()
    {
        if CYLGlobals.sharedInstance.usrObject?.userPlanType == 0
        {
            let paymentPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserPaymentVC") as! CYLUserPaymentVC
            paymentPopup.messageLimitExceed = "wave"
            paymentPopup.delegateMessageReload = self
            self.navigationController?.pushViewController(paymentPopup, animated: false)
        }
        else
        {
            let text = showAlertTitleMessageFont(strTitle: "Wave Limit Exceeded", strMessage: CYLGlobals.sharedInstance.usrObject!.wavesMessage ?? "")
            let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            
            alert.setValue(text.title, forKey: "attributedTitle")
            alert.setValue(text.message, forKey: "attributedMessage")
            
            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                (sender) in
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func reloadMessageReceiverListDelegatePayment() {
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToSendWaveForTravelerEverywhere(index: Int, arr: [JSON], cell: String, section: Int,sectionIndexReload: Int)
    {
        let receiverId = arr[index][cell]["id"].intValue
        let params:[String: Any] = ["otherUserId":"\(receiverId)","waveCallingLocation":"EVERYWHERE"]
        let request = CYLServices.postWaveMessageForServer(dict: params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in

                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue

                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.wavesRemaining = responseData?["wavesRemaining"]?.intValue
                        userObject.wavesMessage = responseData?["message"]?.stringValue
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        UserDefaults.standard.set(1, forKey: "messageWave_count")
                        let waveEmoticonType = arr[index][cell]["wave"]["waveEmoticonType"].intValue == 1 ? "wave sent" : "wave sent back"

                        var cleverTapEventData = [String:Any]()
                        let aryCities = arr[index][cell]["cities"].arrayValue
                        let aryCountries = arr[index][cell]["countries"].arrayValue
                        var detinationType: String!
                        if aryCities.count != 0{
                            detinationType = "City"
                            cleverTapEventData["DestinationCount"] = aryCities.count
                        }
                        else if aryCountries.count != 0{
                            detinationType = "Country"
                            cleverTapEventData["DestinationCount"] = aryCountries.count
                        }
                        else{
                            detinationType = "No Plan"
                            cleverTapEventData["destinationCount"] = 0
                        }
                        cleverTapEventData["DetinationType"] = detinationType
                        cleverTapEventData["IsGenderMatched"] = arr[index][cell]["isGenderMatched"].bool
                        cleverTapEventData["IsCountryMatched"] = arr[index][cell]["isCountryMatched"].bool
    
                        let liveInDestionation = arr[index][cell]["livesInDestinationCountry"].boolValue
    
                        cleverTapEventData["UserId"] = arr[index][cell]["id"].stringValue
                        cleverTapEventData["UserName"] = arr[index][cell]["userName"].stringValue
                        cleverTapEventData["Age"] = arr[index][cell]["age"].stringValue
                        cleverTapEventData["Isolocation"] = arr[index][cell]["isoLocation"].stringValue
                        let g = (arr[index][cell]["gender"].intValue == 0) ? "Male" : "Female"
                        cleverTapEventData["Gender"] = g
                        cleverTapEventData["IsMatched"] = arr[index][cell]["is_matched"].boolValue
                        cleverTapEventData["isOnline"] = arr[index][cell]["showOnline"].boolValue
                        cleverTapEventData["page type"] = "user list"
                        cleverTapEventData["wave type"] = waveEmoticonType
                        cleverTapEventData["LivesInDestinationCountry"] = liveInDestionation
                        sentEventPropertyOnCleverTap("Wave clicked", _property: cleverTapEventData)
                        
                        if arr[index][cell]["wave"]["waveEmoticonType"].intValue == 3
                        {
                            self.view.layoutIfNeeded()
                            let matchedPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLMatchedPopUpVC") as! CYLMatchedPopUpVC
                            matchedPopup.imgRecever = arr[index][cell]["userPicUrl"].stringValue
                            matchedPopup.id = arr[index][cell]["id"].intValue
                            matchedPopup.userName = arr[index][cell]["firstName"].stringValue
                            matchedPopup.modalPresentationStyle = .overCurrentContext
                            matchedPopup.modalTransitionStyle = .crossDissolve
                            matchedPopup.moveChatDetailControllerDelegate = self
                            self.navigationController?.present(matchedPopup, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.wavesMessage = responseData?["message"]?.stringValue
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        DispatchQueue.main.async {
                            self.waveApiFailRequestEverWhere(index: index, arr: arr, cell: cell, section: section, sectionIndexReload: sectionIndexReload)
                        }
                    }
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Wave click everywhere"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    DispatchQueue.main.async {
                        self.waveApiFailRequestEverWhere(index: index, arr: arr, cell: cell, section: section, sectionIndexReload: sectionIndexReload)
                    }
                }
        },
                                                                  withError:
            {
                (error) in
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Wave click everywhere"])
                DispatchQueue.main.async {
                    self.waveApiFailRequestEverWhere(index: index, arr: arr, cell: cell, section: section, sectionIndexReload: sectionIndexReload)
                }
                self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
                
        },
                                                                  andNetworkErr: {
                                                                    (networkFailure) in
                                                                    sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Wave click everywhere"])
                                                                    DispatchQueue.main.async {
                                                                        self.waveApiFailRequestEverWhere(index: index, arr: arr, cell: cell, section: section, sectionIndexReload: sectionIndexReload)
                                                                    }
                                                                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    func requestToGetTravellersList(page:Int,str: String)
    {
        requestToGetProfileForDeletedOrBlockedUserCheck()
        self.isApiCallActive = true
        lastLoadedPage = page
        if str == ""
        {
            self.showLoadingView()
        }
        let params:[String: Any] = ["record":"\(pageSize)","start":"\(page*pageSize)","isInUserCountry":"\(0)"]
        let request = CYLServices.getTravellersList(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                self.isApiCallActive = false
                self.hideLoadingView()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        self.aryEverywhereTravellers.append(contentsOf: (responseData?["data"]?.arrayValue)!)
                        
                        if self.lastLoadedPage == 0
                        {
                            self.aryDestinationCityCountryEverywhereTravellers.removeAll()
                            if (responseData?["countries"] != nil)
                            {
                                if responseData?["countries"] != JSON.null
                                {
                                    self.aryDestinationCityCountryEverywhereTravellers.append(contentsOf: (responseData?["countries"]?.arrayValue)!)
                                }
                            }
                            if (responseData?["cities"] != nil)
                            {
                                if responseData?["cities"] != JSON.null
                                {
                                    self.aryDestinationCityCountryEverywhereTravellers.append(contentsOf: (responseData?["cities"]?.arrayValue)!)
                                }
                            }
                            if let _ = responseData?["startDate"]
                            {
                                self.startDate = (responseData?["startDate"]?.stringValue)!
                            }
                            if let _ = responseData?["endDate"]
                            {
                                self.endDate = (responseData?["endDate"]?.stringValue)!
                            }
                        }
                        
                        self.reloadCollectionData()
                        self.maxResults = (responseData?["total_records"]?.intValue)!
                        if page == 0
                        {
                            if responseData?["matchedTravellersCount"] != nil
                            {
                                sentEventPropertyOnCleverTap("Search travellers everywhere", _property: ["MatchedCount" : (responseData?["matchedTravellersCount"]?.intValue)!,"BestMatchedCount":(responseData?["bestMatchedTravellersCount"]?.intValue)!])
                            }
                            
                        }
                    }
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Search travellers everywhere"])
                    if str == "" && page == 0
                    {
                        self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
                    }
                    else
                    {
                        showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    }
                }
                
        },
                                                                  withError:
            {
                (error) in
                self.isApiCallActive = false
                self.hideLoadingView()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Search travellers everywhere"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                self.isApiCallActive = false
                self.hideLoadingView()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Search travellers everywhere"])
                if str == "" && page == 0
                {
                    self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
                }
                else
                {
                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                }
        })
    }
    func requestToGetProfileForDeletedOrBlockedUserCheck()
    {
        let params:[String: Any] = ["currentUserId":""]
        let request = CYLServices.getOtherUserProfile(dict: params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData!["status"]?.stringValue != "fail"
                    {
                        let userObjectNew = CYLUserObject.init(json: (responseData?["data"])!)
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.wavesRemaining = userObjectNew.wavesRemaining
                        userObject.wavesMessage = userObjectNew.wavesMessage
                        userObject.userPlanType = (responseData?["data"]?["paymentObj"]["planType"].int != nil) ? responseData?["data"]?["paymentObj"]["planType"].int : 0
//                        userObject.userPlanName = (responseData?["data"]?["paymentObj"]["planTypeName"]["planName"].string != nil) ? responseData?["data"]?["paymentObj"]["planTypeName"]["planName"].string : "Free"
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        
                        if let userStatus = responseData?["data"]?["adminBlocked"].boolValue
                        {
                            if userStatus == true
                            {
                                self.doLogout(str: "Your profile has been blocked, due to multiple complaints. If you think this was a mistake, please write your grievance at info@tourgetherapp.com")
                            }
                            else if let userStatus = responseData?["data"]?["is_deleted"].boolValue
                            {
                                if userStatus == true
                                {
                                    self.doLogout(str: "Your profile has been deleted due to multiple reports. You may still create a new profile again. If you think this was a mistake, please let us know your grievance at info@tourgetherapp.com")
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                else
                {
                }
                
        },
                                                                  withError:
            {
                (error) in
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
        })
    }
    
    func showLoadingView()
    {
        if self.footerView == nil || lastLoadedPage == 0
        {
            self.vwLoader.isHidden = false
            //            KVNProgress.show()
        }
    }
    func hideLoadingView()
    {
        if self.footerView != nil
        {
            self.tblTraveller.tableFooterView = nil
            self.vwLoader.isHidden = true
            //            KVNProgress.dismiss()
        }
        else
        {
            self.vwLoader.isHidden = true
            //            KVNProgress.dismiss()
        }
    }
    
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //MARK:- Deleted or Blocked User
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    func doLogout(str: String)
    {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "userObject")
        userDefaults.removeObject(forKey: "travel_filter")
        userDefaults.removeObject(forKey: "date")
        userDefaults.removeObject(forKey: "rating_count")
        userDefaults.removeObject(forKey: "messageWave_count")
        userDefaults.synchronize()
        showAlertView(title: "", message: "\(str)", ref: self)
        CYLFacebookHelper.sharedFInstance().doLogoutFromFacebook()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeRootToLoginVC()
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Header view for Matched Trips
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createMatchedTripsHeadeView(viewHeader: UIView)
    {
        cVCityCountryList.removeFromSuperview()
        viewHeader.addSubview(cVCityCountryList)
        cVCityCountryList.reloadData()
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Collection view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.aryDestinationCityCountryEverywhereTravellers.count != 0
        {
            return aryDestinationCityCountryEverywhereTravellers.count+1
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == aryDestinationCityCountryEverywhereTravellers.count
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "plusButtonCell", for: indexPath as IndexPath) as! plusButtonCollectionViewCell
            //            myCell.backgroundColor = UIColor.init(red: 238/255.0, green: 241/255.0, blue: 245/255.0, alpha: 1.0)
            myCell.layer.cornerRadius = 15
            myCell.clipsToBounds = true
            
            myCell.plusImage.image = UIImage(named: "add_city.png")
            return myCell
        }
        else
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cityCountryCell", for: indexPath as IndexPath) as! cityCountryCollectionViewCell
            myCell.backgroundColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0)
            myCell.layer.cornerRadius = 15
            myCell.clipsToBounds = true
            myCell.titleLabel.text = aryDestinationCityCountryEverywhereTravellers[indexPath.row]["cityName"].stringValue == "" ? aryDestinationCityCountryEverywhereTravellers[indexPath.row]["countryName"].stringValue : aryDestinationCityCountryEverywhereTravellers[indexPath.row]["cityName"].stringValue
            myCell.titleLabel.sizeToFit()
            return myCell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if indexPath.row == aryDestinationCityCountryEverywhereTravellers.count
        {
            return CGSize(width: 30, height: 30)
        }
        else
        {
            let name = aryDestinationCityCountryEverywhereTravellers[indexPath.row]["cityName"].stringValue == "" ? aryDestinationCityCountryEverywhereTravellers[indexPath.row]["countryName"].stringValue : aryDestinationCityCountryEverywhereTravellers[indexPath.row]["cityName"].stringValue
            let wirth = widthForInputString(text: name)
            return CGSize(width: wirth+20, height: 30)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("User tapped on item \(indexPath.row)")
        let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLEditDestinationVC") as! CYLEditDestinationVC
        destinationVC.postNotificationRefreshDestination = "home"
        destinationVC.aryCountry = CYLGlobals.sharedInstance.usrObject!.countries!
        destinationVC.aryCity = CYLGlobals.sharedInstance.usrObject!.cities!
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    //MARK: ********** Rating View ************
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        
        self.dismiss(animated: true, completion: nil)
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let customView = UIView()
        customView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let controller = UIViewController()
        customView.frame = controller.view.frame
        controller.view.addSubview(customView)
        alert.setValue(controller, forKey: "contentViewController")
        
        if rating >= 4.0
        {
            alert.title = "We are glad to know that you love Tourgether. Please show your support by giving us 5 star rating on the play store"
            let height: NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
            alert.view.addConstraint(height)
            
            let rateUs = UIAlertAction(title: "Rate us", style: .default) { (alert) in
                self.requestToSentReating(review: "", rating: rating)
                UIApplication.shared.canOpenURL(URL.init(string: "https://itunes.apple.com/app/travel-buddy-find-a-trip-companion/id1245536821?mt=8")!)
                
            }
            let mayBeLater = UIAlertAction(title: "Maybe later", style: .default) { (alert) in
                UserDefaults.standard.set(0, forKey: "rating_count")
            }
            let noThanks = UIAlertAction(title: "No, Thanks", style: .default) { (alert) in
                self.requestToSentReating(review: "", rating: rating)
            }
            alert.addAction(rateUs)
            alert.addAction(mayBeLater)
            alert.addAction(noThanks)
        }
        else
        {
            alert.title = "Please let us know why did you give that rating. Your feedback would help us to improve our service"
            let height: NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
            alert.view.addConstraint(height)
            
            let txtView: KMPlaceholderTextView = KMPlaceholderTextView(frame: CGRect(x: 20, y: 0, width: customView.frame.size.width-40, height: customView.frame.size.height-10))
            
            txtView.layer.borderColor = UIColor.darkGray.cgColor
            txtView.placeholder = "Start typing..."
            txtView.placeholderColor = .lightGray
            txtView.backgroundColor = .clear
            txtView.tintColor = .black
            txtView.placeholderFont = UIFont(name: "Comfortaa-Bold", size: 13)
            txtView.layer.borderWidth = 1
            txtView.layer.cornerRadius = 5
            txtView.font = UIFont(name: "Comfortaa-Bold", size: 13)
            txtView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin]
            customView.addSubview(txtView)
            
            let mayBeLater = UIAlertAction(title: "No, thanks", style: .default) { (alert) in
                self.requestToSentReating(review: txtView.text!, rating: rating)
            }
            let noThanks = UIAlertAction(title: "Ok, sure", style: .default) { (alert) in
                self.requestToSentReating(review: txtView.text!, rating: rating)
            }
            alert.addAction(mayBeLater)
            alert.addAction(noThanks)
        }
        present(alert, animated: true, completion: nil)
        
    }
    //Api Send Rating Review
    func requestToSentReating(review: String, rating: Float)
    {
        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
        let version = nsObject as! String
        let params:[String: Any] = ["rating":rating,"review":review,"deviceType":"IPHONE","appVersion":"\(version)"]
        let request = CYLServices.postReviewRating(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                
                if (response?.isValid)!
                {
                    let userObject = CYLGlobals.sharedInstance.usrObject!
                    userObject.isAppFeedBack = true
                    CYLGlobals.sharedInstance.usrObject = userObject
                    let userDeflts = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                    userDeflts.set(encodedData, forKey: "userObject")
                    userDeflts.synchronize()
                }
                
        },
                                                                  withError:
            {
                (error) in
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Search travellers everywhere"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Search travellers everywhere"])
                self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
        })
        
    }
    @objc func btnFirstWaveAction(_ sender: UIButton)
    {
        if sender.tag == 1001
        {
            self.vwFirstWave.isHidden = true
            if waveFirstSecondClick == 1
            {
                waveClickStartAnimationFirstEveryWhere()
            }
            else
            {
                waveClickStartAnimationSecondEveryWhere()
            }
        }
        else
        {
            self.vwFirstWave.isHidden = true
        }
    }
    
}
extension CYLEverywhereVC:CYLSendMessagePopUpDelegate
{
    func moveChatDetailController(nameRecever: String, chatId: String, imgRecever: String, receverId: Int) {
        
        let chatDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLChatDetailVC") as! CYLChatDetailVC
        chatDetailVC.userName = nameRecever
        chatDetailVC.chatId = chatId
        chatDetailVC.imgUrl = imgRecever
        chatDetailVC.receiverId = receverId
        chatDetailVC.messageDelegate = (chatId == "") ? self : nil
        self.navigationController?.pushViewController(chatDetailVC, animated: true)
        
    }
}

extension CYLEverywhereVC:CYLMessageDelegate
{
    func delegateToGetUpdatedChatId(strChatId:String)
    {
//        self.strChatId = strChatId
    }
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//MARK: Collection view Cell functions
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
class cityCountryCollectionViewCell: UICollectionViewCell {
    
    var titleLabel:UILabel = {
        let label = UILabel(frame: CGRect(x:10, y: 8.5, width: 60 , height: 30))
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont(name: "Comfortaa-Bold", size: 12)!
        label.numberOfLines = 1
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.titleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class plusButtonCollectionViewCell: UICollectionViewCell {
    
    
    var plusImage:UIImageView = {
        let plusImage = UIImageView(frame: CGRect(x:0, y: 3, width: 24 , height: 24))
        
        return plusImage
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.plusImage)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//MARK: table view data source and delegate functions
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
extension CYLEverywhereVC:UITableViewDataSource
{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let hederView = UIView(frame:CGRect(x:0,y:0,width:self.screenWidth!,height:50))
        let label = UILabel(frame:CGRect(x:20,y:0,width:self.screenWidth!/2,height:50))
        
        if self.aryBastMatchedEverywhereTravellers.count>0
        {
            if section == 0
            {
                label.text = "Best Matches"
            }
            else
            {
                if self.aryUnmatchedEverywhereTravellers.count>0 && self.aryMatchedEverywhereTravellers.count>0
                {
                    label.text = (section == 1) ? "Matched Trips" : "Active Travelers"
                    if section == 1 && self.aryDestinationCityCountryEverywhereTravellers.count > 0
                    {
                        hederView.frame.size.height = 90
                        createMatchedTripsHeadeView(viewHeader: hederView)
                        hederView.addSubview(showDateButtonHeaderView())
                    }
                    else if section == 1 && self.aryDestinationCityCountryEverywhereTravellers.count <= 0
                    {
                        hederView.frame.size.height = 90
                        hederView.addSubview(showDateButtonHeaderView())
                        hederView.addSubview(showNoPlanHeaderView(viewHeader: hederView))
                    }
                }
                else if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count == 0
                {
                    label.text = "Matched Trips"
                    if section == 1 && self.aryDestinationCityCountryEverywhereTravellers.count > 0
                    {
                        hederView.frame.size.height = 90
                        createMatchedTripsHeadeView(viewHeader: hederView)
                        hederView.addSubview(showDateButtonHeaderView())
                    }
                    else if section == 1 && self.aryDestinationCityCountryEverywhereTravellers.count <= 0
                    {
                        hederView.frame.size.height = 90
                        hederView.addSubview(showDateButtonHeaderView())
                        hederView.addSubview(showNoPlanHeaderView(viewHeader: hederView))
                    }
                }
                else if self.aryMatchedEverywhereTravellers.count == 0 && section == 1
                {
                    if section == 1 && self.aryMatchedEverywhereTravellers.count==0
                    {
                        label.text = "Matched Trips"
                        tblTraveller.backgroundColor = UIColor.white
                        if section == 1 && self.aryDestinationCityCountryEverywhereTravellers.count > 0
                        {
                            createMatchedTripsHeadeView(viewHeader: hederView)
                            hederView.addSubview(showDateButtonHeaderView())
                        }
                        else if section == 1 && self.aryDestinationCityCountryEverywhereTravellers.count <= 0
                        {
                            hederView.addSubview(showDateButtonHeaderView())
                            hederView.addSubview(showNoPlanHeaderView(viewHeader: hederView))
                        }
                        
                        let vwShowMessage: UIView = UIView(frame: CGRect(x:0,y:label.frame.size.height+self.cVCityCountryList.frame.size.height ,width:self.screenWidth!,height:180))
                        vwShowMessage.backgroundColor = UIColor.groupTableViewBackground
                        
                        let imgNoUserFound: UIImageView = UIImageView(frame: CGRect(x: self.screenWidth!/2-50, y: 10, width: 100, height: 90))
                        imgNoUserFound.image = UIImage(named: "ic_nouser")
                        vwShowMessage.addSubview(imgNoUserFound)
                        
                        let lblNoContent = UILabel(frame:CGRect(x:16,y:imgNoUserFound.frame.origin.y+imgNoUserFound.frame.size.height+10 ,width:self.screenWidth!-32,height:80))
                        lblNoContent.text = (checkTravelerSelectedDate(endDateStr: self.endDate) == true) ? "No travelers matched going to the chosen destinations between selected dates. You may fine-tune your destinations and dates to get some relevant trip matches." : "your travel dates are in the past! Please update your plan for your next trip."
                        lblNoContent.textColor = UIColor.darkText
                        lblNoContent.font = UIFont(name: "Comfortaa-Bold", size: 13)
                        lblNoContent.textAlignment = .center
                        lblNoContent.numberOfLines = 0
                        lblNoContent.sizeToFit()
                        vwShowMessage.addSubview(lblNoContent)
                        vwShowMessage.frame.size.height = imgNoUserFound.frame.size.height + lblNoContent.frame.size.height + 30
                        hederView.addSubview(vwShowMessage)
                        hederView.frame.size.height = vwShowMessage.frame.size.height + vwShowMessage.frame.origin.y
                    }
                }
                else if self.aryUnmatchedEverywhereTravellers.count>0 && self.aryMatchedEverywhereTravellers.count == 0
                {
                    label.text = "Active Travelers"
                }
                else
                {
                    hederView.isHidden = true
                }
            }
        }
        else
        {
            if self.aryUnmatchedEverywhereTravellers.count>0 && self.aryMatchedEverywhereTravellers.count>0
            {
                label.text = (section == 0) ? "Matched Trips" : "Active Travelers"
                if section == 0 && self.aryDestinationCityCountryEverywhereTravellers.count > 0
                {
                    hederView.frame.size.height = 90
                    createMatchedTripsHeadeView(viewHeader: hederView)
                    hederView.addSubview(showDateButtonHeaderView())
                }
                else if section == 0 && self.aryDestinationCityCountryEverywhereTravellers.count <= 0
                {
                    hederView.frame.size.height = 90
                    hederView.addSubview(showDateButtonHeaderView())
                    hederView.addSubview(showNoPlanHeaderView(viewHeader: hederView))
                }
            }
            else if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count == 0
            {
                label.text = "Matched Trips"
                if section == 0 && self.aryDestinationCityCountryEverywhereTravellers.count > 0
                {
                    hederView.frame.size.height = 90
                    hederView.addSubview(showDateButtonHeaderView())
                    createMatchedTripsHeadeView(viewHeader: hederView)
                }
                else if section == 0 && self.aryDestinationCityCountryEverywhereTravellers.count <= 0
                {
                    hederView.frame.size.height = 90
                    hederView.addSubview(showDateButtonHeaderView())
                    hederView.addSubview(showNoPlanHeaderView(viewHeader: hederView))
                }
            }
            else if self.aryUnmatchedEverywhereTravellers.count>0 && self.aryMatchedEverywhereTravellers.count == 0
            {
                label.text = "Active Travelers"
            }
            else
            {
                hederView.isHidden = true
            }
        }
        
        label.textColor = UIColor(red: 29/255, green: 32/255, blue: 33/255, alpha: 1)
        label.font = UIFont(name: "Comfortaa-Bold", size: 18)!
        label.textAlignment = .left
        hederView.backgroundColor = UIColor.white
        hederView.addSubview(label)
        
        return hederView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if self.aryBastMatchedEverywhereTravellers.count>0
        {
            if section == 1 && self.aryMatchedEverywhereTravellers.count > 0
            {
                return 90
            }
            else if section == 1 && self.aryMatchedEverywhereTravellers.count == 0
            {
                
                let lblNoContent = UILabel(frame:CGRect(x:16,y:100 ,width:self.screenWidth!-32,height:70))
                lblNoContent.text = (checkTravelerSelectedDate(endDateStr: self.endDate) == true) ? "No travelers matched going to the chosen destinations between selected dates. You may fine-tune your destinations and dates to get some relevant trip matches." : "your travel dates are in the past! Please update your plan for your next trip."
                lblNoContent.textColor = UIColor.darkText
                lblNoContent.font = UIFont(name: "Comfortaa-Bold", size: 13)
                lblNoContent.textAlignment = .center
                lblNoContent.numberOfLines = 0
                lblNoContent.sizeToFit()
                return 210+lblNoContent.frame.size.height
            }
            return 50
        }
        else
        {
            if section == 0 && self.aryMatchedEverywhereTravellers.count != 0
            {
                return 90
            }
            return 50
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (self.screenWidth!/2.0)+8.0
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if self.aryBastMatchedEverywhereTravellers.count>0
        {
            if self.aryBastMatchedEverywhereTravellers.count > 0 && self.aryMatchedEverywhereTravellers.count == 0 && self.aryUnmatchedEverywhereTravellers.count == 0
            {
                return 1
            }
            else if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count>0
            {
                return 3
            }
            else if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count==0
            {
                return 2
            }
            else
            {
                return 3
            }
        }
        else
        {
            return (self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count>0) ? 2 : 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.aryBastMatchedEverywhereTravellers.count>0
        {
            if section == 0 { return self.aryBastMatchedEverywhereTravellers.count}
            else
            {
                if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count>0
                {
                    return (section==1) ? self.aryMatchedEverywhereTravellers.count : self.aryUnmatchedEverywhereTravellers.count
                }
                else if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count==0
                {
                    return self.aryMatchedEverywhereTravellers.count
                }
                else if self.aryMatchedEverywhereTravellers.count == 0 && section == 1
                {
                    return self.aryMatchedEverywhereTravellers.count
                }
                else
                {
                    return self.aryUnmatchedEverywhereTravellers.count
                }
            }
            
        }
        else
        {
            if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count>0
            {
                return (section==0) ? self.aryMatchedEverywhereTravellers.count : self.aryUnmatchedEverywhereTravellers.count
            }
            else if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count==0
            {
                return self.aryMatchedEverywhereTravellers.count
            }
            else
            {
                return self.aryUnmatchedEverywhereTravellers.count
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CYLTravellerListCell") as! CYLTravellerListCell
        
        self.setTravelerUnloadImageColor(index: indexPath.row, cell: cell)
        
        var displayArray = [JSON]()
        if self.aryBastMatchedEverywhereTravellers.count>0
        {
            if indexPath.section == 0
            {
                displayArray = self.aryBastMatchedEverywhereTravellers
                cell.btnToFirst.tag = 2000+indexPath.row+(indexPath.section*1000)
                cell.btnToSecond.tag = 2000+indexPath.row+(indexPath.section*1000)
                cell.btnSayWaveFirst.tag = 2000+indexPath.row+(indexPath.section*1000)
                cell.btnSayWaveSecond.tag = 2000+indexPath.row+(indexPath.section*1000)
            }
            else
            {
                if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count>0
                {
                    if indexPath.section == 1
                    {
                        displayArray = self.aryMatchedEverywhereTravellers
                        cell.btnToFirst.tag = 3000+indexPath.row
                        cell.btnToSecond.tag = 3000+indexPath.row
                        cell.btnSayWaveFirst.tag = 3000+indexPath.row
                        cell.btnSayWaveSecond.tag = 3000+indexPath.row
                    }
                    else
                    {
                        displayArray = self.aryUnmatchedEverywhereTravellers
                        cell.btnToFirst.tag = 4000+indexPath.row
                        cell.btnToSecond.tag = 4000+indexPath.row
                        cell.btnSayWaveFirst.tag = 4000+indexPath.row
                        cell.btnSayWaveSecond.tag = 4000+indexPath.row
                    }
                }
                else if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count==0
                {
                    displayArray = self.aryMatchedEverywhereTravellers
                    cell.btnToFirst.tag = 3000+indexPath.row
                    cell.btnToSecond.tag = 3000+indexPath.row
                    cell.btnSayWaveFirst.tag = 3000+indexPath.row
                    cell.btnSayWaveSecond.tag = 3000+indexPath.row
                }
                else if indexPath.section == 1 && self.aryMatchedEverywhereTravellers.count == 0
                {
                    
                }
                else
                {
                    displayArray = self.aryUnmatchedEverywhereTravellers
                    cell.btnToFirst.tag = 4000+indexPath.row
                    cell.btnToSecond.tag = 4000+indexPath.row
                    cell.btnSayWaveFirst.tag = 4000+indexPath.row
                    cell.btnSayWaveSecond.tag = 4000+indexPath.row
                }
            }
        }
        else
        {
            if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count>0
            {
                displayArray = (indexPath.section == 0) ? self.aryMatchedEverywhereTravellers : self.aryUnmatchedEverywhereTravellers
                cell.btnToFirst.tag = 3000+indexPath.row+(indexPath.section*1000)
                cell.btnToSecond.tag = 3000+indexPath.row+(indexPath.section*1000)
                cell.btnSayWaveFirst.tag = 3000+indexPath.row+(indexPath.section*1000)
                cell.btnSayWaveSecond.tag = 3000+indexPath.row+(indexPath.section*1000)
            }
            else if self.aryMatchedEverywhereTravellers.count>0 && self.aryUnmatchedEverywhereTravellers.count==0
            {
                displayArray = self.aryMatchedEverywhereTravellers
                cell.btnToFirst.tag = 3000+indexPath.row
                cell.btnToSecond.tag = 3000+indexPath.row
                cell.btnSayWaveFirst.tag = 3000+indexPath.row
                cell.btnSayWaveSecond.tag = 3000+indexPath.row
            }
            else
            {
                displayArray = self.aryUnmatchedEverywhereTravellers
                cell.btnToFirst.tag = 4000+indexPath.row
                cell.btnToSecond.tag = 4000+indexPath.row
                cell.btnSayWaveFirst.tag = 4000+indexPath.row
                cell.btnSayWaveSecond.tag = 4000+indexPath.row
            }
        }
        cell.comesEveryWhereNearMe = "everywhere"
        cell.object = displayArray[indexPath.row]
        cell.selectionStyle = .none
        //*********for paging api call
        if (tblTraveller.contentOffset.y + tblTraveller.frame.size.height) >= (tblTraveller.contentSize.height - tblTraveller.frame.size.height) && self.maxResults > 0
        {
            self.setupActivityIndicatorInTableViewFooter()
            self.handlePagingForEverywhereTravellerTable()
        }
        
        return cell
    }
    
    //MARK:-- Cell Rendom Color
    func setTravelerUnloadImageColor(index: Int , cell: CYLTravellerListCell)
    {
        if index % 2 == 0 && index % 4 != 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
                
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
            }
        }
        else if index % 3 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 16/255.0, green: 118/255.0, blue: 182/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            }
        }
        else if index % 4 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
            }
        }
        else if index % 5 == 0
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 98/255.0, green: 96/255.0, blue: 68/255.0, alpha: 1.0)
            }
        }
        else
        {
            if #available(iOS 10.0, *) {
                cell.imgUserFirst.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
            } else {
                cell.imgUserFirst.backgroundColor = UIColor(red: 152/255.0, green: 147/255.0, blue: 140/255.0, alpha: 1.0)
                cell.imgUserSecond.backgroundColor = UIColor(red: 194/255.0, green: 218/255.0, blue: 224/255.0, alpha: 1.0)
            }
        }
    }
    
}
extension CYLEverywhereVC:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}
extension CYLEverywhereVC
{
    func refreshViewSlowOrMantinence(msgStr: String , imgName:String)
    {
        if vwEmptyState.frame.size.width == 0
        {
            vwEmptyState = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height))
            vwEmptyState.backgroundColor = UIColor.white
            self.view.addSubview(vwEmptyState)
            let img = UIImageView(frame: CGRect(x: vwEmptyState.frame.width/2-60, y: vwEmptyState.frame.height/2-160, width: 120, height: 120))
            img.image = UIImage(named: imgName)
            
            let lblMsg = UILabel(frame: CGRect(x: 40, y: img.frame.origin.y+img.frame.size.height+20, width: self.view.frame.size.width-80, height: 21))
            lblMsg.font = UIFont(name: "Comfortaa-Bold", size: 20)
            lblMsg.textAlignment = .center
            lblMsg.text = "Oops!"
            
            let lblAlertText = UILabel(frame: CGRect(x: 30, y: lblMsg.frame.origin.y+lblMsg.frame.size.height+15, width: self.view.frame.size.width-60, height: 40))
            lblAlertText.numberOfLines = 0
            lblAlertText.textAlignment = .center
            lblAlertText.text = msgStr
            lblAlertText.adjustsFontSizeToFitWidth = true
            lblAlertText.font = UIFont(name: "Comfortaa-Bold", size: 15)
            
            let btnRefresh: UIButton = UIButton(frame: CGRect(x: vwEmptyState.frame.width/2-100, y: lblAlertText.frame.origin.y+lblAlertText.frame.size.height+35, width: 200, height: 45))
            btnRefresh.addTarget(self, action: #selector(btnRefreshAction), for: .touchUpInside)
            btnRefresh.titleLabel?.textAlignment = .center
            btnRefresh.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 15)
            btnRefresh.titleLabel?.textColor = UIColor.white
            btnRefresh.setTitle("Retry", for: UIControlState.normal)
            btnRefresh.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
            btnRefresh.layer.cornerRadius = 23
            btnRefresh.layer.masksToBounds = true
            
            vwEmptyState.addSubview(img)
            vwEmptyState.addSubview(lblMsg)
            vwEmptyState.addSubview(lblAlertText)
            vwEmptyState.addSubview(btnRefresh)
            vwEmptyState.isHidden = false
        }
        else
        {
            vwEmptyState.isHidden = false
        }
    }
    @objc func btnRefreshAction(sender: UIButton)
    {
        if checkInternetConnection()
        {
            self.requestToGetTravellersList(page: 0, str: "")
            vwEmptyState.isHidden = true
        }
    }
}

