//
//  CYLUserProfileVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/10/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
//import UITextView
import INSPhotoGallery

protocol CYLdelegateBlockedUser {
    func blockUserCheck()
}

class CYLUserProfileVC: CYLBaseViewController,UIScrollViewDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UINavigationControllerDelegate
{
   
    @IBOutlet weak var actIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var lblDestinationPlaceholder: UILabel!
    @IBOutlet var constTblH:NSLayoutConstraint!
    @IBOutlet weak var tblAboutMe: UITableView!
    @IBOutlet weak var vwProfileImg: UIView!
    @IBOutlet weak var lblNoDates: UILabel!
    @IBOutlet weak var lblNoDestination: UILabel!
    @IBOutlet weak var btnToReportBlock: UIButton!
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var scrlProfileImages: UIScrollView!
    @IBOutlet weak var collectionLocation: UICollectionView!
    @IBOutlet weak var constLocationH: NSLayoutConstraint!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblCurrentLocation: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var scrlProfile: UIScrollView!
    @IBOutlet weak var imgOnlineUser: UIImageView!
    @IBOutlet weak var lblGender: UILabel!
    var travellerId:Int?
    var navigationOverlay:UIView?
    var aryLocation = [JSON]()
    var aryImages = [JSON]()
    var aryPreviewImages = [URL]()
    var isBlocked = false
    var isMessageDisabled = false
    var strGender = ""
    var strChatId = ""
    var heightCityCountryList: CGFloat?
    var vwLoader: UIView = UIView()
    //for about us table
    let aryAboutTitle = ["What I'm doing with my life","My past traveling experiences","My interests","About my next trip","I'm looking for"]
    let aryAboutIcon = [#imageLiteral(resourceName: "ic_about_one"),#imageLiteral(resourceName: "ic_about_two"),#imageLiteral(resourceName: "ic_about_three"),#imageLiteral(resourceName: "ic_about_fourth"),#imageLiteral(resourceName: "ic_about_fifth")]
    var aryAboutUs = [[String:AnyObject]]()
    var delegateBlockedUser: CYLdelegateBlockedUser?
    var galleryPreview = INSPhotosViewController()
    var aryPhoto : [INSPhotoViewable] = [INSPhotoViewable]()
    var vwEmptyState: UIView = UIView()
    var travellerDetailDict:[String:JSON] = [:]
    
    var isMessagePopupLimitExced: Bool = Bool()
    var isMessageLimitExcedMessage: String = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loaderGifImage()
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.clear], for: .normal)
        self.navigationController?.delegate = self
        self.scrlProfile.isHidden=true
        self.requestToGetTravellerProfile()
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Other user profile")
        setStatusBarBackgroundColor(color: UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0))
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.navigationBar.isHidden=true
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationOverlay?.backgroundColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isHidden=false
        CYLDBRefProvider.sharedInstance.chatDetailReference.removeAllObservers()
    }
    func loaderGifImage()
    {
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwLoader = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwLoader.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
            window.addSubview(vwLoader)
            let img = UIImageView(frame: CGRect(x: vwLoader.frame.width/2-40, y: vwLoader.frame.height/2-40, width: 80, height: 80))
            img.image = UIImage.sd_animatedGIF(with: imageData!)
            vwLoader.addSubview(img)
            vwLoader.isHidden = true
        }
    }
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    @IBAction func btnToNewBackClicked(_ sender: Any)
    {
        self.btnToBackClicked()
    }
    @IBAction func btnToReportBlockClicked(_ sender: Any)
    {
        self.showActionSheetForReportBlockConfirmation()
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: UserDefined functions functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func btnToBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    func setDataOnUserProfileScreen(object:JSON)
    {
        self.strChatId = object["chatId"].stringValue
        if object["age"].exists() && object["age"].string != nil && object["age"].string != ""
        {
            self.lblUserName.text = object["userName"].stringValue + ", " + object["age"].stringValue
        }
        else
        {
            self.lblUserName.text = object["userName"].stringValue
        }
        var country: String = String()
        var city: String = String()
        if object["cityv2"].exists() || object["country"].exists()
        {
            if object["cityv2"].exists()
            {
                city = (object["cityv2"].string != nil) ? object["cityv2"].stringValue : ""
            }
            if object["country"].exists()
            {
                country = (object["country"].string != nil) ? object["country"].stringValue : ""
            }
            if city == ""
            {
                self.lblCurrentLocation.text = country
            }
            else
            {
                self.lblCurrentLocation.text = (country == "") ? object["cityv2"].stringValue : object["cityv2"].stringValue + ", " + country
            }
        }
        self.lblCurrentLocation.adjustsFontSizeToFitWidth = true
        self.lblStartDate.text = object["startDate"].stringValue
        self.lblEndDate.text = object["endDate"].stringValue
        self.lblNoDates.isHidden = (self.lblStartDate.text == "") ? false : true
        self.lblGender.text = (object["gender"].intValue) == 1 ? "Female":"Male"
        self.constTblH.constant = 0
        if object["updatedAboutMeV2"].dictionary != nil
        {
            let dict = object["updatedAboutMeV2"]
            var tblHeight: CGFloat = CGFloat()
            if (dict["questionOne"].stringValue).trimmingCharacters(in: .whitespaces) != ""
            {
                var object = [String:AnyObject]()
                object["detail"] = dict["questionOne"].stringValue as AnyObject?
                object["logo"] = self.aryAboutIcon[0]
                object["title"] = self.aryAboutTitle[0] as AnyObject?
                self.aryAboutUs.append(object)
                tblHeight += (64+self.heightForInputString(text: dict["questionOne"].stringValue))
            }
            if (dict["questionTwo"].stringValue).trimmingCharacters(in: .whitespaces) != ""
            {
                var object = [String:AnyObject]()
                object["detail"] = dict["questionTwo"].stringValue as AnyObject?
                object["logo"] = self.aryAboutIcon[1]
                object["title"] = self.aryAboutTitle[1] as AnyObject?
                self.aryAboutUs.append(object)
                tblHeight += (64+self.heightForInputString(text: dict["questionTwo"].stringValue))
            }
            if (dict["questionThree"].stringValue).trimmingCharacters(in: .whitespaces) != ""
            {
                var object = [String:AnyObject]()
                object["detail"] = dict["questionThree"].stringValue as AnyObject?
                object["logo"] = self.aryAboutIcon[2]
                object["title"] = self.aryAboutTitle[2] as AnyObject?
                self.aryAboutUs.append(object)
                tblHeight += (64+self.heightForInputString(text: dict["questionThree"].stringValue))
            }
            if (dict["questionFour"].stringValue).trimmingCharacters(in: .whitespaces) != ""
            {
                var object = [String:AnyObject]()
                object["detail"] = dict["questionFour"].stringValue as AnyObject?
                object["logo"] = self.aryAboutIcon[3]
                object["title"] = self.aryAboutTitle[3] as AnyObject?
                self.aryAboutUs.append(object)
                tblHeight += (64+self.heightForInputString(text: dict["questionFour"].stringValue))
            }
            if (dict["questionFive"].stringValue).trimmingCharacters(in: .whitespaces) != ""
            {
                var object = [String:AnyObject]()
                object["detail"] = dict["questionFive"].stringValue as AnyObject?
                object["logo"] = self.aryAboutIcon[4]
                object["title"] = self.aryAboutTitle[4] as AnyObject?
                self.aryAboutUs.append(object)
                tblHeight += (64+self.heightForInputString(text: dict["questionFive"].stringValue))
            }
            self.constTblH.constant = tblHeight
            
        }
        else
        {
            var aboutMeJSON :JSON!
            if let aboutData = (object["updatedAboutMe"].stringValue).data(using: String.Encoding.utf8)
            {
                aboutMeJSON = JSON(data: aboutData)
            }
            for i in 0..<aboutMeJSON.count where aboutMeJSON[i].stringValue != ""
            {
                var object = [String:AnyObject]()
                object["detail"] = aboutMeJSON[i].stringValue as AnyObject?
                object["logo"] = self.aryAboutIcon[i]
                object["title"] = self.aryAboutTitle[i] as AnyObject?
                self.aryAboutUs.append(object)
                self.constTblH.constant += (64+self.heightForInputString(text: aboutMeJSON[i].stringValue))
            }
            if aboutMeJSON.count == 0
            {
                self.constTblH.constant = 0
            }
        }
        
        self.tblAboutMe.reloadData()
        self.aryImages = object["userImages"].arrayValue
        self.strGender = "\(object["gender"].intValue)"
        self.setProfileImagesInScrollView()
        self.lblDestinationPlaceholder.text = ((self.aryLocation.count>1 || self.lblStartDate.text == "Flexible")) ? "Wants to visit" : "Traveling to"
        let destinationType = ((object["cities"].arrayValue).count > 0) ? "City" : ((object["countries"].arrayValue).count > 0) ? "Country" : "No Plan"
        let g = (object["gender"].intValue == 0) ? "Male" : "Female"
        sentEventPropertyOnCleverTap("Other user profile detail", _property: ["UserId": "\(travellerId!)","UserName": "\(object["userName"].stringValue)","Age": "\(object["age"].stringValue)","IsoLocation":"\(object["isoLocation"].stringValue)","Gender": "\(g)","IsMatched":"\(object["is_matched"].boolValue)","DestinationsCount":self.aryLocation.count,"DestinationType":"\(destinationType)","isOnline":"\(object["showOnline"].boolValue)","LivesInDestinationCountry":"\(object["livesInDestinationCountry"].boolValue)"])
        
        
    }
    
    func setProfileImagesInScrollView()
    {
        for i in 0..<self.aryImages.count
        {
            let originX = CGFloat(i)*self.screenWidth!
            let imgView = UIImageView.init(frame: CGRect(x:originX,y:0,width:self.screenWidth!,height:self.scrlProfileImages.frame.size.height))
            imgView.contentMode = .scaleAspectFit
            
            let pictureObject = self.aryImages[i]
            
            if pictureObject["picUrl"].stringValue != ""
            {
                imgView.setShowActivityIndicator(true)
                imgView.setIndicatorStyle(.whiteLarge)
                imgView.sd_setImage(with: URL(string: (pictureObject["picUrl"].stringValue)), placeholderImage: UIImage(named: ""), options: [], completed: { (loadedImage, error, cacheType, url) in

                    imgView.sd_cancelCurrentImageLoad()
                    if error != nil {
                        print("Error code: \(error!.localizedDescription)")
                    } else {
                        imgView.image = loadedImage
                    }
                })
            }
//            imgView.sd_setImage(with: URL(string: (pictureObject["picUrl"].stringValue)), placeholderImage: UIImage(named: "user_placeholder"))
            imgView.clipsToBounds=true
            self.scrlProfileImages.addSubview(imgView)
            
            let previewButton = UIButton()
            previewButton.frame = imgView.frame
            previewButton.setTitle("", for: .normal)
            previewButton.tag = i+1000
            previewButton.addTarget(self, action: #selector(buttonToImagePreviewClicked(sender:)), for: .touchUpInside)
            self.scrlProfileImages.addSubview(previewButton)
            
            self.aryPreviewImages.append(URL.init(string: pictureObject["picUrl"].stringValue)!)
        }
        self.scrlProfileImages.contentSize = CGSize(width:CGFloat(self.aryImages.count)*self.screenWidth!,height:self.scrlProfileImages.frame.size.height)
        
        
        for item in self.aryImages
        {
            aryPhoto.append(INSPhoto(imageURL: URL(string: item["picUrl"].stringValue), thumbnailImage: UIImage(named: "")))
        }
        for i in 0..<aryPhoto.count
        {
            let currentPhoto = aryPhoto[i]
            galleryPreview = INSPhotosViewController(photos: aryPhoto, initialPhoto: currentPhoto, referenceView: self.view)
        }
        self.pgControl.numberOfPages = self.aryImages.count
    }
    func showActionSheetForReportBlockConfirmation()
    {
        //let alert = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        let alert = UIAlertController.init()
        //alert.preferredStyle = .actionSheet
        let reportAction = UIAlertAction.init(title: "Report user", style: .destructive, handler:
        {
            (sender) in
            let reportUserVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLReportUserVC") as! CYLReportUserVC
            reportUserVC.travellerId = self.travellerId
            reportUserVC.travellerDetail = self.travellerDetailDict
            self.navigationController?.pushViewController(reportUserVC, animated: true)
            
        })
        let blockAction = UIAlertAction.init(title: "Block user", style: .destructive, handler: {
            (sender) in
            self.showAlertForBlockUser()
            
        })
        let unblockAction = UIAlertAction.init(title: "Unblock user", style: .default, handler: {
            (sender) in
            self.showAlertForUnBlockUser()
            
        })
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler:
        {
            (sender) in
            
        })
        alert.addAction(reportAction)
        if(self.isBlocked)
        {
            alert.addAction(unblockAction)
        }
        else
        {
            alert.addAction(blockAction)
        }
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    func showAlertForBlockUser()
    {
        let text = showAlertTitleMessageFont(strTitle: "Block user", strMessage: "Are you sure want to block this user? They will no longer be able to contact you.")
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        alert.setValue(text.title, forKey: "attributedTitle")
        alert.setValue(text.message, forKey: "attributedMessage")
        let blockAction = UIAlertAction.init(title: "Block", style: .destructive, handler: {
            (sender) in
            self.requestToBlockUser()
            
        })
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .default, handler:
        {
            (sender) in
            
        })
        
        alert.addAction(cancelAction)
        alert.addAction(blockAction)
        self.present(alert, animated: true, completion: nil)
    }
    func showAlertForUnBlockUser()
    {
        let text = showAlertTitleMessageFont(strTitle: "Unblock user", strMessage: "Are you sure want to unblock this user?")
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        alert.setValue(text.title, forKey: "attributedTitle")
        alert.setValue(text.message, forKey: "attributedMessage")
        let blockAction = UIAlertAction.init(title: "Unblock", style: .default, handler: {
            (sender) in
            self.requestToBlockUser()
            
        })
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .destructive, handler:
        {
            (sender) in
            
        })
        alert.addAction(cancelAction)
        alert.addAction(blockAction)
        self.present(alert, animated: true, completion: nil)
    }
    func isNotificationEnabledFromSettings()->Bool
    {
        let notificationType = UIApplication.shared.currentUserNotificationSettings?.types
        if notificationType?.rawValue == 0
        {
            print("Disabled")
            return false
        } else {
            print("Enabled")
            return true
        }
    }
    func heightForInputString(text:String) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:self.screenWidth!-32, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byCharWrapping
        label.font = UIFont(name: "Comfortaa-Bold", size: 13)!
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom Button actions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToSendMessageClicked(_ sender: Any)
    {
        var boolPushOrPopViewController: Bool = Bool()
        for controller in (self.navigationController?.viewControllers)!
        {
            if controller.isKind(of: CYLChatDetailVC.self)
            {
                boolPushOrPopViewController = true
            }
        }
        self.moveUserProfilePushOrPopViewController(bool: boolPushOrPopViewController)
    }
    
    func moveUserProfilePushOrPopViewController(bool: Bool)
    {
        if bool
        {
            _ = self.navigationController?.popViewController(animated: true)
        }
        else
        {
            if self.isMessageDisabled == false
            {
                self.actIndicator.startAnimating()
                self.btnSendMessage.isUserInteractionEnabled=false
                let chatId = "cht_id_\(CYLGlobals.sharedInstance.usrObject!.id!)_\(self.travellerId!)"
                let chatIdArr = chatId.components(separatedBy: "_")
                let chatIdRev: String = "\(chatIdArr[0])_\(chatIdArr[1])_\(chatIdArr[3])_\(chatIdArr[2])"
                CYLDBRefProvider.sharedInstance.chatDetailReference.child(chatId).observeSingleEvent(of: .value, with: { (snapshot)  in
                    
                    if snapshot.exists()
                    {
                        self.strChatId = chatId
                        self.moveChatDetailVC()
                    }
                    else
                    {
                        CYLDBRefProvider.sharedInstance.chatDetailReference.child(chatIdRev).observeSingleEvent(of: .value, with: { (snapshot)  in
                            
                            if snapshot.exists()
                            {
                                self.strChatId = chatIdRev
                            }
                            self.moveChatDetailVC()
                        })
                    }
                })
                
            }
            else if self.isBlocked
            {
                showAlertView(title: "", message: "You have blocked this user. Please visit their profile and unblock them, from the top right menu option, to continue sending messages.", ref: self)
            }
            else
            {
                showAlertView(title: "", message: "You have been blocked by this user.", ref: self)
            }
        }
    }
    
    
    func moveChatDetailVC()
    {
        self.actIndicator.stopAnimating()
        self.btnSendMessage.isUserInteractionEnabled=true
        let chatDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLChatDetailVC") as! CYLChatDetailVC
        chatDetailVC.userName = self.lblUserName.text?.components(separatedBy: ", ").first
        chatDetailVC.chatId = self.strChatId
        chatDetailVC.imgUrl = (self.aryImages.count > 0) ? self.aryImages[0]["picUrl"].stringValue : ""
        chatDetailVC.receiverId = travellerId!
        chatDetailVC.messageDelegate = (self.strChatId == "") ? self : nil
        chatDetailVC.isMessageLimitExcedMessage = self.isMessageLimitExcedMessage
        chatDetailVC.isMessagePopupLimitExced = self.isMessagePopupLimitExced
        self.navigationController?.pushViewController(chatDetailVC, animated: true)
    }
    @objc func buttonToImagePreviewClicked(sender:UIButton)
    {
        let currentPhoto = aryPhoto[sender.tag - 1000]
        galleryPreview = INSPhotosViewController(photos: aryPhoto, initialPhoto: currentPhoto, referenceView: self.view)
        present(galleryPreview, animated: true, completion: nil)
    }
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Scroll view delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == self.scrlProfileImages
        {
            let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
            self.pgControl.currentPage = Int(pageNumber)
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        //      let yOffset = self.scrlProfile.contentOffset.y * 0.2
        //      let availableOffset = min(yOffset, 60)
        //      let contentRectYOffset = availableOffset / self.vwProfileImg.frame.size.height
        //        self.vwProfileImg.layer.contentsRect = CGRect(x:0.0, y:contentRectYOffset, width:1, height:1)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Collection view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aryLocation.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellLocation = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCountryCollectionCell", for: indexPath as IndexPath) as! CityCountryCollectionCell
        cellLocation.layer.borderWidth = 1.0
        cellLocation.layer.borderColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0).cgColor
        cellLocation.layer.cornerRadius = 15
        cellLocation.clipsToBounds = true
        cellLocation.imgLocation.layer.cornerRadius = 10
        cellLocation.imgLocation.clipsToBounds = true
        cellLocation.object = CYLLocationObject.init(json: self.aryLocation[indexPath.row])
        
        heightCityCountryList = cellLocation.frame.origin.y+cellLocation.frame.height+10
        
        if indexPath.row == aryLocation.count-1
        {
            self.constLocationH.constant = heightCityCountryList ?? 0
        }

        return cellLocation
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var widht:CGFloat = CGFloat()
        
        let object:CYLLocationObject = CYLLocationObject.init(json: self.aryLocation[indexPath.row])
        
        if object.cityName == ""
        {
            widht = widthForInputString(text: (object.countryName)!)
        }
        else
        {
            widht = widthForInputString(text: (object.cityName)! + ", " + (object.countryName)!)
        }
        let wi = widht + 40
        return CGSize(width: wi, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Table view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return aryAboutUs.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CYLAboutMeCell") as! CYLAboutMeCell
            cell.imgLogo.image = self.aryAboutUs[indexPath.row]["logo"] as? UIImage
            cell.lblTitle.text = self.aryAboutUs[indexPath.row]["title"] as? String
            cell.lblDescription.text = self.aryAboutUs[indexPath.row]["detail"] as? String
            cell.lblDescription.sizeToFit()
            //cell.object = CYLLocationObject.init(json: self.aryLocation[indexPath.row])
            cell.selectionStyle = .none
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToGetTravellerProfile()
    {
        self.vwLoader.isHidden = false
        let params:[String: Any] = ["userId":"\(travellerId!)"]
        let request = CYLServices.getOtherUserProfile(dict: params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                self.vwLoader.isHidden = true
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData!["status"]?.stringValue != "fail"
                    {
                        let aryCities = responseData?["data"]!["cities"].arrayValue
                        let aryCountries = responseData?["data"]!["countries"].arrayValue
                        self.aryLocation = aryCities! + aryCountries!
                        self.constLocationH.constant = (self.aryLocation.count>0) ? CGFloat((self.aryLocation.count)*40) : 40
                        self.lblNoDestination.isHidden = (self.aryLocation.count>0) ? true :  false
                        self.setDataOnUserProfileScreen(object:(responseData?["data"]!)!)
                        self.travellerDetailDict = (responseData?["data"]?.dictionaryValue)!
                        self.collectionLocation.reloadData()
                        self.isBlocked = (responseData?["data"]!["blocked"].boolValue)!
                        self.isMessageDisabled = (responseData?["data"]!["messageDisable"].boolValue)!
                        self.imgOnlineUser.image = ((responseData?["data"]!["showOnline"].boolValue)! == false) ? UIImage(named: "") : UIImage(named: "ic_online")
                        self.scrlProfile.isHidden=false
                        
                        if (responseData?["data"]!["isMessageLimitExceed"].exists())!
                        {
                            self.isMessageLimitExcedMessage = (responseData?["data"]!["messageLimitExceedMessage"].stringValue)!
                            if responseData?["data"]!["isMessageLimitExceed"].boolValue == true
                            {
                                self.isMessagePopupLimitExced = true
                            }
                            else
                            {
                                self.isMessagePopupLimitExced = false
                            }
                        }
                        
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Other user profile"])
                    self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
                }
                
        },
                                                                  withError:
            {
                (error) in
                self.vwLoader.isHidden = true
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Other user profile"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                self.vwLoader.isHidden = true
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Other user profile"])
                self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
        })
    }
    func requestToBlockUser()
    {
        self.vwLoader.isHidden = false
        let params:[String: Any] = ["blockedUserId":"\(travellerId!)","is_block":"\(!self.isBlocked)"]
        let request = CYLServices.postBlockUser(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                self.vwLoader.isHidden = true
                if (response?.isValid)!
                {
                    //let responseData = response?.object?.dictionaryValue
                    self.isBlocked = !self.isBlocked
                    self.delegateBlockedUser?.blockUserCheck()
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Block user"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }

        },
                                                                  withError:
            {
                (error) in
                self.vwLoader.isHidden = true
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Block user"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                self.vwLoader.isHidden = true
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Block user"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    
}

extension CYLUserProfileVC:CYLMessageDelegate
{
    func delegateToGetUpdatedChatId(strChatId:String)
    {
        self.strChatId = strChatId
    }
}
extension CYLUserProfileVC
{
    
    func refreshViewSlowOrMantinence(msgStr: String , imgName:String)
    {
        if vwEmptyState.frame.size.width == 0
        {
            vwEmptyState = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height))
            vwEmptyState.backgroundColor = UIColor.white
            self.view.addSubview(vwEmptyState)
            let img = UIImageView(frame: CGRect(x: vwEmptyState.frame.width/2-60, y: vwEmptyState.frame.height/2-160, width: 120, height: 120))
            img.image = UIImage(named: imgName)
            
            let lblMsg = UILabel(frame: CGRect(x: 40, y: img.frame.origin.y+img.frame.size.height+20, width: self.view.frame.size.width-80, height: 21))
            lblMsg.font = UIFont(name: "Comfortaa-Bold", size: 20)
            lblMsg.textAlignment = .center
            lblMsg.text = "Oops!"
            
            let lblAlertText = UILabel(frame: CGRect(x: 60, y: lblMsg.frame.origin.y+lblMsg.frame.size.height+15, width: self.view.frame.size.width-120, height: 40))
            lblAlertText.numberOfLines = 0
            lblAlertText.textAlignment = .center
            lblAlertText.text = msgStr
            lblAlertText.adjustsFontSizeToFitWidth = true
            lblAlertText.font = UIFont(name: "Comfortaa-Bold", size: 15)
            
            let btnRefresh: UIButton = UIButton(frame: CGRect(x: vwEmptyState.frame.width/2-100, y: lblAlertText.frame.origin.y+lblAlertText.frame.size.height+35, width: 200, height: 45))
            btnRefresh.addTarget(self, action: #selector(btnRefreshAction), for: .touchUpInside)
            btnRefresh.titleLabel?.textAlignment = .center
            btnRefresh.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 15)
            btnRefresh.titleLabel?.textColor = UIColor.white
            btnRefresh.setTitle("Retry", for: UIControlState.normal)
            btnRefresh.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
            btnRefresh.layer.cornerRadius = 23
            btnRefresh.layer.masksToBounds = true
            
            vwEmptyState.addSubview(img)
            vwEmptyState.addSubview(lblMsg)
            vwEmptyState.addSubview(lblAlertText)
            vwEmptyState.addSubview(btnRefresh)
            vwEmptyState.isHidden = false
        }
        else
        {
            vwEmptyState.isHidden = false
        }
    }
    @objc func btnRefreshAction(sender: UIButton)
    {
        if checkInternetConnection()
        {
            self.requestToGetTravellerProfile()
            vwEmptyState.isHidden = true
        }
    }
    
}

