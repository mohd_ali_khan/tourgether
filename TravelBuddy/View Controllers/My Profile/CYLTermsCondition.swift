//
//  CYLTermsCondition.swift
//  TravelBuddy
//
//  Created by CodeYeti on 6/13/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLTermsCondition: UIViewController
{

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var urlStr: String = String()
    var titleStr: String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        lblTitle.text = titleStr
        webView.loadRequest(URLRequest(url: URL(string: urlStr)!))
    }
    @IBAction func btnDismissAction(_ sender: UIButton) {
        self.navigationController?.navigationBar.isHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
