//
//  CYLSettingsVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/12/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLSettingsVC: CYLBaseViewController,UIGestureRecognizerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var swtMessageNotyfn: UISwitch!
    @IBOutlet weak var swtMatchNotfyn: UISwitch!
    @IBOutlet weak var swtCleverTapEmail: UISwitch!
    @IBOutlet weak var swtCleverTapNotificaton: UISwitch!
    @IBOutlet weak var swtWaveTapNotificaton: UISwitch!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "User settings")
        self.navigationController?.delegate = self
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.clear], for: .normal)
        self.createSettingScreenDesign()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createSettingScreenDesign()
    {
    let transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
    self.swtMessageNotyfn.transform = transform
    self.swtMatchNotfyn.transform = transform
    self.swtCleverTapNotificaton.transform = transform
    self.swtCleverTapEmail.transform = transform
    self.swtWaveTapNotificaton.transform = transform
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
    self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
    self.title = "Settings"
    let userObject = CYLGlobals.sharedInstance.usrObject!
    self.swtMessageNotyfn.isOn = (userObject.newMessageNotify == 1) ? true : false
    self.swtMatchNotfyn.isOn = (userObject.isMatchNotification == 1) ? true : false
    self.swtCleverTapEmail.isOn = (userObject.isEmailNotification == 1) ? true : false
    self.swtCleverTapNotificaton.isOn = (userObject.isOtherNotifications == 1) ? true : false
    self.swtWaveTapNotificaton.isOn = (userObject.isWaveNotifications == 1) ? true : false
    }
    @objc func btnToBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    func doConfirmationForAccountDeactivation()
    {
        let text = showAlertTitleMessageFont(strTitle: "Confirm", strMessage: "Are you sure you want to delete your account ?")
            let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        
        alert.setValue(text.title, forKey: "attributedTitle")
        alert.setValue(text.message, forKey: "attributedMessage")
            let yesAction = UIAlertAction.init(title: "YES", style: .default, handler: {
                (sender) in
                self.requestToDeleteAndLogoutUserAccount(type: "delete")
            })
            let noAction = UIAlertAction.init(title: "NO", style: .destructive, handler:
            {
                (sender) in
            })
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
    }
    func doConfirmationForAppLogout()
    {
        let text = showAlertTitleMessageFont(strTitle: "Confirm", strMessage: "Are you sure you want to logout ?")
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        alert.setValue(text.title, forKey: "attributedTitle")
        alert.setValue(text.message, forKey: "attributedMessage")
        let yesAction = UIAlertAction.init(title: "YES", style: .default, handler: {
            (sender) in
            self.requestToDeleteAndLogoutUserAccount(type: "logout")
        })
        let noAction = UIAlertAction.init(title: "NO", style: .destructive, handler:
        {
            (sender) in
        })
        alert.addAction(noAction)
        alert.addAction(yesAction)
       
        self.present(alert, animated: true, completion: nil)
        
    }
    func doLogout()
    {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "userObject")
        userDefaults.removeObject(forKey: "travel_filter")
        userDefaults.removeObject(forKey: "date")
        userDefaults.removeObject(forKey: "rating_count")
        userDefaults.removeObject(forKey: "messageWave_count")
        userDefaults.synchronize()
        CYLFacebookHelper.sharedFInstance().doLogoutFromFacebook()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeRootToLoginVC()
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom button actions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToLogoutClicked(_ sender: Any)
    {
        self.doConfirmationForAppLogout()
    }
    @IBAction func btnToDeleteAccountClicked(_ sender: Any)
    {
        self.doConfirmationForAccountDeactivation()
    }
    @IBAction func swtToEnableMessageClicked(_ sender: Any)
    {
         self.requestToUpdateUserNotificationPreferences(notyfnTypeMatch: false)
    }
    @IBAction func swtToEnableMatchClicked(_ sender: Any)
    {
        self.requestToUpdateUserNotificationPreferences(notyfnTypeMatch: true)
    }
    @IBAction func swtToEnableClevertapEmail(_ sender: Any)
    {
        self.requestToUpdateUserNotificationPreferences(notyfnTypeMatch: true)
    }
    @IBAction func swtToEnableClevertapNotification(_ sender: Any)
    {
        self.requestToUpdateUserNotificationPreferences(notyfnTypeMatch: true)
    }
    @IBAction func swtToEnableWaveNotification(_ sender: Any)
    {
        self.requestToUpdateUserNotificationPreferences(notyfnTypeMatch: true)
    }
    @IBAction func btnTermsPrivacyPolicy(_ sender: UIButton)
    {
        if sender.tag == 1001
        {
            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLTermsCondition") as! CYLTermsCondition
            termsVC.titleStr = "Privacy Policy"
            termsVC.urlStr = "https://www.tourgetherapp.com/privacy-policy.html"
            self.navigationController?.present(termsVC, animated: true, completion: nil)
        }
        else if sender.tag == 1002
        {
            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLTermsCondition") as! CYLTermsCondition
            termsVC.urlStr = "https://www.tourgetherapp.com/terms-services.html"
            termsVC.titleStr = "Terms of Services"
            self.navigationController?.present(termsVC, animated: true, completion: nil)
        }
        else
        {
            let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLLicencesVC") as! CYLLicencesVC
            self.navigationController?.pushViewController(settingVC, animated: true)
        }
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToDeleteAndLogoutUserAccount(type: String)
    {
        KVNProgress.show()
//        let params:[String: Any] = ["userId":"\(CYLGlobals.sharedInstance.usrObject!.id!)"]
        let params:[String: Any] = [:]
        var request:CYLRequestObject!
        if type == "logout"
        {
            request = CYLServices.logoutUserAccount(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        }
        else
        {
            request = CYLServices.deactivateUserAccount(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        }
        
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        if type == "logout"
                        {
                            sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Logout")
                        }
                        else
                        {
                            sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Delete account")
                        }
                        self.doLogout()
                    }
                    
                }
                else
                {
                    if type == "logout"
                    {
                        sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Logout"])
                    }
                    else
                    {
                        sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Delete account"])
                    }
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                if type == "logout"
                {
                    sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Logout"])
                }
                else
                {
                    sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Delete account"])
                }
                
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                if type == "logout"
                {
                    sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Logout"])
                }
                else
                {
                    sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Delete account"])
                }
                
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    func requestToUpdateUserNotificationPreferences(notyfnTypeMatch:Bool)
    {
        ////KVNProgress.show()
        let params:[String: Any] = ["isNotify":"\(NSNumber.init(value: self.swtMessageNotyfn.isOn))","isMatchNotify":"\(NSNumber.init(value: self.swtMatchNotfyn.isOn))","isSubscribe":self.swtCleverTapEmail.isOn,"isOtherNotifications":self.swtCleverTapNotificaton.isOn,"isWaveNotify":self.swtWaveTapNotificaton.isOn]
        let request = CYLServices.updateUserNotificationPreference(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                ////KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    //let responseData = response?.object?.dictionaryValue
                    let userObject = CYLGlobals.sharedInstance.usrObject!
                    userObject.newMessageNotify = (self.swtMessageNotyfn.isOn == true) ? 1 : 0
                    userObject.isMatchNotification = (self.swtMatchNotfyn.isOn == true) ? 1 : 0
                    userObject.isEmailNotification = (self.swtCleverTapEmail.isOn == true) ? 1 : 0
                    userObject.isOtherNotifications = (self.swtCleverTapNotificaton.isOn == true) ? 1 : 0
                    userObject.isWaveNotifications = (self.swtWaveTapNotificaton.isOn == true) ? 1 : 0
                    CYLGlobals.sharedInstance.usrObject = userObject
                    let userDeflts = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                    userDeflts.set(encodedData, forKey: "userObject")
                    userDeflts.synchronize()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification_setting_updated"), object: nil)
                    
                    let email = self.swtCleverTapEmail.isOn
                    let push = self.swtCleverTapNotificaton.isOn
                    let profile: Dictionary<String, Any> = [
                        "Identity": CYLGlobals.sharedInstance.usrObject!.id!,
                        "MSG-email": email,
                        "MSG-push": push,
                        "MSG-sms": false
                    ]
                    let strIsoCode = (CYLGlobals.sharedInstance.usrObject?.isoLocation != nil) ? CYLGlobals.sharedInstance.usrObject?.isoLocation : ""
                    CleverTap.sharedInstance()?.profileAddMultiValue(strIsoCode!, forKey: "Other Details")
                    CleverTap.sharedInstance()?.profilePush(profile)
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"User settings"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                ////KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"User settings"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                ////KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"User settings"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }



}
