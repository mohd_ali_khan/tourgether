//
//  CYLAboutMe.swift
//  TravelBuddy
//
//  Created by CodeYeti on 7/7/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
//import UITextView

class CYLAboutMe: UIViewController,UITextViewDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate {
    @IBOutlet weak var txtAboutFifth: UITextView!
    @IBOutlet weak var txtAboutFourth: UITextView!
    @IBOutlet weak var txtAboutThird: UITextView!
    @IBOutlet weak var txtAboutSecond: UITextView!
    @IBOutlet weak var txtAboutFirst: UITextView!
    @IBOutlet weak var constHeightFirstCome: NSLayoutConstraint!
    @IBOutlet weak var constHeightContinueButton: NSLayoutConstraint!
    
    var isDoneEnabled=false
    var dictAboutMe = [String: JSON]()
    var checkLoginFlow: String = String()
    var appObject:AppDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.appObject = UIApplication.shared.delegate as? AppDelegate
        self.createAboutMeScreenDesign()
        self.setPrefilledDataOnAboutMeScreen()
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "About me user")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        isDoneEnabled = true
        return true
    }
    func createAboutMeScreenDesign()
    {
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.clear], for: .normal)
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        if checkLoginFlow == ""
        {
            self.constHeightFirstCome.constant = 0
            self.constHeightContinueButton.constant = 0
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "save"), style: .plain, target: self, action: #selector(self.btnToDoneClicked))
        }
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "About Me"
    }
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    func setPrefilledDataOnAboutMeScreen()
    {
        if dictAboutMe.count > 0
        {
            let textField1 = self.view.viewWithTag(1001) as! UITextView
            textField1.text = dictAboutMe["questionOne"]?.string
            
            let textField2 = self.view.viewWithTag(1002) as! UITextView
            textField2.text = dictAboutMe["questionTwo"]?.string
            
            let textField3 = self.view.viewWithTag(1003) as! UITextView
            textField3.text = dictAboutMe["questionThree"]?.string
            
            let textField4 = self.view.viewWithTag(1004) as! UITextView
            textField4.text = dictAboutMe["questionFour"]?.string
            
            let textField5 = self.view.viewWithTag(1005) as! UITextView
            textField5.text = dictAboutMe["questionFive"]?.string
            
        }
        else
        {
            let userObject = CYLGlobals.sharedInstance.usrObject!
            var aboutMeJSON :JSON!
            if let aboutData = (userObject.aboutMe ?? "")?.data(using: String.Encoding.utf8)
            {
                aboutMeJSON = JSON(data: aboutData)
            }
            print(aboutMeJSON)
            for i in 0..<aboutMeJSON.count
            {
                let textField = self.view.viewWithTag(1001+i) as! UITextView
                textField.text = aboutMeJSON[i].stringValue
            }
        }
    }
    @objc func btnToBackClicked()
    {
        if self.isDoneEnabled
        {
            self.doConfirmationForDataSubmissionAboutMe()
        }
        else
        {
            _=self.navigationController?.popViewController(animated: true)
        }
        
        
        
    }
    func doConfirmationForDataSubmissionAboutMe()
    {
        let text = showAlertTitleMessageFont(strTitle: "Confirm", strMessage: "Do you want to save the changes?")
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        alert.setValue(text.title, forKey: "attributedTitle")
        alert.setValue(text.message, forKey: "attributedMessage")
        let yesAction = UIAlertAction.init(title: "Save", style: .default, handler:{
            (sender) in
            self.requestToUpdateAboutMe()
            
            
        })
        let noAction = UIAlertAction.init(title: "Cancel", style: .destructive, handler:
        {
            (sender) in
            _=self.navigationController?.popViewController(animated: true)
            
        })
        alert.addAction(noAction)
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToUpdateAboutMe()
    {
        KVNProgress.show()
        let params:[String: Any] = ["questionOne": (txtAboutFirst.text!).trimmingCharacters(in: CharacterSet.whitespaces),"questionTwo":(txtAboutSecond.text!).trimmingCharacters(in: CharacterSet.whitespaces),"questionThree":(txtAboutThird.text!).trimmingCharacters(in: CharacterSet.whitespaces),"questionFour":(txtAboutFourth.text!).trimmingCharacters(in: CharacterSet.whitespaces),"questionFive":(txtAboutFifth.text!).trimmingCharacters(in: CharacterSet.whitespaces)]
        let request = CYLServices.postAboutUsDataAboutMe(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        if self.checkLoginFlow != ""
                        {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.changeMainViewToRootVC()
                        }
                        else
                        {
                            self.appObject?.aboutMeV2 = (responseData?["data"]?.dictionary)!
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: nil)
                            _=self.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"About me user"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"About me user"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"About me user"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    @objc func btnToDoneClicked()
    {
        requestToUpdateAboutMe()
    }
    @IBAction func btnContinueAction(_ sender: UIButton) {
        requestToUpdateAboutMe()
    }
    
    

}
