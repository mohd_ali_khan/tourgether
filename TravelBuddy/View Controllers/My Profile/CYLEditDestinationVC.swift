//
//  CYLEditDestinationVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/18/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLEditDestinationVC: CYLBaseViewController,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,CYLSearchDelegate,UINavigationControllerDelegate
{

    @IBOutlet weak var scrlDestination: UIScrollView!

    @IBOutlet weak var tblCountry: UITableView!
    @IBOutlet weak var tblCity: UITableView!
    
    @IBOutlet weak var imgCitiesBack: UIImageView!
    @IBOutlet weak var constImageCities: NSLayoutConstraint!
    @IBOutlet weak var constImageCountry: NSLayoutConstraint!
    @IBOutlet weak var imgCountryBack: UIImageView!
    @IBOutlet weak var lblSelectedSegmentText: UILabel!
    var aryCountry=[CYLLocationObject]()
    var aryCity=[CYLLocationObject]()
    var arySelectedCity=[String]()
    var arySelectedCountry=[String]()
    var isDoneEnabled=false
    var checkSelectedSegmentEditDestination: Int = Int()
    var postNotificationRefreshDestination: String = String()
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: View Controller Life Cycle
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.white], for: .normal)
        createDestinationScreenDesign()
        self.getSelecetdCountryAndCityArray()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Edit user destination")
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Design section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createDestinationScreenDesign()
    {
        self.isDoneEnabled=false
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "save"), style: .plain, target: self, action: #selector(self.btnToDoneClicked))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Travel Destination"
    }
   
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Back button Validation section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @objc func btnToBackClicked()
    {
        if self.isDoneEnabled
        {
            self.doConfirmationForDataSubmission()
        }
        else
        {
            _=self.navigationController?.popViewController(animated: true)
        }

    }
    @objc func btnToDoneClicked()
    {
        self.validateDestinationApiCall()
    }
    func doConfirmationForDataSubmission()
    {
        let text = showAlertTitleMessageFont(strTitle: "Confirm", strMessage: "Do you want to save the changes?")
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        alert.setValue(text.title, forKey: "attributedTitle")
        alert.setValue(text.message, forKey: "attributedMessage")
        let yesAction = UIAlertAction.init(title: "Save", style: .default, handler:{
            (sender) in
            self.validateDestinationApiCall()
            
            
        })
        let noAction = UIAlertAction.init(title: "Cancel", style: .destructive, handler:
            {
                (sender) in
                _=self.navigationController?.popViewController(animated: true)
                
        })
        alert.addAction(noAction)
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
    func validateDestinationApiCall()
    {
        if self.aryCountry.count == 0 && self.checkSelectedSegmentEditDestination == 1
        {
            showAlertView(title: "", message: "Please select atleast one country or choose a different destination option", ref: self)
        }
        else if self.aryCity.count == 0 && self.checkSelectedSegmentEditDestination == 0
        {
            showAlertView(title: "", message: "Please select atleast one city or choose a different destination option", ref: self)
        }
        else
        {
            self.requestToSubmitDestinationData()
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Delete Destination section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func doConfirmationForDestinationDelete(indxx:Int,isCity:Bool)
    {
        let text = showAlertTitleMessageFont(strTitle: "Confirm", strMessage: "Delete this destination?")
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        alert.setValue(text.title, forKey: "attributedTitle")
        alert.setValue(text.message, forKey: "attributedMessage")
        let yesAction = UIAlertAction.init(title: "Delete", style: .destructive, handler: {
            (sender) in
            self.isDoneEnabled = true
            if isCity
            {
                self.deleteCityAtIndex(indxx: indxx) 
            }
            else
            {
                self.deleteCountryAtIndex(indxx: indxx)
                
            }
        })
        let noAction = UIAlertAction.init(title: "Cancel", style: .default, handler:
            {
                (sender) in
                
        })
        alert.addAction(noAction)
        alert.addAction(yesAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    func deleteCityAtIndex(indxx:Int)
    {
        self.arySelectedCity.remove(at: indxx)
        self.aryCity.remove(at: indxx)
        if self.aryCity.count == 0
        {
            self.createEmptyCityCountryTableHeaderView(tblView: self.tblCity)
        }
        self.tblCity.reloadData()
    }
    func deleteCountryAtIndex(indxx:Int)
    {
        self.arySelectedCountry.remove(at: indxx)
        self.aryCountry.remove(at: indxx)
        if self.aryCountry.count == 0
        {
            self.createEmptyCityCountryTableHeaderView(tblView: self.tblCountry)
        }
        self.tblCountry.reloadData()
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Other user defined section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func updateDestinationListInDatabase()
    {
        let userObject = CYLGlobals.sharedInstance.usrObject!
        if self.checkSelectedSegmentEditDestination == 0
        {
           userObject.cities = self.aryCity
           userObject.countries = [CYLLocationObject]()
        }
        else if self.checkSelectedSegmentEditDestination == 1
        {
            userObject.countries = self.aryCountry
            userObject.cities = [CYLLocationObject]()
        }
        CYLGlobals.sharedInstance.usrObject = userObject
        let userDeflts = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
        userDeflts.set(encodedData, forKey: "userObject")
        userDeflts.synchronize()
        
        if self.postNotificationRefreshDestination == "profile"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: nil, userInfo: ["name":""])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: nil, userInfo: ["name":"profile"])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: nil, userInfo: ["name":"profile"])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: nil, userInfo: ["name":"profile"])
        }
        else 
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: ["name":"profile"])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: ["name":""])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: ["name":""])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: ["name":""])
        }
        _=self.navigationController?.popViewController(animated: true)
        self.isDoneEnabled=false
//        //KVNProgress.showSuccess()
    }
    func getSelecetdCountryAndCityArray()
    {
        for object in self.aryCity
        {
            self.arySelectedCity.append("\(object.id!)")
        }
        for object in self.aryCountry
        {
            self.arySelectedCountry.append("\(object.id!)")
        }
        if self.aryCity.count == 0 && self.aryCountry.count == 0
        {
            self.checkSelectedSegmentEditDestination = 1
            imgCitiesBack.image = UIImage(named: "uncheck_btn")
            self.constImageCities.constant = 0
            self.constImageCountry.constant = 15
            imgCountryBack.image = UIImage(named: "Rectangle")
            self.lblSelectedSegmentText.text = "For my next trip, I wanna travel to any of the following countries"
            self.tblCountry.reloadData()
        }
        else if self.aryCity.count >= self.aryCountry.count
        {
            self.checkSelectedSegmentEditDestination = 0
            imgCitiesBack.image = UIImage(named: "Rectangle")
            self.constImageCities.constant = 15
            self.constImageCountry.constant = 0
            imgCountryBack.image = UIImage(named: "uncheck_btn")
            self.lblSelectedSegmentText.text = "For my next trip, I wanna travel to any of the following cities"
            self.tblCity.reloadData()
        }
        else
        {
            self.checkSelectedSegmentEditDestination = 1
            imgCitiesBack.image = UIImage(named: "uncheck_btn")
            self.constImageCities.constant = 0
            self.constImageCountry.constant = 15
            imgCountryBack.image = UIImage(named: "Rectangle")
            self.lblSelectedSegmentText.text = "For my next trip, I wanna travel anywhere within the following countries"
            self.tblCountry.reloadData()
        }
        
        if self.aryCity.count == 0 || self.aryCountry.count == 0
        {
            if self.aryCity.count == 0
            {
                self.createEmptyCityCountryTableHeaderView(tblView: self.tblCity)
            }
            if self.aryCountry.count == 0
            {
                self.createEmptyCityCountryTableHeaderView(tblView: self.tblCountry)
            }
        }
        
        self.perform(#selector(self.disableSaveConfirmationPopup), with: nil, afterDelay: 0.01)
        self.tblCountry.tableFooterView=UIView()
        self.tblCity.tableFooterView=UIView()
    }
    @objc func disableSaveConfirmationPopup()
    {
      self.isDoneEnabled=false
    }
    
    func createEmptyCityCountryTableHeaderView(tblView: UITableView)
    {
        let hederView: UIView = UIView()
        let imgView:UIImageView = UIImageView()
        if screenHeight == 480 || screenHeight == 568
        {
            hederView.frame = CGRect(x:0,y:0,width:self.screenWidth!,height:180)
            imgView.frame = CGRect(x: 80, y: 40, width: self.screenWidth!-160, height: 100)
            imgView.image = UIImage(named: "empty_state.png")
            hederView.addSubview(imgView)
        }
        else
        {
            hederView.frame = CGRect(x:0,y:0,width:self.screenWidth!,height:280)
            imgView.frame = CGRect(x: 40, y: 60, width: self.screenWidth!-80, height: 200)
            
        }
        imgView.image = UIImage(named: "empty_state.png")
        hederView.addSubview(imgView)
        let lblTitle = UILabel.init(frame:CGRect(x:0,y:imgView.frame.origin.y+imgView.frame.size.height+10,width:self.screenWidth!,height:30))
        lblTitle.text = "No Destination Yet"
        lblTitle.textColor = UIColor(red: 178/255, green: 192/255, blue: 198/255, alpha: 1)
        lblTitle.font = UIFont(name: "Comfortaa-Bold", size: 18)
        lblTitle.textAlignment = .center
        hederView.addSubview(lblTitle)
        
        tblView.tableHeaderView = hederView
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom Button Actions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToAddCityClicked(_ sender: Any)
    {
        if self.aryCity.count == 5
        {
            showAlertView(title: "Alert!", message: "You have reached maximum limit of adding destination.", ref: self)
        }
        else
        {
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLCountrySearchVC") as! CYLCountrySearchVC
            searchVC.title = "City"
            searchVC.isCountrySearch=false
            searchVC.delegate=self
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
    }
    @IBAction func btnToAddCountryClicked(_ sender: Any)
    {
        if self.aryCountry.count == 5
        {
            showAlertView(title: "Alert!", message: "You have reached maximum limit of adding destination.", ref: self)
        }
        else
        {
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLCountrySearchVC") as! CYLCountrySearchVC
            searchVC.title = "Country"
            searchVC.isCountrySearch=true
            searchVC.delegate=self
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
    }
    @IBAction func btnToDeleteCityClikced(_ sender: UIButton)
    {
        self.doConfirmationForDestinationDelete(indxx: sender.tag-2000,isCity: true)
    }
    @IBAction func btnToDeleteCountryClicked(_ sender: UIButton)
    {
        self.doConfirmationForDestinationDelete(indxx: sender.tag-3000,isCity: false)
    }
    func delegateToGetSelectedCityCountry(object: JSON)
    {
        self.isDoneEnabled=true
        if object["cityName"] != ""
        {
            if !self.arySelectedCity.contains(object["id"].stringValue)
            {
                self.arySelectedCity.append(object["id"].stringValue)
                self.aryCity.append(CYLLocationObject.init(json: object))
                self.tblCity.tableHeaderView = nil
                self.tblCity.reloadData()
            }
        }
        else
        {
            if !self.arySelectedCountry.contains(object["id"].stringValue)
            {
                self.arySelectedCountry.append(object["id"].stringValue)
                self.aryCountry.append(CYLLocationObject.init(json: object))
                self.tblCountry.tableHeaderView = nil
                self.tblCountry.reloadData()
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Table view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (tableView.tag == 401) ? self.aryCountry.count : self.aryCity.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView.tag == 401
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CYLCountryListCell") as! CYLCountryListCell
            cell.object = self.aryCountry[indexPath.row]
            cell.btnToDeleteCountry.tag = 3000 + indexPath.row
            cell.selectionStyle = .none
            self.scrlDestination.scrollRectToVisible(CGRect(x:0,y:0,width:self.scrlDestination.frame.size.width,height:self.scrlDestination.frame.size.height), animated: false)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CYLCityListCell") as! CYLCityListCell
            cell.object = self.aryCity[indexPath.row]
            cell.btnToDelete.tag = 2000 + indexPath.row
            cell.selectionStyle = .none
            
            self.scrlDestination.scrollRectToVisible(CGRect(x:self.scrlDestination.frame.size.width,y:0,width:self.scrlDestination.frame.size.width,height:self.scrlDestination.frame.size.height), animated: false)
            
            return cell
        }
        
    }
    
   
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToSubmitDestinationData()
    {
        var type: String = String()
        var count: Int = Int()
        KVNProgress.show()
        let countries = (self.arySelectedCountry.count == 0 || self.checkSelectedSegmentEditDestination != 1) ? "" : self.arySelectedCountry.joined(separator: ",")
        let cities = (self.arySelectedCity.count == 0 || self.checkSelectedSegmentEditDestination != 0) ? "" : self.arySelectedCity.joined(separator: ",")
        let params:[String: Any] = ["cities":"\(cities)","countries":"\(countries)"]
        if self.arySelectedCity.count != 0 && self.checkSelectedSegmentEditDestination == 0
        {
            type = "Fixed"
            count = self.arySelectedCity.count
        }
        else if self.arySelectedCountry.count != 0 && self.checkSelectedSegmentEditDestination == 1
        {
            type = "Flexible"
            count = self.arySelectedCountry.count
        }
        sentEventPropertyOnCleverTap("Destination preferences", _property: ["Flexibility":type,"DestinationCount":count])
        
        let request = CYLServices.editDestinationList(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        self.updateDestinationListInDatabase()
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Edit user destination"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Edit user destination"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Edit user destination"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    @IBAction func BtnAddCityCountryList(_ sender: UIButton)
    {
        if sender.tag == 1
        {
            self.isDoneEnabled = true
            self.scrlDestination.scrollRectToVisible(CGRect(x:0,y:0,width:self.scrlDestination.frame.size.width,height:self.scrlDestination.frame.size.height), animated: false)
            self.checkSelectedSegmentEditDestination = 1
            imgCitiesBack.image = UIImage(named: "uncheck_btn")
            self.constImageCities.constant = 0
            self.constImageCountry.constant = 15
            imgCountryBack.image = UIImage(named: "Rectangle")
            self.lblSelectedSegmentText.text = "For my next trip, I wanna travel anywhere within the following countries"
        }
        else
        {
            self.isDoneEnabled = true
            self.scrlDestination.scrollRectToVisible(CGRect(x:self.scrlDestination.frame.size.width,y:0,width:self.scrlDestination.frame.size.width,height:self.scrlDestination.frame.size.height), animated: false)
            self.checkSelectedSegmentEditDestination = 0
            imgCitiesBack.image = UIImage(named: "Rectangle")
            self.constImageCities.constant = 15
            self.constImageCountry.constant = 0
            imgCountryBack.image = UIImage(named: "uncheck_btn")
            self.lblSelectedSegmentText.text = "For my next trip, I wanna travel to any of the following cities"
        }
    }
    
}

