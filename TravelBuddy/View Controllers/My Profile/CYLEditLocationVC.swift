//
//  CYLEditLocationVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 7/12/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLEditLocationVC: UIViewController,UITextFieldDelegate,CYLSearchDelegate
{

   // @IBOutlet weak var constTblCityH: NSLayoutConstraint!
   // @IBOutlet weak var constTblCountryH: NSLayoutConstraint!
   // @IBOutlet weak var tblCountry: UITableView!
   // @IBOutlet weak var tblCity: UITableView!
    @IBOutlet weak var imgCountryIcon: UIImageView!
    @IBOutlet weak var txtCountrySearch: UITextField!
    @IBOutlet weak var txtCitySearch: UITextField!
    var arySearchResult = [JSON]()
    var aryFilterResults = [JSON]()
    var countryPicUrl = ""
    var strIsoCode:String?
    var postNotificationRefreshLocation:String = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createEditDestinationScreenDesign()
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Current location")
        self.setPrefilledDataForEditLocationScreen()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createEditDestinationScreenDesign()
    {
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.clear], for: .normal)
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "My Location"
    }
    @objc func btnToBackClicked()
    {
        if self.postNotificationRefreshLocation == "profile"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: nil, userInfo: ["name":"profile"])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: nil, userInfo: ["name":"profile"])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: nil, userInfo: ["name":"profile"])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: nil, userInfo: ["name":"profile"])
        }
        else
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: ["name":"profile"])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: ["name":""])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: ["name":""])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: ["name":""])
        }
        _=self.navigationController?.popViewController(animated: true)
    }
    func setPrefilledDataForEditLocationScreen()
    {
        let location = CYLGlobals.sharedInstance.usrObject?.city
        let aryLoc = location?.components(separatedBy: ", ")
        if aryLoc?.count == 2
        {
            self.txtCitySearch.text = aryLoc?[0]
            self.txtCountrySearch.text = aryLoc?[1]
        }
        else
        {
            self.txtCountrySearch.text = location
        }
        self.strIsoCode = (CYLGlobals.sharedInstance.usrObject?.isoLocation != nil) ? CYLGlobals.sharedInstance.usrObject?.isoLocation : ""
        if CYLGlobals.sharedInstance.usrObject?.countryPicUrl != nil || CYLGlobals.sharedInstance.usrObject?.countryPicUrl != nil
        {
            self.imgCountryIcon.sd_setImage(with: URL(string:(CYLGlobals.sharedInstance.usrObject?.countryPicUrl!)!))
        }
    }
    @IBAction func btnToEditCountryClicked(_ sender: Any)
    {
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLCountrySearchVC") as! CYLCountrySearchVC
        searchVC.title = "Country"
        searchVC.isCountrySearch=true
        searchVC.delegate=self
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Country search screen delegate
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func delegateToGetSelectedCityCountry(object: JSON)
    {
        if object["cityName"] == ""
        {
            self.countryPicUrl = object["picUrl"].stringValue
            if self.countryPicUrl != ""
            {
            self.imgCountryIcon.sd_setImage(with: URL(string:self.countryPicUrl))
            }
            self.txtCountrySearch.text = object["countryName"].stringValue
            self.strIsoCode = object["isoCode"].stringValue
            let strLocation = (self.txtCitySearch.text == "") ? self.txtCountrySearch.text! : (self.txtCitySearch.text! + ", " + self.txtCountrySearch.text!)
            self.requestToUpdateUserCurrentLocation(strLocation: strLocation, strIsoCode: self.strIsoCode!)
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Table view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
     if textField == self.txtCountrySearch
     {
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLCountrySearchVC") as! CYLCountrySearchVC
        searchVC.title = "Country"
        searchVC.isCountrySearch=true
        searchVC.delegate=self
        self.navigationController?.pushViewController(searchVC, animated: true)
        
     }
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.txtCitySearch
        {
           let strLocation = (self.txtCitySearch.text == "") ? self.txtCountrySearch.text! : (self.txtCitySearch.text! + ", " + self.txtCountrySearch.text!)
           self.requestToUpdateUserCurrentLocation(strLocation: strLocation, strIsoCode: self.strIsoCode!)
        }
        return true
    }
 /*   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
         let isCountry = (textField == self.txtCountrySearch) ? true : false
         if textField.text?.characters.count == 0 && range.location == 0
         {
           //cal api here
            self.requestToGetCityCountrySearch(searchTerm: string, isCountry: isCountry)
         }
         else
         {
           //search locally here
            let keyName = isCountry ? "countryName" : "cityName"
            self.aryFilterResults = filterResultsInArray(aryInput:self.arySearchResult,key:keyName,searchWord:(textField.text! + string))
            if isCountry
            {
                self.tblCountry.reloadData()
            }
            else
            {
                self.tblCity.reloadData()
            }
            
         }
      return true
    }*/
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToUpdateUserCurrentLocation(strLocation:String,strIsoCode:String)
    {
        sentEventPropertyOnCleverTap("User current location update", _property: ["City":self.txtCitySearch.text!,"Country":self.txtCountrySearch.text!])
        KVNProgress.show()
        let params:[String: Any] = ["country":"\(strLocation)","isoLocation":"\(strIsoCode)"]
        let request = CYLServices.postAboutUsData(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.city = strLocation
                        userObject.isoLocation = strIsoCode
                        userObject.countryPicUrl = self.countryPicUrl
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        if self.postNotificationRefreshLocation == "profile"
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: nil, userInfo: ["name":"profile"])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: nil, userInfo: ["name":"profile"])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: nil, userInfo: ["name":"profile"])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: nil, userInfo: ["name":"profile"])
                        }
                        else
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: ["name":"profile"])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: ["name":""])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: ["name":""])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: nil, userInfo: ["name":"profile"])
                        }
//                        _ = self.navigationController?.popViewController(animated: true)
//                        //KVNProgress.showSuccess()
                    }
                    
                }
                else
                {
                    KVNProgress.dismiss()
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Current location"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Current location"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Current location"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }


}
