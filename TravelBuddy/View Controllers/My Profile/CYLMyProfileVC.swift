
//
//  CYLMyProfileVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/7/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
//import UITextView
import Crashlytics
//import UIButton
import IQKeyboardManagerSwift

class CYLMyProfileVC: CYLBaseViewController,UIScrollViewDelegate,UITextViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,CLLocationManagerDelegate,CYLIAPReloadAppData {
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblAboutMe: UILabel!
    @IBOutlet weak var txtAboutMe: UITextView!
    @IBOutlet weak var imgNoDates: UIImageView!
    @IBOutlet weak var imgNoDestination: UIImageView!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var lblCalendarNoPlans: UILabel!
    @IBOutlet weak var lblNoDestinationPlan: UILabel!
    @IBOutlet weak var imgAboutStatus: UIImageView!
    @IBOutlet weak var vwSettingSection: UIView!
    @IBOutlet weak var lblProgressInformation: UILabel!
    @IBOutlet weak var vwProgress: UIProgressView!
    @IBOutlet weak var vwContent: UIView!
    @IBOutlet weak var btnToSettings: UIButton!
    @IBOutlet weak var collectionLocation: UICollectionView!
    @IBOutlet weak var constLocationH: NSLayoutConstraint!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var constCurrentLocationEmpty: NSLayoutConstraint!
    var currentOffset:CGFloat?
    @IBOutlet weak var scrlProfile: UIScrollView!
    @IBOutlet weak var imgCurrentLocation: UIImageView!
    @IBOutlet weak var btnAllowLocation: UIButton!
    @IBOutlet weak var constUpdatePremium: NSLayoutConstraint!
    @IBOutlet weak var lblPremium: UILabel!
    
    var aryLocation = [CYLLocationObject]()
    var appObject:AppDelegate?
    var heightCityCountryList:CGFloat?
    var vwLoader: UIView = UIView()
    let locationManager = CLLocationManager()
    var vwEmptyState: UIView = UIView()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loaderGifImage()
        self.appObject = UIApplication.shared.delegate as? AppDelegate
        self.updateUserProfileData(str: "")
        NotificationCenter.default.removeObserver(NSNotification.Name("update_profile_screen_data"))
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserProfileDataPush(notification:)), name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        createMyProfileScreenDesign()
        self.tabBarController?.navigationItem.titleView = nil
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        self.setUserProfilePicture()
        self.tabBarController?.navigationItem.title = "My Profile"
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Current user profile")
        IQKeyboardManager.shared.enableAutoToolbar=true
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func touchesShouldCancel(in view: UIView) -> Bool {
        if view is UIButton {
            return true
        }
        return false
    }
    
    func loaderGifImage()
    {
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwLoader = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwLoader.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
            window.addSubview(vwLoader)
            let img = UIImageView(frame: CGRect(x: vwLoader.frame.width/2-40, y: vwLoader.frame.height/2-40, width: 80, height: 80))
            img.image = UIImage.sd_animatedGIF(with: imageData!)
            vwLoader.addSubview(img)
            vwLoader.isHidden = true
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom button actions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToEditDestinationClicked(_ sender: Any)
    {
        let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLEditDestinationVC") as! CYLEditDestinationVC
        destinationVC.postNotificationRefreshDestination = "profile"
        destinationVC.aryCountry = CYLGlobals.sharedInstance.usrObject!.countries!
        destinationVC.aryCity = CYLGlobals.sharedInstance.usrObject!.cities!
        self.navigationController?.pushViewController(destinationVC, animated: true)
       
    }
    
    @IBAction func btnToEditCalendarClicked(_ sender: Any)
    {
        
        if self.aryLocation.count == 0
        {
            let text = showAlertTitleMessageFont(strTitle: "Please choose a destination first", strMessage: "Please select atleast one destination to edit the dates.")
            let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            alert.setValue(text.title, forKey: "attributedTitle")
            alert.setValue(text.message, forKey: "attributedMessage")
            let yesAction = UIAlertAction.init(title: "OK", style: .default, handler:
            {
                (sender) in
                let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLEditDestinationVC") as! CYLEditDestinationVC
                destinationVC.postNotificationRefreshDestination = "profile"
                destinationVC.aryCountry = CYLGlobals.sharedInstance.usrObject!.countries!
                destinationVC.aryCity = CYLGlobals.sharedInstance.usrObject!.cities!
                self.navigationController?.pushViewController(destinationVC, animated: true)
            })
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let calendarVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLEditCalendarVC") as! CYLEditCalendarVC
            calendarVC.postNotificationRefreshCalender = "profile"
            calendarVC.startDate = self.lblStartDate.text
            calendarVC.endDate = self.lblEndDate.text
            self.navigationController?.pushViewController(calendarVC, animated: true)
        }
    }
    @IBAction func btnToEditLocationClicked(_ sender: Any)
    {
        
        if CLLocationManager.locationServicesEnabled()
        {
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways
            {
                self.startRotation()
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.delegate = self
                locationManager.startUpdatingLocation()
            }
            else if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted
                {
                    let text = showAlertTitleMessageFont(strTitle: "Update your current location", strMessage: "To enable location, please go to Settings -> Tourgether -> Location and choose the option - While using the app")
                    let alertComtroller = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                    alertComtroller.setValue(text.title, forKey: "attributedTitle")
                    alertComtroller.setValue(text.message, forKey: "attributedMessage")
                    let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel, handler:
                    { action -> Void in
                        self.dismiss(animated: true, completion: nil)
                    })
                    alertComtroller.addAction(cancelAction)
                    
                    self.present(alertComtroller, animated: true, completion: nil)
                }
            else if CLLocationManager.authorizationStatus() == .notDetermined
            {
                self.startRotation()
                self.locationManager.requestWhenInUseAuthorization()
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.delegate = self
                locationManager.startUpdatingLocation()
            }
        }
        else
        {
            let text = showAlertTitleMessageFont(strTitle: "Update your current location", strMessage: "To enable location, please go to Settings -> Tourgether -> Location and choose the option - While using the app")
            let alertComtroller = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            alertComtroller.setValue(text.title, forKey: "attributedTitle")
            alertComtroller.setValue(text.message, forKey: "attributedMessage")
            let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel, handler:
            { action -> Void in
                self.dismiss(animated: true, completion: nil)
            })
            alertComtroller.addAction(cancelAction)
            self.present(alertComtroller, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied || status == CLAuthorizationStatus.restricted)
        {
            self.requestToUpdateCurrentLocation(latitude: 0, longitude: 0, city: "", country: "", isoLocation: "")
            // The user denied authorization
        } else if (status == CLAuthorizationStatus.authorizedWhenInUse)
        {
            manager.startUpdatingLocation()
            // The user accepted authorization
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        manager.stopUpdatingLocation()
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        if locations.count == 1
        {
            CleverTap.setLocation(locValue)
            
            // Add below code to get address for touch coordinates.
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler:
                { (placemarks, error) -> Void in
                    // Place details
                    var placeMark: CLPlacemark!
                    placeMark = placemarks?[0]
                    // Address dictionary
                    //                print(placeMark.addressDictionary as Any)
                    // City
                    var city:String = String()
                    var country: String = String()
                    var isoLocation: String = String()
                    if (placeMark != nil)
                    {
                        city = placeMark.addressDictionary!["City"] as? String ?? ""
                        country = placeMark.addressDictionary!["Country"] as? String ?? ""
                        self.lblLocation.text = "\(city), \(country)"
                        isoLocation = placeMark.isoCountryCode ?? ""
                    }
                    self.requestToUpdateCurrentLocation(latitude: locValue.latitude, longitude: locValue.longitude, city: city, country: country, isoLocation: isoLocation)
            })
            
        }
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createMyProfileScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.viewBack.backgroundColor = UIColor.clear
        self.viewBack.backgroundColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.scrlProfile.backgroundColor = UIColor.clear
        self.scrlProfile.backgroundColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        let transform = CGAffineTransform.init(scaleX: 1, y: 2)
        self.vwProgress.transform = transform
    }
    
    @objc func updateUserProfileDataPush(notification: NSNotification)
    {
        print(notification.userInfo?["name"] ?? "")
        let notifStr = notification.userInfo?["name"] ?? ""
        updateUserProfileData(str: notifStr as! String)
    }
    
    func updateUserProfileData(str: String)
    {
        if str == ""
        {
            vwLoader.isHidden = false
        }
        let params:[String: Any] = ["currentUserId":""]
        let request = CYLServices.getOtherUserProfile(dict: params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                self.vwLoader.isHidden = true
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData!["status"]?.stringValue != "fail"
                    {
                        let userObjectNew = CYLUserObject.init(json: (responseData?["data"])!)
                        let userObject = CYLGlobals.sharedInstance.usrObject
                        userObject?.cities = userObjectNew.cities
                        userObject?.countries = userObjectNew.countries
                        userObject?.userImages = userObjectNew.userImages
                        userObject?.country = userObjectNew.country
                        userObject?.cityv2 = userObjectNew.cityv2
                        userObject?.countryPicUrl = userObjectNew.countryPicUrl
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject!)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        
                        if (responseData?["data"]?["updatedAboutMeV2"].exists())!
                        {
                            if responseData?["data"]?["updatedAboutMeV2"] != JSON.null
                            {
                                self.appObject?.aboutMeV2 = (responseData?["data"]?["updatedAboutMeV2"].dictionary)!
                                self.setDataOnUserProfileScreen()
                                
                            }
                            else
                            {
                                self.setDataOnUserProfileScreen()
                            }
                            
                        }
                        else
                        {
                            self.setDataOnUserProfileScreen()
                        }
                        
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Current user profile"])
                    if str == ""
                    {
                        self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
                    }
                    else
                    {
                        showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    }
                }
                
        },
                                                                  withError:
            {
                (error) in
                self.vwLoader.isHidden = true
                //                //KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Current user profile"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                self.vwLoader.isHidden = true
                //                //KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Current user profile"])
                
                if str == ""
                {
                    self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
                }
                else
                {
                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                }
                
        })
        
    }
    func determineProfileCompleteness(dict :[String:JSON])->Double
    {
        //10% for first all user
        var progress:Double = 0.0
        // 20% for any profile image
        if let aryProfileImg = CYLGlobals.sharedInstance.usrObject?.userImages
        {
            progress = (aryProfileImg.count > 0) ? 0.2 : 0.0
        }
        // 5% for user location entered
        progress = (self.lblLocation.text == "") ? progress : (progress+0.05)
        //20% for any destination
        progress = (self.aryLocation.count > 0) ? (progress+0.2) : progress
        //20% for dates
        progress = !((self.lblStartDate.text == "")||(self.lblStartDate.text == "Flexible")) ? (progress+0.2) : progress
        
        //5% for each answers of about me
        var aboutMeJSON :JSON!
        var completeness = 0
        var txtAboutMePlaceholder = ""
        
        if dict.count > 0
        {
            if dict["questionOne"]?.string != ""
            {
                progress = progress+0.05
                completeness += 1
                txtAboutMePlaceholder = (dict["questionOne"]?.string)!
            }
            if dict["questionTwo"]?.string != ""
            {
                progress = progress+0.05
                completeness += 1
                if txtAboutMePlaceholder == ""
                {
                    txtAboutMePlaceholder = (dict["questionTwo"]?.string)!
                }
            }
            if dict["questionThree"]?.string != ""
            {
                progress = progress+0.05
                completeness += 1
                if txtAboutMePlaceholder == ""
                {
                    txtAboutMePlaceholder = (dict["questionThree"]?.string)!
                }
            }
            if dict["questionFour"]?.string != ""
            {
                progress = progress+0.05
                completeness += 1
                if txtAboutMePlaceholder == ""
                {
                    txtAboutMePlaceholder = (dict["questionFour"]?.string)!
                }
            }
            if dict["questionFive"] != ""
            {
                progress = progress+0.05
                completeness += 1
                if txtAboutMePlaceholder == ""
                {
                    txtAboutMePlaceholder = (dict["questionFive"]?.string)!
                }
            }
        }
        else
        {
            if let aboutData = (CYLGlobals.sharedInstance.usrObject!.aboutMe)?.data(using: String.Encoding.utf8)
            {
                aboutMeJSON = JSON(data: aboutData)
                for i in 0..<aboutMeJSON.count
                {
                    if aboutMeJSON[i].stringValue != ""
                    {
                        progress = progress+0.05
                        completeness += 1
                        if txtAboutMePlaceholder == ""
                        {
                            txtAboutMePlaceholder = aboutMeJSON[i].stringValue
                        }
                    }
                }
            }
        }
        let statusImage = (completeness == 5) ? #imageLiteral(resourceName: "ic_about_status_success") : #imageLiteral(resourceName: "ic_about_status_fail")
        self.imgAboutStatus.image = statusImage
        self.lblAboutMe.text = (txtAboutMePlaceholder == "") ? "Tell us something about yourself" : txtAboutMePlaceholder
        
        progress = progress+0.1
        
        return progress
    }
    func setDataOnUserProfileScreen()
    {
        let userObject = CYLGlobals.sharedInstance.usrObject!
        self.lblUserName.text = userObject.userName!
        var country: String = String()
        var city: String = String()
        self.constCurrentLocationEmpty.constant = 75
        if userObject.cityv2 != nil
        {
            city = userObject.cityv2!
            if userObject.country != nil
            {
                country = userObject.country!
            }
        }
        if userObject.country != nil
        {
            if userObject.cityv2 != nil
            {
                city = userObject.cityv2!
            }
            country = userObject.country!
        }
        if city == ""
        {
            self.lblLocation.text = country
        }
        else
        {
            self.lblLocation.text = (country == "") ? userObject.cityv2! : userObject.cityv2! + ", " + country
        }
        if city == "" && country == ""
        {
            self.lblLocation.text = "No Destonation Selected"
            self.constCurrentLocationEmpty.constant = 105
        }
        if let _ = userObject.startDate
        {
            self.lblStartDate.text = userObject.startDate!
            self.lblEndDate.text = userObject.endDate!
        }
        else
        {
            self.lblStartDate.text = ""
            self.lblEndDate.text = ""
        }
        self.lblCalendarNoPlans.isHidden = (self.lblStartDate.text == "") ? false : true
        self.imgNoDates.isHidden = (self.lblStartDate.text == "" || self.lblStartDate.text == "Flexible") ? false : true
        self.aryLocation = userObject.cities! +  userObject.countries!
        self.lblNoDestinationPlan.isHidden = (self.aryLocation.count == 0) ? false : true
        self.imgNoDestination.isHidden = (self.aryLocation.count == 0) ? false : true
        if userObject.countryPicUrl != nil
        {
            self.imgLocation.sd_setImage(with: URL.init(string: userObject.countryPicUrl!), placeholderImage: UIImage(named: "empty_state.png"))
        }
        
        let percentageCompleteness = self.determineProfileCompleteness(dict: (self.appObject?.aboutMeV2)!)
        self.vwProgress.progress = Float(percentageCompleteness)
        self.lblProgressInformation.text = "Your profile is \(Int(percentageCompleteness*100)) % complete"
        self.constLocationH.constant = CGFloat(self.aryLocation.count*40 )
        self.collectionLocation.reloadData()
    }
    func setUserProfilePicture()
    {
        let aryProfileImg = CYLGlobals.sharedInstance.usrObject?.userImages
        if (aryProfileImg?.count)! > 0
        {
            let pictureObject = aryProfileImg!.first as! [String:AnyObject]
            self.imgUserProfile.sd_setImage(with: URL(string:(pictureObject["picUrl"] as! String) ), placeholderImage: UIImage(named: "user_placeholder"))
        }
        else
        {
            self.imgUserProfile.image = UIImage(named: "user_placeholder")
        }
        if CYLGlobals.sharedInstance.usrObject?.userPlanType == 1
        {
            self.constUpdatePremium.constant = 0
            self.lblPremium.text = "Premium"
            self.lblPremium.font = UIFont(name: "Comfortaa-Bold", size: 14.0)
            self.lblPremium.textAlignment = .right
        }
        else
        {
            self.constUpdatePremium.constant = 85
            self.lblPremium.text = "Basic"
            self.lblPremium.font = UIFont(name: "Comfortaa-Bold", size: 13.0)
            self.lblPremium.textAlignment = .left
        }
    }
    func imageWithColor(color:UIColor)->UIImage
    {
        let rect = CGRect(x:0,y:0,width:1,height:1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Scroll view delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        /* if(velocity.y>0 && self.scrlProfile.frame.size.height+20 < self.scrlProfile.contentSize.height )
         {
         UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
         self.navigationController?.setNavigationBarHidden(true, animated: true)
         print("Hide")
         }, completion: nil)
         
         } else {
         
         UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions(), animations: {
         self.navigationController?.setNavigationBarHidden(false, animated: true)
         print("Unhide")
         }, completion: nil)
         }*/
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Collection view data source and delegate functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aryLocation.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellLocation = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCountryCollectionCell", for: indexPath as IndexPath) as! CityCountryCollectionCell
        cellLocation.layer.borderWidth = 1.0
        cellLocation.layer.borderColor = UIColor(red: 255/255.0, green: 67/255.0, blue: 81/255.0, alpha: 1.0).cgColor
        cellLocation.layer.cornerRadius = 15
        cellLocation.clipsToBounds = true
        cellLocation.imgLocation.layer.cornerRadius = 10
        cellLocation.imgLocation.clipsToBounds = true
        cellLocation.object = self.aryLocation[indexPath.row]
        heightCityCountryList = cellLocation.frame.origin.y+cellLocation.frame.height+10
        if indexPath.row == aryLocation.count-1
        {
            self.constLocationH.constant = heightCityCountryList ?? 0
        }
        return cellLocation
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var widht:CGFloat = CGFloat()
        
        let object:CYLLocationObject = self.aryLocation[indexPath.row]
        
        if object.cityName == ""
        {
            widht = widthForInputString(text: (object.countryName)!)
        }
        else
        {
            widht = widthForInputString(text: (object.cityName)! + ", " + (object.countryName)!)
        }
        let wi = widht + 40
        return CGSize(width: wi, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func requestToUpdateCurrentLocation(latitude:Double,longitude:Double,city: String, country: String,isoLocation:String)
    {
//        //KVNProgress.show()
        let params:[String: Any] = ["latitude":"\(latitude)","longitude":"\(longitude)","city":city,"country":country,"isoLocation":isoLocation]
        let request = CYLServices.postUserCurrentLocation(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.showSuccess()
                self.endRotation()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject!
                        userObject.cityv2 = responseData!["data"]!["userLocation"]["city"].stringValue
                        userObject.country = responseData!["data"]!["userLocation"]["country"].stringValue
                        userObject.isoLocation = responseData!["data"]!["userLocation"]["isoLocation"].stringValue
                        userObject.countryPicUrl = responseData!["data"]!["countryPicUrl"].stringValue
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: nil, userInfo: ["name":"profile"])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: nil, userInfo: ["name":"profile"])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: nil, userInfo: ["name":"profile"])
                        
                        self.setDataOnUserProfileScreen()
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Current Location"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    self.endRotation()
                }
                
        },
                                                                  withError:
            {
                (error) in
                ////KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Current Location"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
                self.endRotation()
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                ////KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Current Location"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                self.endRotation()
                
        })
    }
    @IBAction func btnPaymentAction(_ sender: UIButton)
    {
        let paymentPopup = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserPaymentVC") as! CYLUserPaymentVC
        paymentPopup.messageLimitExceed = "message"
        paymentPopup.delegateMessageReload = self
        self.navigationController?.pushViewController(paymentPopup, animated: false)
    }
    func reloadMessageReceiverListDelegatePayment() {
        
    }
    @IBAction func btnEditImages(_ sender: UIButton)
    {
        let calendarVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserImageSelectionVC") as! CYLUserImageSelectionVC
        self.navigationController?.pushViewController(calendarVC, animated: true)
    }
    @IBAction func btnAboutMe(_ sender: UIButton) {
        let aboutVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLAboutMe") as! CYLAboutMe
        aboutVC.dictAboutMe = (self.appObject?.aboutMeV2)!
        self.navigationController?.pushViewController(aboutVC, animated: true)
    }
    @IBAction func btnSetting(_ sender: UIButton)
    {
        let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLSettingsVC") as! CYLSettingsVC
        self.navigationController?.pushViewController(settingVC, animated: true)
    }
   
    func startRotation() {
        
        self.imgCurrentLocation.image = UIImage(named: "refresh_spineer.png")
        self.btnAllowLocation.isUserInteractionEnabled = false
        let kAnimationKey = "rotation"
        if self.imgCurrentLocation.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = 2.0
            animate.repeatCount = Float.infinity
            animate.fromValue = 0.0
            animate.toValue = Float(CGFloat.pi * 2.0)
            self.imgCurrentLocation.layer.add(animate, forKey: kAnimationKey)
        }
    }
    func endRotation()
    {
        self.btnAllowLocation.isUserInteractionEnabled = true
        let kAnimationKey = "rotation"
        if self.imgCurrentLocation.layer.animation(forKey: kAnimationKey) != nil {
            self.imgCurrentLocation.layer.removeAnimation(forKey: kAnimationKey)
            self.imgCurrentLocation.image = UIImage(named: "ic_gps.png")
        }
    }
    
}
extension CYLMyProfileVC
{
    
    func refreshViewSlowOrMantinence(msgStr: String , imgName:String)
    {
        if vwEmptyState.frame.size.width == 0
        {
            vwEmptyState = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height))
            vwEmptyState.backgroundColor = UIColor.white
            self.view.addSubview(vwEmptyState)
            let img = UIImageView(frame: CGRect(x: vwEmptyState.frame.width/2-60, y: vwEmptyState.frame.height/2-160, width: 120, height: 120))
            img.image = UIImage(named: imgName)
            
            let lblMsg = UILabel(frame: CGRect(x: 40, y: img.frame.origin.y+img.frame.size.height+20, width: self.view.frame.size.width-80, height: 21))
            lblMsg.font = UIFont(name: "Comfortaa-Bold", size: 20)
            lblMsg.textAlignment = .center
            lblMsg.text = "Oops!"
            
            let lblAlertText = UILabel(frame: CGRect(x: 60, y: lblMsg.frame.origin.y+lblMsg.frame.size.height+15, width: self.view.frame.size.width-120, height: 40))
            lblAlertText.numberOfLines = 0
            lblAlertText.textAlignment = .center
            lblAlertText.text = msgStr
            lblAlertText.adjustsFontSizeToFitWidth = true
            lblAlertText.font = UIFont(name: "Comfortaa-Bold", size: 15)
            
            let btnRefresh: UIButton = UIButton(frame: CGRect(x: vwEmptyState.frame.width/2-100, y: lblAlertText.frame.origin.y+lblAlertText.frame.size.height+35, width: 200, height: 45))
            btnRefresh.addTarget(self, action: #selector(btnRefreshAction), for: .touchUpInside)
            btnRefresh.titleLabel?.textAlignment = .center
            btnRefresh.titleLabel?.font = UIFont(name: "Comfortaa-Bold", size: 15)
            btnRefresh.titleLabel?.textColor = UIColor.white
            btnRefresh.setTitle("Retry", for: UIControlState.normal)
            btnRefresh.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
            btnRefresh.layer.cornerRadius = 23
            btnRefresh.layer.masksToBounds = true
            
            vwEmptyState.addSubview(img)
            vwEmptyState.addSubview(lblMsg)
            vwEmptyState.addSubview(lblAlertText)
            vwEmptyState.addSubview(btnRefresh)
            vwEmptyState.isHidden = false
        }
        else
        {
            vwEmptyState.isHidden = false
        }
    }
    @objc func btnRefreshAction(sender: UIButton)
    {
        if checkInternetConnection()
        {
            self.updateUserProfileData(str: "")
            vwEmptyState.isHidden = true
        }
    }
    
}

