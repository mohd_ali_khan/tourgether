//
//  CYLUserImageSelectionVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 28/03/18.
//  Copyright © 2018 Nikhil Sharma. All rights reserved.
//

import UIKit
import INSPhotoGallery
//protocol changeUserProfileImages: class {
//    func changeUesrImagesDeleted(_ arr: [Any]?)
//}

class CYLUserImageSelectionVC: CYLBaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var collectionUserImage: UICollectionView!
    
    var longPressGesture: UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    
    var selectedIndex:Int = Int()
    var aryImages=[Data]()
    var isDoneEnabled=false
    var strComesController: String = String()
    //    var delegateDeleteUserProfileImageArray: changeUserProfileImages?
    var aryProfileImg = [Any]()
    var startMoveImage: Int = Int()
    var galleryPreview = INSPhotosViewController()
    var arr : [INSPhotoViewable] = [INSPhotoViewable]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.delegate = self
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        collectionUserImage.addGestureRecognizer(longPressGesture)
        aryProfileImg = (CYLGlobals.sharedInstance.usrObject?.userImages)!
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "User images updation")
        createSelectImageScreenDesign()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createSelectImageScreenDesign()
    {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
        self.title = "Photos"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "save"), style: .plain, target: self, action: #selector(self.btnToDoneClicked))
    }
    @objc func btnToBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    @objc func btnToDoneClicked()
    {
        var strImageUrl: String = String()
        for item in aryProfileImg
        {
            strImageUrl = strImageUrl + "\((item as! NSDictionary).value(forKey: "picUrl") as! String),"
        }
        if strImageUrl != ""
        {
            strImageUrl.remove(at: strImageUrl.index(before: strImageUrl.endIndex))
            requestToUpdateFacebookImagesOnServer(strUrlAry: strImageUrl)
        }
        else
        {
            showAlertView(title: "", message: "No image", ref: self)
        }
        
    }
    
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer)
    {
        switch(gesture.state) {
            
        case .began:
            guard let selectedIndexPath = collectionUserImage.indexPathForItem(at: gesture.location(in: collectionUserImage)) else {
                break
            }
            startMoveImage = selectedIndexPath.item
            collectionUserImage.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            collectionUserImage.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            let end = collectionUserImage.indexPathForItem(at: gesture.location(in: gesture.view!))
            
            if end?.item == nil
            {
                collectionUserImage.cancelInteractiveMovement()
            }
            else if (end?.item)! >= aryProfileImg.count && startMoveImage <= aryProfileImg.count - 1
            {
                collectionUserImage.cancelInteractiveMovement()
            }
            else if startMoveImage > aryProfileImg.count - 1
            {
                collectionUserImage.cancelInteractiveMovement()
            }
            else
            {
                collectionUserImage.endInteractiveMovement()
            }
        default:
            collectionUserImage.cancelInteractiveMovement()
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Button Add Delete Action
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnAddImages(_ sender: UIButton)
    {
        let alertController = UIAlertController.init(title: "", message: "Share photos of yourself and travel photos.", preferredStyle: .actionSheet)
        
        let takePhoto = UIAlertAction.init(title: "Camera", style: .default, handler:
        { (action) in
            
            let picker = UIImagePickerController.init()
            picker.delegate=self
            picker.allowsEditing=true
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
            alertController.dismiss(animated: true, completion: nil)
            
        })
        alertController.addAction(takePhoto)
        
        let choosePhoto = UIAlertAction.init(title: "Gallery", style: .default, handler:
        { (action) in
            
            let picker = UIImagePickerController.init()
            picker.delegate=self
            picker.allowsEditing=true
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
            alertController.dismiss(animated: true, completion: nil)
            
        })
        alertController.addAction(choosePhoto)
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler:
            nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func btnDeleteImage(_ sender: UIButton)
    {
        let selectedIndex = sender.tag - 100
        let alert = UIAlertController.init()
        let yesAction = UIAlertAction.init(title: "Delete Photo", style: .destructive, handler: {
            (sender) in
            
            self.requestToDeleteImageWithObject(object: self.aryProfileImg[selectedIndex])
            
        })
        let noAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler:
        {
            (sender) in
            
        })
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Image picker functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info["UIImagePickerControllerEditedImage"] as! UIImage
        let imageData = UIImageJPEGRepresentation(chosenImage, 0.2)!
        aryImages.removeAll()
        aryImages.append(imageData)
        self.requestToUploadProfileImages()
        picker.dismiss(animated: true, completion: nil)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToDeleteImageWithObject(object:Any)
    {
        KVNProgress.show()
        let params:[String: Any] = ["photoId":"\((object as! NSDictionary).value(forKey: "id")!)"]
        let request = CYLServices.deleteUserProfileImage(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse: {
            (response) in
            
            // //KVNProgress.dismiss()
            if (response?.isValid)!
            {
                let responseData = response?.object?.dictionaryObject
                if responseData?["status"] as! String != "fail"
                {
                    let userObject = CYLGlobals.sharedInstance.usrObject
                    userObject?.userImages = responseData?["data"] as! [Any]?
                    CYLGlobals.sharedInstance.usrObject = userObject
                    let userDeflts = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject!)
                    userDeflts.set(encodedData, forKey: "userObject")
                    userDeflts.synchronize()
                    KVNProgress.showSuccess()
                    self.aryProfileImg.removeAll()
                    self.arr.removeAll()
                    self.aryProfileImg = (CYLGlobals.sharedInstance.usrObject?.userImages)!
                    self.collectionUserImage.reloadData()
                }
                else
                {
                    KVNProgress.dismiss()
                }
                
            }
            else
            {
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Delete image"])
                showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
            }
        }, withError: {
            (error) in
            KVNProgress.dismiss()
            sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Delete image"])
            showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        }, andNetworkErr: {
            (networkFailure) in
            KVNProgress.dismiss()
            sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Delete image"])
            showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    
    func requestToUploadProfileImages()
    {
        KVNProgress.show()
        let params:[String: Any] = ["":""]
        let request = CYLServices.postAllProfileImages(dict:params,aryImages: self.aryImages, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callDataUploadServiceWithRequest(rqst: request, withResponse: {
            (response) in
            
            KVNProgress.dismiss()
            if (response?.isValid)!
            {
                let responseData = response?.object?.dictionaryObject
                if responseData?["status"] as! String != "fail"
                {
                    let userObject = CYLGlobals.sharedInstance.usrObject
                    userObject?.userImages = responseData?["data"] as! [Any]?
                    CYLGlobals.sharedInstance.usrObject = userObject
                    let userDeflts = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject!)
                    userDeflts.set(encodedData, forKey: "userObject")
                    userDeflts.synchronize()
                    KVNProgress.showSuccess()
                    self.aryProfileImg.removeAll()
                    self.arr.removeAll()
                    self.aryProfileImg = (CYLGlobals.sharedInstance.usrObject?.userImages)!
                    self.collectionUserImage.reloadData()
                }
                else
                {
                    KVNProgress.dismiss()
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Single upload image"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
            }
            else
            {
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Single upload image"])
                showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
            }
        }, withError: {
            (error) in
            KVNProgress.dismiss()
            sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Single upload image"])
            showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        }, andNetworkErr: {
            (networkFailure) in
            KVNProgress.dismiss()
            sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Single upload image"])
            showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    
    
    func requestToUpdateFacebookImagesOnServer(strUrlAry:String)
    {
        let params:[String: Any] = ["images":strUrlAry]
        let request = CYLServices.postImageUrl(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryObject
                    print(responseData ?? [:])
                    if responseData?["status"] as! String != "fail"
                    {
                        let userObject = CYLGlobals.sharedInstance.usrObject
                        userObject?.userImages = responseData?["data"] as! [Any]?
                        CYLGlobals.sharedInstance.usrObject = userObject
                        let userDeflts = UserDefaults.standard
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject!)
                        userDeflts.set(encodedData, forKey: "userObject")
                        userDeflts.synchronize()
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Upload image"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Upload image"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Upload image"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
        
    }
    
}
extension CYLUserImageSelectionVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CYLUserImageCell", for: indexPath) as? CYLUserImageCell{
            
            cell.btnDeleteImage.tag = indexPath.row + 100
            cell.imgUserImage.tag = indexPath.row
            if indexPath.row <= aryProfileImg.count - 1
            {
                let pictureObject = aryProfileImg[indexPath.row] as! [String:AnyObject]
                
                if pictureObject["picUrl"] as? String != ""
                {
                    cell.imgUserImage.setShowActivityIndicator(true)
                    cell.imgUserImage.setIndicatorStyle(.whiteLarge)
                    cell.imgUserImage.sd_setImage(with: URL(string: (pictureObject["picUrl"]as? String)!), placeholderImage: UIImage(named: ""), options: [], completed: { (loadedImage, error, cacheType, url) in
                        
                        cell.imgUserImage.sd_cancelCurrentImageLoad()
                        if error != nil {
                            print("Error code: \(error!.localizedDescription)")
                        } else {
                            cell.imgUserImage.image = loadedImage
                        }
                    })
                }
//                cell.imgUserImage.sd_setImage(with: URL(string: (pictureObject["picUrl"] as! String)), placeholderImage: UIImage(named: "user_placeholder"))
                cell.btnAddImage.isHidden = true
                cell.imgUserImage.backgroundColor = UIColor(red: 25/255.0, green: 34/255.0, blue: 38/255.0, alpha: 1.0)
                cell.btnDeleteImage.isHidden = false
                
            }
            else if indexPath.row > aryProfileImg.count - 1
            {
                cell.imgUserImage.image = UIImage(named: "")
                cell.btnAddImage.isHidden = false
                cell.imgUserImage.backgroundColor = UIColor(red: 187/255.0, green: 187/255.0, blue: 187/255.0, alpha: 1.0)
                cell.btnDeleteImage.isHidden = true
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        print("Starting Index: \(sourceIndexPath.item)")
        print("Ending Index: \(destinationIndexPath.item)")
        
        
        if sourceIndexPath.item <= aryProfileImg.count - 1 && destinationIndexPath.item <= aryProfileImg.count - 1
        {
            let ary = aryProfileImg[sourceIndexPath.item]
            aryProfileImg.remove(at: sourceIndexPath.item)
            aryProfileImg.insert(ary, at: destinationIndexPath.item)
            self.collectionUserImage.reloadData()
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width/2, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        arr.removeAll()
        for i in 0..<aryProfileImg.count
        {
            let pictureObject = aryProfileImg[i] as! [String:AnyObject]
            arr.append(INSPhoto(imageURL: URL(string: pictureObject["picUrl"] as! String), thumbnailImage: UIImage(named: "")))
        }
        
        let currentPhoto = arr[indexPath.row]
        galleryPreview = INSPhotosViewController(photos: arr, initialPhoto: currentPhoto, referenceView: self.view)
        present(galleryPreview, animated: true, completion: nil)
    }
    
    
    
}

