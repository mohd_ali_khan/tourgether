//
//  CYLEditCalendarVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/18/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
import Foundation
import SJFluidSegmentedControl

class CYLEditCalendarVC: CYLBaseViewController,UIGestureRecognizerDelegate,UINavigationControllerDelegate {

    var startDate:String?
    @IBOutlet weak var lblEndDatePlaceholder: UILabel!
    @IBOutlet weak var lblStartDatePlaceholder: UILabel!

    @IBOutlet weak var constStartDatePickerH: NSLayoutConstraint!
    @IBOutlet weak var constEndDatePickerH: NSLayoutConstraint!
    var endDate:String?
    @IBOutlet weak var vwFlexStartContainer: UIView!
    
    @IBOutlet weak var vwFlexEndContainer: UIView!
    
    @IBOutlet weak var pickerEndDate: UIDatePicker!
    @IBOutlet weak var pickerStartDate: UIDatePicker!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var constLastVwTop: NSLayoutConstraint!
    
    @IBOutlet weak var imgFixedBack: UIImageView!
    @IBOutlet weak var imgFixed: UIImageView!
    @IBOutlet weak var imgFlexible: UIImageView!
    @IBOutlet weak var imgFlexibleBack: UIImageView!
    @IBOutlet weak var imgSomedayBack: UIImageView!
    @IBOutlet weak var imgSomeDay: UIImageView!
    @IBOutlet weak var vwSomeDay: UIView!
    
    
    var isDoneEnabled=false
    var pickerFlexStartDate = NTMonthYearPicker()
    var pickerFlexEndDate = NTMonthYearPicker()
    var checkSelectedSegment: Int = Int()
    var postNotificationRefreshCalender: String = String()
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: View Controller Life Cycle
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    override func viewDidLoad()
    {
        super.viewDidLoad()   
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        createCalendarScreenDesign()
        
        self.pickerStartDate.minimumDate = Date()
        self.pickerStartDate.maximumDate = Date().addingTimeInterval(1*365*60*60*24)
        self.pickerStartDate.setDate(Date().addingTimeInterval(1*30*60*60*24), animated: false)
        self.pickerEndDate.minimumDate = Date()
        self.pickerEndDate.maximumDate = Date().addingTimeInterval(1*365*60*60*24)
        self.pickerEndDate.setDate(Date().addingTimeInterval(1*40*60*60*24), animated: false)
        self.setPrefilledDataOnCalendarScreen()
    }
    ///////////////////////////////*********************///////////////
    //MARK:- Swap Back Navigation View
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createCalendarScreenDesign()
    {
        self.isDoneEnabled=false
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "save"), style: .plain, target: self, action: #selector(self.btnToDoneCLicked))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Travel Date"
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Edit travelling date")
        
        self.pickerFlexStartDate.minimumDate = Date()
        self.pickerFlexStartDate.maximumDate = Date().addingTimeInterval(1*365*60*60*24)
        self.pickerFlexStartDate.datePickerMode = NTMonthYearPickerModeMonthAndYear
        self.pickerFlexStartDate.addTarget(self, action: #selector(self.flexStartPickerValueChanged), for: .valueChanged)
        self.pickerFlexStartDate.frame = CGRect(x:0,y:-30,width:UIScreen.main.bounds.width-48,height:self.vwFlexStartContainer.frame.size.height)
        self.vwFlexStartContainer.addSubview(self.pickerFlexStartDate)
        
        self.pickerFlexEndDate.minimumDate = Date()
        self.pickerFlexEndDate.maximumDate = Date().addingTimeInterval(1*365*60*60*24)
        self.pickerFlexEndDate.datePickerMode = NTMonthYearPickerModeMonthAndYear
        self.pickerFlexEndDate.addTarget(self, action: #selector(self.flexEndPickerValueChanged), for: .valueChanged)
        self.pickerFlexEndDate.frame = CGRect(x:0,y:-30,width:UIScreen.main.bounds.width-48,height:self.vwFlexEndContainer.frame.size.height)
        self.vwFlexEndContainer.addSubview(self.pickerFlexEndDate)
       
        self.handleFixedDateSelection()
        
    }
    @objc func btnToBackClicked()
    {
        if self.isDoneEnabled
        {
            self.doConfirmationForDataSubmission()
        }
        else
        {
         _=self.navigationController?.popViewController(animated: true)
        }
    }
    func doConfirmationForDataSubmission()
    {
        let text = showAlertTitleMessageFont(strTitle: "Confirm", strMessage: "Do you want to save the changes?")
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        alert.setValue(text.title, forKey: "attributedTitle")
        alert.setValue(text.message, forKey: "attributedMessage")
        let yesAction = UIAlertAction.init(title: "Save", style: .default, handler: {
            (sender) in
            self.btnToDoneCLicked()

        })
        let noAction = UIAlertAction.init(title: "Cancel", style: .destructive, handler:
            {
                (sender) in
            _=self.navigationController?.popViewController(animated: true)
                
        })
        alert.addAction(noAction)
        alert.addAction(yesAction)
        
        
        self.present(alert, animated: true, completion: nil)
        
    }

    @objc func btnToDoneCLicked()
    {
        if self.validateSelectedDates() == true
        {
         self.requestToSubmitCalendarData()
        }
        else
        {
         showAlertView(title: "", message: "Invalid dates. Start date should be before the end date.", ref: self)
        }
      
    }
    func setPrefilledDataOnCalendarScreen()
    {

     if self.startDate?.components(separatedBy: " ").count == 3
     {
        checkSelectedSegment = 0
        vwSomeDay.isHidden = true
        imgSomedayBack.image = UIImage(named: "uncheck_btn")
        imgSomeDay.isHidden = true
        
        imgFixedBack.image = UIImage(named: "Rectangle")
        imgFixed.isHidden = false
        
        imgFlexibleBack.image = UIImage(named: "uncheck_btn")
        imgFlexible.isHidden = true
        
        self.pickerStartDate.setDate(convertStringIntoDate(strInput: self.startDate!, format: "dd MMM yyyy"), animated: false)
        self.pickerEndDate.setDate(convertStringIntoDate(strInput: self.endDate!, format: "dd MMM yyyy"), animated: false)
        self.handleFixedDateSelection()
        self.pickerFlexStartDate.date = Date()
        self.pickerFlexEndDate.date = Date().addingTimeInterval(1*30*60*60*24)
     }
     else if self.startDate?.components(separatedBy: " ").count == 2
     {
        checkSelectedSegment = 1
        vwSomeDay.isHidden = true
        imgSomedayBack.image = UIImage(named: "uncheck_btn")
        imgSomeDay.isHidden = true
        
        imgFixedBack.image = UIImage(named: "uncheck_btn")
        imgFixed.isHidden = true
        
        imgFlexibleBack.image = UIImage(named: "Rectangle")
        imgFlexible.isHidden = false
        
        self.pickerFlexStartDate.date = convertStringIntoDate(strInput: self.startDate!, format: "MMM yyyy")
        self.pickerFlexEndDate.date = convertStringIntoDate(strInput: self.endDate!, format: "MMM yyyy")
        self.handleFlexibleDateSelection()
     }
     else
     {
        checkSelectedSegment = 2
        vwSomeDay.isHidden = false
        imgSomedayBack.image = UIImage(named: "Rectangle")
        imgSomeDay.isHidden = false
        
        imgFixedBack.image = UIImage(named: "uncheck_btn")
        imgFixed.isHidden = true
        
        imgFlexibleBack.image = UIImage(named: "uncheck_btn")
        imgFlexible.isHidden = true
        
        self.handleSomedayDateSelection()
        //self.pickerFlexStartDate.preselectedMonth = 0
        self.pickerFlexStartDate.date = Date()
        self.pickerFlexEndDate.date = Date().addingTimeInterval(1*30*60*60*24)
        //self.pickerFlexEndDate.preselectedMonth = 1
     }
        self.lblStartDate.text = self.startDate!
        self.lblEndDate.text = self.endDate!
        self.perform(#selector(self.disableSaveConfirmationPopup), with: nil, afterDelay: 0.01)
    }
    @objc func disableSaveConfirmationPopup()
    {
        self.isDoneEnabled=false
    }
    func validateSelectedDates()->Bool
    {
        switch checkSelectedSegment
        {
        case 1:
            return (self.pickerFlexEndDate.date.timeIntervalSince1970 < self.pickerFlexStartDate.date.timeIntervalSince1970) ? false : true
        case 0:
            return (self.pickerEndDate.date.timeIntervalSince1970 < self.pickerStartDate.date.timeIntervalSince1970) ? false : true
        case 2:
            return true
        default:
            return false
        }
//        return true
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Handle Segment section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func handleFixedDateSelection()
    {
        imgSomedayBack.image = UIImage(named: "uncheck_btn")
        imgSomeDay.isHidden = true
        
        imgFixedBack.image = UIImage(named: "Rectangle")
        imgFixed.isHidden = false
        
        imgFlexibleBack.image = UIImage(named: "uncheck_btn")
        imgFlexible.isHidden = true
        
        self.isDoneEnabled=true
        self.vwFlexStartContainer.isHidden=true
        self.vwFlexEndContainer.isHidden=true
        self.constStartDatePickerH.constant = 162
        self.constEndDatePickerH.constant = 162
        self.lblStartDate.text = convertDateIntoString(date: self.pickerStartDate.date,format:"dd MMM yyyy")
        self.lblEndDate.text = convertDateIntoString(date: self.pickerEndDate.date,format:"dd MMM yyyy")
    }
    func handleFlexibleDateSelection()
    {
        imgSomedayBack.image = UIImage(named: "uncheck_btn")
        imgSomeDay.isHidden = true
        
        imgFixedBack.image = UIImage(named: "uncheck_btn")
        imgFixed.isHidden = true
        
        imgFlexibleBack.image = UIImage(named: "Rectangle")
        imgFlexible.isHidden = false
        
        self.isDoneEnabled=true
        self.vwFlexStartContainer.isHidden=false
        self.vwFlexEndContainer.isHidden=false
        self.constStartDatePickerH.constant = 162
        self.constEndDatePickerH.constant = 162
        //self.pickerFlexStartDate.preselectedMonth = 0
        //self.pickerFlexEndDate.preselectedMonth = 1
        self.lblStartDate.text = convertDateIntoString(date: self.pickerFlexStartDate.date, format: "MMM yyyy")
        
        self.lblEndDate.text = convertDateIntoString(date: self.pickerFlexEndDate.date, format: "MMM yyyy")
    }
    func handleSomedayDateSelection()
    {
        imgSomedayBack.image = UIImage(named: "Rectangle")
        imgSomeDay.isHidden = false
        
        imgFixedBack.image = UIImage(named: "uncheck_btn")
        imgFixed.isHidden = true
        
        imgFlexibleBack.image = UIImage(named: "uncheck_btn")
        imgFlexible.isHidden = true
        
        self.isDoneEnabled=true
        self.vwFlexStartContainer.isHidden=true
        self.vwFlexEndContainer.isHidden=true
        self.constStartDatePickerH.constant = 0
        self.constEndDatePickerH.constant = 0
        self.lblStartDate.text = "Flexible"
        self.lblEndDate.text = "Flexible"
        
    }

    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Picker view Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func startPickerValueChanged(_ sender: UIDatePicker)
    {
        self.isDoneEnabled=true
        self.lblStartDate.text = convertDateIntoString(date: self.pickerStartDate.date,format:"dd MMM yyyy")
    }
    @IBAction func endPickerValueChanged(_ sender: UIDatePicker)
    {
        self.isDoneEnabled=true
        self.lblEndDate.text = convertDateIntoString(date: sender.date,format:"dd MMM yyyy")
    }
    @objc func flexStartPickerValueChanged()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        self.lblStartDate.text = dateFormatter .string(from: self.pickerFlexStartDate.date)
    }
    @objc func flexEndPickerValueChanged()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        self.lblEndDate.text = dateFormatter .string(from: self.pickerFlexEndDate.date)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToSubmitCalendarData()
    {
        KVNProgress.show()
        var startDate = [String]()
        var endDate = [String]()
        var params:[String: Any] = [String: Any]()
        switch self.checkSelectedSegment
        {
        case 2:
            startDate = ["0","0","0"]
            endDate = ["0","0","0"]
            params = ["start_year":"\(startDate[2])","start_month":"\(startDate[1])","start_date":"\(startDate[0])","end_year":"\(endDate[2])","end_month":"\(endDate[1])","end_date":"\(endDate[0])"]
            sentEventPropertyOnCleverTap("Date preferences", _property: ["Flexibility" : "SomeDay"])
        case 1:
            startDate = convertDateIntoString(date: self.pickerFlexStartDate.date, format: "dd MM yyyy").components(separatedBy: " ")
            endDate = convertDateIntoString(date: self.pickerFlexEndDate.date, format: "dd MM yyyy").components(separatedBy: " ")
            params = ["start_year":"\(startDate[2])","start_month":"\(startDate[1])","start_date":"0","end_year":"\(endDate[2])","end_month":"\(endDate[1])","end_date":"0"]
            sentEventPropertyOnCleverTap("Date preferences", _property: ["Flexibility" : "Flexible"])
        case 0:
            startDate = convertDateIntoString(date: self.pickerStartDate.date, format: "dd MM yyyy").components(separatedBy: " ")
            endDate = convertDateIntoString(date: self.pickerEndDate.date, format: "dd MM yyyy").components(separatedBy: " ")
            
            params = ["start_year":"\(startDate[2])","start_month":"\(startDate[1])","start_date":"\(startDate[0])","end_year":"\(endDate[2])","end_month":"\(endDate[1])","end_date":"\(endDate[0])"]
            
            let dayFuture = dateDifferenceInDay(firstDate: self.pickerStartDate.date, secondDate: Date(), dateFormate: "dd MM yyyy")
            let duration = dateDifferenceInDay(firstDate: self.pickerEndDate.date, secondDate: self.pickerStartDate.date, dateFormate: "dd MM yyyy")
            sentEventPropertyOnCleverTap("Date preferences", _property: ["Flexibility" : "Fixed","DaysInFuture":dayFuture,"Duration":duration])
        default:
                break
        }
        
        let request = CYLServices.editCalendarList(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                    let userObject = CYLGlobals.sharedInstance.usrObject!
                    userObject.startDate = self.lblStartDate.text
                    userObject.endDate = self.lblEndDate.text
                    CYLGlobals.sharedInstance.usrObject = userObject
                    let userDeflts = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject)
                    userDeflts.set(encodedData, forKey: "userObject")
                    userDeflts.synchronize()
//                    //KVNProgress.showSuccess()
                        
                        if self.postNotificationRefreshCalender == "profile"
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: nil, userInfo: ["name":""])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: nil, userInfo: ["name":"profile"])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: nil, userInfo: ["name":"profile"])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: nil, userInfo: ["name":"profile"])
                        }
                        else //if self.postNotificationRefreshCalender == "home"
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: ["name":"profile"])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter"), object: ["name":""])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_near"), object: ["name":""])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_list_for_applied_filter_local"), object: nil)
                        }
                        
                    _ = self.navigationController?.popViewController(animated: true)
                    
                    }
                    
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Edit travelling date"])
                    showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                }
                
        },
                                                                  withError:
            {
                (error) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Edit travelling date"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Edit travelling date"])
                showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
    @IBAction func btnFixedFlexibleSomedayChange(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 0:
            self.checkSelectedSegment = 0
            vwSomeDay.isHidden = true
            self.lblStartDatePlaceholder.text = "Start Date"
            self.lblEndDatePlaceholder.text = "End Date"
            self.handleFixedDateSelection()
        case 1:
            self.checkSelectedSegment = 1
            vwSomeDay.isHidden = true
            self.lblStartDatePlaceholder.text = "Start Month"
            self.lblEndDatePlaceholder.text = "End Month"
            self.handleFlexibleDateSelection()
        case 2:
            self.checkSelectedSegment = 2
            vwSomeDay.isHidden = false
            self.lblStartDatePlaceholder.text = "Start Month"
            self.lblEndDatePlaceholder.text = "End Month"
            self.handleSomedayDateSelection()
        default:
            break
        }
        
    }
}


