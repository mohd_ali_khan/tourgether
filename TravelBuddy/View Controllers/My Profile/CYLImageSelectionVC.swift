//
//  CYLImageSelectionVC.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/13/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

protocol changeUserProfileImages: class {
    func changeUesrImagesDeleted(_ arr: [Any]?)
}

class CYLImageSelectionVC: CYLBaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate {
    var selectedIndex:Int = Int()
    var aryImages=[Data]()
    var isDoneEnabled=false
    var strComesController: String = String()
    var aryUserImages = [Any]()
    var delegateDeleteUserProfileImageArray: changeUserProfileImages?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.clear], for: .normal)
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "User images updation")
        self.createSelectImageScreenDesign()
        self.setPrefilledmagesOnScreen()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createSelectImageScreenDesign()
    {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .plain, target: self, action: #selector(self.btnToBackClicked))
        self.title = "Photos"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!] 
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.btnToDoneImagesClicked))
        self.isDoneEnabled=false
    }
    @objc func btnToBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
        //        if self.isDoneEnabled
        //        {
        //            self.doConfirmationForDataSubmission()
        //        }
        //        else
        //        {
        //            _=self.navigationController?.popViewController(animated: true)
        //        }
    }
    //    func doConfirmationForDataSubmission()
    //    {
    //        let alert = UIAlertController.init(title: "Confirm", message: "Do you want to save the changes?", preferredStyle: .alert)
    //        let yesAction = UIAlertAction.init(title: "Save", style: .default, handler: {
    //            (sender) in
    //            self.btnToDoneImagesClicked()
    //
    //        })
    //        let noAction = UIAlertAction.init(title: "Cancel", style: .destructive, handler:
    //            {
    //                (sender) in
    //                _=self.navigationController?.popViewController(animated: true)
    //
    //        })
    //        alert.addAction(noAction)
    //        alert.addAction(yesAction)
    //
    //
    //        self.present(alert, animated: true, completion: nil)
    //
    //    }
    //    func btnToDoneImagesClicked()
    //    {
    //      self.requestToUploadProfileImages()
    //    }
    func setPrefilledmagesOnScreen()
    {
        var aryProfileImg = [Any]()
        if strComesController == ""
        {
            aryProfileImg = (CYLGlobals.sharedInstance.usrObject?.userImages)!
        }
        else
        {
            aryProfileImg = self.aryUserImages
        }
        let count:Int = ((aryProfileImg.count) > 6) ? 6 : (aryProfileImg.count)
        for i in 0..<count
        {
            let imgView = self.view.viewWithTag(i+2001) as! UIImageView
            let pictureObject = aryProfileImg[i] as! [String:AnyObject]
            imgView.sd_setImage(with: URL(string: (pictureObject["picUrl"] as! String)), placeholderImage: UIImage(named: "user_placeholder"))
            
            let add_button = self.view.viewWithTag(i+3001) as! UIButton
            add_button.setImage(UIImage.init(named: "ic_delete_image"), for: .normal)
        }
    }
    func resetAllImages()
    {
        for i in 0..<6
        {
            let imgView = self.view.viewWithTag(i+2001) as! UIImageView
            imgView.image = UIImage.init(named: "ic_camera")
            
            if strComesController == ""
            {
                let add_button = self.view.viewWithTag(i+3001) as! UIButton
                add_button.setImage(UIImage.init(named: "ic_add_image"), for: .normal)
            }
            
            
        }
    }
    func doConfirmationForImageDelete(tag:Int)
    {
        //        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        let alert = UIAlertController.init()
        let yesAction = UIAlertAction.init(title: "Delete Photo", style: .destructive, handler: {
            (sender) in
            let selectedIndex = tag
            var aryProfileImg = [Any]()
            if self.strComesController == ""
            {
                aryProfileImg = (CYLGlobals.sharedInstance.usrObject?.userImages)!
                self.requestToDeleteImageWithObject(object: aryProfileImg[selectedIndex-1])
            }
            else
            {
                aryProfileImg = self.aryUserImages
                self.requestToDeleteImageWithObjectByAdmin(object: aryProfileImg[selectedIndex-1])
            }
            
        })
        let noAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler:
        {
            (sender) in
            
        })
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Custom button action
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    @IBAction func btnToChangeImageClicked(_ sender: UIButton)
    {
        let selectedIndex = sender.tag - 3000
        
        var aryProfileImg = [Any]()
        if self.strComesController == ""
        {
            aryProfileImg = (CYLGlobals.sharedInstance.usrObject?.userImages)!
        }
        else
        {
            aryProfileImg = self.aryUserImages
        }
        if (aryProfileImg.count) > selectedIndex-1
        {
            self.doConfirmationForImageDelete(tag: selectedIndex)
        }
        else
        {
            if self.strComesController == ""
            {
                self.isDoneEnabled=true
                self.selectedIndex = sender.tag
                self.handleUserProfileImagePickup()
            }
        }
        
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Image picker functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func handleUserProfileImagePickup()
    {
        let alertController = UIAlertController.init(title: "", message: "Select Profile image", preferredStyle: .actionSheet)
        
        let takePhoto = UIAlertAction.init(title: "Camera", style: .default, handler:
        { (action) in
            
            let picker = UIImagePickerController.init()
            picker.delegate=self
            picker.allowsEditing=true
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
            alertController.dismiss(animated: true, completion: nil)
            
        })
        alertController.addAction(takePhoto)
        
        let choosePhoto = UIAlertAction.init(title: "Gallery", style: .default, handler:
        { (action) in
            
            let picker = UIImagePickerController.init()
            picker.delegate=self
            picker.allowsEditing=true
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
            alertController.dismiss(animated: true, completion: nil)
            
        })
        alertController.addAction(choosePhoto)
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler:
            nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let add_button = self.view.viewWithTag(selectedIndex) as! UIButton
        add_button.setImage(UIImage.init(named: "ic_delete_image"), for: .normal)
        
        let imgView = self.view.viewWithTag(selectedIndex-1000) as! UIImageView
        let chosenImage = info["UIImagePickerControllerEditedImage"] as! UIImage
        imgView.image = chosenImage
        let imageData = UIImageJPEGRepresentation(chosenImage, 0.2)!
        aryImages.removeAll()
        aryImages.append(imageData)
        self.requestToUploadProfileImages()
        picker.dismiss(animated: true, completion: nil)
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToDeleteImageWithObjectByAdmin(object:Any)
    {
        KVNProgress.show()
        let params:[String: Any] = ["photoId":"\((object as! NSDictionary).value(forKey: "id")!)"]
        let request = CYLServices.deleteUserProfileImage(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse: {
            (response) in
            
            KVNProgress.dismiss()
            if (response?.isValid)!
            {
                let responseData = response?.object?.dictionaryObject
                if responseData?["status"] as! String != "fail"
                {
                    self.aryUserImages.removeAll()
                    self.aryUserImages = (responseData?["data"] as! [Any]?)!
                    self.resetAllImages()
                    self.setPrefilledmagesOnScreen()
                    self.delegateDeleteUserProfileImageArray?.changeUesrImagesDeleted(self.aryUserImages)
                }
                
            }
            else
            {
                sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"User images updation"])
                showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
            }
        }, withError: {
            (error) in
            KVNProgress.dismiss()
            sentEventPropertyOnCleverTap("Backend error", _property: ["view":"User images updation"])
            showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        }, andNetworkErr: {
            (networkFailure) in
            KVNProgress.dismiss()
            showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
            sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"User images updation"])
        })
    }
    
    func requestToDeleteImageWithObject(object:Any)
    {
        KVNProgress.show()
        let params:[String: Any] = ["photoId":"\((object as! NSDictionary).value(forKey: "id")!)"]
        let request = CYLServices.deleteUserProfileImage(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse: {
            (response) in
            
            // //KVNProgress.dismiss()
            if (response?.isValid)!
            {
                let responseData = response?.object?.dictionaryObject
                if responseData?["status"] as! String != "fail"
                {
                    let res: [Any] = ((responseData?["data"] as! [Any]?)?.reversed())!
                    let userObject = CYLGlobals.sharedInstance.usrObject
                    userObject?.userImages = res//responseData?["data"] as! [Any]?
                    CYLGlobals.sharedInstance.usrObject = userObject
                    let userDeflts = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject!)
                    userDeflts.set(encodedData, forKey: "userObject")
                    userDeflts.synchronize()
                    KVNProgress.showSuccess()
                    self.resetAllImages()
                    self.setPrefilledmagesOnScreen()
                }
                else
                {
                    KVNProgress.dismiss()
                }
                
            }
            else
            {
                KVNProgress.dismiss()
                sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"User images updation"])
                showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
            }
        }, withError: {
            (error) in
            KVNProgress.dismiss()
            sentEventPropertyOnCleverTap("Backend error", _property: ["view":"User images updation"])
            showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        }, andNetworkErr: {
            (networkFailure) in
            KVNProgress.dismiss()
            showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
            sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"User images updation"])
        })
    }
    
    func requestToUploadProfileImages()
    {
        KVNProgress.show()
//        let params:[String: Any] = ["userId":"\(CYLGlobals.sharedInstance.usrObject!.id!)"]
        let params:[String: Any] = [:]
        let request = CYLServices.postAllProfileImages(dict:params,aryImages: self.aryImages, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callDataUploadServiceWithRequest(rqst: request, withResponse: {
            (response) in
            
            ////KVNProgress.dismiss()
            if (response?.isValid)!
            {
                let responseData = response?.object?.dictionaryObject
                if responseData?["status"] as! String != "fail"
                {
                    let userObject = CYLGlobals.sharedInstance.usrObject
                    userObject?.userImages = responseData?["data"] as! [Any]?
                    CYLGlobals.sharedInstance.usrObject = userObject
                    let userDeflts = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userObject!)
                    userDeflts.set(encodedData, forKey: "userObject")
                    userDeflts.synchronize()
                    KVNProgress.showSuccess()
                    //_=self.navigationController?.popViewController(animated: true)
                    self.resetAllImages()
                    self.setPrefilledmagesOnScreen()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_profile_screen_data"), object: nil)
                }
            }
            else
            {
                KVNProgress.dismiss()
                showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
            }
        }, withError: {
            (error) in
            KVNProgress.dismiss()
            showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        }, andNetworkErr: {
            (networkFailure) in
            KVNProgress.dismiss()
            showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
        })
    }
}

