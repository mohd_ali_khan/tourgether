//
//  CYLNotificationVC.swift
//  TravelBuddy
//
//  Created by codeyeti on 14/08/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit

class CYLNotificationVC: CYLBaseViewController {
    
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var lblNoNotification: UILabel!
    
    var vwLoader: UIView = UIView()
    var aryNotification = [JSON]()
    var footerView:UIView?
    var lastLoadedPage = 0
    let pageSize = 20
    var maxResults = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loaderGifImage()
        tblNotification.estimatedRowHeight = 80
        tblNotification.rowHeight = UITableViewAutomaticDimension
        self.requestToGetNotificationList(page: lastLoadedPage, str: "first")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.items![2].badgeValue = nil
        createNotificationScreenDesign()
        self.tabBarController?.navigationItem.titleView = nil
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        self.tabBarController?.navigationItem.title = "Notifications"
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Notification list")
        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    @objc func refreshPull(_ refreshControl: UIRefreshControl)
    {
        lastLoadedPage = 0
        self.requestToGetNotificationList(page: 0, str: "first")
        refreshControl.endRefreshing()
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: User Defined functions
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func createNotificationScreenDesign()
    {
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18)!]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshPull(_:)), for: .valueChanged)
        if #available(iOS 10.0, *)
        {
            self.tblNotification.refreshControl = refreshControl
        }
        else
        {
            self.tblNotification.backgroundView = refreshControl
        }
    }
    func loaderGifImage()
    {
        if let _ = UIApplication.shared.keyWindow
        {
            let window = UIApplication.shared.keyWindow!
            vwLoader = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            vwLoader.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
            window.addSubview(vwLoader)
            let img = UIImageView(frame: CGRect(x: vwLoader.frame.width/2-40, y: vwLoader.frame.height/2-40, width: 80, height: 80))
            img.image = UIImage.sd_animatedGIF(with: imageData!)
            vwLoader.addSubview(img)
            vwLoader.isHidden = true
        }
    }
    func setupActivityIndicatorInTableViewFooter()
    {
        self.footerView = UIView.init(frame: CGRect(x:0,y:0,width:self.screenWidth!,height:40))
        
        let actIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        actIndicator.tag = 10
        actIndicator.frame = CGRect(x:0.0, y:0.0, width:20.0, height:20.0)
        actIndicator.center.x = (self.footerView?.center.x)!
        actIndicator.center.y = (self.footerView?.center.y)!
        actIndicator.hidesWhenStopped = true
        self.footerView?.addSubview(actIndicator)
        actIndicator.startAnimating()
        self.tblNotification.tableFooterView = self.footerView
    }
    func showLoadingView()
    {
        if self.footerView == nil || lastLoadedPage == 0
        {
            self.vwLoader.isHidden = false
        }
    }
    func hideLoadingView()
    {
        if self.footerView != nil
        {
            self.tblNotification.tableFooterView = nil
            self.vwLoader.isHidden = true
        }
        else
        {
            self.vwLoader.isHidden = true
        }
    }
    func handlePagingForNotificationTable()
    {
        if self.maxResults > 0
        {
            lastLoadedPage += 1
            self.requestToGetNotificationList(page: lastLoadedPage, str: "")
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToGetNotificationList(page: Int, str: String)
    {
        lastLoadedPage = page
        if str != ""
        {
            self.showLoadingView()
        }
        let params:[String: Any] = ["record":"\(pageSize)","start":"\(page*pageSize)"]
        let request = CYLServices.getNotificationMessageList(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                self.hideLoadingView()
                if (response?.object!.dictionaryValue.count != 0)
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        if str != ""
                        {
                            self.aryNotification.removeAll()
                            self.requestToSeenNotification()
                        }
                        self.maxResults = (responseData?["total_records"]?.intValue)!
                        self.aryNotification.append(contentsOf: (responseData?["data"]?.arrayValue)!)
                        self.tblNotification.reloadData()
                        self.lblNoNotification.isHidden = (self.aryNotification.count == 0) ? false : true
                    }
                }
                else
                {
                    sentEventPropertyOnCleverTap("Server unavailable", _property: ["view":"Notification list"])
                    if str == "" && page == 0
                    {
//                        self.refreshViewSlowOrMantinence(msgStr: "Tourgether is down for maintenance. Please try again later.", imgName: "ic_oops")
                    }
                    else
                    {
                        showAlertView(title: "", message: "Tourgether is down for maintenance. Please try again later.", ref: self)
                    }
                }
                
        },
                                                                  withError:
            {
                (error) in
                self.hideLoadingView()
                sentEventPropertyOnCleverTap("Backend error", _property: ["view":"Notification list"])
                showAlertView(title: "", message: (error?.localizedDescription)!, ref: self)
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
                self.hideLoadingView()
                sentEventPropertyOnCleverTap("Slow connection", _property: ["view":"Notification list"])
                if str == "" && page == 0
                {
//                    self.refreshViewSlowOrMantinence(msgStr: "Slow or no internet connection.", imgName: "ic_no_internet")
                }
                else
                {
                    showAlertView(title: "", message: "Slow or no internet connection.", ref: self)
                }
        })
    }
    func requestToSeenNotification()
    {
        let request = CYLServices.getNotificationMessageSeen(dict:[:], headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                self.hideLoadingView()
                if (response?.object!.dictionaryValue.count != 0)
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                    }
                }
                else
                {
                }

        },
                                                                  withError:
            {
                (error) in
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
        })
    }
}

//MARK: table view data source and delegate functions
extension CYLNotificationVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.aryNotification.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CYLNotificationCell") as! CYLNotificationCell

        cell.object = self.aryNotification[indexPath.row]
        
        cell.selectionStyle = .none
        //*********for paging api call
        if (indexPath.row) == (self.aryNotification.count-1) && self.maxResults > 0
        {
            self.setupActivityIndicatorInTableViewFooter()
            self.handlePagingForNotificationTable()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let travellerId = self.aryNotification[indexPath.row]["userId"].intValue
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CYLUserProfileVC") as! CYLUserProfileVC
        userProfileVC.travellerId = travellerId
        self.navigationController?.pushViewController(userProfileVC, animated: true)

    }
}
