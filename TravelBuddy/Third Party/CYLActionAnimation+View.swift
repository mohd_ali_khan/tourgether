import UIKit

public typealias Closure = () -> Void
public typealias Animation = (UIImageView, @escaping Closure) -> Void

public class Action {
    let animation: Animation
    
    init(animation: @escaping Animation) {
        self.animation = animation
    }
}
public extension Action {
    public static func moveX(_ offset: CGFloat, widht: CGFloat , height: CGFloat) -> Action {
    return Action(animation: { view, completion in
      UIImageView.animate(withDuration: 0.4, animations: {
        view.frame.origin.y += offset
        view.frame.origin.x += offset
        view.frame.size.height += height
        view.frame.size.width += widht
      }, completion: { _ in
        completion()
        
      })
    })
  }
    
    public static func swing() -> Action {
        return .layer(animationMaker: {
            let animation = CAKeyframeAnimation(keyPath: "transform.rotation")
            animation.values = [0, 0.3, -0.3, 0.3, 0]
            animation.keyTimes = [0, 0.2, 0.4, 0.6, 0.8, 1]
            animation.duration = 0.8
            animation.isAdditive = true
            
            return animation
        })
    }
    
    
    public static func layer(animationMaker: @escaping () -> CAAnimation) -> Action {
        return Action(animation: { view, completion in
            CATransaction.begin()
            let animation = animationMaker()
            
            // Callback function
            CATransaction.setCompletionBlock {
                completion()
            }
            
            // Do the actual animation and commit the transaction
            view.layer.add(animation, forKey: "")
            CATransaction.commit()
        })
    }

    
}

public extension UIImageView {
    func run(_ action: Action, completion: @escaping Closure = {})
    {
        action.animation(self, completion)
    }
}
public extension Action {
    public static func sequence(_ actions: [Action]) -> Action {
        func loop(view: UIImageView, actions: [Action], completion: @escaping Closure) {
            guard let first = actions.first else {
                completion()
                return
            }
            
            var removedFirstActions = actions
            _ = removedFirstActions.removeFirst()
            first.animation(view, {
                loop(view: view, actions: removedFirstActions, completion: completion)
            })
        }
        
        return Action(animation: { view, completion in
            loop(view: view, actions: actions, completion: completion)
        })
    }
}


