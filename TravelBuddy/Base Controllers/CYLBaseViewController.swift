//
//  CYLBaseViewController.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/6/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit

class CYLBaseViewController: UIViewController
{

    var screenWidth:CGFloat?
    var screenHeight:CGFloat?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let bounds = UIScreen.main.bounds
        self.screenWidth = bounds.size.width
        self.screenHeight = bounds.size.height
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
