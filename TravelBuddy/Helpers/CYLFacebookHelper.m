//
//  CYLFacebookHelper.m
//  Travel Buddy
//
//  Created by Nikhil Sharma on 2/27/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Facebook Permissions////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
//public_profile
//user_friends
//email
//user_about_me
//user_actions.books
//user_actions.fitness
//user_actions.music
//user_actions.news
//user_actions.video
//user_actions:{app_namespace}
//user_birthday
//user_education_history
//user_events
//user_games_activity
//user_hometown
//user_likes
//user_location
//user_managed_groups
//user_photos
//user_posts
//user_relationships
//user_relationship_details
//user_religion_politics
//user_tagged_places
//user_videos
//user_website
//user_work_history
//read_custom_friendlists
//read_insights
//read_audience_network_insights
//read_page_mailboxes
//manage_pages
//publish_pages
//publish_actions
//rsvp_event
//pages_show_list
//pages_manage_cta
//pages_manage_instant_articles
//ads_read
//ads_management
//business_management
//pages_messaging
//pages_messaging_subscriptions
//pages_messaging_payments
//pages_messaging_phone_number
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
#import "CYLFacebookHelper.h"
#import "KVNProgress.h"

@implementation CYLFacebookHelper
static CYLFacebookHelper* sharedFInstance = nil;

+(CYLFacebookHelper*)sharedFInstance
{
    @synchronized([CYLFacebookHelper class])
    {
        if (!sharedFInstance)
            sharedFInstance = [[self alloc] init];
        
        return sharedFInstance;
    }
    return nil;
}
-(id)init
{
    self.loginManager = [[FBSDKLoginManager alloc] init];
    //self.loginManager .loginBehavior = FBSDKLoginBehaviorBrowser;
    
    return self;
}
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////Login Section////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
-(void)doFacebookLoginWithCompletionHandler:(loginResponse)fbResponse andError:(loginError)fbErr andCancelEvent:(loginCancelled)fbCancel
{
     NSLog(@"%@",fbResponse );
     [KVNProgress show];
    if([FBSDKAccessToken currentAccessToken])
    {
        //[self getLoggedInUserProfileInformation];
        NSLog(@"Logged in");
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"email,id,gender,birthday,first_name,last_name,middle_name,cover,is_verified,picture.type(large),address,location{location{country, country_code, city, city_id, latitude, longitude, region, region_id, state, street, name}}"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             [KVNProgress dismiss];
             if(error==nil)
             {
                 self.tokenString = [[FBSDKAccessToken currentAccessToken] tokenString];
                 NSLog(@"################################fetched user:%@##########################", result);
                 fbResponse(result);
             }
             else
             {
                 NSLog(@"%@",error.localizedDescription);
                 fbErr(error);
             }

         }];
    }
    else
    {
        [KVNProgress dismiss];
        [self.loginManager logInWithReadPermissions: @[@"public_profile",@"email",@"user_photos",@"user_birthday",@"user_gender"]
                                 fromViewController:self.baseViewController
                                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             if (error)
             {
                 NSLog(@"Process error");
                 fbErr(error);
             }
             else if (result.isCancelled)
             {
                 NSLog(@"Cancelled");
                 fbCancel(@"Cancelled");
             }
             else
             {
                
                 NSLog(@"Logged in");
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"email,id,gender,birthday,first_name,middle_name,last_name,cover,education,locale,is_verified,picture.type(large),address"}]
//                  [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"email,id,location{location{country, country_code, city, city_id, latitude, longitude, region, region_id, state, street, name}},gender,birthday,first_name,middle_name,last_name,cover,education,locale,is_verified,picture.type(large),address,photos"}]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                  {
                      if(error==nil)
                      {
                          NSLog(@"################################fetched user:%@##########################", result);
                          
                          self.tokenString = [[FBSDKAccessToken currentAccessToken] tokenString];
                          fbResponse(result);
                      }
                      else
                      {
                          NSLog(@"%@",error.localizedDescription);
                          fbErr(error);
                      }
                      
                  }];
             }
         }];
    }
}
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////Logout Section////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
-(void)doLogoutFromFacebook
{
    [self.loginManager logOut];
}
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////Albums List Section//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
-(void)getAlbumListFromFacebookWithCompletionHandler:(albumResponse)fbResponse andError:(albumError)fbError
{
    NSMutableArray *aryAlbum=[[NSMutableArray alloc] init];
    [[[FBSDKGraphRequest alloc]
      initWithGraphPath:@"me/albums"
      parameters: @{@"fields":@"description,photo_count,link,picture,name"}
      HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             NSArray *aryAlbm = [result valueForKey:@"data"];
             for (NSDictionary *dict in aryAlbm)
             {
                 
                 NSDictionary *dctAlbum = [[NSDictionary alloc]initWithObjectsAndKeys:[dict valueForKey:@"id"],@"album_id",[[[dict objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"],@"album_image",[dict valueForKey:@"name"],@"album_name",[dict valueForKey:@"link"],@"album_link",[dict valueForKey:@"photo_count"],@"album_image_count",[dict valueForKey:@"created_time"],@"created_time", nil];
                 
                 [aryAlbum addObject:dctAlbum];
             }
             fbResponse(aryAlbum);
             NSLog(@"$$$$$$$$$$$$$$$$$$$$ Fetched User Album \n %@ \n$$$$$$$$$$$$$$$$$$$$$$$$$$$",aryAlbum);
         }
         else
         {
             NSLog(@"---------------------------error fetching album array = %@------------------------",error.description);
             fbError(error);
         }
     }];
}
-(void)getAlbumImagesListFromFacebookWithCompletionHandler:(albumResponse)fbResponse andError:(albumError)fbError
{
    NSMutableArray *aryAlbum=[[NSMutableArray alloc] init];
    [[[FBSDKGraphRequest alloc]
      initWithGraphPath:@"me"
      parameters: @{@"fields":@"id,name,albums{photos{picture,webp_images,images},name}"}
      HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             NSArray *aryAlbm = [[result valueForKey:@"albums"]valueForKey:@"data"];
             for (NSDictionary *dict in aryAlbm)
             {
                 if([[[dict valueForKey:@"name"] lowercaseString]isEqualToString:@"profile pictures"])
                 {
                     NSArray *aryImages = [[dict valueForKey:@"photos"] valueForKey:@"data"];
                     for(NSDictionary *object in aryImages)
                     {
                         NSString *strImageUrl = [[[object valueForKey:@"images"] objectAtIndex:0] valueForKey:@"source"];
                         [aryAlbum addObject:strImageUrl];
                     }
                 }
                 
                 
             }
             fbResponse(aryAlbum);
             NSLog(@"$$$$$$$$$$$$$$$$$$$$ Fetched User Album \n %@ \n$$$$$$$$$$$$$$$$$$$$$$$$$$$",aryAlbum);
         }
         else
         {
             NSLog(@"---------------------------error fetching album array = %@------------------------",error.description);
             fbError(error);
         }
     }];
}


////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Album Images Section///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
-(void)getImageListFromFacebookFor:(NSString *)strAlbumId completionHandler:(albumResponse)fbResponse andError:(albumError)fbError
{
    NSMutableArray *aryImages=[[NSMutableArray alloc] init];
    [[[FBSDKGraphRequest alloc]
      initWithGraphPath:@"/%@/photos"
      parameters: @{@"fields":@"picture,created_time,name,likes{name,pic_square},place,comments"}
      HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             NSArray *aryImg = [result valueForKey:@"data"];
             for (NSDictionary *dict in aryImg)
             {
                 NSArray *aryLikes= [[NSArray alloc] init];
                 if([dict valueForKey:@"likes"])
                 {
                     //when post have likes
                     aryLikes = [[[dict valueForKey:@"likes"] valueForKey:@"data"] mutableCopy];
                 }
                 NSArray *aryComments = [[NSArray alloc] init];
                 if([dict valueForKey:@"comments"])
                 {
                     //when post have comments
                     aryComments = [[[dict valueForKey:@"comments"] valueForKey:@"data"] mutableCopy];
                 }
                 NSDictionary *dictPlace = [[NSDictionary alloc] init];
                 if([dict valueForKey:@"place"])
                 {
                     //when post have location
                     dictPlace = [dict valueForKey:@"place"];
                 }
                 
                 NSDictionary *dctAlbum = [[NSDictionary alloc]initWithObjectsAndKeys:[dict valueForKey:@"id"],@"image_id",[dict objectForKey:@"picture"] ,@"image_url",[dict valueForKey:@"created_time"],@"created_time",aryLikes,@"image_likes",aryComments,@"image_comments",dictPlace,@"image_places", nil];
                 
                 [aryImages addObject:dctAlbum];
             }
             NSLog(@"%@",aryImages);
             fbResponse(aryImages);
         }
         else
         {
             NSLog(@"---------------------------error fetching images for selected id= %@ %@------------------------",error.description,strAlbumId);
             fbError(error);
         }
     }];
}
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Friend List Section///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
-(void)getUserFriendListWithCompletionHandler:(friendsResponse)fbResponse andError:(friendsError)fbError
{
    NSMutableArray *aryFriends=[[NSMutableArray alloc] init];
    [[[FBSDKGraphRequest alloc]
      initWithGraphPath:@"me/friends"
      parameters: @{@"fields":@"name,picture,email,cover,gender"}
      HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             NSArray *aryFrnds = [result valueForKey:@"data"];
             for (NSDictionary *dict in aryFrnds)
             {
                 NSDictionary *dctFriend = [[NSDictionary alloc]initWithObjectsAndKeys:[dict valueForKey:@"id"],@"friend_id",[dict objectForKey:@"name"] ,@"friend_name",[dict valueForKey:@"gender"],@"friend_gender",[[[dict valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"],@"friend_image",[[dict valueForKey:@"cover"]valueForKey:@"source"],@"friend_cover", nil];
                 
                 [aryFriends addObject:dctFriend];
             }
             NSLog(@"%@",aryFriends);
             fbResponse(aryFriends);
         }
         else
         {
             NSLog(@"---------------------------error fetching friend list %@------------------------",error.description);
             fbError(error);
         }
     }];
    
}
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////User Feeds Section////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
//https://developers.facebook.com/docs/graph-api/reference/v2.8/post
//me/posts
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Share Kit Section///////// ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
-(void)shareContentUsingFacebook
{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:@"http://developers.facebook.com"];
    [FBSDKShareDialog showFromViewController:self.baseViewController withContent:content delegate:nil];
}
@end

