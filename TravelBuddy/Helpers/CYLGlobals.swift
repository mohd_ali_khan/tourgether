//
//  CYLGlobals.swift
//  IndiaNews
//
//  Created by CodeYeti on 1/30/17.
//  Copyright © 2017 Codeyeti Labs. All rights reserved.
//

import UIKit
import Foundation

class CYLGlobals: NSObject
{
    var themeBaseColor:UIColor?
    var themeColorGreen1:UIColor?
    var themeColorGreen2:UIColor?
    var themeColorPink1:UIColor?
    var themeColorPink2:UIColor?
    var dictLanguage:[String:String]?
    var selectedCategory:Int=00
    var selectedLanguageId:Int?
    var selectedStateId:Int?
    var selectedCityId:Int?
    var usrObject:CYLUserObject?
    var isFilterApplied:Bool?
    var aboutMeV2:[String: JSON]?
    
    static let sharedInstance : CYLGlobals =
    {
        let instance = CYLGlobals()
        return instance
    }()
}
