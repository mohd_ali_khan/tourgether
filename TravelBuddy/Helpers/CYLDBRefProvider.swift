//
//  CYLDBRefProvider.swift
//  TravelBuddy
//
//  Created by CodeYeti on 8/30/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import Foundation
import FirebaseDatabase

class CYLDBRefProvider
{
    static let sharedInstance = CYLDBRefProvider()
    private init(){}
    
    var dbRef: DatabaseReference
    {
        return Database.database().reference()
    }
    var userReference:DatabaseReference
    {
        return dbRef.child(FIR_DB_USERS)
    }
    var chatListReference:DatabaseReference
    {
        return dbRef.child(FIR_DB_CHAT_LIST)
    }
    var chatDetailReference:DatabaseReference
    {
        return dbRef.child(FIR_DB_CHAT_DETAIL)
    }
    var chatListReferenceAdmin:DatabaseReference
    {
        return dbRef.child(FIR_DB_CHAT_LIST_ADMIN)
    }
    
}
