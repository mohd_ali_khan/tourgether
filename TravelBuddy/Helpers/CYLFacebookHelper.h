//
//  CYLFacebookHelper.h
//  Travel Buddy
//
//  Created by CodeYeti on 2/27/17.
//  Copyright © 2017 Codeyeti Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface CYLFacebookHelper : NSObject

typedef void(^loginResponse)(NSDictionary *);
typedef void(^loginError)(NSError *);
typedef void(^loginCancelled)(NSString *);

typedef void(^albumResponse)(NSArray *);
typedef void(^albumError)(NSError *);

typedef void(^friendsResponse)(NSArray *);
typedef void(^friendsError)(NSError *);
@property (nonatomic,strong) NSString *tokenString;
@property(nonatomic,strong)FBSDKLoginManager *loginManager;
@property(nonatomic,strong)UIViewController *baseViewController;
+(CYLFacebookHelper*)sharedFInstance;
-(void)doFacebookLoginWithCompletionHandler:(loginResponse)fbResponse andError:(loginError)fbErr andCancelEvent:(loginCancelled)fbCancel;
-(void)doLogoutFromFacebook;
-(void)getAlbumListFromFacebookWithCompletionHandler:(albumResponse)fbResponse andError:(albumError)fbError;
-(void)getAlbumImagesListFromFacebookWithCompletionHandler:(albumResponse)fbResponse andError:(albumError)fbError;
-(void)getImageListFromFacebookFor:(NSString *)strAlbumId completionHandler:(albumResponse)fbResponse andError:(albumError)fbError;
-(void)getUserFriendListWithCompletionHandler:(friendsResponse)fbResponse andError:(friendsError)fbError;
-(void)shareContentUsingFacebook;
@end
