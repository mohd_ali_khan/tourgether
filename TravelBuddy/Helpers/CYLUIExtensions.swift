//
//  CYLUIExtensions.swift
//  IndiaNews
//
//  Created by CodeYeti on 1/30/17.
//  Copyright © 2017 Codeyeti Labs. All rights reserved.
//

import UIKit

class CYLUIExtensions: NSObject
{

}
//ui color extension
extension UIColor
{
    convenience init(red: Int, green: Int, blue: Int)
    {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hexString:Int)
    {
        self.init(red:(hexString >> 16) & 0xff, green:(hexString >> 8) & 0xff, blue:hexString & 0xff)
    }
}
extension String
{
    func localized(withComment:String) -> String
    {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: withComment)
    }
    var localized: String
    {
        return CYLGlobals.sharedInstance.dictLanguage![self]!
    }
    var alphabaticalOrderNumber:UInt32?
    {
       let array = unicodeScalars.filter{$0.isASCII}.map{$0.value}
       return array[0]-65
    }
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:width, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = self
        label.sizeToFit()
        return label.frame.height
    }
}
extension UITextField
{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
}
extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}
extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
