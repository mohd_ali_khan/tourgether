//
//  CYLHelpers.swift
//  IndiaNews
//
//  Created by CodeYeti on 1/30/17.
//  Copyright © 2017 Codeyeti Labs. All rights reserved.
//

import UIKit

func showAlertViewPayment(title: String, message: String, ref:UIViewController)
{
    let alertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
    let titleFont = [NSAttributedStringKey.font : UIFont(name: "Comfortaa-Bold", size: 18.0)!]
    let messageFont = [NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 12.0)!]
    let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
    let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
    
    alertController.setValue(titleAttrString, forKey: "attributedTitle")
    alertController.setValue(messageAttrString, forKey: "attributedMessage")
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        //ref.dismiss(animated: true, completion: nil)
    }
    alertController.addAction(okAction)
    ref.present(alertController, animated: true, completion: nil)
}

func showAlertView(title: String, message: String, ref:UIViewController)
{
    let alertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
    let titleFont = [NSAttributedStringKey.font : UIFont(name: "Comfortaa-Bold", size: 18.0)!]
    let messageFont = [NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 12.0)!]
    
    let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
    let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
    
    alertController.setValue(titleAttrString, forKey: "attributedTitle")
    alertController.setValue(messageAttrString, forKey: "attributedMessage")
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        //ref.dismiss(animated: true, completion: nil)
    }
    alertController.addAction(okAction)
    ref.present(alertController, animated: true, completion: nil)
}
func showAlertTitleMessageFont(strTitle: String, strMessage: String) -> (title: NSAttributedString, message: NSAttributedString)
{
    let titleFont = [NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18.0)!]
    let messageFont = [NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 12.0)!]
    
    let titleAttrString = NSMutableAttributedString(string: strTitle, attributes: titleFont)
    let messageAttrString = NSMutableAttributedString(string: strMessage, attributes: messageFont)
    
    return (titleAttrString , messageAttrString)
}
func btnRefreshAction(sender: UIButton)
{
}

// To check email is valid or not
func isValidEmail(testStr:String) -> Bool
{
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let range = testStr.range(of: emailRegEx, options: .regularExpression, range: nil, locale: nil)
    let result = range != nil ? true : false
    return result
}
func isValidatePhone(value: String) -> Bool {
    
    let numberRegEx  = ".*[0-9]+.*"
    let testCase = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
    let containsNumber = testCase.evaluate(with: value)
    return containsNumber
}
func convertTimestampIntoDateString(date:Double,format:String) -> String
{
    let date = NSDate(timeIntervalSince1970: date/1000.0)
    let dayTimePeriodFormatter = DateFormatter()
    dayTimePeriodFormatter.dateFormat = format
    let dateString = dayTimePeriodFormatter.string(from: date as Date)
    return dateString
}
func convertDateIntoString(date:Date,format:String) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    let dateString = dateFormatter.string(from: date as Date)
    return dateString
}
func convertStringIntoDate(strInput:String,format:String) -> Date
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    if dateFormatter.date(from: strInput) != nil
    {
        let date = dateFormatter.date(from: strInput)
        return date!
    }
    else
    {
        let strDate = dateFormatter.string(from: Date())
        let date = dateFormatter.date(from: strDate)
        return date!
    }
}

func dayDifferenceFromDateDayTimeString(_ interval : String) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(identifier: "UTC")
    
    guard dateFormatter.date(from: interval) != nil else {
        return ""
    }
    let calendar = NSCalendar.current
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(identifier: "UTC")
    let date: Date = dateFormatter.date(from: interval) ?? Date()
    
    if calendar.isDateInYesterday(date) { return "Yesterday" }
    else
    if calendar.isDateInToday(date)
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "h:mm aa"
        dayTimePeriodFormatter.timeZone = TimeZone.ReferenceType.local
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    else
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM yyyy"
        dayTimePeriodFormatter.timeZone = TimeZone.ReferenceType.local
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
}


func dayDifferenceFromDateDayTime(_ interval : TimeInterval) -> String
{
    let calendar = NSCalendar.current
    let date = Date(timeIntervalSince1970: interval/1000)
    if calendar.isDateInYesterday(date) { return "Yesterday" }
    else if calendar.isDateInToday(date) { return convertTimestampIntoDateString(date: Double(interval), format: "h:mm aa") }
    else {return convertTimestampIntoDateString(date: Double(interval), format: "dd MMM yyyy")}
}

func dayDifferenceFromDate(_ interval : TimeInterval) -> String
{
    let calendar = NSCalendar.current
    let date = Date(timeIntervalSince1970: interval/1000)
    if calendar.isDateInYesterday(date) { return "Yesterday" }
    else if calendar.isDateInToday(date) { return "Today" }
    else {return convertTimestampIntoDateString(date: Double(interval), format: "dd/MM/yyyy")}
}

class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}




