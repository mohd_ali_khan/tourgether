//
//  CYLCommonFunctions.swift
//  TravelBuddy
//
//  Created by CodeYeti on 4/7/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

func filterResultsInArray(aryInput:[JSON],key:String,keySecond:String,searchWord:String)->[JSON]
{
    var aryOutput = [JSON]()
    for object in aryInput
    {
        if object[key].stringValue.lowercased() .contains(searchWord.lowercased()) == true || object[keySecond].stringValue.lowercased() .contains(searchWord.lowercased())
        {
            aryOutput.append(object)
        }
    }
    return aryOutput
}
func getCommaSaperatedStringFrom(aryInput:[JSON],onTheBaisOf key:String)->String
{
    var aryOutput = [String]()
    for object in aryInput
    {
        aryOutput.append(object[key].stringValue)
    }
    return aryOutput.joined(separator: ",")
}

func sentGoogleAnalyticAndCleverTapVisitCV(strCVName: String)
{
    CleverTap.sharedInstance()?.recordEvent(strCVName)
}
func sentEventPropertyOnCleverTap(_ eventName: String, _property:[String: Any])
{
    CleverTap.sharedInstance()?.recordEvent(eventName, withProps: _property)
}

func checkInternetConnection() -> Bool
{
    return NetworkReachabilityManager()!.isReachable
}
func dateDifferenceInDay(firstDate: Date, secondDate: Date , dateFormate: String) -> Int
{
    let previousDate = firstDate
    let currentDate = secondDate
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = dateFormate
    let difference = previousDate.timeIntervalSince(currentDate)
    return Int(difference/(60 * 60 * 24 ))
}

func widthForInputString(text:String) -> CGFloat
{
    let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:CGFloat.greatestFiniteMagnitude, height:CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 1
    label.font = UIFont(name: "Comfortaa-Bold", size: 12)!
    label.text = text
    label.sizeToFit()
    return label.frame.width
}

func getMilliesFromDateString(strInput:String,formate: String)->String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = formate
    let date = dateFormatter.date(from: strInput)
    return "\(Int64((date!.timeIntervalSince1970 * 1000.0).rounded()))"
}
func getFromDate(strInput:String,formate: String)->String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = formate
    let date = dateFormatter.date(from: strInput)
    dateFormatter.dateFormat = "ddMMyyyy"
    let dateReturn = dateFormatter.string(from: date!)
    return dateReturn
}
func getDateOfBirthForCleverTap(strInput: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy"
    let date = dateFormatter.date(from: strInput)
    return date!
}

