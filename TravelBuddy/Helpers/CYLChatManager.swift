//
//  CYLChatManager.swift
//  TravelBuddy
//
//  Created by CodeYeti on 8/30/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import Foundation
import Firebase

protocol CYLChatListDelegate: class
{
    func delegateGetChatList(_ recentChat:CYLChatListObject)
    func delegateUpdateUserModelInChatList(_ recentChat:CYLChatListObject)
    
    
    func delegateGetChatListArray(_ aryResult:[CYLChatListObject])
    func delegateGetSentChatListArray(_ aryResult:[CYLChatListObject])
    func delegateFailedSendChatListArray()
    func delegateGetReceivedChatListArray(_ aryResult:[CYLChatListObject])
    func delegateFailedReceivedChatListArray()
}
protocol CYLChatDetailDelegate: class
{
    func delegateGetChatDetailFirstCome(_ recentChat:[AnyObject])
    func delegateGetChatDetail(_ recentChat:CYLMessageObject)
    func delegateGetChatDetailPaging(_ recentChat:CYLMessageObject, _ lastIndex: Int)
    func delegateFailedChatDetail()
}
protocol CYLUnreadCountDelegate: class
{
    func delegateGetTotalUnreadCount(_ unreadCount:String)
}


class CYLChatManager
{
    var currentUserId = "usr_id_\(CYLGlobals.sharedInstance.usrObject!.id!)"
    static let sharedInstance = CYLChatManager()
    private init(){ }
    //MARK: *******************Chat List Module ***************************
    weak var delegateChatList: CYLChatListDelegate?
    var keyValueMessage = String()
    var ChatLoadFirstTime: Bool = Bool()
    
    //MARK: ************ Get User Sent List ***************
    func getUserSentChatListArray()
    {
        let dispatchGroup = DispatchGroup()
        CYLDBRefProvider.sharedInstance.chatListReference.child(currentUserId).child("sender").observe(.value, with:
            {(snapshot) -> Void in
                 print(snapshot.value!)
                var aryChatList = [CYLChatListObject]()
                if let dict = snapshot.value as? NSDictionary
                {
                    var aryChat = dict.allValues as [AnyObject]
                    aryChat = (aryChat as NSArray).sortedArray(using: [NSSortDescriptor(key: "last_message_time", ascending: false)]) as [AnyObject]
//                    print(aryChat)
                    for chatObj in aryChat
                    {
                        if(chatObj["other_user_id"]! != nil)
                        {
                            dispatchGroup.enter()
                            CYLDBRefProvider.sharedInstance.userReference.child("usr_id_\(chatObj["other_user_id"] as! Int)").observeSingleEvent(of:.value, with:
                                { (snapshot) -> Void in
                                    
                                    if snapshot.exists()
                                    {
//                                        print(snapshot.value!)
                                        guard let dict = snapshot.value as? NSDictionary
                                            else    {
                                                print("return Nil")
                                                return
                                                
                                        }
                                        var object = CYLChatListObject.init(chatObj)
                                        let dic = dict["model"] as! NSDictionary
                                        object.otherUserName = dic["name"] as! String
                                        object.otherUserImageUrl = (dic["img_url"] as? String != nil) ? dic["img_url"] as! String : ""
                                        aryChatList.append(object)
                                        dispatchGroup.leave()
                                    }
                                    else
                                    {
                                        dispatchGroup.leave()
                                    }
                                    
                            })
                        }
                    }
                    
                    dispatchGroup.notify(queue: DispatchQueue.main, execute:
                        {
                            //print("complete ho gaya")
//                            print(aryChatList)
                            self.delegateChatList?.delegateGetSentChatListArray(aryChatList)
                    })
                }
                else
                {
                    self.delegateChatList?.delegateGetSentChatListArray(aryChatList)
//                    self.delegateChatList?.delegateFailedSendChatListArray()
                }
        })
        
    }
    
    //MARK: ************ Get User Receiver List ***************
    func getUserReceivedChatListArray()
    {
        let dispatchGroup = DispatchGroup()
        CYLDBRefProvider.sharedInstance.chatListReference.child(currentUserId).child("reciever").observe(.value, with:
            {(snapshot) -> Void in
                
                var aryChatList = [CYLChatListObject]()
                if let dict = snapshot.value as? NSDictionary
                {
                    print(dict)
//                    aryChatList.removeAll()
                    var aryChat = dict.allValues as [AnyObject]
                    aryChat = (aryChat as NSArray).sortedArray(using: [NSSortDescriptor(key: "last_message_time", ascending: false)]) as [AnyObject]
//                    print(aryChat)
                    
                    for chatObj in aryChat
                    {
                        if(chatObj["other_user_id"]! != nil)
                        {
                            dispatchGroup.enter()
                            CYLDBRefProvider.sharedInstance.userReference.child("usr_id_\(chatObj["other_user_id"] as! Int)").observeSingleEvent(of:.value, with:
                                { (snapshot) -> Void in
                                    print(snapshot.value!)
                                    if snapshot.exists()
                                    {
                                        guard let dict = snapshot.value as? NSDictionary
                                            else    {return}
                                        var object = CYLChatListObject.init(chatObj)
                                        let dic = dict["model"] as! NSDictionary
                                        object.customerType = (dic["customer_type"] != nil) ? dic["customer_type"] as! String : "FREE"
                                        object.otherUserImageUrl = (dic["img_url"] as? String != nil) ? dic["img_url"] as! String : ""
                                        object.otherUserName = dic["name"] as! String
                                        aryChatList.append(object)
                                        dispatchGroup.leave()
                                    }
                                    else
                                    {
                                        dispatchGroup.leave()
                                    }
                                    
                            })
                        }
                    }
                    
                    dispatchGroup.notify(queue: DispatchQueue.main, execute:
                        {
                            //print("complete ho gaya")
                            print(aryChatList)
                            self.delegateChatList?.delegateGetReceivedChatListArray(aryChatList)
                    })
                }
                else
                {
                    self.delegateChatList?.delegateGetReceivedChatListArray(aryChatList)
//                    self.delegateChatList?.delegateFailedReceivedChatListArray()
                }
        })
        
        
    }
    
    func removeAllChatListObserversForUser(_ userId:String)
    {
//        CYLDBRefProvider.sharedInstance.chatListReference.child(userId).child("sender").removeAllObservers()
        CYLDBRefProvider.sharedInstance.chatListReference.removeAllObservers()//child(userId).child("reciever").removeAllObservers()
        CYLDBRefProvider.sharedInstance.userReference.removeAllObservers()
//        CYLDBRefProvider.sharedInstance.userReference.child(userId).removeAllObservers()
    }
    //MARK: *******************Chat Detail Module ***************************
    weak var delegateChatDetail: CYLChatDetailDelegate?
    
    //MARK: ******************Single User Paging Chat Detail *******************
    func getUserChatsMessageForChatIdPaging(_ chatId: String, lastMessageTime : Int)
    {
        CYLDBRefProvider.sharedInstance.chatDetailReference.child(chatId).queryOrderedByKey().queryEnding(atValue: keyValueMessage).queryLimited(toLast: 51).observeSingleEvent(of: .value, with:
            {(snapshot) -> Void in
                if snapshot.exists()
                {
//                    print(snapshot.value!)
                    guard let dict = snapshot.value as? NSDictionary
                        else    {return}
                    var aryChat = dict.allValues as [AnyObject]
                    aryChat = (aryChat as NSArray).sortedArray(using: [NSSortDescriptor(key: "sent_time", ascending: false)]) as [AnyObject]
                    
                    let _ = dict.contains { (key, value) -> Bool in
                        
                        if value as? NSDictionary == aryChat.last as? NSDictionary
                        {
                            self.keyValueMessage = key as! String
                        }
                        return false
                    }
                    print(aryChat)
                    for chatObj in aryChat
                    {
                        if chatObj["sent_time"] as! Int != lastMessageTime
                        {
                            let object = CYLMessageObject.init(object: chatObj)
                            self.delegateChatDetail?.delegateGetChatDetailPaging(object, aryChat.count)
                        }
                        else if aryChat.count == 1
                        {
                            self.delegateChatDetail?.delegateFailedChatDetail()
                            KVNProgress.dismiss()
                            return
                        }
                    }
                }
                else
                {
                    self.delegateChatDetail?.delegateFailedChatDetail()
                }
                
        })
    }
    
    func getUserChatsMessageForChatIdPagingFirst(_ chatId: String, vwLoader: UIView)
    {
        CYLDBRefProvider.sharedInstance.chatDetailReference.child(chatId).queryOrderedByKey().queryLimited(toLast: 50).observeSingleEvent(of: .value, with:
            {(snapshot) -> Void in
                if snapshot.exists()
                {
//                    print(snapshot.value!)
                    guard let dict = snapshot.value as? NSDictionary
                        else    {return}
                    var aryChat = dict.allValues as [AnyObject]
                    aryChat = (aryChat as NSArray).sortedArray(using: [NSSortDescriptor(key: "sent_time", ascending: true)]) as [AnyObject]
                    print(aryChat)
                    let _ = dict.contains { (key, value) -> Bool in
                        
                        if value as? NSDictionary == aryChat.first as? NSDictionary
                        {
                            self.keyValueMessage = key as! String
                        }
                        return false
                    }
                    self.ChatLoadFirstTime = false
                    self.delegateChatDetail?.delegateGetChatDetailFirstCome(aryChat)
                    self.getUserChatsMessagesForChatId(chatId)
                }
                else
                {
                    self.ChatLoadFirstTime = true
                    self.getUserChatsMessagesForChatId(chatId)
                    self.delegateChatDetail?.delegateFailedChatDetail()
                }
                
        })
    }
    
    //MARK: *******************Single User Chat Detail  ***************************
    func getUserChatsMessagesForChatId(_ chatId:String)
    {
        CYLDBRefProvider.sharedInstance.chatDetailReference.child(chatId).queryOrderedByKey().queryLimited(toLast: 1).observe(.childAdded, with: { (snapshot) in
            
            print(snapshot)
            
            if self.ChatLoadFirstTime == false
            {
                self.ChatLoadFirstTime = true
            }
            else
            {
                if let dict = snapshot.value as? NSDictionary
                {
                    let object = CYLMessageObject.init(object: dict)
                    self.delegateChatDetail?.delegateGetChatDetail(object)
                }
                else
                {
                    self.delegateChatDetail?.delegateFailedChatDetail()
                }
            }
            print("key value: \(self.keyValueMessage)")
            
        })
        
    }
    func removeAllChatMessageObserversForChatId(_ chatId:String)
    {
        CYLDBRefProvider.sharedInstance.chatDetailReference.child(chatId).removeAllObservers()
    }
  
    
    //MAark: ********************Single Uesr Unread Module ****************************
    func resetUnreadCountForChatIdNew(_ chatId:String,_ senderType:String, vwLoader: UIView)
    {
        CYLDBRefProvider.sharedInstance.chatListReference.child(currentUserId).child("reciever").child(chatId).child("unread_count").setValue(0)
        { (error, dbRef) in
            if error == nil
            {
//                vwLoader.isHidden = true
                //KVNProgress.dismiss()
            }
            else
            {
//                vwLoader.isHidden = true
                //KVNProgress.dismiss()
            }
        }
    }
    
    //MARK: *******************Total Chat Bagde Read Module ***************************
    weak var delegateTotalUnread: CYLUnreadCountDelegate?
    func getTotalChatsUnreadCountBadge()
    {
        CYLDBRefProvider.sharedInstance.chatListReference.child(self.currentUserId).child("reciever").observe( .value, with: { (snapshot) in
                   
//                    print(snapshot.value!)
                    guard let dict = snapshot.value as? NSDictionary
                        else    {return}
                    let aryChat = dict.allValues as [AnyObject]
                    var unreadCountBadge: Int = Int()
                    for chatObj in aryChat
                    {
                        unreadCountBadge = (chatObj["unread_count"] as! Int) > 0 ? unreadCountBadge+1 : unreadCountBadge
                    }
                    self.delegateTotalUnread?.delegateGetTotalUnreadCount("\(unreadCountBadge)")
                })
    }
    
}

