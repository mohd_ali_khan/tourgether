//
//  CYLConfig.swift
//  TravelBuddy
//
//  Created by CodeYeti on 7/6/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import Foundation

let themeColorBlue = UIColor(red: 255.0/255.0, green: 67/255, blue: 81/255, alpha: 1)
let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "loader_gif", withExtension: "gif")!)

let FIR_DB_BASE_URL = "https://tourgether-770e2.firebaseio.com/"
let FIR_DB_USERS = "Users"
let FIR_DB_CHAT_LIST = "Chats"
let FIR_DB_CHAT_DETAIL = "Messages"
let FIR_DB_CHAT_UNREAD = "unread_messages"
let FIR_DB_USER_BLOCKED = "block_users"
let FIR_DB_CHAT_LIST_ADMIN = "Monitoring"

