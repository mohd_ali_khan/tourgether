//
//  CYLServiceMaster
//  IndiaNews
//
//  Created by Nikhil Sharma on 1/13/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

typealias CYLResponse=(_ serviceResponse:CYLResponseObject?)->Void
typealias CYLNetworkError=(_ isNetworkFailure:Bool?)->Void
typealias CYLError=(_ error:NSError?)->Void



class CYLServiceMaster: NSObject
{
    
    class var sharedInstance:CYLServiceMaster
    {
        struct Values
        {
            static var onceToken=CYLServiceMaster()
            static var instance:CYLServiceMaster? = CYLServiceMaster()
        }
        return Values.instance!
    }
    
    func callWebServiceWithRequest(rqst:CYLRequestObject, withResponse completion:@escaping CYLResponse, withError error:@escaping CYLError, andNetworkErr ntwrkErr:@escaping CYLNetworkError)
    {
        var serviceMethod:HTTPMethod?
        
        print("\n\n ***************************\n ########################### \n *************************** \n")
        print("Request URL is=\(rqst.serviceURL)\n\n")
        self.printPostBodyForRequest(rqst: rqst)
        
        let serviceRequest:NSMutableURLRequest=NSMutableURLRequest()
        
        rqst.setHeaders(request: serviceRequest)
        if rqst.serviceMethod == ServiceMethod.METHOD_GET
        {
            serviceMethod = HTTPMethod.get
        }
        else if rqst.serviceMethod == ServiceMethod.METHOD_POST
        {
              serviceMethod = HTTPMethod.post
        }
        else if rqst.serviceMethod == ServiceMethod.METHOD_POST_JSON
        {
              serviceMethod = HTTPMethod.post
        }
        else if rqst.serviceMethod == ServiceMethod.METHOD_PUT
        {
             serviceMethod = HTTPMethod.put
        }
        else if(rqst.serviceMethod ==  ServiceMethod.METHOD_DELETE)
        {
            serviceMethod = HTTPMethod.delete
        }
        Alamofire.request(rqst.serviceURL, method: serviceMethod!, parameters: rqst.paramDictionary , headers: rqst.headerDictionary).responseData
                {
                    response in
                        let responseObj:CYLResponseObject=CYLResponseObject()
                        if response.result.error?._code == nil
                        {
                        //response is valid
                        let jsonResponse = JSON(data: response.data!)
                        responseObj.object = jsonResponse
                        responseObj.statusCode = response.response!.statusCode
                        responseObj.stringValue = jsonResponse.description
                        responseObj.headerFilds = response.response?.allHeaderFields as AnyObject?
                        responseObj.requestNotification = rqst.notificationName
                        responseObj.error=response.result.error as NSError?

                            print("Response Body is")
                            print("***************************\n")
                            print(responseObj.stringValue!)
                            print("***************************")
                            print("\n\n ***************************\n ########################### \n *************************** \n")
                            if responseObj.statusCode != 200 && responseObj.statusCode != 201
                            {
                                responseObj.isValid=false
                            }
                            else
                            {
                                responseObj.isValid=true
                            }
                            if responseObj.error != nil
                            {
                                // do code to return connection error
                                error(responseObj.error as NSError?)
                            }
                            else
                            {
                                // do code to success response
                                completion(responseObj)
                            }
                        }
                        else
                        {
                              ntwrkErr(true)
                        }
                    
        }
        
        }
    
       func callDataUploadServiceWithRequest(rqst:CYLRequestObject, withResponse completion:@escaping CYLResponse, withError error:@escaping CYLError, andNetworkErr ntwrkErr:@escaping CYLNetworkError)
      {
        print("\n\n ***************************\n ########################### \n *************************** \n")
        print("Request URL is=\(rqst.serviceURL)\n\n")
        self.printPostBodyForRequest(rqst: rqst)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, val) in rqst.paramDictionary {
                multipartFormData.append((val as AnyObject).data!(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
            for i in 0..<rqst.arrayImage.count {
                let img = rqst.arrayImage[i]
                multipartFormData.append(img, withName: "images\(i+1)", fileName: "file\(i+1)", mimeType: "image/jpg")
            }
            
        }, usingThreshold: UInt64.init(), to: rqst.serviceURL, method: .post, headers: rqst.headerDictionary) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let responseObj:CYLResponseObject=CYLResponseObject()
                    if response.result.error?._code == nil
                    {
                        //response is valid
                        let jsonResponse = JSON(data: response.data!)
                        responseObj.object = jsonResponse
                        responseObj.statusCode = response.response!.statusCode
                        responseObj.stringValue = jsonResponse.description
                        responseObj.headerFilds = response.response?.allHeaderFields as AnyObject?
                        responseObj.requestNotification = rqst.notificationName
                        responseObj.error=response.result.error as NSError?
                        
                        print("Response Body is")
                        print("***************************\n")
                        print(responseObj.stringValue!)
                        print("***************************")
                        print("\n\n ***************************\n ########################### \n *************************** \n")
                        if responseObj.statusCode != 200 && responseObj.statusCode != 201
                        {
                            responseObj.isValid=false
                        }
                        else
                        {
                            responseObj.isValid=true
                        }
                        if responseObj.error != nil
                        {
                            // do code to return connection error
                            error(responseObj.error as NSError?)
                        }
                        else
                        {
                            // do code to success response
                            completion(responseObj)
                        }
                    }
                    else
                    {
                        ntwrkErr(true)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
func printPostBodyForRequest(rqst:CYLRequestObject)
        {
            
            if rqst.serviceMethod == ServiceMethod.METHOD_GET
            {
                let parmdic:String=rqst .urlCodedParams()
                print("GET body is")
                print("***************************")
                print("\(parmdic)")
                print("***************************")
            }
            else if rqst.serviceMethod == ServiceMethod.METHOD_POST
            {
                let paramDic:String=rqst.urlCodedParams()
                print("Post body is")
                print("***************************")
                print("\(paramDic)")
                print("***************************")
            }
            else if rqst.serviceMethod == ServiceMethod.METHOD_PUT
            {
                let paramDic:NSString=rqst.jsonParams() as NSString
                print("Put body is")
                print("***************************")
                print("\(paramDic)")
                print("***************************")
            }
            else if rqst.serviceMethod == ServiceMethod.METHOD_POST_JSON
            {
                let paramDic:NSString=rqst.jsonParams() as NSString
                print("Post body is")
                print("***************************")
                print("\(paramDic)")
                print("***************************")
            }
            else if rqst.serviceMethod == ServiceMethod.METHOD_IMAGE
            {
            let paramDic:NSString=rqst.jsonParams() as NSString
            print("Post body is")
            print("***************************")
            print("\(paramDic)")
            print("***************************")
            }
            
        }
    }
    
    class CYLImageObject: NSObject
    {
        
        var  imageKey:String=""
        var imageName:String=""
        var imageData:NSData?
        
        func initWithName(name:String, withKey key:String, andData data:NSData)->AnyObject
        {
            self.imageKey=key;
            self.imageData=data;
            self.imageName=name;
            return self;
        }
        //Add a comment to this line
    }
    
