//
//  CYLServices.swift
//  TravelBuddy
//
//  Created by Nikhil Sharma on 1/13/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import Foundation
import UIKit
let baseUrl = "https://api.tourgetherapp.com/"   //production new
//let baseUrl = "http://dev.tourgetherapp.com/"   //development
//let baseUrl = "http://192.168.1.44:8080/travel" //http://192.168.1.5:8080/travel
class CYLServices: NSObject
{
    class func postUserLogin(dict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/create/user/or/login"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        return request
    }
    //user sign in flow
    class func postUserLoginFromEmail(dict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/create/user/from/email"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        return request
    }
    //user login email/pass
    class func postUserSigninFromEmail(dict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/login/user/from/email"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        return request
    }
    class func postUserForgotPassword(dict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/forget/password"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        return request
    }
    class func getUserLoginEmailValid(dict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/is/email/exist"
        request.serviceMethod=ServiceMethod.METHOD_GET
        request.addParamDictonary(dict: dict)
        return request
    }
    class func postImageUrl(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/swap/images"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postByePlan(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/buy/plan"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postFacebookImageUrl(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/download/images"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postAllProfileImagesLogin(dict:[String:Any],aryImages:[Data])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/upload/multiple/image/of/user/from/login"
        request.serviceMethod=ServiceMethod.METHOD_PUT
        request.arrayImage = aryImages
        request.addParamDictonary(dict: dict)
        return request
    }
    
    class func postAllProfileImages(dict:[String:Any],aryImages:[Data], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/upload/multiple/image/of/user"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        request.arrayImage = aryImages
        request.addParamDictonary(dict: dict)
        return request
    }
    class func deleteUserProfileImage(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/delete/photo/by/user"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postUserCurrentLocation(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/user/current/location"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func getAllCityCountryList(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/get/all/country/or/city"
        request.serviceMethod=ServiceMethod.METHOD_GET
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postDestinationList(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/add/city/country/by/user"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postGenderUpdate(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/update/user/gender"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func editDestinationList(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/edit/city/country/by/user"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    
    class func postCalendarList(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/user/set/start/end/date"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func editCalendarList(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/edit/start/end/date"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postAboutUsData(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/update/user"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postAboutUsDataAboutMe(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/update/user/aboutMe/v2"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func getTravellersList(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/search/travellers"
        request.serviceMethod=ServiceMethod.METHOD_GET
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func getNotificationMessageList(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/get/notification/message"
        request.serviceMethod=ServiceMethod.METHOD_GET
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func getNotificationMessageSeen(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/notifications/seen"
        request.serviceMethod=ServiceMethod.METHOD_PUT
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func getTravellersListNearMe(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/search/travellers/nearme"
        request.serviceMethod=ServiceMethod.METHOD_GET
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func getTravellersListLocally(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/search/travellers/locally"
        request.serviceMethod=ServiceMethod.METHOD_GET
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postTravellerFilter(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/add/or/update/user/filter"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
//    class func getUnreadMessageCount(dict:[String:Any])->CYLRequestObject
//    {
//        let request:CYLRequestObject=CYLRequestObject()
//        request.serviceURL=baseUrl + "/no/of/unread/message/by/user"
//        request.serviceMethod=ServiceMethod.METHOD_GET
//        request.addParamDictonary(dict: dict)
//        return request
//    }
//    class func getUserChatList(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
//    {
//        let request:CYLRequestObject=CYLRequestObject()
//        request.serviceURL=baseUrl + "/get/chat/list/of/user"
//        request.serviceMethod=ServiceMethod.METHOD_POST
//        request.addParamDictonary(dict: dict)
//        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
//        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
//        return request
//    }
    class func postComposeMessage(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/send/notification"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    
    class func getOtherUserProfile(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/get/user/profile/by/Id"
        request.serviceMethod=ServiceMethod.METHOD_GET
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func deleteMessageFromFirebase(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/delete/sender/receiver/firebase"
        request.serviceMethod=ServiceMethod.METHOD_DELETE
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postBlockUser(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/block/or/unblock/an/user"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postReportUser(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/report/an/user"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func logoutUserAccount(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/logout/user"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func updateUserNotificationPreference(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/turn/on/off/message/notification"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postUpdateDeviceToken(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/update/device/id"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func getUpdateAppVersion(dict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/force/update/app"
        request.serviceMethod=ServiceMethod.METHOD_GET
        request.addParamDictonary(dict: dict)
        return request
    }
    class func deactivateUserAccount(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/deactivate/account/by/user"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postWaveMessageForServer(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/send/wave"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
    class func postReviewRating(dict:[String:Any], headerDict:[String:Any])->CYLRequestObject
    {
        let request:CYLRequestObject=CYLRequestObject()
        request.serviceURL=baseUrl + "/submit/feedback"
        request.serviceMethod=ServiceMethod.METHOD_POST
        request.addParamDictonary(dict: dict)
        request.addHeader(value: headerDict["user-id"] as AnyObject, forKey: "user-id")
        request.addHeader(value: headerDict["auth-token"] as AnyObject, forKey: "auth-token")
        return request
    }
}
