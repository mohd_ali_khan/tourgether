//
//  CYLResponseObject.swift
//  IndiaNews
//
//  Created by Nikhil Sharma on 1/13/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import Foundation

import UIKit

class CYLResponseObject: NSObject
{
    
    var data:Data?
    var error:NSError?
    var statusCode:Int?
    var errorCode:Int?
    var isValid:Bool=false
    var urlRequest:NSMutableURLRequest?
    var httpResponse:HTTPURLResponse?
    var stringValue:String?
    var object:JSON?
    var headerFilds:AnyObject?
    var requestNotification:String?
    
}
