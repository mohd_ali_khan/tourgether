//
//  AppDelegate.swift
//  TravelBuddy
//
//  Created by codeyeti on 18/06/18.
//  Copyright © 2018 Ankur Sharma. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import UserNotifications
import Fabric
import Crashlytics
import Firebase
import RNNotificationView
import Inapptics
import IQKeyboardManagerSwift
import Branch

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var userSignUpDetail:[String: Any]?
    var selectedIndexPresent: UInt = UInt()
//    var activeChatId:String?
    var aboutMeV2 = [String: JSON]()
    var timer: Timer = Timer()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
    
        FirebaseApp.configure()
        ///////////////////*** Inapptics *** /////////////////////////
        Inapptics.letsGo(withAppToken: "27b3fec0551211e886bdaf56993cc94c")
        
        var rootVC:UIViewController?
        let userDefaults = UserDefaults.standard
        if let _ = userDefaults.value(forKey: "messageWave_count")
        {
        }
        else
        {
            userDefaults.set(0, forKey: "messageWave_count")
        }
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let usrDefaults = userDefaults.value(forKey: "userObject")
        {
            ////////// user update device token and send Clever Tap ///////////
            if #available(iOS 10.0, *) {
                let current = UNUserNotificationCenter.current()
            } else {
                // Fallback on earlier versions
            }
            if let _ = userDefaults.value(forKey: "rating_count")
            {
                var rateCount: Int = userDefaults.value(forKey: "rating_count") as! Int
                rateCount = rateCount + 1
                userDefaults.set(rateCount, forKey: "rating_count")
            }
            
            checkMessageNotificationEnable()
            
            //user is already logged-in
            CYLGlobals.sharedInstance.usrObject = NSKeyedUnarchiver.unarchiveObject(with: usrDefaults as! Data) as? CYLUserObject
            //Pre Login User sent information on cleverTap
            
            let email = (CYLGlobals.sharedInstance.usrObject?.isEmailNotification == 1) ? true : false
            let push = (CYLGlobals.sharedInstance.usrObject?.isOtherNotifications == 1) ? true : false
            var strProfileImg = String()
            if ((CYLGlobals.sharedInstance.usrObject?.userImages)?.count)! > 0
            {
                let pictureObject = (CYLGlobals.sharedInstance.usrObject?.userImages)?.first as! [String:AnyObject]
                strProfileImg = pictureObject["picUrl"] as! String
            }
            let gender = (CYLGlobals.sharedInstance.usrObject!.gender == 0) ? "M" : "F"
            var localTimeZoneName: String { return TimeZone.current.identifier }
            let dob = CYLGlobals.sharedInstance.usrObject?.dateOfBirth
            var firstName = String()
            if let _ = CYLGlobals.sharedInstance.usrObject!.firstName
            {
                firstName =  (CYLGlobals.sharedInstance.usrObject!.firstName == "") ? CYLGlobals.sharedInstance.usrObject!.userName!: CYLGlobals.sharedInstance.usrObject!.firstName!
            }
            else
            {
                firstName = CYLGlobals.sharedInstance.usrObject!.userName!
            }
            
            let profile: Dictionary<String, Any> = [
                "Name": firstName,
                "Identity": CYLGlobals.sharedInstance.usrObject!.id!,
                "Email": CYLGlobals.sharedInstance.usrObject!.email!,
                "Phone": "",
                "Gender": "\(gender)",
                "Employed": "",
                "Education": "",
                "Married": "",
                "DOB": (CYLGlobals.sharedInstance.usrObject?.dateOfBirth==nil) ? Date() : convertTimestampIntoDateString(date: Double(dob!), format: "dd/MM/yyyy"),
                "Age": 0,
                "Tz":"\(localTimeZoneName)",
                "Photo": "\(strProfileImg)",
                "MSG-email": email,
                "MSG-push": push,
                "MSG-sms": false
            ]
            let strIsoCode = (CYLGlobals.sharedInstance.usrObject?.isoLocation != nil) ? CYLGlobals.sharedInstance.usrObject?.isoLocation : ""
            CleverTap.sharedInstance()?.profileAddMultiValue(strIsoCode!, forKey: "Other Details")
            CleverTap.sharedInstance()?.profilePush(profile)
            //////////////////*** Branch *** ////////////////////////////
            
            Inapptics.identifyUser(withId: "\(CYLGlobals.sharedInstance.usrObject!.id!)", email: "\(CYLGlobals.sharedInstance.usrObject!.email!)", name:firstName)
            
            if let _ = userDefaults.value(forKey: "travel_filter")
            {
                CYLGlobals.sharedInstance.isFilterApplied=true
            }
            else
            {
                CYLGlobals.sharedInstance.isFilterApplied=false
            }
            switch CYLGlobals.sharedInstance.usrObject!.progress!
            {
            case 0: 
                //push to destination screen here
                rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLDestinationScreenVC") as! CYLDestinationScreenVC
            case 1:
                //push to destination screen here
                if CYLGlobals.sharedInstance.usrObject?.cities?.count != 0 || CYLGlobals.sharedInstance.usrObject?.countries?.count != 0
                {
                    rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLCalendarScreenVC") as! CYLCalendarScreenVC
                }
                else
                {
                    rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
                }
            case 2:
                //push to home screen screen here
                rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
            default:
                break
            }
        }
        else
        {
            rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLPageViewController") as! CYLPageViewController
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        let navController = UINavigationController.init(rootViewController: rootVC!)
        self.window?.rootViewController = navController
        self.window?.backgroundColor = UIColor.black
        self.window?.makeKeyAndVisible()
        UIApplication.shared.statusBarStyle = .default
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        //        UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 255.0/255.0, green: 67.0/255.0, blue: 81.0/255.0, alpha: 1)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside=true
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        Fabric.with([Crashlytics.self])
        self.sendCrashLogUserInformation()
        
        self.updateAppFourseFully(window: rootVC!, appState: "appStart")
        
        if let branch = Branch.getInstance()
        {
            branch.setRequestMetadataKey("$clevertap_attribution_id", value: CleverTap.sharedInstance()?.profileGetAttributionIdentifier() as NSObject?)
            
            branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
                if error == nil {
                    // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                    // params will be empty if no data found
                    // ... insert custom logic here ...
                    print("params: %@", params as? [String: AnyObject] ?? {})
                }
            })
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        self.updateAppFourseFully(window: (application.keyWindow?.rootViewController)!, appState: "appBackgroundToActive")
        print("Background to Active State")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(AppDelegate.updateTimer)), userInfo: nil, repeats: true)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        CYLDBRefProvider.sharedInstance.chatListReference.removeAllObservers()
        CYLDBRefProvider.sharedInstance.userReference.removeAllObservers()
    }
    @objc func updateTimer()
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification_setting_updated"), object: nil)
        timer.invalidate()
    }
    //MARK:- Check NOtification enable //////////////////////
    func checkMessageNotificationEnable()
    {
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in
                
                if settings.authorizationStatus == .notDetermined || settings.authorizationStatus == .denied
                {
                    
                }
                else if settings.authorizationStatus == .authorized
                {
                    self.requestToAllowPushNotification()
                }
            })
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                requestToAllowPushNotification()
            }
        }
    }
    
    func changeMainViewToRootVC()
    {
        sentEventPropertyOnCleverTap("Notification opened", _property: ["type":"none"])
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
        let navController = UINavigationController.init(rootViewController: rootVC)
        self.window?.rootViewController = navController
    }
    func changeRootToLoginVC()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLPageViewController") as! CYLPageViewController
        let navController = UINavigationController.init(rootViewController: rootVC)
        self.window?.rootViewController = navController
    }
    func sendCrashLogUserInformation()
    {
        if let _ = UserDefaults.standard.value(forKey: "userObject")
        {
            let usrObj = CYLGlobals.sharedInstance.usrObject
            // TODO: Use the current user's information
            // You can call any combination of these three methods
            Crashlytics.sharedInstance().setUserEmail("\(usrObj?.email! ?? "guestUser@fabric.io")")
            Crashlytics.sharedInstance().setUserIdentifier("\(usrObj?.id! ?? 0)")
            Crashlytics.sharedInstance().setUserName(usrObj?.userName!)
        }
        else
        {
            // TODO: Use the current user's information
            // You can call any combination of these three methods
            Crashlytics.sharedInstance().setUserEmail("guestUser@fabric.io")
            Crashlytics.sharedInstance().setUserIdentifier("123456")
            Crashlytics.sharedInstance().setUserName("Guest User")
        }
        
    }
    
    func requestToAllowPushNotification()
    {
        if #available(iOS 10.0, *)
        {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert , .sound]) { (greanted, error) in
                if greanted {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound];
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications();
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification_setting_updated"), object: nil)
        }
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        print("url \(url)")
        print("url host :\(url.host ?? "")")
        print("url path :\(url.path)")
        
        let branchHandled = Branch.getInstance().application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        print(branchHandled)
        
        if (!branchHandled) {
            CleverTap.sharedInstance()?.handleOpen(url, sourceApplication: nil)
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        else
        {
            sentGoogleAnalyticAndCleverTapVisitCV(strCVName: "Open Branch Link")
            Branch.getInstance().application(application, open: url, options: nil)
            CleverTap.sharedInstance()?.handleOpen(url, sourceApplication: nil)
            return true
        }
    }
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([Any]?) -> Void) -> Bool
    {
        // handler for Universal Links
        openAppCustomUrl()
        Branch.getInstance().continue(userActivity)
        return true
    }

    //MARK:- App Open Branch Or Custom Link
    func openAppCustomUrl()
    {
        var rootVC:UIViewController?
        let userDefaults = UserDefaults.standard
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let _ = userDefaults.value(forKey: "userObject")//userDefaults.value(forKey: "message_count")
        {
            if let _ = userDefaults.value(forKey: "travel_filter")
            {
                CYLGlobals.sharedInstance.isFilterApplied=true
            }
            else
            {
                CYLGlobals.sharedInstance.isFilterApplied=false
            }
            switch CYLGlobals.sharedInstance.usrObject!.progress!
            {
            case 0:
                //push to destination screen here
                rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLDestinationScreenVC") as! CYLDestinationScreenVC
            case 1:
                //push to destination screen here
                if CYLGlobals.sharedInstance.usrObject?.cities?.count != 0 || CYLGlobals.sharedInstance.usrObject?.countries?.count != 0
                {
                    rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLCalendarScreenVC") as! CYLCalendarScreenVC
                }
                else
                {
                    rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
                }
            case 2:
                //push to home screen screen here
                rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
            default:
                break
            }
        }
        else
        {
            rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLPageViewController") as! CYLPageViewController
        }
        let navController = UINavigationController.init(rootViewController: rootVC!)
        self.window?.rootViewController = navController
        self.window?.backgroundColor = UIColor.black
        self.window?.makeKeyAndVisible()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let myuserInfo = notification.request.content.userInfo
        let aps = myuserInfo["aps"] as! [String:Any]
        
        if myuserInfo["type"] == nil
        {
            let msg = aps["alert"] as! [String:Any]
            self.showCleverTapNotificationBannerWithTitle((msg["title"] as! String?)!, msg: (msg["body"] as! String?)!)
        }
        else if myuserInfo["type"] as! String == "wave"
        {
            //***********for foreground state************
            self.showChatNotificationBannerWithTitle((aps["alert"] as! String?)!)
        }
        else if myuserInfo["type"] as! String == "message"
        {
            //***********for foreground state************
            self.showChatNotificationBannerWithTitle((aps["alert"] as! String?)!)
        }
        else if myuserInfo["type"] as! String == "match"
        {
            //handle match type push notification
            //***********for foreground state************
            self.showMatchNotificationBannerWithTitle((aps["alert"] as! String?)!)
        }
        else
        {
            //handle general type push notification
            //***********for foreground state************
            self.showGeneralNotificationBannerWithTitle((aps["alert"] as! String?)!)
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        let userInfo = response.notification.request.content.userInfo as! [String:Any]
        let aps = userInfo["aps"] as! [String:Any]
        if userInfo["type"] == nil
        {
            self.changeMainViewToRootVC()
        }
        else if userInfo["type"] as! String == "message"
        {
            let gender = (userInfo["gender"] as? String == "0") ? "Male" : "Female"
            sentEventPropertyOnCleverTap("Notification opened", _property: ["type":"chat messages","gender":gender,"IsGenderMatched":"\(userInfo["isGenderMatched"] as? String ?? "")","IsCountryMatched":"\(userInfo["isCountryMatched"] as? String ?? "")","userName":"\(userInfo["userName"] as? String ?? "")","userId":"\(userInfo["usetId"] as? String ?? "")"])
            //handle chat type push notification
            //***********for inactive and background state************
            self.handleChatWithObject(myuserInfo: userInfo)
        }
        else if userInfo["type"] as! String == "wave"
        {
            let gender = (userInfo["gender"] as? String == "0") ? "Male" : "Female"
            sentEventPropertyOnCleverTap("Notification opened", _property: ["type":"wave messages","gender":gender,"IsGenderMatched":"\(userInfo["isGenderMatched"] as? String ?? "")","IsCountryMatched":"\(userInfo["isCountryMatched"] as? String ?? "")","userName":"\(userInfo["userName"] as? String ?? "")","userId":"\(userInfo["usetId"] as? String ?? "")"])
            //handle chat type push notification
            //***********for inactive and background state************
            self.handleMessageWithObject(myuserInfo: userInfo)
        }
        else if userInfo["type"] as! String == "match"
        {
            let gender = (userInfo["gender"] as? String == "0") ? "Male" : "Female"
            sentEventPropertyOnCleverTap("Notification opened", _property: ["type":"matched trip","gender":gender,"IsGenderMatched":"\(userInfo["isGenderMatched"] as? String ?? "")","IsCountryMatched":"\(userInfo["isCountryMatched"] as? String ?? "")","userName":"\(userInfo["userName"] as? String ?? "")","userId":"\(userInfo["usetId"] as? String ?? "")"])
            //handle match type push notification
            //***********for inactive and background state************
            self.handleMatchUserProfileWithObject(myuserInfo: userInfo)
        }
        else
        {
            //handle general type push notification
            //***********for foreground state************
            self.showGeneralNotificationBannerWithTitle((aps["alert"] as! String?)!)
        }
        
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let myuserInfo = userInfo as! [String:Any]
        print(myuserInfo)
        let aps = myuserInfo["aps"] as! [String:Any]
        
        if  userInfo["type"] == nil
        {
            
            if application.applicationState == .active
            {
                //***********for foreground state************
                let msg = aps["alert"] as! [String:Any]
                self.showCleverTapNotificationBannerWithTitle((msg["title"] as! String?)!, msg: (msg["body"] as! String?)!)
            }
            else
            {
                //***********for inactive and background state************
                self.changeMainViewToRootVC()
            }
            
        }
        else if myuserInfo["type"] as! String == "message"
        {
            //handle chat type push notification
            
            if application.applicationState == .active
            {
                //***********for foreground state************
                self.showChatNotificationBannerWithTitle((aps["alert"] as! String?)!)
            }
            else
            {
                let gender = (myuserInfo["gender"] as? String == "0") ? "Male" : "Female"
                sentEventPropertyOnCleverTap("Notification opened", _property: ["type":"chat messages","gender":gender,"IsGenderMatched":"\(myuserInfo["isGenderMatched"] as? String ?? "")","IsCountryMatched":"\(myuserInfo["isCountryMatched"] as? String ?? "")","userName":"\(myuserInfo["userName"] as? String ?? "")","userId":"\(myuserInfo["usetId"] as? String ?? "")"])
                //***********for inactive and background state************
                self.handleChatWithObject(myuserInfo: myuserInfo)
            }
        }
            
        else if myuserInfo["type"] as! String == "wave"
        {
            if application.applicationState == .active
            {
                //***********for foreground state************
                self.showChatNotificationBannerWithTitle((aps["alert"] as! String?)!)
            }
            else
            {
                let gender = (myuserInfo["gender"] as? String == "0") ? "Male" : "Female"
                sentEventPropertyOnCleverTap("Notification opened", _property: ["type":"wave messages","gender":gender,"IsGenderMatched":"\(myuserInfo["isGenderMatched"] as? String ?? "")","IsCountryMatched":"\(myuserInfo["isCountryMatched"] as? String ?? "")","userName":"\(myuserInfo["userName"] as? String ?? "")","userId":"\(myuserInfo["usetId"] as? String ?? "")"])
                //handle chat type push notification
                //***********for inactive and background state************
                self.handleMessageWithObject(myuserInfo: myuserInfo)
            }
        }
        else if myuserInfo["type"] as! String == "match"
        {
            //handle match type push notification
            
            if application.applicationState == .active
            {
                //***********for foreground state************
                self.showMatchNotificationBannerWithTitle((aps["alert"] as! String?)!)
            }
            else
            {
                let gender = (myuserInfo["gender"] as? String == "0") ? "Male" : "Female"
                sentEventPropertyOnCleverTap("Notification opened", _property: ["type":"matched trip","gender":gender,"IsGenderMatched":"\(myuserInfo["isGenderMatched"] as? String ?? "")","IsCountryMatched":"\(myuserInfo["isCountryMatched"] as? String ?? "")","userName":"\(myuserInfo["userName"] as? String ?? "")","userId":"\(myuserInfo["usetId"] as? String ?? "")"])
                //***********for inactive and background state************
                self.handleMatchUserProfileWithObject(myuserInfo: myuserInfo)
            }
        }
        else
        {
            //handle general type push notification
            if application.applicationState == .active
            {
                //***********for foreground state************
                self.showGeneralNotificationBannerWithTitle((aps["alert"] as! String?)!)
            }
            
        }
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("*************************device token is \(token)***************************")
        //        if let _ = CYLGlobals.sharedInstance.usrObject?.id
        //        {
        UserDefaults.standard.set(token, forKey: "deviceToken")
        UserDefaults.standard.synchronize()
        CleverTap.sharedInstance()?.setPushToken(deviceToken)
        self.requestToUpdateDeviceToken(deviceToken: token)
        //        }
    }
    //MARK:- **************** updateAppFourseFully ****************
    func updateAppFourseFully(window: UIViewController,appState:String)
    {
        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
        let version = nsObject as! String
        let params:[String: Any] = ["appVersion":version,"deviceType":"IPHONE"]
        let request = CYLServices.getUpdateAppVersion(dict: params)
        CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
            { (response) in
                //KVNProgress.dismiss()
                if (response?.isValid)!
                {
                    let responseData = response?.object?.dictionaryValue
                    if responseData?["status"]?.stringValue != "fail"
                    {
                        let alertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
                        let titleFont = [NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 18.0)!]
                        let messageFont = [NSAttributedStringKey.font: UIFont(name: "Comfortaa-Bold", size: 12.0)!]
                        
                        if (responseData?["forceUpdate"]?.boolValue == true)
                        {
                            //Force Update
                            let titleAttrString = NSMutableAttributedString(string: "Tourgether Update", attributes: titleFont)
                            let messageAttrString = NSMutableAttributedString(string: "The version of Tourgether you are using is old, and no longer supported. Please update to the latest version", attributes: messageFont)
                            
                            alertController.setValue(titleAttrString, forKey: "attributedTitle")
                            alertController.setValue(messageAttrString, forKey: "attributedMessage")
                            let okAction = UIAlertAction(title: "Update", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                                //ref.dismiss(animated: true, completion: nil)
                                UIApplication.shared.openURL(URL.init(string: "https://itunes.apple.com/app/travel-buddy-find-a-trip-companion/id1245536821?mt=8")!)
                            }
                            alertController.addAction(okAction)
                            window.present(alertController, animated: true, completion: nil)
                        }
                        else if (responseData?["recommendedUpdate"]?.boolValue == true && appState != "appBackgroundToActive")
                        {
                            // recommended Update
                            let titleAttrString = NSMutableAttributedString(string: "New Version Available", attributes: titleFont)
                            let messageAttrString = NSMutableAttributedString(string: "You are using an old version of Tourgether. Would you like to update to the latest one?", attributes: messageFont)
                            
                            alertController.setValue(titleAttrString, forKey: "attributedTitle")
                            alertController.setValue(messageAttrString, forKey: "attributedMessage")
                            let noThanks = UIAlertAction(title: "No, Thanks", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                                //ref.dismiss(animated: true, completion: nil)
                                
                            }
                            let upDate = UIAlertAction(title: "Update", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                                //ref.dismiss(animated: true, completion: nil)
                                UIApplication.shared.canOpenURL(URL.init(string: "https://itunes.apple.com/app/travel-buddy-find-a-trip-companion/id1245536821?mt=8")!)
                            }
                            alertController.addAction(noThanks)
                            alertController.addAction(upDate)
                            window.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
                
        },
                                                                  withError:
            {
                (error) in
        },
                                                                  andNetworkErr:
            {
                (networkFailure) in
        })
    }
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    //MARK: Api Call Section
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    func requestToUpdateDeviceToken(deviceToken:String)
    {
        //KVNProgress.show()
        if let userId = CYLGlobals.sharedInstance.usrObject?.id
        {
            //            R74-796-8Z5Z , 6ca-4a2 Dev
            //            48K-449-WR5Z , c44-3b4 Dis
            var isoLocation = ""
            if let _ = CYLGlobals.sharedInstance.usrObject!.isoLocation
            {
                isoLocation = CYLGlobals.sharedInstance.usrObject!.isoLocation!
            }
            CleverTap.setCredentialsWithAccountID("48K-449-WR5Z", token: deviceToken, region: isoLocation)//changeCredentials(withAccountID: "48K-449-WR5Z", token: deviceToken, region: isoLocation)
            
            let params:[String: Any] = ["userId":"\(userId)","deviceId":deviceToken,"isoLocation":isoLocation]
            let request = CYLServices.postUpdateDeviceToken(dict:params, headerDict: ["user-id":"\(CYLGlobals.sharedInstance.usrObject!.id!)","auth-token":"\(CYLGlobals.sharedInstance.usrObject!.authToken ?? "")"])
            CYLServiceMaster.sharedInstance.callWebServiceWithRequest(rqst: request, withResponse:
                { (response) in
                    //KVNProgress.dismiss()
                    if (response?.isValid)!
                    {
                        let responseData = response?.object?.dictionaryValue
                        if responseData?["status"]?.stringValue != "fail"
                        {
                            
                        }
                        
                    }
                    else
                    {
                        
                    }
                    
            },
                                                                      withError:
                {
                    (error) in
            },
                                                                      andNetworkErr:
                {
                    (networkFailure) in
            })
        }
    }
    func showMatchNotificationBannerWithTitle(_ strTitle:String)
    {
        RNNotificationView.show(withImage: UIImage(named: "ic_logo"),
                                title: "Tourgether",
                                message: strTitle,
                                duration: 2,
                                iconSize: CGSize(width: 22, height: 22), // Optional setup
            onTap:
            {
                //print("Did tap notification")
        })
    }
    func showGeneralNotificationBannerWithTitle(_ strTitle:String)
    {
        RNNotificationView.show(withImage: UIImage(named: "ic_logo"),
                                title: "Tourgether",
                                message: strTitle,
                                duration: 2,
                                iconSize: CGSize(width: 22, height: 22), // Optional setup
            onTap:
            {
                //print("Did tap notification")
        })
    }
    
    func showCleverTapNotificationBannerWithTitle(_ strTitle:String, msg: String)
    {
        RNNotificationView.show(withImage: UIImage(named: "ic_logo"),
                                title: strTitle,
                                message: msg,
                                duration: 2,
                                iconSize: CGSize(width: 22, height: 22), // Optional setup
            onTap:
            {
                //print("Did tap notification")
        })
    }
    func handleMatchUserProfileWithObject(myuserInfo:[String:Any])
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
        let navController = UINavigationController.init(rootViewController: rootVC)
        self.window?.rootViewController = navController
        self.perform(#selector(navigateToUserProfileWithObject(_:)), with: myuserInfo, afterDelay: 0.0)
        
    }
    @objc func navigateToUserProfileWithObject(_ userInfo:[String:Any])
    {
        NotificationCenter.default.post(name: NSNotification.Name("notify_handle_preselected_user_profile"), object: userInfo)
    }
    func handleChatWithObject(myuserInfo:[String:Any])
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
        rootVC.selectedIndex = 1
        let navController = UINavigationController.init(rootViewController: rootVC)
        self.window?.rootViewController = navController
        self.perform(#selector(navigateToChatMessageWithObject(_:)), with: myuserInfo, afterDelay: 0.0)
        
    }
    func handleMessageWithObject(myuserInfo:[String:Any])
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = storyBoard.instantiateViewController(withIdentifier: "CYLHomeTabController") as! CYLHomeTabController
        rootVC.selectedIndex = 1
        let navController = UINavigationController.init(rootViewController: rootVC)
        self.window?.rootViewController = navController
    }
    @objc func navigateToChatMessageWithObject(_ userInfo:[String:Any])
    {
        NotificationCenter.default.post(name: NSNotification.Name("notify_handle_chat_details"), object: userInfo)
    }
    func showChatNotificationBannerWithTitle(_ strTitle:String)
    {
        if var aryTopControllers = UIApplication.shared.keyWindow?.rootViewController?.childViewControllers
        {
            if !((aryTopControllers.count == 2 && aryTopControllers[1].isKind(of: CYLChatDetailVC.self)) || (aryTopControllers.count == 3 && aryTopControllers[2].isKind(of: CYLChatDetailVC.self)) || aryTopControllers.count == 0)
            {
                RNNotificationView.show(withImage: UIImage(named: "ic_logo"),
                                        title: "Tourgether",
                                        message: strTitle,
                                        duration: 2,
                                        iconSize: CGSize(width: 22, height: 22), // Optional setup
                    onTap:
                    {
                        //print("Did tap notification")
                })
            }
        }
    }

}
