//
//  CYLChatListObject.swguardt
//  TravelBuddy
//
//  Created by CodeYeti on 9/5/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import Foundation

struct CYLChatListObject
{
//    var userId = ""
    var chatId = ""
    var recentMsg = ""
    var recentMsgTime = ""
    var otherUserId = ""
    var otherUserName = ""
    var otherUserImageUrl = ""
    var chatUnreadCount = ""
    var isBlocked = ""
    var type = ""
    var customerType = "FREE"
    var maskingType = ""
//    var waveType = ""
    
    init()
    {
        
    }
    init(_ object:AnyObject)
    {
        guard let isBlocked = object["is_blocked"]
            else
        {
            print("is blocked not found")
            self.isBlocked = "false"
            return
        }
        self.isBlocked = (isBlocked as! Bool == true) ? "true" : "false"
        
        guard let chat_id = object["chat_id"]
            else
        {
            print("chat id not found")
            self.chatId = ""
            return
        }
        self.chatId = chat_id as! String
        
        guard let lastMsg = object["last_message"]
            else
        {
            print("recent message not found")
            self.recentMsg = ""
            return
        }
        self.recentMsg = lastMsg as! String
        
        guard let usrId = object["other_user_id"]
            else
        {
            print("chat with id not found")
            self.otherUserId = ""
            return
        }
        self.otherUserId = "\(usrId!)"
        
        guard let chatTime = object["last_message_time"]
            else
        {
            print("recent message time not found")
            self.recentMsgTime = ""
            return
        }
        self.recentMsgTime = "\(chatTime!)"
        
        guard let unreadCount = object["unread_count"]
            else
        {
            print("unread count not found")
            self.chatUnreadCount = "0"
            return
        }
        self.chatUnreadCount = "\(unreadCount!)"
        
        guard let maskingType = object["masking_type"] as? String
            else
        {
            print("chat with masking not found")
            self.maskingType = ""
            return
        }
        self.maskingType = maskingType
        
        guard let usrType = object["customer_type"] as? String
            else
        {
            print("chat with customer type not found")
            self.customerType = "FREE"
            return
        }
        self.customerType = usrType
        
        guard let usrImg = object["img_url"] as? String
            else
        {
            print("chat image not found")
            self.otherUserImageUrl = ""
            return
        }
        self.otherUserImageUrl = usrImg
        
        guard let usrName = object["name"] as? String
            else
        {
            print("chat with name not found")
            self.otherUserName = ""
            return
        }
        self.otherUserName = usrName
        
    }

    
    init(_ chatInfo:NSDictionary)
    {
       
        guard let isBlocked = chatInfo["is_blocked"]
            else
        {
            print("is blocked not found")
            self.isBlocked = "false"
            return
        }
        self.isBlocked = (isBlocked as! Bool == true) ? "true" : "false"
        
        guard let chat_id = chatInfo["chat_id"]
            else
        {
            print("chat id not found")
            self.chatId = ""
            return
        }
        self.chatId = chat_id as! String
        
        guard let lastMsg = chatInfo["last_message"]
            else
        {
            print("recent message not found")
            self.recentMsg = ""
            return
        }
        self.recentMsg = lastMsg as! String
        
        guard let usrId = chatInfo["other_user_id"]
            else
        {
            print("chat with id not found")
            self.otherUserId = ""
            return
        }
        self.otherUserId = "\(usrId)"

        guard let chatTime = chatInfo["last_message_time"]
            else
        {
            print("recent message time not found")
            self.recentMsgTime = ""
            return
        }
        self.recentMsgTime = "\(chatTime)"
        
        guard let unreadCount = chatInfo["unread_count"]
            else
        {
            print("unread count not found")
            self.chatUnreadCount = "0"
            return
        }
        self.chatUnreadCount = "\(unreadCount)"
        
        guard let maskingType = chatInfo["masking_type"] as? String
        else
        {
            print("chat with masking not found")
            self.maskingType = ""
            return
        }
        self.maskingType = maskingType
        
        guard let usrType = chatInfo["customer_type"] as? String
            else
        {
            print("chat with customer type not found")
            self.customerType = "FREE"
            return
        }
        self.customerType = usrType
        
        guard let usrImg = chatInfo["img_url"] as? String
            else
        {
            print("chat image not found")
            self.otherUserImageUrl = ""
            return
        }
        self.otherUserImageUrl = usrImg
        
        guard let usrName = chatInfo["name"] as? String
            else
        {
            print("chat with name not found")
            self.otherUserName = ""
            return
        }
        self.otherUserName = usrName
        
        
    }
    
    
}

