//
//  CYLCities.swift
//
//  Created by Nikhil Sharma on 4/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation

public final class CYLLocationObject: NSObject,NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let countryName = "countryName"
    static let cityName = "cityName"
    static let id = "id"
    static let countryId = "countryId"
    static let cityPicUrl = "cityPicUrl"
    static let picUrl = "picUrl"
    static let destinationMatched = "destinationMatched"
    static let livesInDestinationCountry = "livesInDestinationCountry"
  }

  // MARK: Properties
  public var countryName: String?
  public var cityName: String?
  public var id: Int?
  public var countryId: Int?
  public var cityPicUrl: String?
  public var picUrl:String?
    public var destinationMatched: Bool?
    public var livesInDestinationCountry: Bool?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    countryName = json[SerializationKeys.countryName].string
    cityName = json[SerializationKeys.cityName].string
    id = json[SerializationKeys.id].int
    countryId = json[SerializationKeys.countryId].int
    cityPicUrl = json[SerializationKeys.cityPicUrl].string
    picUrl = json[SerializationKeys.picUrl].string
    destinationMatched = json[SerializationKeys.destinationMatched].bool
    livesInDestinationCountry = json[SerializationKeys.livesInDestinationCountry].bool
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = countryName { dictionary[SerializationKeys.countryName] = value }
    if let value = cityName { dictionary[SerializationKeys.cityName] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = countryId { dictionary[SerializationKeys.countryId] = value }
    if let value = cityPicUrl { dictionary[SerializationKeys.cityPicUrl] = value }
    if let value = picUrl { dictionary[SerializationKeys.picUrl] = value }
    if let value = destinationMatched { dictionary[SerializationKeys.destinationMatched] = value}
    if let value = livesInDestinationCountry { dictionary[SerializationKeys.livesInDestinationCountry] = value}
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.countryName = aDecoder.decodeObject(forKey: SerializationKeys.countryName) as? String
    self.cityName = aDecoder.decodeObject(forKey: SerializationKeys.cityName) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.countryId = aDecoder.decodeObject(forKey: SerializationKeys.countryId) as? Int
    self.cityPicUrl = aDecoder.decodeObject(forKey: SerializationKeys.cityPicUrl) as? String
    self.picUrl = aDecoder.decodeObject(forKey: SerializationKeys.picUrl) as? String
    self.destinationMatched = aDecoder.decodeObject(forKey: SerializationKeys.destinationMatched) as? Bool
    self.livesInDestinationCountry = aDecoder.decodeObject(forKey: SerializationKeys.livesInDestinationCountry) as? Bool
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(countryName, forKey: SerializationKeys.countryName)
    aCoder.encode(cityName, forKey: SerializationKeys.cityName)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(countryId, forKey: SerializationKeys.countryId)
    aCoder.encode(cityPicUrl, forKey: SerializationKeys.cityPicUrl)
    aCoder.encode(picUrl, forKey: SerializationKeys.picUrl)
    aCoder.encode(destinationMatched, forKey: SerializationKeys.destinationMatched)
    aCoder.encode(livesInDestinationCountry, forKey: SerializationKeys.livesInDestinationCountry)
  }

}
