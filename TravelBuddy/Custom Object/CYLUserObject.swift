//
//  CYLUserObject.swift
//
//  Created by Nikhil Sharma on 4/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation


public final class CYLUserObject: NSObject,NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let countries = "countries"
        static let city = "city"
        static let country = "country"
        static let cities = "cities"
        static let startDate = "startDate"
        static let fbToken = "fbToken"
        static let creationTime = "creationTime"
        static let email = "email"
        static let aboutMe = "updatedAboutMe"
        static let noOfTravellers = "noOfTravellers"
        static let gender = "gender"
        static let userName = "userName"
        static let firstName = "firstName"
        static let dateOfBirth = "dateOfBirth"
        static let newMessageNotify = "isNewMessageNotify"
        static let id = "id"
        static let progress = "progress"
        static let isDeleted = "is_deleted"
        static let endDate = "endDate"
        static let userImages = "userImages"
        static let isoLocation = "isoLocation"
        static let isMatchNotification = "isMatchNotification"
        static let isEmailNotification = "isEmailNotification"
        static let isOtherNotifications = "isOtherNotifications"
        static let isWaveNotifications = "isWaveNotify"
        static let countryPicUrl = "countryPicUrl"
        static let createdByAdmin = "createdByAdmin"
        static let cityv2 = "cityv2"
        static let isAppFeedBack = "isAppFeedBack"
        static let authToken = "authToken"
        static let wavesRemaining = "wavesRemaining"
        static let wavesMessage = "wavesMessage"
        static let userPlanType = "planType"
//        static let userPlanName = "planName"
    }
    
    // MARK: Properties
    public var countries: [CYLLocationObject]?
    public var city: String?
    public var country: String?
    public var cityv2: String?
    public var cities: [CYLLocationObject]?
    public var startDate: String?
    public var fbToken: String?
    public var creationTime: Int?
    public var email: String?
    public var aboutMe: String?
    public var noOfTravellers:String?
    public var gender: Int?
    public var userName: String?
    public var firstName: String?
    public var dateOfBirth: Int?
    public var newMessageNotify: Int?
    public var id: Int?
    public var progress: Int?
    public var isDeleted: Int?
    public var endDate: String?
    public var userImages: [Any]?
    public var isoLocation: String?
    public var isMatchNotification:Int?
    public var isEmailNotification:Int?
    public var isOtherNotifications: Int?
    public var isWaveNotifications: Int?
    public var countryPicUrl: String?
    public var createdByAdmin: Bool?
    public var isAppFeedBack: Bool?
    public var authToken: String?
    public var wavesRemaining: Int?
    public var wavesMessage: String?
    public var userPlanType : Int?
//    public var userPlanName : String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        if let items = json[SerializationKeys.countries].array { countries = items.map { CYLLocationObject(json: $0) } }
        city = json[SerializationKeys.city].string
        country = json[SerializationKeys.country].string
        cityv2 = json[SerializationKeys.cityv2].string
        if let items = json[SerializationKeys.cities].array { cities = items.map { CYLLocationObject(json: $0) } }
        startDate = json[SerializationKeys.startDate].string
        fbToken = json[SerializationKeys.fbToken].string
        creationTime = json[SerializationKeys.creationTime].int
        email = json[SerializationKeys.email].string
        aboutMe = json[SerializationKeys.aboutMe].string
        noOfTravellers = json[SerializationKeys.noOfTravellers].string
        gender = json[SerializationKeys.gender].int
        userName = json[SerializationKeys.userName].string
        if let items = json[SerializationKeys.firstName].string {firstName = items }
        dateOfBirth = json[SerializationKeys.dateOfBirth].int
        newMessageNotify = json[SerializationKeys.newMessageNotify].int
        id = json[SerializationKeys.id].int
        progress = json[SerializationKeys.progress].int
        isDeleted = json[SerializationKeys.isDeleted].int
        endDate = json[SerializationKeys.endDate].string
        if let items = json[SerializationKeys.userImages].array { userImages = items.map { $0.object} }
        isoLocation = json[SerializationKeys.isoLocation].string
        isMatchNotification = json[SerializationKeys.isMatchNotification].int
        isEmailNotification = json[SerializationKeys.isEmailNotification].int
        isOtherNotifications = json[SerializationKeys.isOtherNotifications].int
        isWaveNotifications = json[SerializationKeys.isWaveNotifications].int
        countryPicUrl = json[SerializationKeys.countryPicUrl].string
        createdByAdmin = json[SerializationKeys.createdByAdmin].boolValue
        isAppFeedBack = json[SerializationKeys.isAppFeedBack].boolValue
        authToken = json[SerializationKeys.authToken].string
        wavesRemaining = json[SerializationKeys.wavesRemaining].int
        wavesMessage = json[SerializationKeys.wavesMessage].string
        userPlanType = json[SerializationKeys.userPlanType].int
//        userPlanName = json[SerializationKeys.userPlanName].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = countries { dictionary[SerializationKeys.countries] = value.map { $0.dictionaryRepresentation() } }
        if let value = city { dictionary[SerializationKeys.city] = value }
        if let value = country { dictionary[SerializationKeys.country] = value}
        if let value = cityv2 { dictionary[SerializationKeys.cityv2] = value}
        if let value = cities { dictionary[SerializationKeys.cities] = value.map { $0.dictionaryRepresentation() } }
        if let value = startDate { dictionary[SerializationKeys.startDate] = value }
        if let value = creationTime { dictionary[SerializationKeys.creationTime] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = aboutMe { dictionary[SerializationKeys.aboutMe] = value }
        if let value = noOfTravellers { dictionary[SerializationKeys.noOfTravellers] = value }
        if let value = gender { dictionary[SerializationKeys.gender] = value }
        if let value = userName { dictionary[SerializationKeys.userName] = value }
        if let value = firstName { dictionary[SerializationKeys.firstName] = value }
        if let value = dateOfBirth { dictionary[SerializationKeys.dateOfBirth] = value }
        dictionary[SerializationKeys.newMessageNotify] = newMessageNotify
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = progress { dictionary[SerializationKeys.progress] = value }
        dictionary[SerializationKeys.isDeleted] = isDeleted
        if let value = endDate { dictionary[SerializationKeys.endDate] = value }
        if let value = userImages { dictionary[SerializationKeys.userImages] = value }
        if let value = isoLocation { dictionary[SerializationKeys.isoLocation] = value }
        dictionary[SerializationKeys.isMatchNotification] = isMatchNotification
        dictionary[SerializationKeys.isEmailNotification] = isEmailNotification
        dictionary[SerializationKeys.isOtherNotifications] = isOtherNotifications
        dictionary[SerializationKeys.isWaveNotifications] = isWaveNotifications
        if let value = countryPicUrl { dictionary[SerializationKeys.countryPicUrl] = value }
        if let value = createdByAdmin { dictionary[SerializationKeys.createdByAdmin] = value }
        if let value = isAppFeedBack { dictionary[SerializationKeys.isAppFeedBack] = value }
        if let value = authToken { dictionary[SerializationKeys.authToken] = value}
        if let value = wavesRemaining { dictionary[SerializationKeys.wavesRemaining] = value }
        if let value = wavesMessage { dictionary[SerializationKeys.wavesMessage] = value}
        if let value = userPlanType { dictionary[SerializationKeys.userPlanType] = value }
//        if let value = userPlanName { dictionary[SerializationKeys.userPlanName] = value}
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.countries = aDecoder.decodeObject(forKey: SerializationKeys.countries) as? [CYLLocationObject]
        self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
        self.country = aDecoder.decodeObject(forKey: SerializationKeys.country) as? String
        self.cityv2 = aDecoder.decodeObject(forKey: SerializationKeys.cityv2) as? String
        self.cities = aDecoder.decodeObject(forKey: SerializationKeys.cities) as? [CYLLocationObject]
        self.startDate = aDecoder.decodeObject(forKey: SerializationKeys.startDate) as? String
        self.fbToken = aDecoder.decodeObject(forKey: SerializationKeys.fbToken) as? String
        self.creationTime = aDecoder.decodeObject(forKey: SerializationKeys.creationTime) as? Int
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.aboutMe = aDecoder.decodeObject(forKey: SerializationKeys.aboutMe) as? String
        self.noOfTravellers = aDecoder.decodeObject(forKey: SerializationKeys.noOfTravellers) as? String
        self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? Int
        self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
        self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
        self.dateOfBirth = aDecoder.decodeObject(forKey: SerializationKeys.dateOfBirth) as? Int
        self.newMessageNotify = aDecoder.decodeObject(forKey: SerializationKeys.newMessageNotify) as? Int
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.progress = aDecoder.decodeObject(forKey: SerializationKeys.progress) as? Int
        self.isDeleted = aDecoder.decodeObject(forKey: SerializationKeys.isDeleted) as? Int
        self.endDate = aDecoder.decodeObject(forKey: SerializationKeys.endDate) as? String
        self.userImages = aDecoder.decodeObject(forKey: SerializationKeys.userImages) as? [Any]
        self.isoLocation = aDecoder.decodeObject(forKey: SerializationKeys.isoLocation) as? String
        self.isMatchNotification = aDecoder.decodeObject(forKey: SerializationKeys.isMatchNotification) as? Int
        self.isWaveNotifications = aDecoder.decodeObject(forKey: SerializationKeys.isWaveNotifications) as? Int
        self.isEmailNotification = aDecoder.decodeObject(forKey: SerializationKeys.isEmailNotification) as? Int
        self.isOtherNotifications = aDecoder.decodeObject(forKey: SerializationKeys.isOtherNotifications) as? Int
        self.countryPicUrl = aDecoder.decodeObject(forKey: SerializationKeys.countryPicUrl) as? String
        self.createdByAdmin = aDecoder.decodeObject(forKey: SerializationKeys.createdByAdmin) as? Bool
        self.isAppFeedBack = aDecoder.decodeObject(forKey: SerializationKeys.isAppFeedBack) as? Bool
        self.authToken = aDecoder.decodeObject(forKey: SerializationKeys.authToken) as? String
        self.wavesRemaining = aDecoder.decodeObject(forKey: SerializationKeys.wavesRemaining) as? Int
        self.wavesMessage = aDecoder.decodeObject(forKey: SerializationKeys.wavesMessage) as? String
        self.userPlanType = aDecoder.decodeObject(forKey: SerializationKeys.userPlanType) as? Int
//        self.userPlanName = aDecoder.decodeObject(forKey: SerializationKeys.userPlanName) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(countries, forKey: SerializationKeys.countries)
        aCoder.encode(city, forKey: SerializationKeys.city)
        aCoder.encode(country, forKey: SerializationKeys.country)
        aCoder.encode(cityv2, forKey: SerializationKeys.cityv2)
        aCoder.encode(cities, forKey: SerializationKeys.cities)
        aCoder.encode(startDate, forKey: SerializationKeys.startDate)
        aCoder.encode(fbToken, forKey: SerializationKeys.fbToken)
        aCoder.encode(creationTime, forKey: SerializationKeys.creationTime)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(aboutMe, forKey: SerializationKeys.aboutMe)
        aCoder.encode(noOfTravellers, forKey: SerializationKeys.noOfTravellers)
        aCoder.encode(gender, forKey: SerializationKeys.gender)
        aCoder.encode(userName, forKey: SerializationKeys.userName)
        aCoder.encode(firstName, forKey: SerializationKeys.firstName)
        aCoder.encode(dateOfBirth, forKey: SerializationKeys.dateOfBirth)
        aCoder.encode(newMessageNotify, forKey: SerializationKeys.newMessageNotify)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(progress, forKey: SerializationKeys.progress)
        aCoder.encode(isDeleted, forKey: SerializationKeys.isDeleted)
        aCoder.encode(endDate, forKey: SerializationKeys.endDate)
        aCoder.encode(userImages, forKey: SerializationKeys.userImages)
        aCoder.encode(isoLocation, forKey: SerializationKeys.isoLocation)
        aCoder.encode(isMatchNotification, forKey: SerializationKeys.isMatchNotification)
        aCoder.encode(isEmailNotification, forKey: SerializationKeys.isEmailNotification)
        aCoder.encode(isOtherNotifications, forKey: SerializationKeys.isOtherNotifications)
        aCoder.encode(isWaveNotifications, forKey: SerializationKeys.isWaveNotifications)
        aCoder.encode(countryPicUrl, forKey: SerializationKeys.countryPicUrl)
        aCoder.encode(createdByAdmin, forKey: SerializationKeys.createdByAdmin)
        aCoder.encode(isAppFeedBack, forKey: SerializationKeys.isAppFeedBack)
        aCoder.encode(authToken, forKey: SerializationKeys.authToken)
        aCoder.encode(wavesRemaining, forKey: SerializationKeys.wavesRemaining)
        aCoder.encode(wavesMessage, forKey: SerializationKeys.wavesMessage)
        aCoder.encode(userPlanType, forKey: SerializationKeys.userPlanType)
//        aCoder.encode(userPlanName, forKey: SerializationKeys.userPlanName)
    }
    
}

