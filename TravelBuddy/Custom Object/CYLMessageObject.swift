//
//  CYLMessageObject.swift
//
//  Created by Nikhil Sharma on 4/24/17
//  Copyright (c) . All rights reserved.
//

import Foundation

public final class CYLMessageObject: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let senderId = "sender_id"
    static let id = "msg_id"
    static let creationTime = "sent_time"
    static let messageText = "message"
    static let waveType = "wave_type"
    static let receiverId = "receiver_id"
  }

  // MARK: Properties
  public var senderId: Int?
  public var id: Int?
  public var creationTime: Int?
  public var messageText: String?
    public var waveType: String?
    public var receiverId: Int?
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    senderId = json[SerializationKeys.senderId].int
    id = json[SerializationKeys.id].int
    creationTime = json[SerializationKeys.creationTime].int
    messageText = json[SerializationKeys.messageText].string
    waveType = json[SerializationKeys.waveType].string
    receiverId = json[SerializationKeys.receiverId].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = senderId { dictionary[SerializationKeys.senderId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = creationTime { dictionary[SerializationKeys.creationTime] = value }
    if let value = messageText { dictionary[SerializationKeys.messageText] = value }
    if let value = waveType { dictionary[SerializationKeys.waveType] = value }
    if let value = receiverId { dictionary[SerializationKeys.receiverId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.senderId = aDecoder.decodeObject(forKey: SerializationKeys.senderId) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.creationTime = aDecoder.decodeObject(forKey: SerializationKeys.creationTime) as? Int
    self.messageText = aDecoder.decodeObject(forKey: SerializationKeys.messageText) as? String
    self.waveType = aDecoder.decodeObject(forKey: SerializationKeys.waveType) as? String
    self.receiverId = aDecoder.decodeObject(forKey: SerializationKeys.receiverId) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(senderId, forKey: SerializationKeys.senderId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(creationTime, forKey: SerializationKeys.creationTime)
    aCoder.encode(messageText, forKey: SerializationKeys.messageText)
    aCoder.encode(waveType, forKey: SerializationKeys.waveType)
    aCoder.encode(receiverId, forKey: SerializationKeys.receiverId)
  }

}
